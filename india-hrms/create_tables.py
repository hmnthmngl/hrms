
from run import db
from models import PrivilegeModel
from sqlalchemy import text


db.create_all()

sql1 = text("insert into users (username,password,reset_password) values('babu@prutech.com','$pbkdf2-sha256$29000$bS3FWMu5lxKCUMo5p9Q6xw$JtYC3xXuP.E7povefSh7Nu2ZNKBV0ZJw335HJZu3O0I','False');")
sql2 = text("""insert into employee (first_name,last_name,email_id,date_of_birth,gender,primary_phone_number,
marital_status,employment_start_date,employee_status,department_id,designation_id,emp_type_id,
emp_number,billable_resource
) values('Dilli','Babu','babu@prutech.com','MDktMDEtMTk1Mg==','M',
'7013447798','S','2019-03-09','A',1,1,1,
500000,'N');""")
sql4 = text("insert into user_roles (user_id,roles) values(1,'{3}');")
sql5 = text("""insert into invite_status (employee_number,employee_email,invitation_status,sent_time,designation_id,department_id,billable_resource,employee_role,joining_date) values
(500000,'babu@prutech.com','Inactive',CURRENT_DATE,1,1,'N','HR Manager',CURRENT_DATE);""")
db.engine.execute(sql5)
db.engine.execute(sql1)
db.engine.execute(sql4)
db.engine.execute(sql2)
sql0 = text("insert into consultant_id_gen (consultant_id) values (1)")
db.engine.execute(sql0)


