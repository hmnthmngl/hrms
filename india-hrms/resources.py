from flask_restful import Resource
from models import UserModel, RevokedTokenModel, ProjectTypeModel,VacationWeeks,SickHours, \
    Employee1Model, ProjectEmployeeModel, RoleModel, PrivilegeModel, RolePrivilegeModel, \
    DepartmentModel, DesignationModel, EmploymentTypeModel, ClientDetailsModel, \
    BussinessSectorModel, ClientContactModel, ProjectModel, \
    StaffingSupplierModel, SupplierContactModel, \
    ThirdPartyConsultantModel, UserRolesModel, \
    EmpInactivationReasonDataModel,EmergencyRelationModel, Address1Model, \
    CountryModel, StateModel, CitiesModel, \
    DashboardStats, \
     StaffingSupplierModel, SupplierAddressModel, SupplierContactModel,TerminationReasonDataModel,EmployeePrimaryProjectModel, \
    ThirdPartyConsultantModel, ConsultantContactModel, ConsultantIdModel, \
    SupplierContactTypeModel, InternalTimesheetModel,InternalTimesheetTypesModel, EmployeeInviteStatus,PasswordMailStatus
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,get_jwt_identity, get_raw_jwt)
from flask import request, jsonify  
import uuid
import run
from run import mail,db
from flask_mail import Message
from passlib.hash import pbkdf2_sha256 as sha256
import base64
import itertools
import logging
import os, csv, pandas as pd, json
from sqlalchemy.sql import func
from flask_expects_json import expects_json  
from datetime import datetime, timedelta ,date
from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
import marshmallow.validate
import re
from marshmallow import Schema, fields, post_load, ValidationError, validates, validate, EXCLUDE,INCLUDE
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import calendar  
import time
from sqlalchemy.sql import operators, extract
from sqlalchemy import and_, or_
from sqlalchemy import exc
from functools import wraps
from sqlalchemy.sql.expression import ColumnElement

#logging.basicConfig(filename='server.log',level=logging.INFO,format='%(asctime)s-%(name)s-%(message)s')
#logging.basicConfig(level=logging.INFO,format='%(asctime)s-%(name)s-%(message)s')
static_data_instance = run.before_first_request()

def prefix_decorator(class_name):
    def decorated_function(original_function):
        @wraps(original_function)
        def wrapper_function(*arsg,**kwargs):
            try:
                logging.info('Zebra {0}'.format(class_name))
                result = original_function(*arsg,**kwargs)
                return result
            except KeyError as e:
                logging.error("Error, Please Provide {0}".format(str(e).replace("'",'')))
                return {"errorMessage": "Error, Please Provide {0}".format(str(e).replace("'",''))},500
            except exc.ProgrammingError as e:
                logging.error("Invalid {0}".format(re.sub(r"[\d_]"," ",list(e.params.keys())[0]).strip()))
                db.session.rollback()
                return {"errorMessage": "Invalid {0}".format(re.sub(r"[\d_]"," ",list(e.params.keys())[0]).strip())},500
            except exc.IntegrityError as e:
                logging.error("Error, {0} Already Exists".format(e.params[list(e.params.keys())[0]]))
                db.session.rollback()
                print(e)
                return {"errorMessage": "Error, {0} Already Exists".format(e.params[list(e.params.keys())[0]])},500 
            except Exception as e:
                logging.exception('Exception Occured , {0}'.format(str(e)))
                db.session.rollback()
                return {"errorMessage":str(e)},500
            finally:
                db.session.close()
        return wrapper_function
    return decorated_function


class VacationWeeksDropDowns(Resource):
    def get(self):
        return {"vacation_weeks":[i.vacation_weeks for i in VacationWeeks.query.all()]}

class SickHoursDropDowns(Resource):
    def get(self):
        return {"sick_hours":[i.sick_hours for i in SickHours.query.all()]}


class InactiveReasonDD(Resource):
    def get(self):
        return static_data_instance['inactive_reason_map']

        
class GetCountries(Resource):
    def get(self):
        return static_data_instance['countries_map']
        

class EmergencyRelationType(Resource):
    def get(self):
        return {"emergency_relation_type": static_data_instance['emergency_relation_map']}
        
class InternalTimesheetTypeDropDown(Resource):
    def get(self):
        return {"its_types": static_data_instance['ts_types_map']}
        
class EdubranchDropDowns(Resource):
    def get(self):
        return static_data_instance['edu_branch_map']
        
class EmploymentType(Resource):
    def get(self):
        return {"employment_types": static_data_instance['employment_type_map']}
        
class DepartmentList(Resource):
    def get(self):
        return static_data_instance['department_map']
        
        
class Terminationdd(Resource):
    def get(self):
        return static_data_instance['termination_reason_map']
        


class DesignationType(Resource):
    def get(self):
        return {"designation_type": static_data_instance['designation_map']}


class CoverageTypesDropDowns(Resource):
    def get(self):
        return {"coverage_types": static_data_instance['coverage_type_map'] }


class BenefitsType(Resource):
    def get(self):
        return {"benefit_types": static_data_instance['benefit_type_map']}

class EadType(Resource):
    def get(self):
        return {"ead_type": static_data_instance['ead_type_map']}
        
        
class ProjectTypeList(Resource):
    def get(self):
        return static_data_instance['project_type_map']
        


class PrivilegesList(Resource):
    def get(self):   
        return static_data_instance['privileges_map']

class SupplierConType(Resource):
    def get(self):  
            return {"contact_type": static_data_instance['supplier_contact_type_map']}



class HRDashboardBNBR(Resource):
    @prefix_decorator('HRDashboardBNBR')
    def get(self):
        total_employees = Employee1Model.query.filter_by(employee_status='A').count()
        billable_emps = Employee1Model.query.filter(Employee1Model.billable_resource=='Y',Employee1Model.employee_status=='A').all()
        billable_emp_list = [i.employee_id for i in billable_emps]
        billable = len(billable_emp_list)
        non_billable = total_employees-billable
        on_billing = list(set(list(itertools.chain.from_iterable([i.employee_id for i in ProjectEmployeeModel.query.all()]))))
        print([i for i in on_billing if ('CON' not in str(i)) and (int(i) in billable_emp_list) ])
        on_billing = len([i for i in on_billing if ('CON' not in str(i)) and (int(i) in billable_emp_list) ])
        on_bench = billable-on_billing
        todays_date = date.today()
        tomorrow_date = todays_date+timedelta(1)
        all_records = DashboardStats.query.order_by(DashboardStats.date.asc()).all()
        print(billable,non_billable,on_billing,on_bench)
        
        if all_records:
            if [todays_date.month,todays_date.year] == [all_records[-1].date.month,all_records[-1].date.year]:
                record = DashboardStats.query.filter_by(stat_id=all_records[-1].stat_id).first()
                record.date = todays_date
                record.billable = billable
                record.on_billing = on_billing
                record.on_bench = on_bench
                record.total_employees = total_employees
                record.non_billing = non_billable
                record.save_to_db()
                data = DashboardStats.return_all()
                logging.info("Success")
                return {"resources_count": data[::-1][0:13]} 
            else:
                record = DashboardStats(date=todays_date,billable=billable,on_billing=on_billing,on_bench=on_bench,total_employees=total_employees,non_billing=non_billable)
                record.save_to_db()
                data = DashboardStats.return_all()
                logging.info("Success")
                return {"resources_count": data[::-1][0:13]} 
        else:
            record = DashboardStats(date=todays_date,billable=billable,on_billing=on_billing,on_bench=on_bench,total_employees=total_employees,non_billing=non_billable)
            record.save_to_db()
            data = DashboardStats.return_all()
            logging.info("Success")
            return {"resources_count": data[::-1][0:13]} 

class HRDashboardNJ(Resource):
    @prefix_decorator('HRDashboardNJ')
    def get(self): 
        present_date = datetime.now().strftime("%m-%d-%Y")
        date = present_date.split('-')[0] + '-' +'01'+ '-' + present_date.split('-')[-1]
        x = Employee1Model.return_all_for_new_hires(date)
        logging.info("Success")
        return Employee1Model.return_all_for_new_hires(date) 


class RepManOrNot(Resource):
    @jwt_required
    @prefix_decorator('RepManOrNot')
    def get(self):   
        username = get_jwt_identity() 
        emp_id  = Employee1Model.query.filter_by(email_id=username).first()  
        if not emp_id:
            logging.debug("Employee does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id ).first()
        repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).first()
        print(pro_manager,repo_manager)
        if pro_manager and repo_manager:
            projects = [i.project_id for i in ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id ).all()]
            a = []
            for i in projects:
                b = ProjectEmployeeModel.query.filter_by(project_id=i).first()
                if b:
                    a.append(b.employee_id)
            project_employees = list(set([int(j) for i in a for j in i]))
            reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all()]))
            z = project_employees + reporting_employees
            return InternalTimesheetModel.return_all_reporting_manager(z,emp_id.employee_id )
        elif pro_manager:
            pid = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id ).all()
            list_ts = []
            b = []
            for i in pid:
                list_ts.append(InternalTimesheetModel.return_all_by_pro(i.project_id,emp_id.employee_id ))
            for j in list_ts:
                b.append(j["timeSheets"])
            return {"timeSheets": list(filter((None).__ne__, list(itertools.chain.from_iterable(b)))) }
        elif repo_manager:
            employees = [i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all()]
            return InternalTimesheetModel.return_all_reporting_manager(employees,emp_id.employee_id )
        else:
            return {"timeSheets": []}

class ManItsProFilter(Resource):
    @jwt_required
    @prefix_decorator('ManItsProFilter')
    def get(self): 
        username = get_jwt_identity() 
        emp_id = Employee1Model.query.filter_by(email_id=username).first()  
        if emp_id: 
            pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id).all()
            repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id).first()
            if (pro_manager and repo_manager) or pro_manager:
                projects = [i.project_id for i in pro_manager]
                return ProjectModel.find_by_pro_list(projects)  
            else:
                return {"Projects":[]}
        else:
            logging.debug("Employee Does Not Exist")
            return {'errorMessage':'Employee Does Not Exists'}

class ProjectManagerDD(Resource):
    @jwt_required
    @prefix_decorator('ProjectManagerDD')
    def get(self):  
        username = get_jwt_identity()
        logging.info("Success")
        return Employee1Model.return_all_active_projectfor()

class ManItsEmpFilter(Resource):
    @jwt_required
    @prefix_decorator('ManItsEmpFilter')
    def get(self):
        username = get_jwt_identity() 
        emp_id  = Employee1Model.query.filter_by(email_id=username).first()
        if not emp_id:
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"} 
        pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id).all()
        repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).first()
        projects = [i.project_id for i in pro_manager]
        
        if (pro_manager and repo_manager):
            print('entered')
            a = []
            for i in projects:
                b = ProjectEmployeeModel.query.filter_by(project_id=i).first()
                if b:
                    a.extend(b.employee_id)
            print(a,'---------------------------------------')   
            project_employees = list(set([int(i) for i in a if str(i)[0] != 'C' ]))
            reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all()]))
            z = list(set(project_employees + reporting_employees))
            z = [str(i) for i in z if i != emp_id.employee_id]
            print(z,'--------------------------------------------------')
            return Employee1Model.return_all_emps(list(set(z)),emp_id.employee_id)
        elif pro_manager:
            print('entered pro_manager')
            a = []
            for i in projects:
                employees = ProjectEmployeeModel.query.filter_by(project_id=i).first()
                if employees:
                    a.extend(employees.employee_id)
            print(a)
            b = list(set([i for i in a if i != str(emp_id.employee_id) and str(i)[0]!='C']))
            print(b,emp_id.employee_id,'*****************')
            return Employee1Model.return_all_emps(b,emp_id.employee_id)
        elif repo_manager:
            employees = [str(i.employee_id) for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all() if i.employee_id != emp_id.employee_id]
            return Employee1Model.return_all_emps(list(set(employees)),emp_id.employee_id)
        else:
            return {"Employees":[]}

class HrItsEmployeeFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsEmployeeFilter')
    def get(self):
        username = get_jwt_identity() 
        return Employee1Model.return_all_active_projectfor_et1(username)

#to get all time sheets
class GetAllTimesheet(Resource):
    @prefix_decorator('GetAllTimesheet')
    def get(self):
        return InternalTimesheetModel.return_all()

# to get project timesheets
class InternalTeamTimeSheet(Resource):
    @jwt_required
    @prefix_decorator('InternalTeamTimeSheet')
    def get(self):  
        username = get_jwt_identity()
        user = UserModel.query.filter_by(username=username).first().userid
        pid = ProjectModel.query.filter_by(project_manager_id=user).all()
        list_ts = []
        b = []
        for i in pid:
            list_ts.append(InternalTimesheetModel.return_all_by_pro(i.project_id,user))
        for j in list_ts:
            b.append(j["timeSheets"])
        return {"timeSheets": list(filter((None).__ne__, list(itertools.chain.from_iterable(b)))) }

class ProjectListEmp(Resource):
    @jwt_required
    @prefix_decorator('ProjectListEmp')
    def get(self):   
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employeeid = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        managerprojects = ProjectModel.query.filter_by(project_manager_id=employeeid).all()
        if managerprojects:
            logging.info("Success")
            return {"projects_list": [i.project_name for i in managerprojects]}

class TotalConsultants(Resource):
    @prefix_decorator('TotalConsultants')
    def get(self):
        total_consultants = ThirdPartyConsultantModel.query.count()
        active_consultants = ThirdPartyConsultantModel.query.filter_by(consultant_status='A').count()
        inactive_consutants = ThirdPartyConsultantModel.query.filter(ThirdPartyConsultantModel.consultant_status.in_(['I','T'])).count()
        logging.info('Success')
        return {"total_consultants": total_consultants, "active_consultants": active_consultants, "inactive_consultants": inactive_consutants}

class TotalSuppliers(Resource):
    @prefix_decorator('TotalSuppliers')
    def get(self): 
        total_suppliers = StaffingSupplierModel.query.count()
        active_suppliers = StaffingSupplierModel.query.filter_by(contract_status='A').count()
        inactive_suppliers = StaffingSupplierModel.query.filter(StaffingSupplierModel.contract_status.in_(['I','T'])).count()
        logging.info("Success")
        return {"total_suppliers": total_suppliers, "active_suppliers": active_suppliers, "inactive_suppliers": inactive_suppliers}

class ConsultantList(Resource):
    @prefix_decorator('ConsultantList')
    def get(self):  
        return ThirdPartyConsultantModel.return_all_consultants()

class RecruiterList(Resource):
    @prefix_decorator('RecruiterList')
    def get(self):  
        return Employee1Model.return_all_recruiters(Employee1Model.query.filter(Employee1Model.department_id.in_([3,6]),Employee1Model.employee_status=='A').order_by(Employee1Model.first_name.asc()).all())
        #return Employee1Model.return_all_recruiters(Employee1Model.query.filter_by(department_id=5,employee_status='A').order_by(Employee1Model.first_name.asc()).all())  

class GetAllSuppliers(Resource):
    @jwt_required
    @prefix_decorator('GetAllSuppliers')
    def get(self):
        return StaffingSupplierModel.return_all_suppliers()  

class GetAllSuppliersForCons(Resource):
    @jwt_required
    @prefix_decorator('GetAllSuppliersForCons')
    def get(self):
        return StaffingSupplierModel.return_all_suppliers_cons()

class AllSuppliers(Resource):
    @jwt_required
    @prefix_decorator('AllSuppliers')
    def get(self): 
        return StaffingSupplierModel.return_all_suppliers_for_consultants()

class TotalEmployees(Resource):
    @prefix_decorator('TotalEmployees')
    def get(self): 
        total_employees = Employee1Model.query.count()
        active_employees = Employee1Model.query.filter_by(employee_status='A').count()   
        inactive_employees = Employee1Model.query.filter(Employee1Model.employee_status.in_(['I','T'])).count()
        logging.info("Success")
        return {"total_employees": total_employees, "active_employees": active_employees, "inactive_employees": inactive_employees}

class ProjectsList2(Resource):
    @jwt_required
    @prefix_decorator('ProjectsList2')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employee_id = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        manager_id = ProjectModel.query.filter_by(project_manager_id=employee_id).all()
        project_creator = ProjectModel.query.filter_by(created_by=employee_id).all()
        x = ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(employee_id)])).all()
        projects = []
        for i in manager_id:
            projects.append(i.project_id)
        for i in project_creator:
            projects.append(i.project_id)
        for i in x:
            projects.append(i.project_id)
        projects = list(set(projects))
        complete_data = ProjectModel.for_projects_list(projects)
        total_projects = len(projects)
        complete_data.sort(key=lambda x:x.get('modified_time'),reverse=True)
        complete_data.sort(key=lambda x:x.get('project_status'))
        count = 0
        for i in complete_data:
            if i['project_status'] == 'Active':
                count += 1
        logging.info("Success")
        return {"projects":complete_data,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}

class OwnClientsList(Resource):
    @jwt_required
    @prefix_decorator('OwnClientsList')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employee_id = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        logging.info("Success")
        return ProjectEmployeeModel.return_all_clients(employee_id)

class ManagersList(Resource):
    @prefix_decorator('ManagersList')
    def get(self):  
        manager_role_id = RoleModel.find_by_role_name('Manager')
        roles = UserRolesModel.query.all()
        managers_id_list = []
        for i in roles:
            if str(manager_role_id) in i.roles:
                managers_id_list.append(i.user_id)
        logging.info("Success")
        return Employee1Model.return_all_managers(managers_id_list)

class AllUsers(Resource):
    @jwt_required
    @prefix_decorator('AllUsers')
    def get(self): 
        username = get_jwt_identity()
        if not username:
            logging.debug('User {} Does Not Exist'.format(username))
            return {'message': 'User {} Does Not Exist'.format(username)}
        return UserModel.return_all()

class RolesList(Resource):
    @jwt_required
    @prefix_decorator('RolesList')
    def get(self): 
        return RoleModel.return_all()

class UserLogin(Resource):
    @jwt_required
    @prefix_decorator('UserLogin')
    def get(self):
        username = get_jwt_identity()
        userid = UserModel.query.filter_by(username=username).first()
        current_emp = Employee1Model.find_by_employee_id(userid.userid)
        emp_designation = DesignationModel.find_by_designation_id(current_emp.designation_id)
        emp_department = DepartmentModel.find_by_department_id(current_emp.department_id)
        empnumber = current_emp.employee_id
        # proemp = ProjectEmployeeModel.return_all_empprojects(current_emp.employee_id)
        reporting_manager_name = Employee1Model.query.filter_by(employee_id=current_emp.reporting_manager_id).first()
        recruiter = current_emp.recruiter
        reporting_manager = reporting_manager_name.first_name + ' ' +  reporting_manager_name.last_name if reporting_manager_name else ''
        termination = TerminationReasonDataModel.query.filter_by(termination_reason_id=current_emp.termination_reason_id).first()
        termination_reason = termination.termination_reason if termination else ''
        termination_intent = termination.termination_intent if termination else ''
        termination_id = current_emp.termination_reason_id
        inactive_reason = EmpInactivationReasonDataModel.query.filter_by(emp_inactive_reason_id = current_emp.inactive_reason_id).first()

        if current_emp.employment_end_date:
            employment_end_date = str(current_emp.employment_end_date)
        else:
            employment_end_date = ''

        if emp_designation:
            emp_designation = emp_designation.designation_name
        else:
            emp_designation = None

        if emp_department:
            emp_department = emp_department.department_name
        else:
            emp_department = None

        return { 
            "inactive_reason_id" : inactive_reason.emp_inactive_reason if inactive_reason else '',
            "resource_billable":"Yes" if current_emp.billable_resource == 'Y' else "No", 
            "termination_date":str(current_emp.termination_date),
            "comments" : current_emp.comments,
            "employee_status_last_updated_date":str(current_emp.employee_status_last_updtd_date).split(' ')[0] if current_emp.employee_status_last_updtd_date else '',
            'employee_id': current_emp.employee_id,
            'employee_number': current_emp.emp_number,
            'first_name': current_emp.first_name,
            'middle_name': current_emp.middle_name,
            'last_name': current_emp.last_name,
            'reporting_manager_name': reporting_manager,
            'recruiter_name': recruiter,
            'reporting_manager_id': current_emp.reporting_manager_id,
            'email': current_emp.email_id.lower(),
            'gender': 'Male' if current_emp.gender=='M' else 'Female' if current_emp.gender=='F' else 'Other' if current_emp.gender=='O' else '',
            'primary_phone_number': current_emp.primary_phone_number,
            'alternate_phone_number': current_emp.alternate_phone_number,
            'marital_status': 'Single' if current_emp.marital_status=='S' else 'Married' if current_emp.marital_status=='M' else 'Divorced' if current_emp.marital_status=='D' else 'Widowed' if current_emp.marital_status=='W' else '',
            'employment_start_date': str(current_emp.employment_start_date),
            'employment_end_date': str(employment_end_date),
            'recruitment method': current_emp.recruitment_method,  
            'sick_days': current_emp.eligible_sick_hours if current_emp.eligible_sick_hours  else '',
            'eligible_vacation_days':  current_emp.eligible_vacation_weeks if current_emp.eligible_vacation_weeks else '',
            'paid_holidays':'Yes' if current_emp.paid_holidays is True else 'No' if  current_emp.paid_holidays is False else '', 
            'employee_status': 'Active' if current_emp.employee_status=='A' else 'Inactive' if current_emp.employee_status=='I' else 'Terminated' if current_emp.employee_status=='T' else '',
            'termination_reason': termination_reason,
            'termination_intent': termination_intent,
            'termination_id': termination_id, 
            'rehire_eligibility': current_emp.rehire_eligibility,
            'employee_designation': emp_designation,
            'employee_department': emp_department,
            'account_manager':current_emp.account_manager,

        }

class UserLogoutAccess(Resource):
    @jwt_required
    @prefix_decorator('UserLogoutAccess')
    def get(self):
        logging.info('Entered into {0}'.format(self.__class__.__name__))
        jti = get_raw_jwt()['jti']
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        return {'message': 'Logged Out Successfully'}

class GetOwnRole(Resource):
    @jwt_required
    @prefix_decorator('GetOwnRole')
    def get(self): 
        username = get_jwt_identity()
        requested_emp_id = UserModel.find_by_username(username)
        if requested_emp_id:
            requested_emp_role_id = UserRolesModel.find_to_get_user_id(requested_emp_id.userid)
            if requested_emp_role_id:
                user_role_obj = UserRolesModel.query.filter_by(user_id=requested_emp_role_id).first()
                return {'roles': [RoleModel.find_by_role_id(x) for x in user_role_obj.roles]}
            else:
                return {'roles':[]}
        else:
            return{"errorMessage":"Requested Employee Does Not Exist"}      

class TotalClients(Resource):
    @prefix_decorator('TotalClients')
    def get(self):  
        total_clients = ClientDetailsModel.query.count()
        active_clients = ClientDetailsModel.query.filter_by(client_status='A').count()
        inactive_clients = ClientDetailsModel.query.filter(ClientDetailsModel.client_status.in_(['P','I','T'])).count()
        logging.info('success')
        return {"total_clients": total_clients, "active_clients": active_clients, "inactive_clients": inactive_clients}

class ClientListForProject(Resource):
    @jwt_required
    @prefix_decorator('ClientListForProject')
    def get(self):
        return ClientDetailsModel.return_all_clientids()

class ContractList(Resource):
    @jwt_required
    @prefix_decorator('ContractList')
    def get(self): 
        return ClientVendorshipModel.return_all()

class GetStateByName(Resource):
    @prefix_decorator('GetStateByName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['country_name'] and type(data['country_name']) == str:
            country_obj = CountryModel.query.filter_by(country_name=data['country_name']).first()
            if country_obj:
                logging.info('states returned succesfully')
                return StateModel.return_all(country_obj.country_id)
            else:
                logging.debug('Could Not Find States for {}'.format(data['country_name']))
                return {'errorMessage':'Could Not Find States for {}'.format(data['country_name'])}
        else:
            logging.debug("Invalid Country Name")
            return {'errorMessage': "Invalid Country Name"}       

class GetCitiesByName(Resource):
    @prefix_decorator('GetCitiesByName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['state_name'] and type(data['state_name']) == str:
            state_obj = StateModel.query.filter_by(state_name=data['state_name']).first()
            if state_obj:
                logging.info('cities returned succesfully')
                return CitiesModel.return_all(state_obj.state_id)
            else:
                logging.debug('Could Not Find Cities for {}'.format(data['state_name']))
                return {'errorMessage':'Could Not Find Cities for {}'.format(data['state_name'])}
        else:
            logging.debug("Invalid State Name")
            return {'errorMessage': "Invalid State Name"}

class TeamTsDropDown(Resource):
    @jwt_required
    @prefix_decorator('TeamTsDropDown')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        manager_pro = ProjectModel.query.filter_by(project_manager_id=user_id).all()
        return ProjectModel.find_by_pro_list1(manager_pro)

class EmployeeList(Resource):
    @jwt_required
    @prefix_decorator('EmployeeList')
    def get(self): 
        return Employee1Model.return_all()

class NewClientList(Resource):
    @jwt_required
    @prefix_decorator('NewClientList')
    def get(self): 
        return ClientDetailsModel.return_all()

class ProjectsList(Resource):
    @jwt_required
    @prefix_decorator('ProjectsList')
    def get(self):
        return ProjectModel.return_all()

class AllEmpList(Resource):
    @jwt_required
    @prefix_decorator('AllEmpList')
    def get(self):
        employee_obj = Employee1Model.return_all_active_forproject()
        consultant_obj = ThirdPartyConsultantModel.return_all_active_forproject()
        all_emp = {}
        all_emp['Employees'] = employee_obj['Employees'] + consutant_obj['consultants']  
        return all_emp

class BillableActiveEmpList(Resource):
    @jwt_required
    @prefix_decorator('BillableActiveEmpList')
    def get(self):   
        employee_obj = Employee1Model.return_all_active_forproject()
        consultant_obj = ThirdPartyConsultantModel.return_all_active_forproject()
        all_emp = {}
        all_emp['Employees'] = employee_obj['Employees'] + consultant_obj['consultants']
        return all_emp

class ManagerEmp(Resource):
    @jwt_required
    @prefix_decorator('ManagerEmp')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employee_id = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        pros = ProjectModel.query.filter_by(project_manager_id=employee_id).all()
        res = []
        ct_res = []
        for i in pros:
            res.append(ProjectEmployeeModel.find_by_project_id(i.project_id))
        for j in res:
            ct_res.append(j['projects_resources'])
        resources = [dict(y) for y in set(tuple(x.items()) for x in list(itertools.chain.from_iterable(ct_res)))]
        logging.info("Success")
        return {"resources": resources}



class EmployeeInactive(Resource):
    @jwt_required
    @prefix_decorator('EmployeeInactive')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date = datetime.now()
        record = Employee1Model.query.filter_by(employee_id=data['employeeId']).first()
        if not record:
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(data['employeeId'])])).all():
            logging.debug("Failed,Employee Assigned To Active Project")
            return {"errorMessage":"Failed,Employee Assigned To Active Project"}
        record.employee_status = 'I'
        record.inactive_reason_id = data['inactive_reason_id']
        record.comments = data['comments']
        record.modified_time = date
        record.employee_status_last_updtd_date = date
        record.save_to_db()  
        logging.info("Employee Inactivated successfully")      
        return {"Message": "Employee Inactivated successfully"}

class ConsultantInactive(Resource):
    @jwt_required
    @prefix_decorator('ConsultantInactive')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        record = ThirdPartyConsultantModel.query.filter_by(consultant_id=data['consultantId']).first()
        if record:
            record.consultant_status = 'I'
            record.comments = data['comments']
            record.modified_time = date_obj
            record.consultant_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info("Consultant Inactivated successfully")
            return {"Message": "Consultant Inactivated successfully"}
        else:
            logging.debug('Consultant Does not Exists')
            return {'errorMessage':'Consultant Does not Exists'}   


class EmployeeSignUp(Resource):
    @prefix_decorator('EmployeeSignUp')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['emailId'].lower()
        invite_record_obj = EmployeeInviteStatus.query.filter_by(employee_number=data['employeeNumber'], employee_email=email).first()
        print(invite_record_obj)
        if not invite_record_obj:
            logging.info('Data Mismatch, Cannot Add Employee')
            return {"errorMessage":"Data Mismatch, Cannot Add Employee"}
        if Employee1Model.query.filter_by(email_id=email).first():
            logging.debug("Employee With {0} Already Exists".format(email))
            return {"errorMessage":"Employee With {0} Already Exists".format(email)}
        try:
            role = RoleModel.query.filter_by(role_name=data['role']).first()
            dept = DepartmentModel.query.filter_by(department_name=data['employee_department']).first()
            desg = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
            if dept:
                dept = dept.department_id
            else:
                logging.debug('Department Does Not Exists')
                return {'errorMessage':'Department Does Not Exists'}
            if desg:
                desg = desg.designation_id
            else:
                logging.debug('Designation Does Not Exists')
                return {'errorMessage':'Designation Does Not Exists'}
            if role:
                role_id = role.id
            else:
                logging.debug("Role Does Not Exists")
                return {"errorMessage":"Role Does Not Exists"}
            schema2 = EmployeeSignupSchema(unknown=EXCLUDE)
            empdet = schema2.load(data)
            if not UserModel.query.filter_by(username=email).first():
                user_obj = UserModel(username=email,password=UserModel.generate_hash(data['password']),reset_password=data['reset_password'])
                user_obj.save_to_db()
                user_id = UserModel.query.filter_by(username=email).first()
                if user_id:
                    user_role_obj = UserRolesModel(user_id=user_id.userid, roles=[str(role_id)])
                    user_role_obj.save_to_db()
                else:
                    logging.debug('Unable to save User')
                    return {'errorMessage':'Unable to save User'}
            user_id = UserModel.query.filter_by(username=email).first()

            date_obj = datetime.now()
            employee_details_obj = Employee1Model(
                 employee_id=user_id.userid,
                 first_name=data['employeeFirstName'],
                 middle_name=data['employeeMiddleName'],
                 last_name=data['employeeLastName'],
                 email_id=email,
                 gender='M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O' if data['gender']=='Other' else '',
                 primary_phone_number=data['primaryPhoneNumber'],
                 alternate_phone_number=data['alternatePhoneNumber'],
                 marital_status='S' if data['maritalStatus']=='Single' else 'M' if data['maritalStatus']=='Married' else 'D' if data['maritalStatus']=='Divorced' else 'W' if data['maritalStatus']=='widowed' else '',
                 employment_start_date=data['joiningDate'],
                 billable_resource=data['billableResource'][0],
                 employee_status='A',
                 emp_number=data['employeeNumber'],
                 department_id=dept,
                 designation_id=desg,
                 created_time = date_obj,
                 modified_time = date_obj,
                 employee_status_last_updtd_date = date_obj
             )
            employee_details_obj.save_to_db()
            invite_record_obj.invitation_status = "Inactive"
            invite_record_obj.save_to_db()
            logging.debug("Basic Details Added Succesfully")
            return {"message": "Basic Details Added Succesfully", "employeeId": user_id.userid}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class AddressSchema(Schema):
    address_line1 = fields.String(validate=validate.Length(max=255),data_key="addressLine1",required=False)
    address_line2 = fields.String(validate=validate.Length(max=255),data_key="addressLine2",required=False)
    city = fields.String(validate=validate.Length(max=100),data_key="city",required=False)
    state_name = fields.String(validate=validate.Length(max=100),data_key="state",required=False)
    zip_code = fields.String(data_key="zip",required=False)
    country = fields.String(validate=validate.Length(max=100),data_key="country",required=False)

    @validates('zip_code')
    def validate_age(self, zip_code):
        if not (len(zip_code) == 5 or len(zip_code) == 6):
            raise ValidationError('The zip code provided is invalid')

    @post_load
    def save_address(self, data, **kwargs):
        return data


class EmployeeSignupSchema(Schema):
    first_name = fields.String(validate=validate.Length(max=120),data_key="employeeFirstName",required=True)
    middle_name = fields.String(validate=validate.Length(max=120),data_key="employeeMiddleName",required=False)
    last_name = fields.String(validate=validate.Length(max=120),data_key="employeeLastName",required=True)
    email_id = fields.String(validate=validate.Length(max=120),data_key="emailId",required=True)
    gender = fields.String(validate=validate.Length(max=120),data_key="gender",required=True)
    primary_phone_number = fields.String(validate=validate.Length(max=120),data_key="primaryPhoneNumber",required=True)
    alternate_phone_number = fields.String(validate=validate.Length(max=120),data_key="alternatePhoneNumber",required=False)
    marital_status = fields.String(validate=validate.Length(max=120),data_key="maritalStatus",required=False)
    emp_number = fields.String(data_key="employeeNumber",required=True)

    @validates('email_id')
    def validate_age(self, email_id):
        if (email_id == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',email_id)) and len(email_id)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

    @post_load
    def save_employee(self, data, **kwargs):
        return data


class UsersSchema(Schema):
    username = fields.Email(data_key="emailId",required=True)
    password = fields.String(validate=validate.Length(max=120),data_key="password",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data


class EmergencySchema(Schema):
    employee_id = fields.Integer(data_key="employeeId",required=True)
    emergency_relation_type = fields.String(validate=validate.Length(max=50),data_key="emergencyRelationType",required=True)
    contact_first_name = fields.String(validate=validate.Length(max=30),data_key="emergencyFirstName",required=True)
    contact_middle_name = fields.String(validate=validate.Length(max=30),data_key="emergencyMiddleName",required=False)
    contact_last_name = fields.String(validate=validate.Length(max=30),data_key="emergencyLastName",required=True)
    mobile = fields.String(validate=validate.Length(max=30),data_key="mobile",required=True)
    alt_mobile = fields.String(validate=validate.Length(max=30),data_key="altMobile",required=False)
    contact_email = fields.Email(allow_none=True,data_key="emergencyEmail",required=False)

    @validates('contact_email')
    def validate_age(self, contact_email):
        if (contact_email == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',contact_email)) and len(contact_email)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

    @validates('mobile')
    def validate_age(self, mobile):
        if len(mobile) != 10:
            raise ValidationError('The Mobile Number is invalid')

    @validates('alt_mobile')
    def validate_age(self, alt_mobile):
        if len(alt_mobile) == 0 or len(alt_mobile) == 10:
            pass
        else:
            raise ValidationError('The Alternate Mobile Number is invalid')


    @post_load
    def save_emergency_details(self, data, **kwargs):
        return data


               
class InvitationValidation(Resource):
    @prefix_decorator('InvitationValidation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        desg = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
        dept = DepartmentModel.query.filter_by(department_name=data['employee_department']).first()
        invitation_obj = EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=data['employeeNumber'],employee_email=data['email'].lower(),employee_role=data['role'],designation_id=desg.designation_id,department_id=dept.department_id,joining_date=data['joining_date'],billable_resource=data['billable_resource'][0]).first()
        if invitation_obj:
            logging.info("Success")
            return {"message":"Success"}
        else:
            logging.debug("Employee Already Registered")
            return {"errorMessage":"Employee Already Registered"}

class AllEmployees(Resource):
    @prefix_decorator('AllEmployees')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        current_user = UserModel.find_by_username(email)
        emp_name = Employee1Model.query.filter_by(email_id=email).first()
        if not current_user:
            logging.debug('User {} Doesn\'t Exist'.format(email))
            return {'message': 'User {} Doesn\'t Exist'.format(email)} 
        if emp_name:  
            if UserModel.verify_hash(data['password'], current_user.password):
                employee_obj = Employee1Model.query.filter_by(email_id=email).first()
                if employee_obj.employee_status == 'T':
                    if datetime.today() > datetime.strptime(str(employee_obj.termination_date), "%Y-%m-%d"):
                        return {"message":"Login Not Allowed"}
                expires = timedelta(hours = 12)
                access_token = create_access_token(identity=email, expires_delta=expires)
                refresh_token = create_refresh_token(identity=email, expires_delta=expires)
                logging.info('Success')
                return {
                    'employee_name': emp_name.first_name+' '+emp_name.last_name,
                    'user_name': data['username'],
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'message':"Logged In Successfully",
                    'reset_password':current_user.reset_password if current_user.reset_password else ''
                }
            else:
                logging.debug("Invalid Email/Password")
                return {"message": "Invalid Email/Password"}
        else:
            logging.info('User {} Doesn\'t Exist'.format(email))
            return {'message': 'User {} Doesn\'t Exist'.format(email)}


class EmpListPro(Resource):
    @jwt_required
    @prefix_decorator('EmpListPro')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        employees = ProjectEmployeeModel.query.filter_by(project_id=data["project_id"]).first()
        if not employees:
            logging.debug('Employees Does Not Exist')
            return {"errorMessages":"Employees Does Not Exist"}
        return Employee1Model.return_all_emps(employees.employee_id)

class ClientProjects(Resource):
    @jwt_required
    @prefix_decorator('ClientProjects')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ClientDetailsModel.query.filter_by(client_id=data['client_id']).first():
            logging.debug("Client Does Not Exist")
            return {"errorMessage":"Client Does Not Exist"}
        return ProjectModel.return_all_client_projects(data['client_id'])


class ClientResource(Resource):
    @jwt_required
    @prefix_decorator('ClientResource')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ClientDetailsModel.query.filter_by(client_id=data['client_id']).first():
            return {"errorMessage":"Client Does Not Exist"}
        res = [ProjectEmployeeModel.find_by_project_id(i.project_id) for i in ProjectModel.query.filter_by(client_id=data['client_id']).all()]
        return {"resources": [dict(y) for y in set(tuple(x.items()) for x in list(itertools.chain.from_iterable([j['projects_resources'] for j in res])))]}
       

class ChangePassword(Resource):
    @jwt_required
    @prefix_decorator('ChangePassword')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        record = UserModel.query.filter_by(username=username).first()
        if record:
            if UserModel.verify_hash(data['current_password'], record.password):
                print('no')
                if UserModel.verify_hash(data['new_password'], record.password):
                    print('no1')
                    logging.debug("New Password And Current Password Cannot Be Same")
                    return {"errorMessage": "New Password And Current Password Cannot Be Same"}
                record.password = UserModel.generate_hash(data['new_password'])
                record.reset_password = False
                record.save_to_db()
                logging.info("Password Reset is Successful")
                return {"message": "Password Reset is Successful"}
            else:
                logging.debug("Incorrect Current Password")
                return {"errorMessage": "Incorrect Current Password"}
        else:
            print('yes')
            logging.debug("User {} Does Not Exist".format(username))
            return {"errorMessage":"User {} Does Not Exist".format(username)}

class PasswordMailValidation(Resource):
    @prefix_decorator('PasswordMailValidation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if PasswordMailStatus.query.filter_by(token=data['token'],token_status="Active").first():
            logging.info('success')
            return {"message":"success"}
        else:
            login.debug("Your Link Was Invalid")
            return {"errorMessage":"Your Link Was Invalid"}

class SendMailToken(Resource):
    @prefix_decorator('SendMailToken')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        record = UserModel.query.filter_by(username=email).first()
        if record :
            names = email.split('@')[0]
            token = record.get_token()
            reset_url = data['base_url'] + '?token=' + token
            msg = Message('Password reset link', sender='support@prutech.com', recipients=[email])
            msg.html = """<b style = "font-family:calibri,garamond,serif;font-size:16px;"> Hi {0},<br>
            <br> You received Password reset link from PRUTECH to HRMS Portal.
            <br>
            <br> To reset <a href={1}>click here</a></br>
            <br> Thanks,
            <br> PRUTECH Team.
            </b>""".format(names, reset_url)
            mail.send(msg)
            mail_validation_obj = PasswordMailStatus(token=token,token_status="Active")
            mail_validation_obj.save_to_db()
            logging.info("Maill Sent")
            return {"message": "Mail Sent"}
        else:
            logging.debug("{0} Is Not A Valid User".format(email))
            return {"errorMessage":"{0} Is Not A Valid User".format(email)}

class ResetPassword(Resource):
    @prefix_decorator('ResetPassword')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        user_id = UserModel.verify_token(data['token'])
        if user_id:
            record = UserModel.query.filter_by(userid=user_id).first()
            if UserModel.verify_hash(data['new_password'], record.password):
                logging.debug("Choose a password that you haven't already used with this account.")
                return {'errorMessage':"Choose a password that you haven't already used with this account."}
            record.password = UserModel.generate_hash(data['new_password'])
            record.save_to_db()
            mail_validation_obj = PasswordMailStatus.query.filter_by(token=data['token']).first()
            mail_validation_obj.token_status= "Inactive"
            mail_validation_obj.save_to_db()
            logging.info("Password Has Been Reset Successfully")
            return {"message": "Password Has Been Reset Successfully"}
        else:
            logging.debug("Token Expired or Invalid")
            return {"message": "Token Expired or Invalid"}  

class Hemanth(Resource):
    def get(self):
        a = UserModel.query.filter_by(username='hemanth.munagala@prutech.com').first().password
        b = UserModel.generate_hash('Hemanth@7')
        c = UserModel.generate_hash('Hemanth@7')
        return {'old':a,'new':b,'latest':c}

class GetRp(Resource):
    @jwt_required
    @prefix_decorator('GetRp')
    def post(self):
        username = get_jwt_identity()
        data = request.get_json(force=True)
        print(data)
        userid = UserModel.query.filter_by(username=data['username'].lower()).first()
        if userid:
            user_role_obj = UserRolesModel.query.filter_by(user_id=userid.userid).first()
            if user_role_obj:
                list_of_role_privileges = []
                for x in user_role_obj.roles:
                    role_privilege_obj = RolePrivilegeModel.query.filter_by(role_id=int(x)).first().role_privileges
                    if role_privilege_obj:
                        for i in role_privilege_obj:
                            list_of_role_privileges.append(PrivilegeModel.query.filter_by(id=int(i)).first().privilege_name)
                    else:
                        logging.debug('Role {0} Does Not Have Privileges'.format(RoleModel.query.filter_by(id=int(i)).first().role_name))
                        return {'errorMessage':'Role {0} Does Not Have Privileges'.format(RoleModel.query.filter_by(id=int(i)).first().role_name)}
                logging.info("Success")
                return {"user_role_privileges": list_of_role_privileges}
            else:
                logging.debug('No Role(s) Assigned To The User')
                return {'errorMessage':'No Role(s) Assigned To The User'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage':'User Does Not Exist'}


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    @prefix_decorator('UserLogoutRefresh')
    def post(self):
        jti = get_raw_jwt()['jti']
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        logging.info('Refresh Token Has Been Revoked')
        return {'message': 'Refresh Token Has Been Revoked'}


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    @prefix_decorator('TokenRefresh')
    def post(self):
        current_user = get_jwt_identity()
        expires = timedelta(hours = 12)
        access_token = create_access_token(identity=current_user, expires_delta=expires)
        logging.info('Success')
        return {'access_token': access_token}

# this endpoint we are using for project resource assignment. Used this in timesheet also change this in timesheet as it is dedicated for project module

class EmployeeDetailsById(Resource):
    @jwt_required
    @prefix_decorator('EmployeeDetailsById')
    def post(self): 
        username = get_jwt_identity()
        data = request.get_json(force=True)
        if not Employee1Model.query.filter_by(employee_id=data['employee_id']).first():
            return {"errorMessage":"Employee With {0} Id Does Not Exist".format(data['employee_id'])}
        print(data)
        current_emp = Employee1Model.find_by_employee_id(data['employee_id'])
        emp_designation = DesignationModel.find_by_designation_id(current_emp.designation_id)
        emp_department = DepartmentModel.find_by_department_id(current_emp.department_id)
        empnumber = current_emp.employee_id
        # proemp = ProjectEmployeeModel.return_all_empprojects(data['employee_id'])
        roles = [RoleModel.query.filter_by(id=x).first().role_name for x in UserRolesModel.find_by_user_id2(empnumber)]
        reporting_manager_name = Employee1Model.query.filter_by(employee_id=current_emp.reporting_manager_id).first()
        recruiter = current_emp.recruiter
        reporting_manager = reporting_manager_name.first_name + ' ' +  reporting_manager_name.last_name if reporting_manager_name else ''
        termination = TerminationReasonDataModel.query.filter_by(termination_reason_id=current_emp.termination_reason_id).first()
        termination_reason = termination.termination_reason if termination else ''
        termination_intent = termination.termination_intent if termination else ''
        termination_id = current_emp.termination_reason_id
        inactive_reason = EmpInactivationReasonDataModel.query.filter_by(emp_inactive_reason_id = current_emp.inactive_reason_id).first()

        if current_emp.employment_end_date:
            employment_end_date = str(current_emp.employment_end_date)
        else:
            employment_end_date = ''

        if emp_designation:
            emp_designation = emp_designation.designation_name
        else:
            emp_designation = None

        if emp_department:
            emp_department = emp_department.department_name
        else:
            emp_department = None


        return {
            "inactive_reason_id" : inactive_reason.emp_inactive_reason if inactive_reason else '',
            "resource_billable":"Yes" if current_emp.billable_resource == 'Y' else "No", 
            "termination_date":str(current_emp.termination_date),
            "comments" : current_emp.comments,
            "employee_Status_last_updated_date":str(current_emp.employee_status_last_updtd_date).split(' ')[0] if current_emp.employee_status_last_updtd_date else '',
            'role': roles,
            'employee_id': current_emp.employee_id,
            'employee_number': current_emp.emp_number,
            'first_name': current_emp.first_name,
            'middle_name': current_emp.middle_name,
            'reporting_manager_id': current_emp.reporting_manager_id,
            'last_name': current_emp.last_name,
            'reporting_manager_name': reporting_manager,
            'recruiter_name': recruiter,
            'email': current_emp.email_id.lower(),
            'gender': 'Male' if current_emp.gender=='M' else 'Female' if current_emp.gender=='F' else 'Other' if current_emp.gender=='O' else '',
            'primary_phone_number': current_emp.primary_phone_number,
            'alternate_phone_number': current_emp.alternate_phone_number,
            'marital_status': 'Single' if current_emp.marital_status=='S' else 'Married' if current_emp.marital_status=='M' else 'Divorced' if current_emp.marital_status=='D' else 'Widowed' if current_emp.marital_status=='W' else '',
            'employment_start_date': str(current_emp.employment_start_date),
            'employment_end_date': str(employment_end_date),
            'recruitment_method': current_emp.recruitment_method,
            'sick_days': current_emp.eligible_sick_hours if current_emp.eligible_sick_hours  else '',
            'eligible_vacation_days':  current_emp.eligible_vacation_weeks if current_emp.eligible_vacation_weeks else '',
            'paid_holidays':'Yes' if current_emp.paid_holidays is True else 'No' if  current_emp.paid_holidays is False else '', 
            'employee_status': 'Active' if current_emp.employee_status=='A' else 'Inactive' if current_emp.employee_status=='I' else 'Terminated' if current_emp.employee_status=='T' else '',
            'termination_reason': termination_reason,
            'termination_intent': termination_intent,
            'termination_id': termination_id,
            'rehire_eligibility': current_emp.rehire_eligibility,
            'employee_designation': emp_designation,
            'employee_department': emp_department,
            'account_manager':current_emp.account_manager,
        }

class EmployeeDetails(Resource):
    @jwt_required
    @prefix_decorator('EmployeeDetails')
    def post(self): 
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        if not Employee1Model.query.filter_by(emp_number=data['employee_number']).first():
            return {"errorMessage":"Employee With {0} EMployee Number Does Not Exist".format(data['employee_number'])}

        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        emp_designation = DesignationModel.find_by_designation_id(current_emp.designation_id)
        emp_department = DepartmentModel.find_by_department_id(current_emp.department_id)
        empnumber = current_emp.employee_id
        roles = [RoleModel.query.filter_by(id=x).first().role_name for x in UserRolesModel.find_by_user_id2(empnumber)]
        # proemp = ProjectEmployeeModel.return_all_empprojects(empnumber)
        reporting_manager_name = Employee1Model.query.filter_by(employee_id=current_emp.reporting_manager_id).first()
        recruiter = current_emp.recruiter
        reporting_manager = reporting_manager_name.first_name + ' ' +  reporting_manager_name.last_name if reporting_manager_name else ''
        termination = TerminationReasonDataModel.query.filter_by(termination_reason_id=current_emp.termination_reason_id).first()
        termination_reason = termination.termination_reason if termination else ''
        termination_intent = termination.termination_intent if termination else ''
        termination_id = current_emp.termination_reason_id
        inactive_reason = EmpInactivationReasonDataModel.query.filter_by(emp_inactive_reason_id = current_emp.inactive_reason_id).first()

        if current_emp.employment_end_date:
            employment_end_date = str(current_emp.employment_end_date)
        else:
            employment_end_date = ''

        if emp_designation:
            emp_designation = emp_designation.designation_name
        else:
            emp_designation = None

        if emp_department:
            emp_department = emp_department.department_name
        else:
            emp_department = None

        return {
            "inactive_reason_id" : str(inactive_reason.emp_inactive_reason) if inactive_reason else '',
            "resource_billable": "Yes" if current_emp.billable_resource == 'Y' else "No", 
            "termination_date":str(current_emp.termination_date),
            "comments" : str(current_emp.comments),
            "employee_Status_last_updated_date": str(current_emp.employee_status_last_updtd_date).split(' ')[0] if current_emp.employee_status_last_updtd_date else '',
            'employee_id': current_emp.employee_id,
            'employee_number': current_emp.emp_number,
            'first_name': current_emp.first_name,
            'middle_name': current_emp.middle_name,
            'last_name': current_emp.last_name,
            'reporting_manager_name': reporting_manager,
            'recruiter_name': recruiter,
            'email': current_emp.email_id.lower() if current_emp.email_id else '',
            'reporting_manager_id': current_emp.reporting_manager_id,
            'gender': 'Male' if current_emp.gender=='M' else 'Female' if current_emp.gender=='F' else 'Other' if current_emp.gender=='O' else '',
            'primary_phone_number': current_emp.primary_phone_number,
            'alternate_phone_number': current_emp.alternate_phone_number,
            'marital_status': 'Single' if current_emp.marital_status=='S' else 'Married' if current_emp.marital_status=='M' else 'Divorced' if current_emp.marital_status=='D' else 'Widowed' if current_emp.marital_status=='W' else '',
            'employment_start_date': str(current_emp.employment_start_date),
            'employment_end_date': str(employment_end_date),
            'recruitment_method': current_emp.recruitment_method,
            'sick_days': current_emp.eligible_sick_hours if current_emp.eligible_sick_hours  else '',
            'eligible_vacation_days':  current_emp.eligible_vacation_weeks if current_emp.eligible_vacation_weeks else '',
            'paid_holidays':'Yes' if current_emp.paid_holidays is True else 'No' if  current_emp.paid_holidays is False else '', 
            'employee_status': 'Active' if current_emp.employee_status=='A' else 'Inactive' if current_emp.employee_status=='I' else 'Terminated' if current_emp.employee_status=='T' else '',
            'termination_reason': termination_reason,
            'termination_intent': termination_intent,
            'termination_id': termination_id,
            'rehire_eligibility': current_emp.rehire_eligibility,
            'employee_designation': emp_designation,
            'employee_department': emp_department,
            'role': roles,
            'account_manager':current_emp.account_manager,

        }


class GetRole(Resource):
    @jwt_required
    @prefix_decorator('GetRole')
    def post(self):
        username = get_jwt_identity()
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        userid = UserModel.query.filter_by(username=email).first()
        if userid:            
            user_roles_obj = UserRolesModel.query.filter_by(user_id=userid.userid).first()
            if user_roles_obj:
                logging.info("Roles Returned Succesfully")
                return {'roles': [RoleModel.find_by_role_id(x) for x in user_roles_obj.roles]}
            else:
                logging.debug("Role(s) Not Assigned To The User {}".format(email))
                return {"errorMessage":"Role(s) Not Assigned To The User {}".format(email)}
        else:
            logging.debug("User Does Not Exist")
            return{"errorMessage":"User Does Not Exist"}

class GetUserPrivileges(Resource):
    @jwt_required
    @prefix_decorator('GetUserPrivileges')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        user_id = UserModel.query.filter_by(username=data['emailId'].lower()).first()
        if user_id:
            user_roles = UserRolesModel.query.filter_by(user_id=user_id.userid).first()
            if user_roles:
                privileges_list = []
                for role in user_roles.roles:
                    for privilege_id in RolePrivilegeModel.query.filter_by(role_id=role).first().role_privileges:
                        privileges_list.append(PrivilegeModel.query.filter_by(id=privilege_id).first().privilege_name)
                return {"privileges": privileges_list}
            else:
                return {'errorMessage': "User Doesn't Have Any Roles Assigned"}
        else:
            return {"errorMessage":"User Does Not Exist"}

class ValidateClientName(Resource):
    @jwt_required
    @prefix_decorator('ValidateClientName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['organization_name'].lower().strip() not in [i.organization_name.lower().strip() for i in ClientDetailsModel.query.all()]:
            logging.info('Success')
            return {'message':'Success'}
        else:
            logging.debug('Client With Same Name Already Exists')
            return {'errorMessage':'Client With Same Name Already Exists'}

class ClientDetailsSchema(Schema):
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    number_of_contracts = fields.Integer(data_key="project_type_name",required=False)
    client_contract_details = fields.String(validate=validate.Length(max=255),data_key="client_contract_details",required=False)
    organization_name = fields.String(validate=validate.Length(max=255),data_key="organization_name",required=True)
    comments = fields.String(validate=validate.Length(max=255),data_key="comments",required=False)  
    client_status = fields.String(validate=validate.Length(max=1),data_key="client_status",required=True)

    @post_load
    def save_project(self, data, **kwargs):
        return data



class ClientContactSchema(Schema):
    contact_id = fields.String(validate=validate.Length(max=40),data_key="contact_id",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    department_id = fields.Integer(data_key="department_type",required=True)
    contact_address_id = fields.Integer(data_key="contact_address_id",required=False)
    contact_full_name = fields.String(validate=validate.Length(max=150),data_key="contact_full_name",required=True)
    contact_title = fields.String(validate=validate.Length(max=50),data_key="contact_title",required=True)
    contact_work_phone = fields.String(validate=validate.Length(max=20),data_key="contact_work_phone",required=True)
    contact_cell_phone = fields.String(validate=validate.Length(max=20),data_key="contact_cell_phone",required=False)
    contact_email = fields.Email(data_key="contact_email",required=True)
    reporting_manager_name = fields.String(validate=validate.Length(max=150),data_key="reporting_manager_name",required=True)

    @post_load
    def save_project(self, data, **kwargs):
        return data



class AddClient(Resource):
    @jwt_required
    @prefix_decorator('AddClient')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        contact_emails = []
        contact_phone_numbers = []
        for i in data['contacts']:
            contact_emails.append(i['contact_email'].lower())
            contact_phone_numbers.append(i['contact_work_phone'])
        if ClientDetailsModel.query.filter(func.lower(ClientDetailsModel.organization_name)==data['organization_name'].lower().strip()).first():
            logging.debug("A client already exists with same name")
            return {"errorMessage": "A Client Already Exists With Same Name"} 
            return {"errorMessage":"Contract Number Cannot Be Same"}
        if len(list(set(contact_emails))) != len(contact_emails):
            logging.debug("Contact Email Cannot Be Same")
            return {"errorMessage":"Contact Email Cannot Be Same"}
        if len(list(set(contact_phone_numbers))) != len(contact_phone_numbers):
            logging.debug("Contact Phone Numbers Cannot Be Same")
            return {"errorMessage":"Contact Phone Numbers Cannot Be Same"}
        if ClientDetailsModel.query.filter_by(organization_name=data['organization_name']).first():
            logging.debug("A client already exists with same name")
            return {"errorMessage": "A client already exists with same name"} 
        else:
            client_id = str(uuid.uuid4())
            try:
                try:    
                    data['client_status'] = 'A' if data['client_status']=='Active' else 'I' if data['client_status']=='Inactive' else 'T' if data['client_status']=='Terminated' else ''
                except Exception as e:  
                    logging.exception("Please Provide Valid Client Vendorship Type")
                    return {'errorMessage':"Please Provide Valid Client Vendorship Type"}
                schema = ClientDetailsSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                for i in data['contacts']:
                    try:
                        i['department_type'] = DepartmentModel.find_by_department_name(i['department_type'])
                        contract_id = str(uuid.uuid4())
                        contact_id = str(uuid.uuid4())
                    except Exception as e:
                        logging.exception("Please Provide Valid Department Type")
                        return {"errorMessage":"Please Provide Valid Department Type"}
                    schema3 = AddressSchema(unknown=EXCLUDE)
                    project = schema3.load(i)
                    result3 = schema3.dump(project)
                    schema4 = ClientContactSchema(unknown=EXCLUDE)
                    project = schema4.load(i)
                    result4 = schema4.dump(project)    
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
                #raise err
            new_client_details = ClientDetailsModel(
                client_id=client_id,
                client_status=result['client_status'],
                organization_name = data['organization_name'],
                created_time=date_obj,
                modified_time=date_obj,
                client_status_last_updtd_date=date_obj
            )
            new_client_details.save_to_db()

            for i in data['contacts']:
                add = Address1Model(
                    address_line1=i['addressLine1'],
                    address_line2=i['addressLine2'],
                    city=i['city'],
                    state_name=i['state'],
                    zip_code=i['zip'],
                    country=i['country']
                )
                add.save_to_db()
                addid = add.address_id
                contact_id = str(uuid.uuid4())
                new_client_contact = ClientContactModel(
                    contact_id=contact_id,
                    client_id=client_id,
                    department_id=i['department_type'],
                    contact_address_id=addid,
                    contact_full_name=i['contact_full_name'],
                    contact_title=i['contact_title'],
                    contact_work_phone=i['contact_work_phone'],
                    contact_email=i['contact_email'].lower(),
                    reporting_manager_name=i['reporting_manager_name'],
                    status = 'A',
                    created_by = userid,
                    created_time = date_obj,
                    modified_time = date_obj
                )
                new_client_contact.save_to_db()
            logging.debug("Client Added Successfully")
            return {"message": "Client Added Successfully"} 

class ClientView(Resource):
    @jwt_required
    @prefix_decorator('ClientView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        client_obj = ClientDetailsModel.query.filter_by(client_id=data['client_id']).first()
        if not client_obj:
            logging.debug("Client Does Not Exist")
            return {"errorMessage":"Client Does Not Exist"}
        clientcondetails = ClientContactModel.return_all_new(data['client_id'])['client_contact_details']
        return {"client_id": client_obj.client_id,
                "client_status": 'Active' if client_obj.client_status=='A' else 'Inactive' if client_obj.client_status=='I' else 'Terminated' if client_obj.client_status=='T' else 'Prospect'  if client_obj.client_status=='P' else '',
                'organization_name': client_obj.organization_name,
                "clientcondetails": clientcondetails,
                "termination_reason":client_obj.comments,
                "client_created_date":str(datetime.strptime(str(client_obj.created_time),"%Y-%m-%d %H:%M:%S.%f").date()),
                "client_Status_last_updated_date":str(client_obj.client_status_last_updtd_date)
                }

class EditClientContact(Resource):
    @jwt_required
    @prefix_decorator('EditClientContact')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        record3 = ClientContactModel.query.filter_by(contact_id=data['contact_id']).first()
        record5 = Address1Model.query.filter_by(address_id=record3.contact_address_id).first()
        if record3 and record5 and userid:
            all_similar_phone = ClientContactModel.query.filter_by(client_id=record3.client_id,contact_work_phone=data['contact_work_phone']).all() 
            all_similar_email = ClientContactModel.query.filter_by(client_id=record3.client_id,contact_email=data['contact_email'].lower()).all()
            
            if all_similar_phone:
                for i in all_similar_phone:
                    if i.contact_id != data['contact_id']:
                        logging.info("Already A Contact Exists With Same Contact Phone Number")
                        return {"errorMessage":"Already A Contact Exists With Same Contact Phone Number"}
            if all_similar_email:
                for i in all_similar_email:
                    if i.contact_id != data['contact_id']:  
                        logging.info("Already A Contact Exists With Same Email")
                        return {"errorMessage":"Already A Contact Exists With Same Email"}
            try:
                data['department_type'] = DepartmentModel.find_by_department_name(data['department_type'])
            except Exception as e:   
                logging.exception('Please Provide Valid Department Type')
                return {'errorMessage': 'Please Provide Valid Department Type'}
            try:
                schema = ClientContactSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                schema1 = AddressSchema(unknown=EXCLUDE)
                project = schema1.load(data)
                result1 = schema1.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                #raise err
                return {'errorMessage': err.messages}, 500
            record3.contact_full_name = result['contact_full_name']
            record3.contact_title = result['contact_title']
            record3.contact_work_phone = result['contact_work_phone']
            record3.contact_email = result['contact_email'].lower()
            record3.department_id = result['department_type']
            record3.reporting_manager_name = result['reporting_manager_name']
            record3.modified_by = userid.employee_id
            record3.modified_time = datetime.now()
            record5.address_line1 = result1['addressLine1']
            record5.address_line2 = result1['addressLine2']
            record5.city = result1['city']
            record5.state_name = result1['state']
            record5.zip_code = result1['zip']
            record5.country = result1['country']
            record3.save_to_db()
            record5.save_to_db()
            logging.debug('Client Contact Details Updated Successfully')
            return {'message': 'Client Contact Details Updated Successfully'}
        else: 
            logging.exception('Client Contact Does Not Exist')
            return {'message': 'Client Contact Does Not Exist'}

class AddClientContact(Resource):
    @jwt_required
    @prefix_decorator('AddClientContact')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()   
        date_obj = datetime.now()
        if ClientContactModel.query.filter_by(client_id=data['client_id'],contact_email=data['contact_email'].lower()).first():
            logging.debug("A Contact Already Exists With Same Contact Email")
            return {"errorMessage": "A Contact Already Exists With Same Contact Email"}
        if ClientContactModel.query.filter_by(client_id=data['client_id'],contact_work_phone=data['contact_work_phone']).first():
            logging.debug("A Contact Already Exists With Same Contact Phone Number")
            return {"errorMessage": "A Contact Already Exists With Same Contact Phone Number"}    
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        record1 = ClientDetailsModel.find_by_client_id(data['client_id'])
        if username:
            contact_id = str(uuid.uuid4())
            try:            
                data['department_type'] = DepartmentModel.find_by_department_name(data['department_type'])
            except Exception as e:
                logging.exception('Please Provide Valid Department Type')
                return {'errorMessage': 'Please Provide Valid Department Type'}
            try:
                schema = ClientContactSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                schema1 = AddressSchema(unknown=EXCLUDE)
                project = schema1.load(data)
                result1 = schema1.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
            add = Address1Model(
                address_line1=result1['addressLine1'],
                address_line2=result1['addressLine2'],
                city=result1['city'],
                state_name=result1['state'],
                zip_code=result1['zip'],
                country=result1['country']
            )
            add.save_to_db()
            addid = add.address_id
            new_client_contact = ClientContactModel(
                contact_id=contact_id,
                client_id=result['client_id'],
                department_id=result['department_type'],
                contact_address_id=addid,
                contact_full_name=result['contact_full_name'],
                contact_title=result['contact_title'],
                contact_work_phone=result['contact_work_phone'],
                contact_email=result['contact_email'].lower(),
                reporting_manager_name=result['reporting_manager_name'],
                status = 'A',
                created_by = userid,
                modified_time = date_obj,
                created_time = date_obj
            )
            new_client_contact.save_to_db()
            record1.modified_time = date_obj
            record1.save_to_db()
            logging.debug('Client Contact Added Successfully')
            return {'message': 'Client Contact Added Successfully'}
        else: 
            logging.exception('User Does Not Exist')
            return {'message': 'User Does Not Exist'}


class DeleteClient(Resource):
    @jwt_required
    @prefix_decorator('DeleteClient')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        clientprorecord = ProjectModel.query.filter_by(client_id=data['client_id']).first()
        client_record = ClientDetailsModel.query.filter_by(client_id=data['client_id']).first()
        if not client_record:
                logging.exception('Client Does Not Exist')
                return {"errorMessage":"Client Does Not Exist"}
        if clientprorecord:
            if clientprorecord.project_status == 'A':
                return {"errorMessage":"Failed,There Is An Active Project Under This Client"}
            else:
                if client_record:
                    client_record.client_status = 'I'
                    client_record.client_status_last_updtd_date = date_obj
                    client_record.comments = data['comments']
                    client_record.modified_time = date_obj
                    client_record.save_to_db()
                    return {"Message": "Client Inactivated Successfully"}
        else:
            if client_record:
                client_record.client_status = 'I'
                client_record.client_status_last_updtd_date = date_obj
                client_record.comments = data['comments']
                client_record.modified_time = date_obj
                client_record.save_to_db()
                return {"Message": "Client Inactivated Successfully"}

class DeleteSupplier(Resource):
    @jwt_required
    @prefix_decorator('DeleteSupplier')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        clientprorecord = ThirdPartyConsultantModel.query.filter_by(supplier_id=data['supplier_id']).all()
        if clientprorecord:
            for i in clientprorecord:
                if i.consultant_status == 'A':
                    logging.debug("Failed,There Is An Active Consultant Under This Supplier")
                    return {"errorMessage":"Failed,There Is An Active Consultant Under This Supplier"}
        record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        record.contract_status = 'I'
        record.supplier_status_last_updtd_date = date_obj
        record.comments = data['comments']
        record.modified_time = date_obj
        record.save_to_db()
        logging.info("Supplier Inactivated Successfully")
        return {"Message": "Supplier Inactivated Successfully"}


class AddPrivilege(Resource):
    @jwt_required
    def post(self):
        data = request.get_json(force=True)
        if data['privilege_name']:
            if PrivilegeModel.query.filter_by(privilege_name=data['privilege_name']).first():
                return {'message': 'Privilege {} Already Exists'.format(data['privilege_name'])}
            else:
                new_privilege = PrivilegeModel(
                    privilege_name=data['privilege_name']
                )
                new_privilege.save_to_db()
                return {'message': 'privilege Added Successfully'}
        else:
            return{"errorMessage":"Please Provide Valid Privilege Name"}


class RoleAssignment(Resource):
    @jwt_required
    @prefix_decorator('RoleAssignment')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        requested_emp_id_for = UserModel.find_by_username(data['username'].lower())
        if requested_emp_id_for:
            a = [RoleModel.find_by_role_name(i) for i in data['roles']]                    
            record = UserRolesModel.query.filter_by(user_id=requested_emp_id_for.userid).first()
            if not record:
                new_record = UserRolesModel(
                    user_id=requested_emp_id_for.userid,
                    roles=a
                )
                new_record.save_to_db()
            else:
                record.user_id = requested_emp_id_for.userid,
                record.roles = a
                record.save_to_db()
            logging.info("Added Succefully")
            return {"message": "Added Succefully"}
        else:
            logging.debug("User Does Not Exist")
            return{"errorMessage":"User Does Not Exist"}

class AddPrivilegesToRole(Resource):
    @jwt_required
    @prefix_decorator('AddPrivilegesToRole')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        requested_emp_id = UserModel.find_by_username(username)
        requested_role_id = RoleModel.find_by_role_name(data['role_name'])
        if requested_emp_id:
            role_privileges = RolePrivilegeModel(
                role_id=requested_role_id,
                role_privileges=data['role_privileges'],
            )
            role_privileges.save_to_db()
            return {'message': 'Privileges Added Successfully'}
        else:
            return {'message': 'Please Provide Valid Data'}

class UserInviteSchema(Schema):
    username = fields.Email(validate=validate.Length(max=120),required=False)
    base_url = fields.String(required=False)
    role = fields.String(validate=validate.Length(max=255),required=False)
    employee_number = fields.String(validate=validate.Length(max=15),required=False)
    employee_designation = fields.String(validate=validate.Length(max=100),required=False)
    employee_department = fields.String(validate=validate.Length(max=100),required=False)
    billable_resource = fields.String(validate=validate.Length(max=3),required=False)
    date_of_joining = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)



    @post_load
    def create_user(self, data, **kwargs):
        return data

class UserallInviteSchema(Schema):
    username = fields.Email(validate=validate.Length(max=120),required=False,data_key="Work Email")
    role = fields.String(validate=validate.Length(max=255),required=False,data_key="Role")
    employee_number = fields.String(validate=validate.Length(max=15),required=False,data_key="Employee Number")
    employee_designation = fields.String(validate=validate.Length(max=100),required=False,data_key="Job Title")
    employee_department = fields.String(validate=validate.Length(max=100),required=False,data_key="Department")
    billable_resource = fields.String(validate=validate.Length(max=3),required=False,data_key="Billable Resource")
    date_of_joining = fields.Date(format='%d-%b-%Y',allow_none=True,required=True,data_key="Date Joined")


    @post_load
    def create_user(self, data, **kwargs):
        return data


#
class InviteAll(Resource):
    @jwt_required
    def post(self):
        data = request.get_json(force=True)
        #print(data)
        sent_user = get_jwt_identity()  
        all_departments = [i.department_name for i in DepartmentModel.query.all()]
        all_designations = [i.designation_name for i in DesignationModel.query.all()]
        all_roles = [i.role_name for i in RoleModel.query.all()]
        print(all_roles)  
        for result in data['employees']:
            try:  
                schema = UserallInviteSchema(unknown=INCLUDE)
                userinvite = schema.load(result)
                op = schema.dump(userinvite)  
                #print(op)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        for i in data['employees']:
            if i['Department'] not in all_departments:
                return {'errorMessage':f"Invalid Department Provided For {i['Full Name']}"}
            if i['Job Title'] not in all_designations:
                return {'errorMessage':f"Invalid Job Title Provided For {i['Full Name']}"}
            if i['Billable Resource'] not in ['Yes','No']:
                return {'errorMessage':f"Invalid Billable Resource Provided For {i['Full Name']}"}
            if i['Role'] not in all_roles:
                return {'errorMessage':f"Invalid Role Provided For {i['Full Name']}"}
            if Employee1Model.query.filter_by(emp_number=i['Employee Number']).first():
                logging.debug(f"Employee Number Already Exists for email {i['Work Email']}")
                return {"errorMessage": f"Employee Number Already Exists {i['Work Email']}"}
            elif Employee1Model.query.filter_by(employee_status="Active", email_id=i['Work Email']).first():
                logging.debug(f"An Active User Already Exists With Same {i['Work Email']} Email")
                return {"errorMessage": f"An Active User Already Exists With Same {i['Work Email']} Email"}
            elif EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=i['Employee Number'],employee_email=i['Work Email']).first():
                logging.debug(f"An Invitation Mail Has Already Been Sent to {i['Work Email']}")
                return {"errorMessage": f"An Invitation Mail Has Already Been Sent to {i['Work Email']}"}
            elif (EmployeeInviteStatus.query.filter_by(employee_number=i['Employee Number']).first()):
                logging.debug(f"An Invitation Mail With Same Employee Number of [i['Work Email']] Already Been Sent")
                return {"errorMessage": f"An Invitation Mail With Same Employee Number of {i['Work Email']} Already Been Sent "}
            elif (EmployeeInviteStatus.query.filter_by(employee_email=i['Work Email']).first()):
                logging.debug(f"An Invitation Mail With Same Email {i['Work Email']} Already Been Sent")
                return {"errorMessage": f"An Invitation Mail With Same Email {i['Work Email']} Already Been Sent"}

        for i in data['employees']: 

            dept = DepartmentModel.query.filter_by(department_name=i['Department']).first().department_id
            desg = DesignationModel.query.filter_by(designation_name=i['Job Title']).first().designation_id
            role = RoleModel.query.filter_by(role_name=i['Role']).first().id
            email = i['Work Email'].lower()
            samp_username_bytes = email.encode('ascii')
            base64_username = base64.b64encode(samp_username_bytes)  
            username = base64_username.decode("ascii")
            samp_role_bytes = i['Role'].encode('ascii')
            base64_role = base64.b64encode(samp_role_bytes)
            role = base64_role.decode("ascii")
            samp_emp_num_bytes = str(i['Employee Number']).encode('ascii')
            base64_emp_num = base64.b64encode(samp_emp_num_bytes)
            emp_num = base64_emp_num.decode("ascii")
            samp_emp_dept_bytes = i['Department'].encode('ascii')
            base64_emp_dept = base64.b64encode(samp_emp_dept_bytes)
            emp_dept = base64_emp_dept.decode("ascii")
            samp_emp_desg_bytes = i['Job Title'].encode('ascii')
            base64_emp_desg = base64.b64encode(samp_emp_desg_bytes)
            emp_desg = base64_emp_desg.decode("ascii")  
            samp_emp_billabilty_bytes = i['Billable Resource'].encode('ascii')
            base64_emp_billability = base64.b64encode(samp_emp_billabilty_bytes)
            emp_br = base64_emp_billability.decode("ascii")
            samp_emp_joining_date_bytes = i['Date Joined'].encode('ascii')
            base64_emp_joining_date = base64.b64encode(samp_emp_joining_date_bytes)
            emp_jd = base64_emp_joining_date.decode("ascii")
            user = Employee1Model.query.filter_by(email_id=sent_user).first()
            if not user:
                logging.info("User {} Does Not Exist".format(sent_user))
                return {"errorMessage":"User {} Does Not Exist".format(sent_user)}
            base_url = data['base_url'] + '?email=' + username + '&role_id=' + role + '&emp_num=' + emp_num + '&emp_dept=' + emp_dept + '&emp_desg=' + emp_desg + '&emp_jd=' + emp_jd + '&emp_br='+emp_br
            msg = Message('Prutech invitation link', sender='support@prutech.com', recipients=[email])
            msg.html = """Hi <b>{0}</b>,<br>
            <br> Greetings from PRUTECH Team!</br>
            <p style="text-align:center"><b>Invitation</b></p>
            <p style="text-align:center"> {1} invited you to join </p>
            <p style="text-align:center"> PRUTECH HR Portal <a href={2}>Signup Here</a></p>   
            <br> <b>Regards,
            <br> PRUTECH Team.
            </b>""".format((email.split('@')[0]).split('.')[0].title(),user.first_name.title()+" "+user.last_name.title(),base_url)
            mail.send(msg)
            invite_record = EmployeeInviteStatus(employee_number=i['Employee Number'],
                                                 employee_email=email,designation_id=desg,department_id=dept ,invitation_status="Active",employee_role=i['Role'],
                                                 sent_time=datetime.now().strftime('%m-%d-%Y'),billable_resource=i['Billable Resource'][0],joining_date=i['Date Joined'])
            invite_record.save_to_db()

        logging.debug( "Invitations Sent Successfully")  
        return {"message": "Invitations Sent Successfully"}

    
      
class RoleEdit(Resource):
    @jwt_required  
    @prefix_decorator('RoleEdit')
    def post(self):
        logging.info('Entered into {0}'.format(self.__class__.__name__))
        data = request.get_json(force=True)
        print(data)
        record = RoleModel.query.filter_by(id=data['role_id']).first()
        record.role_name = data['role_name']
        record.save_to_db()
        logging.info("Role Edited To {}".format(data['role_name']))
        return {"message": "Role Edited To {}".format(data['role_name'])}

class GetRolePrivileges(Resource):
    @jwt_required
    @prefix_decorator('GetRolePrivileges')
    def post(self):
        logging.info('Entered into {0}'.format(self.__class__.__name__))
        data = request.get_json(force=True)
        print(data)
        requested_role_id = RoleModel.find_by_role_name(data['role_name'])
        role_privileges_obj = RolePrivilegeModel.query.filter_by(role_id=requested_role_id).first()
        if role_privileges_obj:
            logging.info("Success")
            return {
                'role_name': data['role_name'],
                'privileges': [PrivilegeModel.query.filter_by(id=x).first().privilege_name for x in role_privileges_obj.role_privileges]
            }
        else:
            logging.debug("Role {} Dosen't Have Privileges".format(data['role_name']))
            return {"errorMessage":"Role {} Dosen't Have Privileges".format(data['role_name'])}

class ProjectSchema(Schema):
    project_id = fields.String(validate=validate.Length(max=40),data_key="project_id",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    work_location = fields.String(validate=validate.Length(min=0,max=255),data_key="work_location",required=False)
    project_name = fields.String(validate=validate.Length(min=1,max=40),data_key="project_name",required=True)
    project_description = fields.String(validate=validate.Length(min=0,max=255),data_key="project_description",required=True)  
    scheduled_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="scheduled_start_date",required=False)
    scheduled_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="scheduled_end_date",required=False)
    actual_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="actual_start_date",required=False)
    actual_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="actual_end_date",required=False)
    revenue_amount = fields.Float(allow_none=True,data_key="project_revenue",required=False)
    project_manager_id = fields.Integer(data_key="project_manager",required=True)
    proj_manager_phone_number = fields.String(validate=validate.Length(max=20),data_key="project_manager_contact_number",required=False)  
    project_sponsor_name = fields.String(validate=validate.Length(min=0,max=150),data_key="project_sponsor",required=False)
    project_status = fields.String(validate=validate.Length(max=1),data_key="project_status",required=False)
    daily_work_hours = fields.Float(data_key="daily_work_hours",required=False)
    comments = fields.String(validate=validate.Length(max=255),data_key="comments",required=False)

    @post_load
    def save_project(self, data, **kwargs):
        return data

class ValidateProjectName(Resource):
    @jwt_required
    @prefix_decorator('ValidateProjectName')
    def post(self):
        data = request.get_json(force=True)
        print(data)  
        if not data['project_name']:
            logging.debug('Please Provide Valid Project Name')
            return {'errorMessage':'Please Provide Valid Project Name'}
        projects_dict = {i.project_name.lower().strip():i.project_id for i in ProjectModel.query.all()}
        if len(data) == 2:
            if projects_dict.get(data['project_name'].lower().strip(),1) != 1:
                if projects_dict[data['project_name'].lower().strip()] != data['project_id']:
                    logging.debug("Project With Same Name Already Exists")
                    return {'errorMessage':'Project With Same Name Already Exists'}
                else:
                    logging.debug("Success")
                    return {'message':'Success'}

            else:
                logging.debug("Success")
                return {'message':'Success'}
        else:
            if projects_dict.get(data['project_name'].lower().strip(),1) != 1:
                logging.debug("Project With Same Name Already Exists")
                return {'errorMessage':'Project With Same Name Already Exists'}
            else:
                logging.debug("Success")
                return {'message':'Success'}   


class AddProject(Resource):
    @jwt_required
    @prefix_decorator('AddProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if userid:
            try:
                if ProjectModel.query.filter(func.lower(ProjectModel.project_name)==data['project_name'].lower()).first():
                    logging.debug("A Project Already Exists With Same Name")
                    return {"errorMessage":"A Project Already Exists With Same Name"}
                if data['project_type_name']:
                    data['project_type_name'] = ProjectTypeModel.query.filter_by(project_type_name=data['project_type_name']).first()
                data['project_manager'] = Employee1Model.find_by_employeenumber(data['project_manager'])
                if data['project_manager']:
                    data['project_manager'] =data['project_manager'].employee_id 
                else:
                    logging.exception("Project Manager Does Not Exist")
                    return {'errorMessage':"Project Manager Does Not Exist"}
                if data['project_type_name']:
                    data['project_type_name'] =data['project_type_name'].project_type_id 
#                else:
#                    logging.exception("Project Type Does Not Exist")
#                    return {'errorMessage':"Project Type Does Not Exist"}
                data['project_status'] = 'A'
                schema = ProjectSchema(unknown=INCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                project_id = str(uuid.uuid4())
                if not ClientDetailsModel.query.filter_by(client_id=result['client_id']).first():
                    return {"errorMessage":"Client Does Not Exist"}
                new_project = ProjectModel(
                    project_id=project_id,
                    client_id=result['client_id'],
                    project_type_id=data['project_type_name'],
                    project_name=result['project_name'].strip(),
                    project_description=result['project_description'],
                    revenue_amount=result['project_revenue'],
                    project_sponsor_name=result['project_sponsor'],
                    work_location=result['work_location'],
                    scheduled_start_date=result['scheduled_start_date'],
                    scheduled_end_date=result['scheduled_end_date'],
                    project_manager_id=result['project_manager'],
                    proj_manager_phone_number=result['project_manager_contact_number'],
                    project_status=data['project_status'],
                    created_time=date_obj,
                    modified_time=date_obj,
                    created_by = userid.employee_id,
                    modified_by = userid.employee_id,
                    project_status_last_updtd_date = date_obj
                )
                new_project.save_to_db()
                a = [str(data['project_manager'])]
                new_project_employee = ProjectEmployeeModel(project_id=new_project.project_id, employee_id=list(set(a)))
                new_project_employee.save_to_db()

                logging.debug('Project Added Successfully')
                return {'message': 'Project Added Successfully'}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        else:
            logging.debug("User Does Not Exists")
            return{"errorMessage":"User Does Not Exists"}


class ProjectEdit(Resource):
    @jwt_required
    @prefix_decorator('ProjectEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        record = ProjectModel.find_by_project_type_id(data['project_id'])
        record1= ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
        record2 = ProjectModel.query.filter(func.lower(ProjectModel.project_name)==data['project_name'].lower()).first()
        print(data['project_id'])
        if record2 and record2.project_id!=data['project_id']:
            print(record2.project_id)
            logging.debug("A Project Already Exists With Same Name")
            return {"errorMessage":"A Project Already Exists With Same Name"}
        if record and record1:
            data['project_type_name'] = ProjectTypeModel.query.filter_by(project_type_name = data['project_type_name']).first()
            if data['project_type_name']:
                data['project_type_name'] = data['project_type_name'].project_type_id
            else:
                data['project_type_name'] = None
            data['project_manager'] = Employee1Model.find_by_employeenumber(data['project_manager'])
            if data['project_manager']:
                data['project_manager'] = data['project_manager'].employee_id
            data['project_status'] = 'A' if data['project_status']=='Active' else 'I' if data['project_status']=='Inactive' else 'T' if data['project_status']=='Terminated' else ''
            try:
                schema = ProjectSchema(unknown=INCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                manager  = record.project_manager_id
                # change the values you want to update
                record.project_type_id = data['project_type_name']
                record.work_location = result['work_location']
                record.project_name = result['project_name'].strip()
                record.project_description = result['project_description']
                record.scheduled_start_date = result['scheduled_start_date']
                record.scheduled_end_date = result['scheduled_end_date']
                record.actual_start_date = result['actual_start_date']
                record.actual_end_date = result['actual_end_date']
                record.revenue_amount = result['project_revenue']
                record.project_manager_id =   result['project_manager']
                record.proj_manager_phone_number = result['project_manager_contact_number']
                record.project_sponsor_name = result['project_sponsor']
                record.project_status = result['project_status']
                record.modified_time = datetime.now()
                record.modified_by = userid
                # commit changes
                record.save_to_db()
                project = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
                if project:
                    emp_list = project.employee_id
                    emp_list.append(str(result['project_manager']))
                    project.delete_from_db()            
                    project1 = ProjectEmployeeModel(project_id=data['project_id'],employee_id=list(set(emp_list)))
                    project1.save_to_db()
                else:
                    new_project_employee = ProjectEmployeeModel(project_id=data['project_id'], employee_id=list(set(a)))
                    new_project_employee.save_to_db()
                logging.debug("Project Details Updated Successfully")
                return {"message": "Project Details Updated Successfully"}
            except ValidationError as err:
                logging.exception('Exception Occured in {0}, {1}'.format(self.__class__.__name__,err.messages))
                return {'errorMessage': err.messages}, 500
        else:
            logging.debug('Please Provide Valid Project Id')
            return {"errorMessage":'Please Provide Valid Project Id'}

class ResourceAssignment(Resource):
    @jwt_required
    @prefix_decorator('ResourceAssignment')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        requested_emp_id = Employee1Model.find_by_username(username)
        record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
        if record and requested_emp_id:
            project_manager = record.project_manager_id
            if project_manager not in [i['employee_num'] for i in data['assigned_employees']]:
                logging.info("Cannot Unassign Project Manager, Assign Project To Other Employee And Retry")
                return {"errorMessage":"Cannot Unassign Project Manager, Assign Project To Other Employee And Retry"}
            record1 = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
            list_of_removed_employees = []
            for i in record1.employee_id:
                if i not in [str(j['employee_num']) for j in data['assigned_employees']]:
                     list_of_removed_employees.append(i)
            #for j in list_of_removed_employees:
                #rec = EmployeePrimaryProjectModel.query.filter_by(employee_id=int(j),project_id=data['project_id']).first()
                #if rec:
                    #if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(j)])).count()>1:
                        #logging.debug("Cannot Unassign Employee As It Is A Primary Project")
                        #return {"errorMessage":"Cannot Unassign Employee As It Is A Primary Project"}
            for k in data['assigned_employees']:
                if 'CON' in str(k['employee_num']):
                    if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(k['employee_num'])])).count()>=10:
                        e = ThirdPartyConsultantModel.query.filter_by(consultant_id=k['employee_num']).first()
                        logging.debug('{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name))
                        return {'errorMessage':'{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name)}
                elif not ProjectModel.query.filter_by(project_manager_id=k['employee_num']).first():
                    if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(k['employee_num'])])).count()>=10:
                        e = Employee1Model.query.filter_by(employee_id=k['employee_num']).first()
                        logging.debug('{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name))
                        return {'errorMessage':'{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name)} 
            a = []
            for x in data['assigned_employees']:
                #if ('CON' not in str(x['employee_num'])) and not EmployeePrimaryProjectModel.query.filter_by(employee_id=x['employee_num']).first():
                    #record = EmployeePrimaryProjectModel(employee_id=x['employee_num'],project_id=data['project_id'],created_by = requested_emp_id.employee_id,created_time = date_obj)
                    #record.save_to_db()
                a.append(str(x['employee_num']))
            project = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
            if project:
                project.project_id = data['project_id']
                project.employee_id = a
                project.save_to_db()
                record.modified_time=date_obj
                record.save_to_db()
                logging.info('Resources Updated Successfully')
                return {'message': 'Resources Updated Successfully'}
            else:
                new_project_employee = ProjectEmployeeModel(project_id=data['project_id'], employee_id=list(set(a)))
                new_project_employee.save_to_db()
                record.modified_time=date_obj
                record.save_to_db()
                logging.info('Resources Updated Successfully')
                return {'message': 'Resources Updated Successfully'}
        else: 
            logging.debug('Please Provide Valid Project Id')
            return {'errorMessage': 'Please Provide Valid Project Id'}

class ProjectView(Resource):
    @jwt_required
    @prefix_decorator('ProjectView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ProjectModel.query.filter_by(project_id=data['project_id']).first():
            logging.debug("Project Does Not Exist")
            return {"errorMessage":"Project Does Not Exist"}
        return ProjectEmployeeModel.find_by_project_id2(data['project_id'])

class EmpProjects(Resource):
    @jwt_required
    @prefix_decorator('EmpProjects')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not Employee1Model.query.filter_by(employee_id=data['employee_id']).first():
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        logging.info("Success")
        return ProjectEmployeeModel.return_all_empprojects(data['employee_id'])

class AddPrivilegeToRole(Resource):
    @jwt_required
    @prefix_decorator('AddPrivilegeToRole')
    def post(self):
        data = request.get_json(force=True)
        print(data)

        requested_role_id = RoleModel.find_by_role_name(data['role_name'])
        if requested_role_id:
            record = RolePrivilegeModel.query.filter_by(role_id=requested_role_id).first()
            privileges = []
            for i in data['role_privileges']:
                privileges.append(PrivilegeModel.query.filter_by(privilege_name=i).first().id)
            if not record:
                role_privileges = RolePrivilegeModel(
                    role_id=requested_role_id,
                    role_privileges=privileges,
                )
                role_privileges.save_to_db()
                logging.info('Privileges Added Successfully')
                return {'message': 'Privileges Added Successfully'}
            else:
                record.role_id = requested_role_id
                record.role_privileges = privileges
                record.save_to_db()
                logging.info('Privileges Added Successfully')
                return {'message': 'Privileges Added Successfully'}
        else:
            logging.debug("Role Does Not Exist")
            return{"errorMessage":"Role Does Not Exist"}


class EmployeeBasicDetailsEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeBasicDetailsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        if not data['employee_number']:
            logging.debug("Please Provide Valid Employee Number")
            return {'errorMessage':"Please Provide Valid Employee Number"}
        record = Employee1Model.find_by_employeenumber(data['employee_number'])
        data['gender'] = 'M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O' if data['gender']=='Other' else ''
        data['marital_status'] = 'S' if data['marital_status']=='Single' else 'M' if data['marital_status']=='Married' else 'D' if data['marital_status']=='Divorced' else 'W' if data['marital_status']=='Widowed' else ''
        try:
            schema = EmployeeBasicEditSchema2(unknown=INCLUDE)
            project = schema.load(data)
            result = schema.dump(project)
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage': err.messages}, 500
        if record:  
            # change the values you want to update
            record.first_name = result['first_name']
            record.middle_name = result['middle_name']
            record.last_name = result['last_name']
            record.gender =   result['gender']
            record.primary_phone_number = result['primary_phone_number']
            record.alternate_phone_number = result['alternate_phone_number']
            record.marital_status = result['marital_status']
            record.modified_time = date_obj
            # commit changes
            record.save_to_db()
            logging.debug("Employee Basic Details Updated Successfully")
            return {"message": "Employee Basic Details Updated Successfully"}
        else:
            logging.debug("No Such Employee Found")
            return {"message": "No Such Employee Found"}

class EmployeeBasicEditSchema1(Schema):
    employee_number = fields.String(validate=validate.Length(max=15),required=False)
    gender = fields.String(validate=validate.Length(max=1),required=False)
    marital_status = fields.String(validate=validate.Length(max=1),required=False)
    first_name = fields.String(validate=validate.Length(max=75),required=False)
    middle_name = fields.String(validate=validate.Length(max=75),required=False)
    last_name = fields.String(validate=validate.Length(max=75),required=False)
    gender = fields.String(validate=validate.Length(max=150),required=False)
    primary_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    alternate_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    us_citizenship = fields.Boolean(required=False)    

    @post_load
    def save_address(self, data, **kwargs):
        return data
        
class EmployeeBasicEditSchema2(Schema):
    employee_number = fields.String(validate=validate.Length(max=15),required=False)
    gender = fields.String(validate=validate.Length(max=1),required=False)
    marital_status = fields.String(validate=validate.Length(max=1),required=False)
    first_name = fields.String(validate=validate.Length(max=75),required=False)
    middle_name = fields.String(validate=validate.Length(max=75),required=False)
    last_name = fields.String(validate=validate.Length(max=75),required=False)
    gender = fields.String(validate=validate.Length(max=150),required=False)
    primary_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    alternate_phone_number = fields.String(validate=validate.Length(max=20),required=False)

    @post_load
    def save_address(self, data, **kwargs):
        return data

class EmploymentDetailsEditSchema(Schema):
    employee_number = fields.String(validate=validate.Length(max=15),required=False)
    employee_status = fields.String(validate=validate.Length(max=1),required=False)
    employee_department = fields.Integer(required=False)
    employee_designation = fields.Integer(required=False)
    termination_reason = fields.Integer(allow_none=True,required=False)
    account_manager = fields.String(validate=validate.Length(max=150),required=False)
    reporting_manager_id = fields.Integer(required=False)
    recruiter = fields.String(validate=validate.Length(max=150),required=False)
    termination_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    rehire_eligibility = fields.Boolean(required=False,allow_none=True)

    @post_load
    def save_address(self, data, **kwargs):
        return data

class EmploymentDetailsEdit(Resource):
    @jwt_required
    @prefix_decorator('EmploymentDetailsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        depid = DepartmentModel.find_by_department_name(data['employee_department'])
        desid = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
        data['employee_status'] = 'A' if data['employee_status']=='Active' else 'I' if data['employee_status']=='Inactive' else 'T' if data['employee_status']=='Terminated' else ''
        if depid and desid:
            data['employee_designation'] = desid.designation_id
            data['employee_department'] = depid
        else:   
            logging.debug("Please Choose Valid Designation/Department/Employment Type")
            return {"errorMessage":"Please Choose Valid Designation/Department/Employment Type"}
        try:    
            schema = EmploymentDetailsEditSchema(unknown=INCLUDE)
            employmentdet = schema.load(data)
            result = schema.dump(employmentdet) 
            if result['employee_status'] != 'A':
                if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(current_emp.employee_id)])).all():
                    return {"errorMessage":"Failed,Employee Assigned To Active Project"}
            current_emp1 = Employee1Model.find_by_employeenumber(result['employee_number'])
            if current_emp1.employee_status != data['employee_status']:
                last_update_date = date_obj
            else:
                last_update_date = current_emp1.employee_status_last_updtd_date
            if current_emp:
                if result['employee_status'] == 'T': 
                    current_emp.rehire_eligibility = result['rehire_eligibility']
                if result['employee_status'] == 'I': 
                    current_emp.inactive_reason_id = data['inactive_reason_id']
                if result['employee_status'] == 'A': 
                    current_emp.inactive_reason_id = None
                current_emp.department_id = result['employee_department']
                current_emp.designation_id = result['employee_designation']
                current_emp.employee_status = result['employee_status']
                current_emp.termination_reason_id = result['termination_reason']
                current_emp.account_manager = result['account_manager']
                current_emp.reporting_manager_id= result['reporting_manager_id']
                current_emp.recruiter = result['recruiter']
                current_emp.termination_date = result['termination_date']
                current_emp.modified_time = date_obj
                current_emp.employee_status_last_updtd_date = last_update_date
                current_emp.save_to_db()
                if result['employee_status'] == 'T':
                    logging.debug("Employee Terminated Successfully")
                    return {"message": "Employee Terminated Successfully"}
                elif result['employee_status'] == 'I':
                    logging.debug("Employee Inactivated Successfully")
                    return {"message": "Employee Inactivated Successfully"}
                else:
                    logging.debug("Employee Details Updated Successfully")
                    return {"message": "Employee Details Updated Successfully"}
            else:
                b = Employee1Model(
                    employee_id=current_emp.employee_id,
                    department_id=result['employee_department'],
                    designation_id=result['employee_designation'],
                    employee_status=result['employee_status'],
                    termination_reason_id=result['termination_reason'],
                    account_manager=result['account_manager'],
                    reporting_manager_id = result['reporting_manager_id'],
                    recruiter = result['recruiter'],
                    termination_date = result['termination_date'],
                    modified_time = date_obj,
                    employee_status_last_updtd_date=datetime.now(),
                    rehire_eligibility = result['rehire_eligibility'],
                    inactive_reason_id = data['inactive_reason_id'] if data['inactive_reason_id'] else None,
                )
                b.save_to_db()
                if result['employee_status'] == 'T':
                    logging.debug("Employee Terminated Successfully")
                    return {"message": "Employee Terminated Successfully"}
                elif result['employee_status'] == 'I':
                    logging.debug("Employee Inactivated Successfully")
                    return {"message": "Employee Inactivated Successfully"}
                else:
                    logging.debug("Employee Details Updated Successfully")
                    return {"message": "Employee Details Updated Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500



class EmployeeLcaEditSchema1(Schema):
    work_address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    work_address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    work_city = fields.String(validate=validate.Length(max=100),required=False)
    work_state_name = fields.String(validate=validate.Length(max=100),required=False)
    work_zip_code = fields.String(required=False)
    work_country = fields.String(validate=validate.Length(max=100),required=False)
    alt_address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    alt_address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    alt_city = fields.String(validate=validate.Length(max=100),required=False)
    alt_state_name = fields.String(validate=validate.Length(max=100),required=False)
    alt_zip_code = fields.String(required=False)
    alt_country = fields.String(validate=validate.Length(max=100),required=False)
    lca_salary = fields.Float(required=False)
    lca_wage_level = fields.Float(required=False)
    lca_prevailing_wages = fields.Float(required=False)
    lca_start_date = fields.Date(format='%m-%d-%Y',required=True)
    lca_end_date = fields.Date(format='%m-%d-%Y',required=True)
    current_status = fields.String(validate=validate.Length(max=1),required=False)

    @post_load
    def save_address(self, data, **kwargs):
        return data


class DeleteProject(Resource):
    @jwt_required
    @prefix_decorator('DeleteProject')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        proemprecord = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
        if proemprecord:
            if len(proemprecord.employee_id)>1:
                logging.debug("Failed, As Active Employees Still Assigned To This Project")
                return {"errorMessage":"Failed, As Active Employees Still Assigned To This Project"}
            record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
            record.project_status = 'I'
            record.comments = data['comments']
            record.modified_time = date_obj
            record.project_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info( "Project Deactivated Successfully")
            return {"Message": "Project Deactivated Successfully"}
        else:
            record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
            record.project_status = 'I'
            record.comments = data['comments']
            record.modified_time = datetime.now()
            record.project_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info( "Project Deactivated Successfully")
            return {"Message": "Project Deactivated Successfully"}


class UserInvite(Resource):
    @jwt_required
    @prefix_decorator('UserInvite')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        sent_user = get_jwt_identity()
        try:
            schema = UserInviteSchema(unknown=EXCLUDE)
            userinvite = schema.load(data)
            result = schema.dump(userinvite)
            dept = DepartmentModel.query.filter_by(department_name=result['employee_department']).first()
            desg = DesignationModel.query.filter_by(designation_name=result['employee_designation']).first()
            role = RoleModel.query.filter_by(role_name=result['role']).first()
            email = result['username'].lower()
            if dept:
                dept = dept.department_id
            else:
                logging.debug('Department Does Not Exists')
                return {'errorMessage':'Department Does Not Exists'}
            if desg:
                desg = desg.designation_id
            else:
                logging.debug('Designation Does Not Exists')
                return {'errorMessage':'Designation Does Not Exists'}
            if role:
                role = role.id
            else:
                logging.debug("Role Does Not Exists")
                return {"errorMessage":"Role Does Not Exists"} 
            if Employee1Model.query.filter_by(emp_number=result['employee_number']).first():
                logging.debug("Employee Number Already Exists")
                return {"errorMessage": "Employee Number Already Exists"}
            elif Employee1Model.query.filter_by(employee_status="Active", email_id=email).first():
                logging.debug("An Active User Already Exists With Same Email")
                return {"errorMessage": "An Active User Already Exists With Same Email"}
            elif EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=result['employee_number'],employee_email=email).first():
                logging.debug("An Invitation Mail Has Already Been Sent")
                return {"errorMessage": "An Invitation Mail Has Already Been Sent"}
            elif (EmployeeInviteStatus.query.filter_by(employee_number=result['employee_number']).first()):
                logging.debug("An Invitation Mail With Same Employee Number Already Been Sent")
                return {"errorMessage": "An Invitation Mail With Same Employee Number Already Been Sent"}
            elif (EmployeeInviteStatus.query.filter_by(employee_email=email).first()):
                logging.debug("An Invitation Mail With Same Email Already Been Sent")
                return {"errorMessage": "An Invitation Mail With Same Email Already Been Sent"}
            else:
                samp_username_bytes = email.encode('ascii')
                base64_username = base64.b64encode(samp_username_bytes)  
                username = base64_username.decode("ascii")
                samp_role_bytes = result['role'].encode('ascii')
                base64_role = base64.b64encode(samp_role_bytes)
                role = base64_role.decode("ascii")
                samp_emp_num_bytes = str(result['employee_number']).encode('ascii')
                base64_emp_num = base64.b64encode(samp_emp_num_bytes)
                emp_num = base64_emp_num.decode("ascii")
                samp_emp_dept_bytes = result['employee_department'].encode('ascii')
                base64_emp_dept = base64.b64encode(samp_emp_dept_bytes)
                emp_dept = base64_emp_dept.decode("ascii")
                samp_emp_desg_bytes = result['employee_designation'].encode('ascii')
                base64_emp_desg = base64.b64encode(samp_emp_desg_bytes)
                emp_desg = base64_emp_desg.decode("ascii")
                samp_emp_billabilty_bytes = result['billable_resource'].encode('ascii')
                base64_emp_billability = base64.b64encode(samp_emp_billabilty_bytes)
                emp_br = base64_emp_billability.decode("ascii")
                samp_emp_joining_date_bytes = result['date_of_joining'].encode('ascii')
                base64_emp_joining_date = base64.b64encode(samp_emp_joining_date_bytes)
                emp_jd = base64_emp_joining_date.decode("ascii")
                user = Employee1Model.query.filter_by(email_id=sent_user).first()
                if not user:
                    logging.info("User {} Does Not Exist".format(sent_user))
                    {"errorMessage":"User {} Does Not Exist".format(sent_user)}
                base_url = result['base_url'] + '?email=' + username + '&role_id=' + role + '&emp_num=' + emp_num + '&emp_dept=' + emp_dept + '&emp_desg=' + emp_desg + '&emp_jd=' + emp_jd + '&emp_br='+emp_br
                msg = Message('Prutech invitation link', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Greetings from PRUTECH Team!</br>
                <p style="text-align:center"><b>Invitation</b></p>
                <p style="text-align:center"> {1} invited you to join </p>
                <p style="text-align:center"> PRUTECH HR Portal <a href={2}>Signup Here</a></p>   
                <br> <b>Regards,
                <br> PRUTECH Team.
                </b>""".format((result['username'].split('@')[0]).split('.')[0].title(),user.first_name.title()+" "+user.last_name.title(),base_url)
                mail.send(msg)
                invite_record = EmployeeInviteStatus(employee_number=result['employee_number'],
                                                     employee_email=email,designation_id=desg,department_id=dept ,invitation_status="Active",employee_role=result['role'],
                                                     sent_time=datetime.now().strftime('%m-%d-%Y'),billable_resource=result['billable_resource'][0],joining_date=result['date_of_joining'])
                invite_record.save_to_db()
                logging.debug( "Invitation Sent Successfully")
                return {"message": "Invitation Sent Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage': err.messages}, 500

class AddSupplier(Resource):
    @jwt_required
    @prefix_decorator('AddSupplier')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        data['contract_status'] = 'A' if data['contract_status']=='Active' else 'I' if data['contract_status']=='Inactive' else 'T' if data['contract_status']=='Terminated' else ''
        if StaffingSupplierModel.query.filter_by(supplier_name=data['supplier_name']).first():
            return {"errorMessage":"Supplier Name Already Exists"}
        else:
            try:
                schema1 = StaffingSupplierSchema(unknown=EXCLUDE)  
                supplier = schema1.load(data)
                result1 = schema1.dump(supplier)
                for data_add in data['address']:
                    schema2 = AddressSchema(unknown=EXCLUDE)
                    address = schema2.load(data_add)
                    result2 = schema2.dump(address)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
            sup = StaffingSupplierModel(
                supplier_name=result1['supplier_name'],
                contact_number=result1['contact_number'],
                contract_status=result1['contract_status'],
                owner_name=result1['owner_name'],
                company_website_url=result1['company_website_url'],
                created_by = userid,
                created_time=date_obj,
                modified_time=date_obj,
                supplier_status_last_updtd_date = date_obj
            )
            sup.save_to_db()
            for data_add in data['address']:
                b = Address1Model(
                    address_line1=data_add['addressLine1'],
                    address_line2=data_add['addressLine2'],
                    city=data_add['city'],
                    state_name=data_add['state'],
                    zip_code=data_add['zip'],
                    country=data_add['country']
                )
                b.save_to_db()
                addid = b.address_id
                c = SupplierAddressModel(
                    address_id=addid,
                    supplier_id=sup.supplier_id,
                    status = 'A',
                    modified_time = date_obj ,
                    created_time = date_obj,
                    created_by = userid
                )
                c.save_to_db()
            return {
                "message": "Supplier Details Added Successfully",
                "supplier_id": sup.supplier_id
            }

class ValidateSupplierName(Resource):
    @jwt_required
    @prefix_decorator('ValidateSupplierName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['supplier_name'].lower().strip() not in [i.supplier_name.lower().strip() for i in StaffingSupplierModel.query.all()]:
            logging.info('Success')
            return {'message':'Success'}
        else:
            logging.debug('Supplier With Same Name Already Exists')
            return {'errorMessage':'Supplier With Same Name Already Exists'}

class StaffingSupplierSchema(Schema):
    supplier_name = fields.String(validate=validate.Length(max=150),data_key="supplier_name",required=True)
    supplier_id = fields.Integer(data_key="supplier_id",required=False)
    contract_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="contract_start_date",required=False)
    contract_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="contract_end_date",required=False)
    contact_number = fields.String(validate=validate.Length(max=20),data_key="contact_number",required=True)
    termination_reason = fields.String(allow_none=True,validate=validate.Length(max=255),data_key="termination_reason",required=False)
    contract_status = fields.String(validate=validate.Length(max=1),data_key="contract_status",required=True)
    owner_name = fields.String(validate=validate.Length(max=150),data_key="owner_name",required=False)
    company_website_url = fields.String(validate=validate.Length(max=255),data_key="company_website_url",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data
  
class SupplierContactSchema(Schema):
    contact_id = fields.Integer(data_key="consultant_id",required=False)
    supplier_id = fields.Integer(data_key="supplier_id",required=False)
    supplier_contact_type_id = fields.Integer(data_key="contact_type",required=True)
    contact_first_name = fields.String(validate=validate.Length(max=75),data_key="contact_first_name",required=True)
    contact_last_name = fields.String(validate=validate.Length(max=75),data_key="contact_last_name",required=True)
    contact_middle_name = fields.String(validate=validate.Length(max=75),data_key="contact_middle_name",required=False)
    contact_phone = fields.String(validate=validate.Length(max=20),data_key="contact_phone",required=True)
    contact_email = fields.String(validate=validate.Length(max=120),data_key="contact_email",required=True)
    contact_title = fields.String(validate=validate.Length(max=50),data_key="contact_title",required=True)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

class AddSupplierContact(Resource):
    @jwt_required
    @prefix_decorator('AddSupplierContact')
    def post(self):
        i = request.get_json(force=True)
        logging.debug(i)
        for data in i['contact']:
            if SupplierContactModel.query.filter_by(supplier_id=i['supplier_id'],contact_email=data['contact_email'].lower(),contact_status='A').first():
                return {"errorMessage":"Already Supplier Contact Exists With Same Email"}
            if SupplierContactModel.query.filter_by(supplier_id=i['supplier_id'],contact_phone=data['contact_phone'],contact_status='A').first():        
                return {"errorMessage":"Already Supplier Contact Exists With Same Contact Phone Number"}
            d = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first()
            if not d:
                logging.debug("Invalid Contact Type")
                return {"errorMessage":"Invalid Contact Type"}
        for data in i['contact']:
            data['contact_type'] = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first().supplier_contact_type_id
            try:
                schema = SupplierContactSchema(unknown=INCLUDE)
                supplier = schema.load(data)
                result = schema.dump(supplier)
                a = SupplierContactModel(
                    supplier_id=i['supplier_id'],
                    contact_first_name=data['contact_first_name'],
                    contact_last_name=data['contact_last_name'],
                    contact_middle_name=data['contact_middle_name'],
                    supplier_contact_type_id=data['contact_type'],
                    contact_phone=data['contact_phone'],
                    contact_email=data['contact_email'].lower(),
                    contact_title=data['contact_title'],
                    contact_status='A'
                )
                a.save_to_db()
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        logging.debug("Supplier Contact Details Added")
        return {"message": "Supplier Contact Details Added"}

class AddSupplierContact1(Resource):
    @jwt_required
    @prefix_decorator('AddSupplierContact1')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_email=data['contact_email'].lower(),contact_status='A').first():
            logging.debug("Already Supplier Contact Exists With Same Email")
            return {"errorMessage":"Already Supplier Contact Exists With Same Email"}
        if SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_phone=data['contact_phone'],contact_status='A').first():
            logging.debug("Already Supplier Contact Exists With Same Contact Phone Number")        
            return {"errorMessage":"Already Supplier Contact Exists With Same Contact Phone Number"}
        data['contact_type'] = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first()
        record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if not record:
            logging.debug('Supplier Does Not Exist')
            return {'errorMessage': 'Supplier Does Not Exist'}
        data['contact_type'] = data['contact_type'].supplier_contact_type_id
        schema = SupplierContactSchema(unknown=INCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        a = SupplierContactModel(
        supplier_id=result['supplier_id'],
        contact_first_name=result['contact_first_name'],
        contact_last_name=result['contact_last_name'],
        contact_middle_name=result['contact_middle_name'],
        supplier_contact_type_id=result['contact_type'],
        contact_phone=result['contact_phone'],
        contact_email=result['contact_email'].lower(),
        contact_title=result['contact_title'],
        contact_status='A'
    )
        a.save_to_db()
        record.modified_time = datetime.now()
        record.save_to_db()
        logging.debug( "Supplier Contact Details Added")
        return {"message": "Supplier Contact Details Added"}

class SupplierView(Resource):
    @jwt_required
    @prefix_decorator('SupplierView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        supplier_obj = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if not supplier_obj:
            logging.debug("Suppier Does Not Exist")
            return {"errorMessage":"Suppier Does Not Exist"}
        supplier_address_obj = SupplierAddressModel.return_all(data['supplier_id'])
        supplier_contact_obj = SupplierContactModel.return_all(data['supplier_id'])
        consultants_obj = ThirdPartyConsultantModel.return_all_supplier_consultants(data['supplier_id'])
        logging.info("Success")
        return {"supplier_id": supplier_obj.supplier_id,
                "supplier_name": supplier_obj.supplier_name,
                "contact_number": supplier_obj.contact_number,
                "termination_reason": supplier_obj.termination_reason,
                "contract_status": 'Active' if supplier_obj.contract_status=='A' else 'Inactive' if supplier_obj.contract_status=='I' else 'Terminated' if supplier_obj.contract_status=='T' else '',
                "owner_name": supplier_obj.owner_name,
                "company_website_url": supplier_obj.company_website_url,
                "comments":supplier_obj.comments,
                'address': supplier_address_obj['supplier_address'],
                "supplier_contacts": supplier_contact_obj['supplier_contacts'],
                "supplier_Status_last_updated_date":str(supplier_obj.supplier_status_last_updtd_date),
                "consultants":consultants_obj['consultants']
                }

class SupplierDetailsEdit(Resource):
    @jwt_required
    @prefix_decorator('SupplierDetailsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record1 = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        dup_supp_name_rec = StaffingSupplierModel.query.filter_by(supplier_name=data['supplier_name']).all()
        for i in dup_supp_name_rec:
            
            if (str(data['supplier_id']) != str(i.supplier_id)):
                print(i.supplier_id,data['supplier_id'])
                return {"errorMessage":"Supplier Name Already Exists"}
        if not record1:
            return {'errorMessage': 'Supplier Does Not Exist'}
        if data['contract_status'] != 'Active':
            all_consultants = ThirdPartyConsultantModel.query.filter_by(supplier_id=data['supplier_id']).all()
            if all_consultants:
                for i in all_consultants:
                    if i.consultant_status == 'A':
                        return {"errorMessage":"Failed, Active Consultants Exists Under This Supplier"}
        data['contract_status'] = 'A' if data['contract_status']=='Active' else 'I' if data['contract_status']=='Inactive' else 'T' if data['contract_status']=='Terminated' else ''
        schema = StaffingSupplierSchema(unknown=EXCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        if record1.contract_status != result['contract_status']:
            last_update_date = date_obj
        else:
            last_update_date = record1.supplier_status_last_updtd_date
        record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
        # change the values you want to update
        record.supplier_name = result['supplier_name']
        record.contact_number = result['contact_number']
        record.owner_name = result['owner_name']
        record.company_website_url = result['company_website_url']
        record.contract_status = result['contract_status']
        record.modified_time = date_obj
        record.supplier_status_last_updtd_date = last_update_date
        record.termination_reason = result['termination_reason']
        # commit changes
        record.save_to_db()
        if data['contract_status'] == 'A':
            logging.debug("Details Updated Sucessfully")
            return {"message": "Details Updated Sucessfully"}
        elif data['contract_status'] == 'I':
            logging.debug("Supplier Inactivated Sucessfully")
            return {"message":"Supplier Inactivated Sucessfully"}
        else:
            logging.debug("Supplier Terminated Sucessfully")
            return {"message":"Supplier Terminated Sucessfully"}

class SupplierAddressEdit(Resource):
    @jwt_required
    @prefix_decorator('SupplierAddressEdit')
    def post(self):
        data = request.get_json(force=True) 
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        record4 = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if not record4:
            logging.debug('Supplier Does Not Exist')
            return {'errorMessage': 'Supplier Does Not Exist'}
        schema = AddressSchema(unknown=EXCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        if data['address_id']:
            record = Address1Model.query.filter_by(address_id=data['address_id']).first()
            if record:
                # change the values you want to update
                record.address_line1 = result['addressLine1']
                record.address_line2 = result['addressLine2']
                record.city = result['city']
                record.state_name = result['state']
                record.country = result['country']
                record.zip_code = result['zip']
                # commit changes
                record.save_to_db()
                record1 = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
                record1.modified_time = date_obj
                record1.save_to_db()
                record2 = SupplierAddressModel.query.filter_by(address_id=data['address_id']).first()
                record2.modified_time = date_obj
                record2.modified_by = userid.employee_id
                record2.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
            else:
                a = Address1Model(
                    address_line1=result['addressLine1'],
                    address_line2=result['addressLine2'],
                    city=result['city'],
                    state_name=result['state'],
                    country=result['country'],
                    zip_code=result['zip'])
                a.save_to_db()
                b = SupplierAddressModel(
                    supplier_id=data['supplier_id'],
                    address_id=a.address_id,
                    status='A',
                    created_by=userid.employee_id,
                    created_time=date_obj,
                    modified_time=date_obj)
                b.save_to_db()
                record4.modified_time = date_obj
                record4.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
        else:
            a = Address1Model(
                address_line1=result['addressLine1'],
                address_line2=result['addressLine2'],
                city=result['city'],
                state_name=result['state'],
                country=result['country'],
                zip_code=result['zip'])
            a.save_to_db()
            b = SupplierAddressModel(
                supplier_id=data['supplier_id'],
                address_id=a.address_id,
                status='A',
                created_by=userid.employee_id,
                created_time=date_obj,
                modified_time=date_obj)
            b.save_to_db()
            record4.modified_time = date_obj
            record4.save_to_db()
            logging.debug("Details Updated Sucessfully")
            return {"message": "Details Updated Sucessfully"}

class SupplierContactEdit(Resource):
    @jwt_required
    @prefix_decorator('')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        all_similar_phone = SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_phone=data['contact_phone'],contact_status='A').all() 
        all_similar_email = SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_email=data['contact_email'].lower(),contact_status='A').all()
        if all_similar_phone:
            for i in all_similar_phone:
                if i.contact_id != data['contact_id']:
                    logging.debug("Already A Contact Exists With Same Contact Phone Number")
                    return {"errorMessage":"Already A Contact Exists With Same Contact Phone Number"}
        if all_similar_email:
            for i in all_similar_email:
                if i.contact_id != data['contact_id']:
                    logging.debug("Already A Contact Exists With Same Email")
                    return {"errorMessage":"Already A Contact Exists With Same Email"}
        data['contact_type'] = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first()
        if data['contact_type']:
            data['contact_type'] = data['contact_type'].supplier_contact_type_id
        else:
            logging.debug('Invalid Contact Type')
            return {'errorMessage': 'Invalid Contact Type'}
        schema = SupplierContactSchema(unknown=INCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        record = SupplierContactModel.query.filter_by(contact_id=data['contact_id']).first()
        if record:
            # change the values you want to update
            record.contact_first_name = result['contact_first_name']
            record.contact_last_name = result['contact_last_name']
            record.supplier_contact_type_id = result['contact_type']
            record.contact_phone = result['contact_phone']
            record.contact_email = result['contact_email'].lower()
            record.contact_title = result['contact_title']
            # commit changes
            record.save_to_db()
            record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
            record.modified_time = date_obj
            record.save_to_db()
            logging.debug("Details Updated Successfully")
            return {"message": "Details Updated Sucessfully"}
        else:
            a = SupplierContactModel(
                supplier_id=result['supplier_id'],
                contact_first_name=result['contact_first_name'],
                contact_last_name=result['contact_last_name'],
                supplier_contact_type_id=result['contact_type'],
                contact_phone=result['contact_phone'],
                contact_email=result['contact_email'].lower(),
                contact_title=result['contact_title'])
            a.save_to_db()
            record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
            record.modified_time = date_obj
            record.save_to_db()
            logging.debug("Details Updated Successfully")
            return {"message": "Details Updated Successfully"}

class ValidateConsultantNumber(Resource):
    @jwt_required
    @prefix_decorator('ValidateConsultantNumber')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if str(data['consultant_number']).lower().strip() not in [i.consultant_number.lower().strip() for i in ThirdPartyConsultantModel.query.all()]:
            logging.info("Success")
            return {'message':'Success'}
        else:
            logging.debug('Consultant With Same Number Already Exists')
            return {'errorMessage':'Consultant With Same Number Already Exists'}

class AddThirdPartyConsultant(Resource):
    @jwt_required
    @prefix_decorator('AddThirdPartyConsultant')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        if ThirdPartyConsultantModel.query.filter_by(consultant_number=data['consultant_number']).first():
            logging.debug("Consultant With Same Consultant Number Already Exists")
            return{"errorMessage":"Consultant With Same Consultant Number Already Exists"}
        conobj = ConsultantIdModel.query.filter_by(id=1).first()
        conid = conobj.consultant_id
        conidgen = 'CON' + str(conid)
        conobj.consultant_id = conid + 1
        conobj.save_to_db()
        try:
            try:
                data['emergency_relation_id'] = EmergencyRelationModel.query.filter_by(emergency_relation_type=data['emergency_relation_id']).first().emergency_relation_id
                data['gender'] = 'M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O' if data['gender']=='Other' else ''
                data['status'] = 'A' if data['status']=='Active' else 'I' if data['status']=='Inactive' else 'T' if data['status']=='Terminated' else ''
            except Exception as e:
                logging.exception('Exception Occured , {0}'.format(str(e)))
                return {'errorMessage':'Please Provide Valid Data'}
            schema1 = ConsultantSchema(unknown=INCLUDE)
            consultant = schema1.load(data)
            result = schema1.dump(consultant)
            schema2 = ConsultantContactSchema(unknown=EXCLUDE)
            contact = schema2.load(data)
            result1 = schema2.dump(contact)
            tparty = ThirdPartyConsultantModel(
                consultant_id=conidgen,
                supplier_id=result['supplier_name'], #supplier_id
                consultant_number=data['consultant_number'],
                first_name=result['first_name'],
                middle_name=result['middle_name'],  
                last_name=result['last_name'],
                date_of_birth=base64.b64encode(result['date_of_birth'].encode('ascii')).decode('ascii'),
                personal_cell_phone=result['mobile'],  #personal_cell_phone
                personal_email_id=result['email'].lower(),   #personal_email_id
                consultant_status = data['status'],
                gender=data['gender'],
                created_time=date_obj,
                modified_time=date_obj,
                consultant_status_last_updtd_date=date_obj,
            )
            tparty.save_to_db()
            tpartyid = tparty.consultant_id
            conscon = ConsultantContactModel(
                consultant_id=tpartyid,
                emergency_first_name=result1['emergency_first_name'],
                emergency_middle_name=result1['emergency_middle_name'],
                emergency_last_name=result1['emergency_last_name'],
                emergency_phone_number=result1['emergency_mobile'],
                emergency_alternate_phone_number=result1['emergency_alternate_mobile'], 
                emergency_relation_id=result1['emergency_relation_id'], #emergency_relation_type
                emergency_email=result1['emergency_email'].lower()
            )
            conscon.save_to_db()
            return {
                "message": "Consultant details added successfully",
                "consultant_id": tparty.consultant_id
            }
        except ValidationError as err:  
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500

class ConsultantView(Resource):
    @jwt_required
    @prefix_decorator('ConsultantView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        cons = ThirdPartyConsultantModel.query.filter_by(consultant_id=data['consultant_id']).first()
        conscon = ConsultantContactModel.query.filter_by(consultant_id=data['consultant_id']).first()
        proemp = ProjectEmployeeModel.return_all_empprojects(data['consultant_id'])
        if not cons:
            logging.debug("Consultant Does Not Exist")
            return {"errorMessage":"Consultant Does Not Exist"}
        if conscon:
            emergency_first_name = conscon.emergency_first_name
            emergency_middle_name = conscon.emergency_middle_name
            emergency_last_name = conscon.emergency_last_name
            emergency_phone_number = conscon.emergency_phone_number
            emergency_alternate_phone_number = conscon.emergency_alternate_phone_number
            emergency_relation_type = EmergencyRelationModel.query.filter_by(emergency_relation_id=conscon.emergency_relation_id).first().emergency_relation_type
            emergency_relation_id = conscon.emergency_relation_id
            emergency_email = conscon.emergency_email.lower()
            emergency_contact_id = conscon.emergency_contact_id
        else:
            emergency_first_name = ''
            emergency_middle_name = ''
            emergency_last_name = ''
            emergency_phone_number = ''
            emergency_alternate_phone_number = ''
            emergency_relation_type = ''
            emergency_relation_id = ''
            emergency_email = ''
            emergency_contact_id = ''
        return {'consultant_id': cons.consultant_id,
                'supplier_id': cons.supplier_id,
                'supplier_name': StaffingSupplierModel.query.filter_by(supplier_id=cons.supplier_id).first().supplier_name,
                'consultant_number': cons.consultant_number,
                'first_name': cons.first_name,
                'middle_name': cons.middle_name,
                'last_name': cons.last_name,
                'date_of_birth': cons.date_of_birth,
                'consultant_status_last_updated_date': str(cons.consultant_status_last_updtd_date),
                'personal_cell_phone': cons.personal_cell_phone,
                'personal_email_id': cons.personal_email_id.lower(),
                'gender': 'Male' if cons.gender=='M' else 'Female' if cons.gender=='F' else 'Other' if cons.gender=='O' else '',
                'consultant_status': 'Active' if cons.consultant_status=='A' else 'Inactive' if cons.consultant_status=='I' else 'Terminated' if cons.consultant_status=='T' else '',
                'rehire_eligibility': 'Yes' if cons.rehire_eligibility=='Y' else 'No' if cons.rehire_eligibility=='N' else '',
                'termination_reason': cons.termination_reason,
                'project_details': proemp['employee_projects'] if proemp else [],
                'emergency_first_name': emergency_first_name,
                'emergency_middle_name': emergency_middle_name,
                'emergency_last_name': emergency_last_name,
                'emergency_phone_number': emergency_phone_number,
                'emergency_alternate_phone_number': emergency_alternate_phone_number,
                'emergency_relation_type': emergency_relation_type,
                'emergency_relation_id': emergency_relation_id,
                'emergency_email': emergency_email.lower(),
                'emergency_contact_id' :emergency_contact_id
                }            

class ConsultantEdit(Resource):
    @jwt_required
    @prefix_decorator('ConsultantEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        try:
            data['supplier_name']=StaffingSupplierModel.query.filter_by(supplier_name=data['supplier_name']).first().supplier_id
            data['gender'] = 'M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O'  if data['gender']=='Other' else ''
        except Exception as e:
            logging.exception('Exception Occured , {0}'.format(str(e)))
            return {'errorMessage':str(e)}
        try:
            schema = ConsultantSchema()
            consultant = schema.load(data)
            result = schema.dump(consultant)
            record = ThirdPartyConsultantModel.query.filter_by(consultant_id=result['consultant_id']).first()
            # change the values you want to update
            record.personal_email_id = result['email'].lower()
            record.first_name = result['first_name']
            record.gender = result['gender']
            record.last_name = result['last_name']
            record.middle_name = result['middle_name']
            record.personal_cell_phone = result['mobile']
            record.supplier_id = result['supplier_name']
            record.modified_time = datetime.now()
            # commit changes
            record.save_to_db()
            logging.debug("Consultant Details Updated Sucessfully")
            return {"message": "Consultant Details Updated Sucessfully"}
        except ValidationError as err: 
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500

class ConsultantSchema(Schema):
    consultant_id = fields.String(validate=validate.Length(max=10),data_key="consultant_id",required=False)
    supplier_id = fields.Integer(data_key="supplier_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    middle_name = fields.String(validate=validate.Length(max=75),data_key="middle_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    last_name = fields.String(validate=validate.Length(max=75),data_key="last_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    first_name = fields.String(validate=validate.Length(max=75),data_key="first_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    personal_email_id = fields.Email(data_key="email",required=False,error_messages={"required": "Please Provide A Valid Email Id."})
    gender = fields.String(validate=validate.Length(max=1),data_key="gender",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    personal_cell_phone = fields.String(validate=validate.Length(max=20),data_key="mobile",required=False,error_messages={"required": "Please Provide A Valid Phone Number."})
    consultant_status = fields.String(validate=validate.Length(max=1),data_key="status",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    rehire_eligibility = fields.String(validate=validate.Length(max=1),data_key="rehire_eligibility",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    termination_reason = fields.String(validate=validate.Length(max=255),data_key="termination_reason",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    date_of_birth = fields.String(allow_none=True,validate=validate.Length(max=20),data_key="date_of_birth",required=False,error_messages={"required": "Please Provide A Valid First Name."})

    @post_load
    def save_employee(self, data, **kwargs):
        return data



class ConsultantContactSchema(Schema):
    consultant_id = fields.String(validate=validate.Length(max=10),data_key="consultant_id",required=False)
    emergency_contact_id = fields.Integer(data_key="supplier_name",required=False)
    emergency_first_name = fields.String(validate=validate.Length(max=75),data_key="emergency_first_name",required=True)
    emergency_last_name = fields.String(validate=validate.Length(max=75),data_key="emergency_last_name",required=True)
    emergency_middle_name = fields.String(validate=validate.Length(max=75),data_key="emergency_middle_name",required=False)
    emergency_email = fields.Email(data_key="emergency_email",required=True)
    emergency_phone_number = fields.String(validate=validate.Length(max=20),data_key="emergency_mobile",required=True)
    emergency_alternate_phone_number = fields.String(validate=validate.Length(max=20),data_key="emergency_alternate_mobile",required=False)
    emergency_relation_id = fields.Integer(data_key="emergency_relation_id",required=True)

    @post_load
    def save_employee(self, data, **kwargs):
        return data


class ConsultantEmerEdit(Resource):
    @jwt_required
    @prefix_decorator('ConsultantEmerEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        data['emergency_relation_id'] = EmergencyRelationModel.query.filter_by(emergency_relation_type=data['emergency_relation_id']).first().emergency_relation_id
        if not data['emergency_relation_id']:
            logging.debug('Please Provide Valid Emergency Relation Type')
            return {'errorMessage':'Please Provide Valid Emergency Relation Type'}
        try:
            schema = ConsultantContactSchema()
            contact = schema.load(data)
            result = schema.dump(contact)
            record = ConsultantContactModel.query.filter_by(consultant_id=result['consultant_id']).first()
            record1 = ThirdPartyConsultantModel.query.filter_by(consultant_id=result['consultant_id']).first()
            if record:
                # change the values you want to update
                record.emergency_email = result['emergency_email'].lower()
                record.emergency_first_name = result['emergency_first_name']
                record.emergency_last_name = result['emergency_last_name']
                record.emergency_phone_number = result['emergency_mobile']
                record.emergency_relation_id = result['emergency_relation_id']
                record.emergency_alternate_phone_number = result['emergency_alternate_mobile']
                record.save_to_db()
                record1.modified_time = date_obj
                record1.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
            else:
                new_record = ConsultantContactModel(consultant_id=result['consultant_id'],
                                                 emergency_first_name=result['emergency_first_name'],
                                                 emergency_last_name=result['emergency_last_name'],
                                                 emergency_phone_number=result['emergency_mobile'],
                                                 emergency_alternate_phone_number=result['emergency_alternate_mobile'],
                                                 emergency_relation_id=result['emergency_relation'],
                                                 emergency_email=result['emergency_email'].lower())   
                new_record.save_to_db()
                record1.modified_time = date_obj
                record1.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
        except ValidationError as err: 
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500


class EmployeeProjects(Resource):
    @jwt_required
    @prefix_decorator('EmployeeProjects')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        emp = Employee1Model.query.filter_by(email_id=data['emailId'].lower()).first()
        if not emp:
            logging.debug("Employee Does Not Exists")
            return {'errorMessage':"Employee Does Not Exists"}
        return ProjectEmployeeModel.return_all_employeeprojects(emp.employee_id)

class ViewInternalTimesheet(Resource):
    @prefix_decorator('ViewInternalTimesheet')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not data['emailId']:
            return {'errorMessage':"Please Provide Valid Email Id"}
        
        emp = Employee1Model.query.filter_by(email_id=data['emailId'].lower()).first()
        if not emp:
            logging.debug("Employee Does Not Exists")
            return {'errorMessage':"Employee Does Not Exists"}
        try:
            datetime.strptime(data['startDate'].split('T')[0],"%m-%d-%Y")
            datetime.strptime(data['endDate'].split('T')[0],"%m-%d-%Y")
            return InternalTimesheetModel.return_time_sheets(emp.employee_id,data['startDate'],data['endDate'])
        except:  
            logging.exception('Exception Occured')
            return {'errorMessage':"Please Provide Valid Start Date/End Date"}

class InternalProjectTimeSheetFilter(Resource):
    @jwt_required
    @prefix_decorator('InternalProjectTimeSheetFilter')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity() 
        emp_id = Employee1Model.query.filter_by(email_id=username).first().employee_id   
        pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id).first()
        repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id).first()

        if data['date'] == '' and (data['project_id'] == '') and (data['emp_num'] ==''):
            if pro_manager and repo_manager:
                print('entered repo and pro manager')
                projects = [i.project_id for i in ProjectModel.query.filter_by(project_manager_id=emp_id).all()]
                list_pro_ts = [InternalTimesheetModel.return_all_by_pro(i,emp_id) for i in projects]
                list_pro_ts = list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_pro_ts]))))
                reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id).all()]))
                list_rep_ts = InternalTimesheetModel.return_all_reporting_manager(reporting_employees,emp_id) 

                dictts = {}
                
                for d in (list_pro_ts+list_rep_ts['timesheets']):
                    try:
                        dictts[d['timeSheetId']] = d
                    except:
                        dictts[d['timesheetId']] = d
                return {"timesheets": list(dictts.values())}

            elif pro_manager:
                print('entered pro manager')
                pid = ProjectModel.query.filter_by(project_manager_id=emp_id).all()
                list_ts = [InternalTimesheetModel.return_all_by_pro(i.project_id,emp_id) for i in pid]
                ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
                return ts
            elif repo_manager: 
                print('entered repo manager')
                employees = [i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id).all()]
                print(employees)
                return InternalTimesheetModel.return_all_reporting_manager(employees,emp_id) 
            else:
                return {"timesheets":[]}
        elif data['date'] and data['project_id'] and data['emp_num']:
            list_ts = [InternalTimesheetModel.defall(data['project_id'],emp_id,data['date'],data['emp_num'])]
            ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
            return ts
        elif data['project_id'] and data['emp_num'] and not data['date']:
            list_ts = [InternalTimesheetModel.defall1(data['project_id'],emp_id,data['emp_num'])]
            ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
            return ts
        elif  data['date'] and not data['project_id'] and data['emp_num'] :
            list_ts = [InternalTimesheetModel.defall2(emp_id,data['date'],data['emp_num'])]
            ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
            return ts
        elif  data['date'] and data['project_id'] and not data['emp_num']:
            print('poiuyhnbvcd')
            list_ts = [InternalTimesheetModel.defall3(emp_id,data['date'],data['project_id'])]
            ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
            return ts
        elif data['project_id'] and not data['date'] and not data['emp_num']:
            print('zxdgfchvjbkiugyth')
            list_ts = [InternalTimesheetModel.defall4(emp_id,data['project_id'])]
            ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
            return ts
        elif data['emp_num'] and not data['date'] and not data['project_id']:
            print('dgfchvjbkiugyth')
            list_ts = [InternalTimesheetModel.defall5(emp_id,data['emp_num'])]
            ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
            return ts
        elif data['date']:
            try:
                datetime.strptime(data['date'][0].split('T')[0],"%m-%d-%Y")
                datetime.strptime(data['date'][1].split('T')[0],"%m-%d-%Y")
            except:
                logging.debug('Please Provide Valid Date')
                return{'errorMessage':'Please Provide Valid Date'}


            if pro_manager and repo_manager:
                projects = [i.project_id for i in ProjectModel.query.filter_by(project_manager_id=emp_id).all()]
                list_pro_ts = [InternalTimesheetModel.defo(i,emp_id,data['date']) for i in projects]
                list_pro_ts = list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_pro_ts]))))

                reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id).all()]))
                list_rep_ts = InternalTimesheetModel.abc(reporting_employees,emp_id,data['date'])
                dictts = {}
                
                for d in (list_pro_ts+list_rep_ts['timesheets']):
                    try:
                        dictts[d['timeSheetId']] = d
                    except:
                        dictts[d['timesheetId']] = d
                return {"timesheets": list(dictts.values())}
            elif repo_manager:
                employees = [i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id).all()]
                return InternalTimesheetModel.abc(employees,emp_id,data['date'])

            elif pro_manager: 
                pid = ProjectModel.query.filter_by(project_manager_id=emp_id).all()
                list_ts = [InternalTimesheetModel.defo(i.project_id,emp_id,data['date']) for i in pid]
                return {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) } 
            else:
                return {"timesheets":[]}
               

   

class HrItsDeptEmpFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsDeptEmpFilter')
    def post(self):
        data = request.get_json(force=True)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        print(data)
        employees = Employee1Model.query.filter_by(department_id=data['department_id']).all()
        employee_list = [str(i.employee_id) for i in employees]
        return Employee1Model.return_all_emps(employee_list,userid.employee_id)

class SaveInternalTImesheet(Resource):
    @prefix_decorator('SaveInternalTImesheet')
    def post(self):
        data = request.get_json(force=True) 
        print(data)  
        date_obj = datetime.now()
        for item in data:
            if item['tsStartTime'] == '00:00 am':
                hours = 0.00 
            else:
                hours = round(((datetime.strptime(item['tsEndTime'], '%I:%M %p')-datetime.strptime(item['tsStartTime'], '%I:%M %p')).total_seconds()/60.0)/60.0,2)
            emp_id = Employee1Model.query.filter_by(email_id=item['emailId'].lower()).first()
            if item['timeSheetId'] != 0:
                timesheet_obj = InternalTimesheetModel.query.filter_by(timesheet_id=item['timeSheetId']).first()
                timesheet_obj.timesheet_type_id = InternalTimesheetTypesModel.query.filter_by(timesheet_type=item['tsType']).first().timesheet_type_id
                timesheet_obj.ts_day = datetime.strptime(item['tsDate'], '%m-%d-%Y').strftime('%A')
                timesheet_obj.ts_date = item['tsDate']
                timesheet_obj.ts_start_time = item['tsStartTime']
                timesheet_obj.ts_end_time = item['tsEndTime']
                timesheet_obj.total_hours = hours
                timesheet_obj.project_id = item['projectId']
                timesheet_obj.remarks = item['remarks']
                timesheet_obj.last_updated_time_stamp = date_obj
                timesheet_obj.save_to_db()
            else:
                if item['projectId']:
                    project_id = item['projectId']
                else:
                    project_id = None   
                timesheet_obj = InternalTimesheetModel(
                employee_number = emp_id.employee_id,
                timesheet_type_id = InternalTimesheetTypesModel.query.filter_by(timesheet_type=item['tsType']).first().timesheet_type_id,
                ts_day = datetime.strptime(item['tsDate'], '%m-%d-%Y').strftime('%A'),
                ts_date = item['tsDate'],
                ts_start_time = item['tsStartTime'],
                ts_end_time = item['tsEndTime'],
                total_hours = hours,
                project_id = project_id,
                remarks = item['remarks'],    
                status = 'S',
                created_time_stamp = date_obj,
                department_id=emp_id.department_id
                )
                timesheet_obj.save_to_db()
        logging.info("TimeSheets Saved Successfully")
        return {"message": "TimeSheets Saved Successfully"}

class InternalTimeSheetApproval(Resource):
    @jwt_required
    @prefix_decorator('InternalTimeSheetApproval')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        user_id = Employee1Model.query.filter_by(email_id=username).first().employee_id
        for i in data:
            if not i['timesheetId'] or i['timesheetId'] == 'undefined':
                return {"errorMessage": "Please Provide Valid Timesheet"}
            record = InternalTimesheetModel.query.filter_by(timesheet_id=i['timesheetId']).first()
            if record:
                emp = Employee1Model.query.filter_by(employee_id=record.employee_number).first()
                if not emp:
                    logging.debug('Employee for provided TimeSheet Does Not Exists')
                    return {'errorMessage':'Employee for provided TimeSheet Does Not Exists'}
        for item in data:
            record = InternalTimesheetModel.query.filter_by(timesheet_id=item['timesheetId']).first()
            record.status = 'A' if item["timesheetApprovalStatus"]=='Approved' else 'R' if item["timesheetApprovalStatus"]=='Rejected' else ''
            record.approver_comments = item["comments"]
            record.approver_id = user_id
            record.approved_time_stamp = datetime.now()
            record.save_to_db()
            emp = Employee1Model.query.filter_by(employee_id=record.employee_number).first()
            emp_email = emp.email_id
            emp_name = emp.first_name + ' ' + emp.last_name
            project = ProjectModel.query.filter_by(project_id=record.project_id).first()
            project_name = project.project_name if project else ''
            day = str(record.ts_date)
            if item["timesheetApprovalStatus"] != "Approved":
                msg = Message('Timesheet Status Notification', sender='support@prutech.com', recipients=[emp_email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Your timesheet for project <b>{1}</b> on <b>{2}</b>
                <br> has been <b>{3}</b> due to {4} </br>
                <br> <b>Thanks,
                <br> PRUTECH Team.
                </b>""".format(emp_name, project_name, day, 'Rejected', item["comments"])
                mail.send(msg)
        return {"message": item["timesheetApprovalStatus"]}


class DeleteTimesheets(Resource):
    @prefix_decorator('DeleteTimesheets')
    def delete(self): 
        data = request.get_json(force=True)
        print(data)
        time_sheet = InternalTimesheetModel.query.filter_by(timesheet_id=data['timesheetId']).first()
        if time_sheet:
            time_sheet.delete_from_db()
            return {"message":"Timesheet Deleted Successfully"}
        else: 
            logging.exception('Please Provide Valid Timesheet Id')
            return {"errorMessage":"Please Provide Valid Timesheet Id"}

class ActivateEmployee(Resource):
    @jwt_required
    @prefix_decorator('ActivateEmployee')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = Employee1Model.query.filter_by(employee_id=data['employee_id']).first()
        if record:    
            record.employee_status = 'A'
            record.inactive_reason_id = None
            record.employee_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Employee Activated Successfully")
            return {"Message": "Employee Activated Successfully"}
        else:
            logging.debug('Employee Does Not Exist')
            return {'errorMessage': 'Employee Does Not Exist'}

class ActivateClient(Resource):
    @jwt_required
    @prefix_decorator('ActivateClient')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = ClientDetailsModel.query.filter_by(client_id=data['client_id']).first()
        if record:
            record.client_status = 'A'
            record.client_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Client Activated Successfully")
            return {"Message": "Client Activated Successfully"}
        else:
            logging.debug('Client Does Not Exist')
            return {'errorMessage': 'Client Does Not Exist'}

class ActivateProject(Resource):
    @jwt_required
    @prefix_decorator('ActivateProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
        if record:    
            record.project_status = 'A'
            record.project_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Project Activated Successfully")
            return {"Message": "Project Activated Successfully"}
        else:
            logging.debug('Project Does Not Exist')
            return {'errorMessage': 'Project Does Not Exist'}

class ActivateSupplier(Resource):
    @jwt_required
    @prefix_decorator('ActivateSupplier')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if record:    
            record.contract_status = 'A'
            record.modified_time = date_obj
            record.supplier_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info("Supplier Activated Successfully")
            return {"Message": "Supplier Activated Successfully"}
        else:
            logging.debug('Supplier Does Not Exist')
            return {'errorMessage': 'Supplier Does Not Exist'}

class ActivateConsultant(Resource):
    @jwt_required
    @prefix_decorator('ActivateConsultant')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = ThirdPartyConsultantModel.query.filter_by(consultant_id=data['consultant_id']).first()
        if record:
            record.consultant_status = 'A'
            record.consultant_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Consultant Activated Successfully")
            return {"Message": "Consultant Activated Successfully"}
        else:
            logging.debug("Consultant Does not Exists")
            return {"errorMessage":"Consultant Does not Exists"}     


class DesignationValidation(Resource):
    @prefix_decorator('DesignationValidation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        dept = DepartmentModel.query.filter_by(department_name=data['employee_department']).first()
        desg = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
        email = data['email'].lower()
        if not dept:
            logging.debug('Please Select Valid Department')
            return {'errorMessage': 'Please Select Valid Department'}
        if not desg:
            logging.debug('Please Select Valid Designation')
            return {'errorMessage': 'Please Select Valid Designation'}
        if Employee1Model.query.filter_by(emp_number=data['employeeNumber']).first():
            logging.debug("Employee number already exists")
            return {"message": "Employee number already exists"}
        elif Employee1Model.query.filter_by(employee_status="Active", email_id=email).first():
            logging.debug("An active user already exists with same email")
            return {"message": "An active user already exists with same email"}
        elif EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=data['employeeNumber'],employee_email=email).first():
            logging.debug("Employee with this Employee Number/Email Id already exists")
            return {"message": "Employee with this Employee Number/Email Id already exists"}
        elif (EmployeeInviteStatus.query.filter_by(employee_number=data['employeeNumber']).first()):
            logging.debug("Employee with this Employee Number already exists")
            return {"message": "Employee with this Employee Number already exists"}
        elif (EmployeeInviteStatus.query.filter_by(employee_email=email).first()):
            if Employee1Model.query.filter_by(email_id=email).first():
                logging.debug("Employee with this email Id already exists")
                return {"message": "Employee already exists"}
            else:
                record = EmployeeInviteStatus.query.filter_by(employee_email=email).first()
                record.employee_number=data['employeeNumber']
                record.designation_id=desg.designation_id
                record.department_id=dept.department_id
                record.invitation_status="Active"
                record.employee_role=data['role']
                record.sent_time=date_obj.strftime('%m-%d-%Y')
                record.save_to_db()
                logging.info("Successfully saved")
                return {"message":"Successfully saved"}
        else:
            invite_record = EmployeeInviteStatus(employee_number=data['employeeNumber'],employee_email=email,designation_id=desg.designation_id,department_id=dept.department_id ,invitation_status="Active",employee_role=data['role'],sent_time=date_obj.strftime('%m-%d-%Y'))
            invite_record.save_to_db()
            logging.info("Successfully saved")
            return {"message":"Successfully saved"}


class SuppAddInactivation(Resource):
    @jwt_required
    @prefix_decorator('SuppAddInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = SupplierAddressModel.query.filter_by(address_id=data['supplier_address_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Supplier Address Deleted Successfully")
            return {"message":"Supplier Address Deleted Successfully"}
        else:
            logging.debug('Supplier Address Does Not Exist')
            return {'errorMessage': 'Supplier Address Does Not Exist'}

class ClientContactInactivation(Resource):
    @jwt_required
    @prefix_decorator('ClientContactInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = ClientContactModel.query.filter_by(contact_id=data['contact_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Client Contact Deleted Successfully")
            return {"message":"Client Contact Deleted Successfully"}
        else:
            logging.debug('Client Contact Does Not Exist')
            return {'errorMessage': 'Client Contact Does Not Exist'}


class EmployeeListFilter(Resource):
    @jwt_required
    @prefix_decorator('EmployeeListFilter')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['date'] != '':  
            datetime.strptime(data['date'][0],"%m-%d-%Y")
            datetime.strptime(data['date'][1],"%m-%d-%Y")
            min_date = data['date'][0].split('T')[0]
            max_date = data['date'][1].split('T')[0]  
        else:
            min_date = '01-01-1977'
            max_date = datetime.now().strftime('%m-%d-%Y')
        return Employee1Model.return_all_for_date_filter(start_date=min_date ,end_date=max_date)

class ModifyPrimaryProject(Resource):
    @jwt_required
    @prefix_decorator('ModifyPrimaryProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity() 
        date_obj = datetime.now()
        user_id = Employee1Model.query.filter_by(email_id=username).first().employee_id
        if user_id:
            record = EmployeePrimaryProjectModel.query.filter_by(employee_id=data['employee_id']).first()
            if record:
                record.project_id = data['project_id']
                record.modified_by = user_id
                record.modiified_time = date_obj
                record.save_to_db()
                logging.info("Primary Project Updated Successfully")
                return {"message":"Primary Project Updated Successfully"}
            else:
                primepro = EmployeePrimaryProjectModel(
                    employee_id = data['employee_id'],
                    project_id = data['project_id'],
                    created_by = user_id,
                    created_time = date_obj
                    )
                primepro.save_to_db()
                logging.info("Primary Project Added Successfully")
                return {"message":"Primary Project Added Successfully"}
        else:
            logging.debug("User Does Not Exist")
            return{"errorMessage":"User Does Not Exist"}

class AttorneySchema(Schema):
    attorney_id = fields.Integer(data_key="attorney_id",required=False)
    attorney_name = fields.String(validate=validate.Length(max=255),data_key="attorney_name",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

class LoginInvitation(Resource):
    @jwt_required
    @prefix_decorator('LoginInvitation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        record = UserModel.query.filter_by(username=email).first()    
        if record:
            emp = Employee1Model.query.filter_by(employee_id=record.userid).first()
            if emp:                    
                msg = Message('Prutech Login Credentials', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br>Welcome to PruTech Solutions. Your Login ID & the URL are given below.</br>
                <br>UserId: {1}</br>
                <p>To Login you need a password and a temporary password will be sent to you shortly in a separate email. </p>   
                <br> Thank You,
                <br> Prutech Solutions Inc.
                """.format(emp.first_name+' '+emp.last_name,email)
                mail.send(msg)
                msg = Message('Temporary Password', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br>Please find the below temporary password. Kindly reset your password to access your Prutech Solutions account.</br>
                <br>Temporary Password: {1}
                <p><a href={2}>Click Here</a> to Login </p><br>
                <br>Note: Your account can be accessed only after your password is reset.</br>
                <br> Thank You,
                <br> Prutech Solutions Inc.
                """.format(emp.first_name+' '+emp.last_name,data['password'],data['base_url'])
                time.sleep(10)
                mail.send(msg)
                logging.info( "Login Details Sent Successfully")
                return {"message": "Login Details Sent Successfully"}
            else:
                logging.debug('Employee Does Not Exist')
                return {'errorMessage': 'Employee Does Not Exist'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage': 'User Does Not Exist'}

class AddRoleWithPrivileges(Resource):
    @jwt_required
    @prefix_decorator('AddRoleWithPrivileges')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        for i in RoleModel.query.all():
            if i.role_name.lower()==data['role_name'].lower():
                logging.debug('Role {} exists'.format(data['role_name']))
                return {'errorMessage': 'Role {} exists'.format(data['role_name'])}
        new_role = RoleModel(role_name=data['role_name'])
        new_role.save_to_db()
        privileges = [PrivilegeModel.query.filter_by(privilege_name=i).first().id for i in data['role_privileges']]
        role_privileges = RolePrivilegeModel(
            role_id=new_role.id,
            role_privileges=privileges,
        )
        role_privileges.save_to_db()
        logging.info('Role Added Successfully')
        return {'message': 'Role Added Successfully'}

class LoginEmail(Resource):
    @prefix_decorator('LoginEmail')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        record = UserModel.query.filter_by(username=email).first()
        if record:
            emp = Employee1Model.query.filter_by(employee_id=record.userid).first()
            if emp:
                msg = Message('Prutech Login link', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Greetings from PRUTECH Team!</br>
                <br> Your Registration is successful</br>
                <p> You can login to PRUTECH HR Portal with following link</p>
                <p>  <a href={1}>Login Here</a></p>   
                <br> Thank You,
                <br> Prutech Solutions Inc.
                """.format(emp.first_name.title()+" "+emp.last_name.title(),data['base_url'])
                mail.send(msg)
                logging.info("Mail Sent Successfully")
                return {"message": "Mail Sent Successfully"}
            else:
                logging.debug('Employee Does Not Exist')
                return {'errorMessage': 'Employee Does Not Exist'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage': 'User Does Not Exist'}
        
class ValidateRole(Resource):
    @jwt_required
    @prefix_decorator('ValidateRole')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        for i in RoleModel.query.all():
            if i.role_name.lower()==data['role_name'].lower():
                return {'errorMessage': 'Role {} exists'.format(data['role_name'])}
        logging.info("Success")
        return {'message':'Success'}

class SupplierContactInactive(Resource):
    @jwt_required
    @prefix_decorator('SupplierContactInactive')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()       
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if userid:
            sup = SupplierContactModel.query.filter_by(contact_id=data['contact_id']).first()
            record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
            if sup and record:
                sup.contact_status = 'I'
                sup.save_to_db()
                record.modified_time = datetime.now()
                record.modified_by = userid.employee_id
                record.save_to_db()
                logging.info("Success")
                return {'message':'Contact Deleted Successfully'}
            else:
                logging.debug('Supplier Contact Does Not Exist')
                return {'errorMessage':'Supplier Contact Does Not Exist'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage':'User Does Not Exist'}

# to view employee timesheets

class HrItsProjectFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsProjectFilter')
    def get(self):
        return ProjectModel.return_all()

class HrItsCombinedFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsCombinedFilter')
    def post(self):
        data = request.get_json(force=True)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        print(data)
        if not ProjectModel.query.filter_by(project_id=data['project_id']).first():
            logging.debug("Project Does Not Exist")
            return {"errorMessage":"Project Does Not Exist"} 
        employees_list = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
        employees_list = [i for i in employees_list.employee_id if str(i)[0] != 'C']
        if employees_list:
            return Employee1Model.return_all_emps(employees_list,userid.employee_id)
        else:
            logging.debug("No Employees Assigned To The Project")
            return {'errorMessage':"No Employees Assigned To The Project"}

class MyTimeSheetProjectDropDown(Resource):
    @jwt_required    
    @prefix_decorator('MyTimeSheetProjectDropDown')
    def get(self):
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("Employee Does not Exist")
            return {"errorMessage":"Employee Does not Exist"}
        return ProjectEmployeeModel.all_employee_projects(userid.employee_id)          

class MyTimeSheets(Resource):
    @jwt_required    
    @prefix_decorator('MyTimeSheets')
    def get(self):
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("Employee Does not Exist")
            return {"errorMessage":"Employee Does not Exist"}
        return InternalTimesheetModel.all_employee_timesheets(userid.employee_id)           

class MyTimesheetsFilter(Resource):
    @jwt_required  
    @prefix_decorator('MyTimesheetsFilter')
    def post(self):
        data = request.get_json(force=True) 
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            return {"errorMessage":"User Does Not Exist"}
        if not data['date'] and not data['project_id']:
            return InternalTimesheetModel.all_employee_timesheets(userid.employee_id)
        if data['project_id'] and not data['date']:
            return InternalTimesheetModel.filtered_employee_timesheets(userid.employee_id,data['project_id'],data['date'])
        try:
            datetime.strptime(data['date'][0],"%m-%d-%Y")
            datetime.strptime(data['date'][1],"%m-%d-%Y")
        except: 
            logging.exception('Exception Occured')
            return {"errorMessage":"Invalid Date"}   

        return InternalTimesheetModel.filtered_employee_timesheets(userid.employee_id,data['project_id'],data['date'])

class AllTimeSheetProjectDropDown(Resource):
    @jwt_required  
    @prefix_decorator('AllTimeSheetProjectDropDown')  
    def get(self):
        return ProjectModel.return_all_active_projects()     


#to get all timesheets except theirs
class AllTimesheet(Resource):
    @jwt_required
    @prefix_decorator('AllTimesheet')
    def get(self):      
        username = get_jwt_identity()        
        user = UserModel.query.filter_by(username=username).first()
        if not user:
            return {"errorMessage":"User Does Not Exist"}
        return InternalTimesheetModel.return_all_time_sheets(user.userid)

class AllInternalTimeSheetFilter(Resource):
    @jwt_required
    @prefix_decorator('AllInternalTimeSheetFilter')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()        
        user = UserModel.query.filter_by(username=username).first()
        if not user:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        if not data['project_id'] and not data['dept_id'] and not data['date'] and not data['emp_num']:
            return InternalTimesheetModel.return_all_time_sheets(user.userid)
        return InternalTimesheetModel.return_all_by_all_ts(data['emp_num'],data['project_id'],data['date'],data['dept_id'],user.userid)

class UpdatePaidTimeOff(Resource):
    @jwt_required
    @prefix_decorator('UpdatePaidTimeOff')
    def post(self):
        data = request.get_json(force=True)  
        print(data)
        employee_obj = Employee1Model.query.filter_by(emp_number=data['employee_number']).first() 
        if not employee_obj:
            return {"errorMessage":"Employee With {} Employee Number Does Not Exist".format(data['employee_number'])}
        employee_obj.eligible_sick_hours = data['sick']
        employee_obj.eligible_vacation_weeks = data['vacationWeek']
        employee_obj.paid_holidays = True if data['paidHolidays'] == 'Yes' else False
        employee_obj.save_to_db()
        return {"message":"Employee Paid Time Off Deatils Updated Successfully"}   

class HRDashboardFT(Resource):
    @prefix_decorator('HRDashboardFT')
    def get(self): 
        present_date = datetime.now().strftime("%m-%d-%Y")
        date = present_date.split('-')[0] + '-' +'01'+ '-' + present_date.split('-')[-1]
        logging.info("Success")
        return Employee1Model.return_all_for_future_terminations(present_date)



class TerminatedEmpList(Resource):
    @jwt_required  
    @prefix_decorator('TerminationsCount')
    def get(self):  
        todays_date = datetime.now().date()
        year_start_date = '{}-01-01'.format(str(todays_date.year))
        return Employee1Model.return_all_terminated_employees(year_start_date,str(todays_date))


