from run import db
from run import ma
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey   
from sqlalchemy.dialects.postgresql import ARRAY
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from run import app
import datetime
from history_meta import Versioned
from sqlalchemy.sql import func
from marshmallow import EXCLUDE
import itertools
from dateutil.relativedelta import relativedelta
from sqlalchemy import or_

#versioning_manager.init(db.Model,actor_cls='UserModel')
#make_versioned(user_cls='UserModel')

class EmployeeInviteStatus(db.Model):  
    __tablename__ = 'invite_status'

    invite_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_number = db.Column(db.String(15), nullable=False)
    employee_email = db.Column(db.String(120), nullable=False)
    employee_role = db.Column(db.String(30), nullable=True)
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'))
    designation_id = db.Column(db.Integer, ForeignKey('designation_reference.designation_id'))
    joining_date = db.Column(db.Date, nullable=True)
    billable_resource = db.Column(db.String(1),nullable=True)
    invitation_status = db.Column(db.String(10), nullable=False)
    sent_time = db.Column(db.Date, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def re(cls):
        def to_json(x):
            return {
                'id': x.invite_id
                }
        return {'id': list(map(lambda x: to_json(x), cls.query.all()))}



class PasswordMailStatus(db.Model):         
    __tablename__ = 'password_mail_status'

    token_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(255), nullable=False)
    token_status = db.Column(db.String(30), nullable=False)    


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class CountryModel(db.Model):
    __tablename__ = 'countries_reference'

    country_id = db.Column(db.Integer, primary_key=True)
    country_name = db.Column(db.String(60), nullable=False)
    phone_code = db.Column(db.String(20), nullable=True)
    currency = db.Column(db.String(20), nullable=True)
    city_model_ref = relationship("CitiesModel", uselist=False, backref="countries_reference")
    state_model_ref = relationship("StateModel", uselist=False, backref="countries_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'country_name': x.country_name,
                'country_id': x.country_id,
                'phone_code': x.phone_code
                }
        return {'countries': list(map(lambda x: to_json(x), cls.query.all()))}

class StateModel(db.Model):
    __tablename__ = 'states_reference'

    state_id = db.Column(db.Integer, primary_key=True)
    state_name = db.Column(db.String(60), nullable=False)
    country_id = db.Column(db.Integer, ForeignKey('countries_reference.country_id'))
    city_model_ref = relationship("CitiesModel", uselist=False, backref="states_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls, countryid):
        def to_json(x):
            return {
                'state_name': x.state_name,
                'state_id': x.state_id,
                }
        return {'states': list(map(lambda x: to_json(x), cls.query.filter_by(country_id=countryid).all()))}

    @classmethod
    def return_all1(cls):
        def to_json(x):
            return {
                'state_name': x.state_name,
                'state_id': x.state_id,
                }
        return {'states': list(map(lambda x: to_json(x), cls.query.all()))}

class CitiesModel(db.Model):
    __tablename__ = 'cities_reference'

    city_id = db.Column(db.Integer, primary_key=True)
    city_name = db.Column(db.String(70), nullable=False)
    state_id = db.Column(db.Integer, ForeignKey('states_reference.state_id'))
    country_id = db.Column(db.Integer, ForeignKey('countries_reference.country_id'))

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls, stateid):
        def to_json(x):
            return {
                'city_name': x.city_name,
                'city_id': x.city_id,
                }
        return {'cities': list(map(lambda x: to_json(x), cls.query.filter_by(state_id=stateid).all()))}

    @classmethod
    def return_all1(cls):
        def to_json(x):
            return {
                'city_name': x.city_name,
                'city_id': x.city_id,
                }
        return {'cities': list(map(lambda x: to_json(x), cls.query.all()))}


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'

    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)


class PrivilegeModel(db.Model):
    __tablename__ = 'privileges'

    id = db.Column(db.Integer, primary_key=True)
    privilege_name = db.Column(db.String(255), nullable=False, unique=True)
    privilege_category = db.Column(db.String(50), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'privilege_id': x.id,
                'privilege_name': x.privilege_name,
                'privlege_category':x.privilege_category
            }

        return {'privileges': list(map(lambda x: to_json(x), PrivilegeModel.query.order_by(cls.privilege_category).all()))}

    @classmethod
    def ordered(cls,key):
        def to_json(x):
            return {
                'privilege_id': x.id,
                'privilege_name': x.privilege_name,
                'privlege_category':x.privilege_category
            }

        return list(map(lambda x: to_json(x), PrivilegeModel.query.filter_by(privilege_category=key).all()))


class EmergencyRelationModel(db.Model):
    __tablename__ = 'emergency_relation_reference'

    emergency_relation_id = db.Column(db.Integer, primary_key=True)
    emergency_relation_type = db.Column(db.String(20), nullable=False)
    consultant_relation_ref = relationship("ConsultantContactModel", uselist=False, backref="emergency_relation_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'emergencyRelationId': x.emergency_relation_id,
                'emergencyRelationType': x.emergency_relation_type
            }

        return {'emergencyRelation': list(map(lambda x: to_json(x), EmergencyRelationModel.query.all()))}





class ClientContactModel(db.Model):
    __tablename__ = 'client_contact'

    contact_id = db.Column(db.String(40), primary_key=True)
    client_id = db.Column(db.String(40), ForeignKey('client_details.client_id'))
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'))
    contact_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    contact_full_name = db.Column(db.String(150), nullable=False)
    contact_title = db.Column(db.String(50), nullable=False)
    contact_work_phone = db.Column(db.String(20), nullable=False)
    contact_cell_phone = db.Column(db.String(20), nullable=True)
    contact_email = db.Column(db.String(120), nullable=False)
    reporting_manager_name = db.Column(db.String(150), nullable=False)
    status = db.Column(db.String(1),nullable=True)
    created_by = db.Column(db.Integer, nullable=True)
    modified_by = db.Column(db.Integer, nullable=True)
    created_time = db.Column(db.DateTime, nullable=True)
    modified_time = db.Column(db.DateTime, nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_id(cls, client_id):
        return cls.query.filter_by(client_id=client_id,status='A').first()


    @classmethod
    def return_all_new(cls,clientid):
        def to_json(x):
            clientconadd = Address1Model.query.filter_by(address_id = x.contact_address_id).first()
            return {
                'contact_id': x.contact_id,
                'client_id': x.client_id,
                'department_id': x.department_id,
                'department_name': DepartmentModel.find_by_department_id(x.department_id).department_name,
                'contact_address_id': x.contact_address_id,
                'contact_full_name':x.contact_full_name,
                'contact_title': x.contact_title,
                'contact_work_phone': x.contact_work_phone,
                'contact_email': x.contact_email,
                'reporting_manager_name': x.reporting_manager_name,
                'address_line1': clientconadd.address_line1,
                'address_line2': clientconadd.address_line2,
                'city': clientconadd.city,
                'state_name': clientconadd.state_name,
                'country': clientconadd.country,
                'zip_code': clientconadd.zip_code,
            }

        return {'client_contact_details': list(map(lambda x: to_json(x), cls.query.filter_by(client_id=clientid,status='A').all()))}




class ProjectEmployeeModel(db.Model):
    __tablename__ = 'project_employee'

    project_employee_id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.String(40), ForeignKey('project.project_id'))
    employee_id = db.Column(ARRAY(db.Text))

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'project_id': x.project_id,
                'employee_id': x.employee_id,
            }

        return {'project_employee_details': list(map(lambda x: to_json(x), cls.query.all()))}


    @classmethod
    def all_employee_projects(cls, employee_id):

        def to_json(x):

            return {
            "project_name": ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
            "project_id": x.project_id
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter(cls.employee_id.contains([str(employee_id)])).all()))}

    @classmethod
    def find_by_pro_list(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x).first()
            return {
            "project_name": a.project_name,
            "project_id": a.project_id

            }

        return {'Projects': list(map(lambda x: to_json(x), pro_list))}


    @classmethod
    def return_all_employeeprojects(cls,empid): 
        def to_json(x):
            pro =  ProjectModel.query.filter_by(project_id = x.project_id).first()
            client = ClientDetailsModel.query.filter_by(client_id=pro.client_id).first()
            return {
            'project_id': x.project_id,
            'project_name': pro.project_name,
            'client_id': client.client_id,
            'client_name': client.organization_name
            }

        return {'employee_projects': list(map(lambda x: to_json(x), cls.query.filter(ProjectEmployeeModel.employee_id.contains([str(empid)])).all()))}


    @classmethod
    def find_by_project_id(cls, proid):
        b = ProjectModel.query.filter_by(project_id=proid).first()
        w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
        emp = ProjectEmployeeModel.query.filter_by(project_id=proid).first()

        def to_json(x):
            if 'CON' in str(x):
                a = ThirdPartyConsultantModel.query.filter_by(consultant_id=x).first()
                return {
                    'employee_id':a.consultant_id,
                    'emp_number': a.consultant_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.personal_email_id,
                    'project': b.project_name
                    }
            else:
                a = Employee1Model.find_by_employee_id(int(x))
                return {
                    'employee_id':a.employee_id,
                    'emp_number': a.emp_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.email_id,
                    'project': b.project_name
                    }
        if emp:
            return {'projects_resources': list(map(lambda x: to_json(x), emp.employee_id))}
        else:
            return {'projects_resources': []}

    @classmethod
    def find_by_project_id2(cls, proid):
        b = ProjectModel.query.filter_by(project_id=proid).first()
        w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
        created_by = Employee1Model.query.filter_by(employee_id=b.created_by).first()
        modified_by = Employee1Model.query.filter_by(employee_id=b.modified_by).first()
        if modified_by:
            modifier = modified_by.first_name+' '+modified_by.last_name
        else:
            modifier = ''
        emp = cls.query.filter_by(project_id=proid).first()

        def to_json(x):
            if 'CON' in str(x):
                a = ThirdPartyConsultantModel.query.filter_by(consultant_id=x).first()
                return {
                    'employee_id':a.consultant_id,
                    'emp_number': a.consultant_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.personal_email_id
                    }
            else:
                a = Employee1Model.find_by_employee_id(int(x))
                return {
                    'employee_id':a.employee_id,
                    'emp_number': a.emp_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.email_id,
                    }

        if emp:
            return {'project_id': b.project_id,   
                'client_id': b.client_id,
                'client_name':ClientDetailsModel.query.filter_by(client_id=b.client_id).first().organization_name,
                'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(b.project_type_id) else None,
                'project_name': b.project_name,
                'project_description': b.project_description,
                'scheduled_start_date': str(b.scheduled_start_date),
                'scheduled_end_date': str(b.scheduled_end_date),
                'actual_start_date': str(b.actual_start_date),
                'actual_end_date': str(b.actual_end_date),
                'project_revenue': str(b.revenue_amount) if b.revenue_amount else '',
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_emp_number':w.emp_number,
                'project_manager_contact': b.proj_manager_phone_number,
                'project_sponsor': b.project_sponsor_name,
                'work_location':b.work_location,
                'project_status': 'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                'termination_reason':b.comments,
                'created_by':created_by.first_name + ' ' + created_by.last_name,
                'modified_by':modifier,
                "project_Status_last_updated_date":str(b.project_status_last_updtd_date),
                'projects_resources': list(map(lambda x: to_json(x), emp.employee_id))}
                 
                

        else:
            return {'project_id': b.project_id,
                'client_id': b.client_id,
                'client_name':ClientDetailsModel.query.filter_by(client_id=b.client_id).first().organization_name,
                'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(b.project_type_id) else None,
                'project_name': b.project_name,
                'project_description': b.project_description,
                'scheduled_start_date': str(b.scheduled_start_date),
                'scheduled_end_date': str(b.scheduled_end_date),
                'actual_start_date': str(b.actual_start_date),
                'actual_end_date': str(b.actual_end_date),
                'project_revenue': str(b.revenue_amount) if b.revenue_amount else '',
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_emp_number':w.emp_number,
                'project_manager_contact': b.proj_manager_phone_number,
                'project_sponsor': b.project_sponsor_name,
                'work_location':b.work_location,
                'termination_reason':b.comments,
                'created_by':created_by.first_name + ' ' + created_by.last_name,
                'modified_by':modifier,
                'project_status':'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                "project_Status_last_updated_date":str(b.project_status_last_updtd_date),
                'projects_resources': []}

    @classmethod
    def return_all_new(cls, z,a):
        total_records = ProjectEmployeeModel.query.all()
        list_of_projects = []
        all_projects = []
        for i in total_records:
            if str(z) in i.employee_id:
                list_of_projects.append(i.project_id)
        c = []
        d = []
        for i in list_of_projects:
            b = ProjectModel.query.filter_by(project_id=i).first().modified_time
            c.append(b)
            d.append(b)
        d.sort(reverse=True)
        for i in range(len(d)):
            all_projects.append(list_of_projects[c.index(d[i])])
        if a == 1:
            b = ProjectModel.query.filter_by(created_by=z).all()
            for i in b:
                all_projects.append(i.project_id)

        def to_json(x):
            b = ProjectModel.query.filter_by(project_id=x).first()
            w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
            z = ClientDetailsModel.query.filter_by(client_id=b.client_id).first().organization_name
            creator = Employee1Model.query.filter_by(employee_id=b.created_by).first()

            return {   
                'project_id': b.project_id, 'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(b.project_type_id) else None,
                'project_name': b.project_name, 'project_description': b.project_description,
                'scheduled_start_date': str(b.scheduled_start_date), 'scheduled_end_date': str(b.scheduled_end_date),
                'actual_start_date': str(b.actual_start_date), 'actual_end_date': str(b.actual_end_date),
                'revenue_amount': str(b.revenue_amount),
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_contact': w.primary_phone_number, 'project_sponsor_name': b.project_sponsor_name,
                'project_status': 'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                'client_name':z,
                'created_by': creator.first_name+' '+creator.last_name
            }
        projects = list(map(lambda x: to_json(x), all_projects))
        projects = list({v['project_id']:v for v in projects}.values())
        total_projects = len(projects)
        count = 0
        for i in projects:
            if i['project_status'] == 'Active':
                count += 1


        return {'projects': projects,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}


    @classmethod
    def return_all_clients(cls, z):
        total_records = ProjectEmployeeModel.query.all()
        list_of_projects = []
        for i in total_records:
            if str(z) in i.employee_id:
                list_of_projects.append(i.project_id)
        def to_json(x):
            b = ProjectModel.query.filter_by(project_id=x).first()
            w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
            z = ClientDetailsModel.query.filter_by(client_id=b.client_id).first()

            return {
                'project_id': b.project_id, 'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(b.project_type_id) else None,
                'project_name': b.project_name, 'project_description': b.project_description,
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_status':'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                'client_name':z.organization_name,
                'client_id':z.client_id,
                'client_status':z.client_status
            }

        return {'clients': list(map(lambda x: to_json(x), list_of_projects))}



    @classmethod
    def return_all_empprojects(cls,empid):
        def to_json(x):
            pro =  ProjectModel.query.filter_by(project_id = x.project_id).first()
            # bill = EmployeeBillRateModel.find_by_emp_pro(empid,x.project_id)
            # if bill:
            #     from_date = '' if bill.from_date == None else bill.from_date
            #     to_date = '' if bill.to_date == None else bill.to_date
            #     return {
            #     'employee_id':empid,
            #     'project_id': x.project_id,
            #     'project_name': pro.project_name,
            #     'client_name': ClientDetailsModel.query.filter_by(client_id = pro.client_id).first().organization_name,
            #     'work_location': pro.work_location,
            #     'project_manager_name': Employee1Model.query.filter_by(employee_id=pro.project_manager_id).first().first_name,
            #     'bill_id': bill.bill_id,
            #     'bill_rate': str(bill.bill_rate),
            #     'pay_rate': str(bill.pay_rate),
            #     'work_week_hours': str(bill.work_week_hours),
            #     'from_date':str(from_date),
            #     'to_date':str(to_date),
            #     'resource_billable':'Yes' if bill.resource_billable=='Y' else 'No' if bill.resource_billable=='N' else '',
            #     'primary_project': True if EmployeePrimaryProjectModel.query.filter(EmployeePrimaryProjectModel.project_id==x.project_id,EmployeePrimaryProjectModel.employee_id==empid).first() else False

            #     }
            # else:
            return {
            'employee_id':empid,
            'project_id': x.project_id,
            'project_name': pro.project_name,
            'client_name': ClientDetailsModel.query.filter_by(client_id = pro.client_id).first().organization_name,
            'work_location':pro.work_location,
            'project_manager_name': Employee1Model.query.filter_by(employee_id=pro.project_manager_id).first().first_name,
            'bill_id': '',
            'bill_rate':'',
            'pay_rate':'',
            'work_week_hours':'',
            'from_date':'',
            'to_date':'',
            'resource_billable':''
            #'primary_project': True if EmployeePrimaryProjectModel.query.filter(EmployeePrimaryProjectModel.project_id==x.project_id,EmployeePrimaryProjectModel.employee_id==empid).first() else False

            }
        return {'employee_projects': list(map(lambda x: to_json(x), cls.query.filter(ProjectEmployeeModel.employee_id.contains([str(empid)])).all()))}


class StaffingSupplierModel(db.Model):
    __tablename__ = 'staffing_supplier'

    supplier_id = db.Column(db.Integer, primary_key=True)
    supplier_name = db.Column(db.String(150), nullable=False)
    contact_number = db.Column(db.String(20), nullable=True)
    termination_reason = db.Column(db.String(255), nullable=True)
    contract_status = db.Column(db.String(1), nullable=False)
    owner_name = db.Column(db.String(150), nullable=True)
    company_website_url = db.Column(db.String(255), nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    supplier_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    supplier_address_rel = relationship("SupplierAddressModel", uselist=False, backref="staffing_supplier")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_suppliers(cls):
        def to_json(x):
            return {
                'supplier_id': x.supplier_id,  
                'supplier_name': x.supplier_name,  
                'contract_status': 'Active' if x.contract_status=='A' else 'Inactive' if x.contract_status=='I' else 'Terminated' if x.contract_status=='T' else '',
            }

        return {'suppliers': list(map(lambda x: to_json(x), cls.query.order_by(StaffingSupplierModel.contract_status.asc(),StaffingSupplierModel.modified_time.desc()).all()))}
    
    @classmethod
    def return_all_suppliers_cons(cls):
        def to_json(x):
            return {
                'supplier_id': x.supplier_id,  
                'supplier_name': x.supplier_name,  
                'contract_status': 'Active' if x.contract_status=='A' else 'Inactive' if x.contract_status=='I' else 'Terminated' if x.contract_status=='T' else '',
            }

        return {'suppliers': list(map(lambda x: to_json(x), cls.query.filter_by(contract_status='A').order_by(StaffingSupplierModel.supplier_name.asc()).all()))}

    @classmethod
    def return_all_suppliers_for_consultants(cls):
        def to_json(x):
            return {
                'supplier_id': x.supplier_id,  
                'supplier_name': x.supplier_name,
                'contract_status': 'Active' if x.contract_status=='A' else 'Inactive' if x.contract_status=='I' else 'Terminated' if x.contract_status=='T' else '',
            }

        return {'suppliers': list(map(lambda x: to_json(x), cls.query.filter_by(contract_status='A').all()))}


class SupplierAddressModel(db.Model):
    __tablename__='supplier_address'

    supplier_address_id=db.Column(db.Integer, primary_key=True)
    supplier_id= db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    address_id= db.Column(db.Integer, ForeignKey('address.address_id'))
    status = db.Column(db.String(1),nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True) 
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls,supid):
        def to_json(x):
            y = Address1Model.query.filter_by(address_id=x.address_id).first()
            return {
                'supplier_address_id':x.supplier_address_id,
                'supplier_id': x.supplier_id,
                'address_id': x.address_id,
                'address_line1': y.address_line1,
                'address_line2': y.address_line2,
                'city':y.city,
                'state_name':y.state_name,
                'zip_code':y.zip_code,
                'country':y.country
            }

        return {'supplier_address': list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supid,status='A').all()))}

class SupplierContactModel(db.Model):
    __tablename__ = 'supplier_contact'

    contact_id = db.Column(db.Integer, primary_key=True)
    supplier_id = db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    supplier_contact_type_id = db.Column(db.Integer, ForeignKey('supplier_contact_type_reference.supplier_contact_type_id'))
    contact_first_name = db.Column(db.String(75), nullable=True)
    contact_last_name = db.Column(db.String(75), nullable=True)
    contact_middle_name = db.Column(db.String(75), nullable=True)
    contact_phone = db.Column(db.String(50), nullable=True)
    contact_email = db.Column(db.String(50), nullable=True)
    contact_title = db.Column(db.String(30), nullable=True)
    contact_status = db.Column(db.String(1), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls,supid):
        def to_json(x):
            return {
                'contact_id' : x.contact_id,
                'supplier_id': x.supplier_id,
                'contact_first_name': x.contact_first_name,
                'contact_last_name': x.contact_last_name,
                'contact_middle_name': x.contact_middle_name,
                'contact_type':SupplierContactTypeModel.query.filter_by(supplier_contact_type_id=x.supplier_contact_type_id).first().supplier_contact_type,
                'contact_phone':x.contact_phone,
                'contact_email':x.contact_email,   
                'contact_title':x.contact_title
            }

        return {'supplier_contacts': list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supid,contact_status='A').all()))}

class SupplierContactTypeModel(db.Model):
    __tablename__ = 'supplier_contact_type_reference'

    supplier_contact_type_id = db.Column(db.Integer, primary_key=True)
    supplier_contact_type = db.Column(db.String(50), nullable=False)
    consultant_contact_rel = relationship("SupplierContactModel", uselist=False, backref="supplier_contact_type")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class ConsultantIdModel(db.Model):
    __tablename__ = 'consultant_id_gen'

    id=db.Column(db.Integer, primary_key=True)
    consultant_id = db.Column(db.Integer, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class ThirdPartyConsultantModel(db.Model):
    __tablename__ = 'third_party_consultant'

    consultant_id = db.Column(db.String(10), primary_key=True)
    supplier_id = db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    consultant_number = db.Column(db.String(10), nullable=True)
    first_name = db.Column(db.String(75), nullable=False)
    middle_name = db.Column(db.String(75), nullable=True)
    last_name = db.Column(db.String(75), nullable=False)
    date_of_birth = db.Column(db.String(20), nullable=True)
    personal_cell_phone = db.Column(db.String(20), nullable=True)
    personal_email_id = db.Column(db.String(120), nullable=True)
    gender = db.Column(db.String(1), nullable=False)
    consultant_status = db.Column(db.String(1), nullable=True)
    rehire_eligibility=db.Column(db.String(1), nullable=True)
    termination_reason=db.Column(db.String(255), nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    consultant_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    consultant_contact_rel = relationship("ConsultantContactModel", uselist=False, backref="third_party_consultant")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_consultants(cls):
        def to_json(x):
            return {
                'consultant_id': x.consultant_id,
                'supplier_name': StaffingSupplierModel.query.filter_by(supplier_id=x.supplier_id).first().supplier_name,
                'consultant_first_name': x.first_name,
                'consultant_last_name': x.last_name,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other' if x.gender=='O' else '',
                'consultant_status': 'Active' if x.consultant_status=='A' else 'Inactive' if x.consultant_status=='I' else 'Terminated' if x.consultant_status=='T' else ''
            }

        return {'consultants': list(map(lambda x: to_json(x), cls.query.order_by(ThirdPartyConsultantModel.consultant_status.asc(),ThirdPartyConsultantModel.modified_time.desc()).all()))}

    @classmethod
    def return_all_active_forproject(cls):
        def to_json(x):
            return {
                'employee_id':x.consultant_id,
                'employee_number':x.consultant_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email':x.personal_email_id,
                'gender':x.gender,
                'employee_status':'Active' if x.consultant_status=='A' else 'Inactive' if x.consultant_status=='I' else 'Terminated' if x.consultant_status=='T' else ''

            }

        return {'consultants': list(map(lambda x: to_json(x), cls.query.filter_by(consultant_status='A').all()))}

    @classmethod
    def return_all_supplier_consultants(cls,supplier_id):
        def to_json(x):
            return {
                'employee_id':x.consultant_id,
                'employee_number':x.consultant_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email':x.personal_email_id,
                'gender':'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other' if x.gender=='O' else '',
                'employee_status':'Active' if x.consultant_status=='A' else 'Inactive' if x.consultant_status=='I' else 'Terminated' if x.consultant_status=='T' else ''

            }

        return {'consultants': list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supplier_id).all()))}





class ConsultantContactModel(db.Model):
    __tablename__ = 'consultant_emergency_contact'

    emergency_contact_id=db.Column(db.Integer, primary_key=True)
    consultant_id = db.Column(db.String(10), ForeignKey('third_party_consultant.consultant_id'))
    emergency_first_name = db.Column(db.String(75), nullable=True)
    emergency_middle_name = db.Column(db.String(75), nullable=True)
    emergency_last_name = db.Column(db.String(75), nullable=True)
    emergency_phone_number = db.Column(db.String(20), nullable=True)
    emergency_alternate_phone_number = db.Column(db.String(20), nullable=True)
    emergency_relation_id = db.Column(db.Integer, ForeignKey('emergency_relation_reference.emergency_relation_id'))
    emergency_email = db.Column(db.String(120), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class RolePrivilegeModel(db.Model):
    __tablename__ = 'roleprivileges'

    role_privileges_id = db.Column(db.Integer, primary_key=True)
    role_id = db.Column(db.Integer, ForeignKey('roles.id'))
    role_privileges = db.Column(ARRAY(db.Text), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'role_id': x.id,
                'role_name': x.role_name
            }

        return {'role_privileges': list(map(lambda x: to_json(x), RolePrivilegeModel.query.all()))}

    @classmethod
    def find_by_role_name(cls, role_name):
        return cls.query.filter_by(role_name=role_name).first()

    @classmethod
    def find_by_role_id(cls, role_id):
        return cls.query.filter_by(role_id=role_id).first()


class RoleModel(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    role_name = db.Column(db.String(255), nullable=False, unique=True)
    role_privilege_ref = relationship("RolePrivilegeModel", uselist=False, backref="roles")

    def save_to_db(self):
        db.session.add(self)  
        db.session.commit()  

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'role_id': x.id,
                'role_name': (x.role_name),
            }

        return {'Roles': list(map(lambda x: to_json(x), RoleModel.query.all()))}

    @classmethod
    def find_by_role_id(cls, role_id):
        return cls.query.filter_by(id=role_id).first().role_name

    @classmethod
    def find_by_role_id_to(cls, role_id):
        return cls.query.filter_by(id=role_id).first()

    @classmethod  
    def find_by_role_name(cls, role_name):
        return cls.query.filter_by(role_name=role_name).first().id


class SupplierContactSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = SupplierContactModel
        unknown = EXCLUDE



class Employee1Model(db.Model):
    __tablename__ = 'employee'

    employee_id = db.Column(db.Integer, primary_key=True)
    emp_number = db.Column(db.String(15), nullable=True,unique=True)
    first_name = db.Column(db.String(75), nullable=False)
    middle_name = db.Column(db.String(75), nullable=True)
    last_name = db.Column(db.String(75), nullable=False)
    email_id = db.Column(db.String(120), nullable=False)
    date_of_birth = db.Column(db.String(20), nullable=True)
    gender = db.Column(db.String(1), nullable=False)
    primary_phone_number = db.Column(db.String(20), nullable=True)
    alternate_phone_number = db.Column(db.String(20), nullable=True)
    marital_status = db.Column(db.String(1), nullable=True)
    employment_start_date = db.Column(db.Date, nullable=False)
    employment_end_date = db.Column(db.Date, nullable=True)
    reporting_manager_name = db.Column(db.String(150), nullable=True)
    rep_manager_phone_number = db.Column(db.String(20), nullable=True)
    recruitment_method = db.Column(db.String(30), nullable=True)
    eligible_sick_hours = db.Column(db.String(30), nullable=True)
    eligible_vacation_weeks = db.Column(db.String(30), nullable=True)
    paid_holidays = db.Column(db.Boolean, nullable=True)
    employee_status = db.Column(db.String(1), nullable=True)
    termination_reason_id = db.Column(db.Integer, ForeignKey('termination_reason.termination_reason_id'))  
    inactive_reason_id = db.Column(db.Integer, ForeignKey('emp_inactive_reason.emp_inactive_reason_id'))
    rehire_eligibility = db.Column(db.Boolean, nullable=True)  
    billable_resource = db.Column(db.String(1),nullable=True)
    account_manager = db.Column(db.String(150), nullable=True)
    recruiter = db.Column(db.String(150), nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    employee_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    termination_date = db.Column(db.Date,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'))
    designation_id = db.Column(db.Integer, ForeignKey('designation_reference.designation_id'))
    emp_type_id = db.Column(db.Integer, ForeignKey('employment_type_reference.emp_type_id'))
    recruiter_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    reporting_manager_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    pro_emp = relationship("ProjectModel", uselist=False, backref="employee")


    def save_to_db(self):

        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_new(cls):
        a = cls.query(db.func.max(cls.emp_number).label("total_amount")).filter(cls.emp_number>= emp_number).all()
        return a

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(email_id=username).first()

    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(emp_number=empnumber).first()

    @classmethod
    def find_by_employee_id(cls, empid):
        return cls.query.filter_by(employee_id=empid).first()




    @classmethod
    def return_all_for_date_filter(cls,start_date,end_date):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'last_name': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'date_of_birth': x.date_of_birth,
                'employment_start_date': str(x.employment_start_date),
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated', 
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.employment_start_date>=start_date,cls.employment_start_date<=end_date).all()))}

    @classmethod
    def return_all_for_new_hires(cls,date):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'middle_name':x.middle_name,
                'last_name': x.last_name,
                'employment_start_date': str(x.employment_start_date),
                'account_manager': x.account_manager if x.account_manager else '',
                'created_date': str(x.created_time.date())
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.employment_start_date>=date).all()))}


    @classmethod
    def return_all_for_future_terminations(cls,date):
        def to_json(x):
            termination_obj = TerminationReasonDataModel.query.filter_by(termination_reason_id=x.termination_reason_id).first()
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'middle_name':x.middle_name,
                'last_name': x.last_name,
                'termination_date': str(x.termination_date),
                'termination_intent':termination_obj.termination_intent,
                'termination_reason': termination_obj.termination_reason,
                'account_manager': x.account_manager if x.account_manager else '',
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.termination_date>=date).all()))}

    @classmethod
    def return_all_terminated_employees(cls,start_date,end_date):
        def to_json(x):
            termination_obj = TerminationReasonDataModel.query.filter_by(termination_reason_id=x.termination_reason_id).first()
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'middle_name':x.middle_name,
                'last_name': x.last_name,
                'termination_date': str(x.termination_date),
                'termination_intent':termination_obj.termination_intent,
                'termination_reason': termination_obj.termination_reason,
                'account_manager': x.account_manager if x.account_manager else ''            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.termination_date>=start_date,cls.termination_date<end_date).all()))}




    @classmethod
    def return_all_active_forproject(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }


        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))}

    @classmethod
    def return_all_billable_active_employees(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }

   
        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A',billable_resource='Y').all()))}


    @classmethod
    def return_all_active_projectfor(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'employee_name': x.first_name + ' ' + x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }


        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))}

    @classmethod
    def return_all_active_projectfor_et(cls,username):
        def to_json(x):
            if x.email_id != username:
                return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'employee_name': x.first_name + ' ' + x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
                }


        return {'Employees': list(filter((None).__ne__,list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))))}

    @classmethod
    def return_all_active_projectfor_et1(cls,username):
        def to_json(x):
            if x.email_id != username:
                return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'last_name': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'date_of_birth': x.date_of_birth,
                'employment_start_date': str(x.employment_start_date),
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
                }

        return {'Employees': list(filter((None).__ne__,list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))))}


    @classmethod
    def return_all_emps(cls, emplist,empid):
        print('entered return al emps')
        def to_json(x):
            if x[0]=='C':
                y = ThirdPartyConsultantModel.query.filter_by(consultant_id=x).first()
                emp_num = y.consultant_id
                return {
                    'employee_id': emp_num,
                    'first_name': y.first_name,
                    'last_name': y.last_name
                }

            else:
                if x != str(empid):
                    y = cls.find_by_employee_id(x)
                    emp_num = y.employee_id
            
                    return {
                    'employee_id': emp_num,
                     'first_name': y.first_name,
                     'last_name': y.last_name

                    }

        return {'Employees': list(filter((None).__ne__,list(map(lambda x: to_json(x), emplist)))) }


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'last_name': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'date_of_birth': x.date_of_birth,
                'employment_start_date': str(x.employment_start_date),
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }
        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.order_by(Employee1Model.employee_status.asc(),Employee1Model.modified_time.desc()).all()))}


    @classmethod
    def return_all_empnumber(cls):
        def to_json(x):
            return {
                'employee_id': x.employee_id,
                'employee_number': x.emp_number,
                'name': x.first_name+" "+x.last_name
            }

        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.all()))}


    @classmethod
    def return_all_managers(cls, managerslist):
        def to_json(x):
            y = cls.find_by_employee_id(x)
            return {
                'employee_number': y.emp_number,
                'name': y.first_name+' '+y.last_name,
                'email': y.email_id,
            }

        return {'Employees': list(map(lambda x: to_json(x), managerslist))}


    @classmethod
    def return_all_recruiters(cls, des):
 
        def to_json(x):
            return {
            'employee_id': x.employee_id,
            'employee_number': x.emp_number,
            'employee_name': x.first_name+' '+x.last_name

            }

        return {'recruiters': list(map(lambda x: to_json(x), des))}


class InternalTimesheetTypesModel(db.Model):
    __tablename__ = 'internal_timesheet_type_reference'

    timesheet_type_id = db.Column(db.Integer, primary_key=True)
    timesheet_type = db.Column(db.String(120), nullable=False) 

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    
class InternalTimesheetModel(db.Model):
    __tablename__ = 'internal_timesheet'

    timesheet_id = db.Column(db.Integer, primary_key=True)
    employee_number = db.Column(db.Integer,nullable=True) 
    timesheet_type_id = db.Column(db.Integer,nullable=True)
    ts_day = db.Column(db.String(10), nullable=True)
    ts_date = db.Column(db.Date, nullable=False)
    ts_start_time = db.Column(db.Time, nullable=False)
    ts_end_time = db.Column(db.Time, nullable=False)
    project_id = db.Column(db.String(40), ForeignKey('project.project_id'), nullable=True) #foreign key
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'), nullable=True)
    total_hours = db.Column(db.Float, nullable=False)
    status = db.Column(db.String(1), nullable=True)
    remarks = db.Column(db.String(1000), nullable=True)
    created_time_stamp = db.Column(db.DateTime, nullable=True)
    last_updated_time_stamp = db.Column(db.DateTime, nullable=True) 
    approver_id = db.Column(db.Integer,ForeignKey('employee.employee_id'), nullable=True)
    approver = relationship('Employee1Model', foreign_keys='InternalTimesheetModel.approver_id')
    approver_comments = db.Column(db.String(1000), nullable=True)
    approved_time_stamp = db.Column(db.DateTime, nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



    @classmethod
    def all_employee_timesheets(cls,empid):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=empid).first()
            approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name if x.approver_id else 'N/A'
            project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name if ProjectModel.query.filter_by(project_id=x.project_id).first() else 'N/A'
            department_name = DepartmentModel.query.filter_by(department_id=x.department_id).first().department_name if DepartmentModel.query.filter_by(department_id=x.department_id).first() else ''
            approver_comments = x.approver_comments if x.approver_comments else ''
            work_hours = round(x.total_hours,2) if x.timesheet_type_id in [1,2] else 0.00
            non_work = 0.00 if x.timesheet_type_id in [1,2] else round(x.total_hours,2)
            
            return {
                'timeSheetId': x.timesheet_id,
                'empNumber':emp.emp_number,
                'employeeName':emp.first_name+' '+emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }

        return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter_by(employee_number = empid).order_by(cls.ts_date.desc()).all()))}

    @classmethod
    def filtered_employee_timesheets(cls,userid,project_id,date):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name if x.approver_id else 'N/A' 
            project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name if ProjectModel.query.filter_by(project_id=x.project_id).first() else 'N/A'
            work_hours = round(x.total_hours,2) if x.timesheet_type_id in [1,2] else 0.00
            non_work = 0.00 if x.timesheet_type_id in [1,2] else round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''
            department_name = DepartmentModel.query.filter_by(department_id=x.department_id).first().department_name if DepartmentModel.query.filter_by(department_id=x.department_id).first() else ''
            
            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name + ' ' + emp.last_name,
                'empNumber':emp.emp_number,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type if InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first() else '',   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }
        if project_id and not date:
            print('yes')
            return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == userid,InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id)).all()))}
        elif project_id:
            return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == userid,InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= date[0],InternalTimesheetModel.ts_date <= date[1]).all()))}
        else:
            return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == userid,InternalTimesheetModel.ts_date >= date[0],InternalTimesheetModel.ts_date <= date[1]).all()))}




    @classmethod   # to get all timesheets
    def return_all(cls):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'

            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''


            return {
                'timesheetId': x.timesheet_id,
                'employeeNumber':x.employee_number ,
                'empNumber':emp.emp_number,
                'employeeName':emp.first_name+' '+emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                'day': x.ts_day,
                'tsDate': str(x.ts_date),
                'tsStartTime': str(x.ts_start_time),
                'tsEndTime': str(x.ts_end_time),
                'projectId': x.project_id,
                'projectName':project,
                'totalHours': x.total_hours,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'createdTimeStamp': str(x.created_time_stamp),
                'approverName': approver,
                'approverComents':approver_comments,
                'departmentName': department_name

            }
        return {'timesheets': list(map(lambda x: to_json(x), cls.query.order_by(cls.ts_date.desc()).all()))}

    @classmethod   # to get all timesheets
    def return_all_time_sheets(cls,user):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first() 
            if emp.employee_id != user:

                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'

                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''



                return {
                    'timesheetId': x.timesheet_id,
                    'employeeNumber':x.employee_number ,
                    'empNumber':emp.emp_number,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'day': x.ts_day,
                    'tsDate': str(x.ts_date),
                   'tsStartTime': str(x.ts_start_time),
                   'tsEndTime': str(x.ts_end_time),
                   'projectId': x.project_id,
                   'projectName':project,
                   'totalHours': x.total_hours,
                   'workHours': work_hours,
                   'nonWorkHours': non_work,
                   'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                   'remarks': x.remarks,
                   'createdTimeStamp': str(x.created_time_stamp),
                   'approverName': approver,
                   'approverComents':approver_comments,
                   'departmentName': department_name

                }
        return {'timesheets': list(filter((None).__ne__,list(map(lambda x: to_json(x,user), cls.query.order_by(cls.ts_date.desc()).all()))))}



    @classmethod
    def return_all_reporting_manager(cls,emp_list,emp_id):
        def inner_func(x,emp_id):
            def to_json(y,emp_id):
                emp = Employee1Model.query.filter_by(employee_id=y.employee_number).first()

                if emp_id != emp.employee_id:
                    if y.approver_id:
                        approver = Employee1Model.query.filter_by(employee_id=y.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=y.approver_id).first().last_name
                    else:
                        approver = 'N/A'
                    if y.project_id:
                        project = ProjectModel.query.filter_by(project_id=y.project_id).first().project_name
                        project_id = y.project_id
                    else:
                        project = 'N/A'
                        project_id = ''
                    if y.timesheet_type_id in [1,2]:
                        work_hours = round(y.total_hours,2)
                        non_work = 0.00
                    else:
                        work_hours = 0.00
                        non_work = round(y.total_hours,2)
                    approver_comments = y.approver_comments if y.approver_comments else ''

                    department = DepartmentModel.query.filter_by(department_id=y.department_id).first()
                    department_name = department.department_name if department else ''

                   
                    return {
                        'timesheetId': y.timesheet_id,
                        'employeeNumber':y.employee_number ,
                        'empNumber':emp.emp_number,
                        'employeeName':emp.first_name+' '+emp.last_name,
                        'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=y.timesheet_type_id).first().timesheet_type,
                        'day': y.ts_day,
                        'tsDate': str(y.ts_date),
                        'tsStartTime': str(y.ts_start_time),
                        'tsEndTime': str(y.ts_end_time),  
                        'projectId': project_id,
                        'projectName':project,
                        'totalHours': y.total_hours,
                        'workHours': work_hours,
                        'nonWorkHours': non_work,
                        'status': 'Approved' if y.status=='A' else 'Rejected' if y.status=='R' else 'Submitted' if y.status=='S' else '',
                        'remarks': y.remarks,
                        'createdTimeStamp': str(y.created_time_stamp),
                        'approverName': approver,
                        'approverComents':approver_comments,
                        'departmentName': department_name
                    }

            return list(map(lambda y: to_json(y,emp_id), cls.query.filter_by(employee_number=x).all()))
        return {'timesheets': list(filter((None).__ne__,list(itertools.chain.from_iterable(list(map(lambda x: inner_func(x,emp_id), emp_list))))))}


    @classmethod
    def abc(cls,emp_list,emp_id,date):
        def inner_func(x,emp_id,date):
            def to_json(y,emp_id):
                emp = Employee1Model.query.filter_by(employee_id=y.employee_number).first()

                if emp_id != emp.employee_id:
                    if y.approver_id:
                        approver = Employee1Model.query.filter_by(employee_id=y.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=y.approver_id).first().last_name
                    else:
                        approver = 'N/A'
                    if y.project_id:
                        project = ProjectModel.query.filter_by(project_id=y.project_id).first().project_name
                        project_id = y.project_id
                    else:
                        project = 'N/A'
                        project_id = ''
                    if y.timesheet_type_id in [1,2]:
                        work_hours = round(y.total_hours,2)
                        non_work = 0.00
                    else:
                        work_hours = 0.00
                        non_work = round(y.total_hours,2)
                    approver_comments = y.approver_comments if y.approver_comments else ''

                    department = DepartmentModel.query.filter_by(department_id=y.department_id).first()
                    department_name = department.department_name if department else ''

                   
                    return {
                        'timesheetId': y.timesheet_id,
                        'employeeNumber':y.employee_number ,
                        'empNumber':emp.emp_number,
                        'employeeName':emp.first_name+' '+emp.last_name,
                        'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=y.timesheet_type_id).first().timesheet_type,
                        'day': y.ts_day,
                        'tsDate': str(y.ts_date),
                        'tsStartTime': str(y.ts_start_time),
                        'tsEndTime': str(y.ts_end_time),  
                        'projectId': project_id,
                        'projectName':project,
                        'totalHours': y.total_hours,
                        'workHours': work_hours,
                        'nonWorkHours': non_work,
                        'status': 'Approved' if y.status=='A' else 'Rejected' if y.status=='R' else 'Submitted' if y.status=='S' else '',
                        'remarks': y.remarks,
                        'createdTimeStamp': str(y.created_time_stamp),
                        'approverName': approver,
                        'approverComents':approver_comments,
                        'departmentName': department_name
                    }

            return list(map(lambda y: to_json(y,emp_id), cls.query.filter(cls.employee_number==x,cls.ts_date >= date[0],cls.ts_date <= date[1]).all()))
        return {'timesheets': list(filter((None).__ne__,list(itertools.chain.from_iterable(list(map(lambda x: inner_func(x,emp_id,date), emp_list))))))}

    @classmethod
    def return_time_sheets(cls,emp,startDate,endDate):
        def to_json(x):
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'

            if x.project_id:
                project = x.project_id
            else:
                project = ''
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''

            return {
                'timeSheetId': x.timesheet_id,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverComents':approver_comments,
                'approverName':approver,
                'departmentName': department_name
            }

        return {'time_sheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == emp,InternalTimesheetModel.ts_date >= startDate,InternalTimesheetModel.ts_date <= endDate)
.all()))}


    @classmethod
    def return_all_timesheets(cls,empid):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=empid).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''


            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name+' '+emp.last_name,
                'empNumber':emp.emp_number,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }


        return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter_by(employee_number = empid).order_by(cls.ts_date.desc()).all()))}

    @classmethod
    def return_all_by_pro(cls, proid,user):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter_by(project_id=proid).order_by(InternalTimesheetModel.ts_date.desc(),InternalTimesheetModel.project_id).all()))}

    @classmethod
    def defo(cls, proid,user,date):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.project_id==proid,cls.ts_date >= date[0],cls.ts_date <= date[1]).order_by(cls.ts_date.desc(),cls.project_id).all()))}


    @classmethod
    def defall(cls, proid,user,dates,emp_id):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:  
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.project_id==proid if proid else cls.project_id,cls.ts_date >= dates[0] if dates else cls.ts_date ,cls.ts_date <= dates[1] if dates else cls.ts_date,cls.employee_number==emp_id if emp_id else cls.employee_number).order_by(cls.ts_date.desc(),cls.project_id).all()))}

    @classmethod
    def defall1(cls, proid,user,emp_id):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:  
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.project_id==proid,cls.employee_number==emp_id).order_by(cls.ts_date.desc(),cls.project_id).all()))}

    @classmethod
    def defall2(cls,user,dates,emp_id):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:  
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.ts_date >= dates[0] if dates else cls.ts_date ,cls.ts_date <= dates[1] if dates else cls.ts_date,cls.employee_number==emp_id if emp_id else cls.employee_number).order_by(cls.ts_date.desc()).all()))}

    @classmethod
    def defall3(cls,user,dates, proid):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:  
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.project_id==proid if proid else cls.project_id,cls.ts_date >= dates[0] if dates else cls.ts_date ,cls.ts_date <= dates[1] if dates else cls.ts_date).order_by(cls.ts_date.desc(),cls.project_id).all()))}

    @classmethod
    def defall4(cls,user,proid):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:  
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.project_id==proid if proid else cls.project_id).order_by(cls.ts_date.desc(),cls.project_id).all()))}

    @classmethod
    def defall5(cls,user,emp_id):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:  
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter(cls.employee_number==emp_id if emp_id else cls.employee_number).order_by(cls.ts_date.desc(),cls.project_id).all()))}


    @classmethod
    def return_all_by_man_ts(cls,emp_num,project_id,date):
        if date == '':
            min_date = '01-01-1997'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[0] != '':
            min_date = date[0].split('T')[0]
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[1] != '': 
            max_date = date[1].split('T')[0]
            min_date = '01-01-1997'
        else:
            max_date = date[1].split('T')[0]
            min_date = date[0].split('T')[0]
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''


            return {
                'timesheetId': x.timesheet_id,
                'employeeName':emp.first_name + ' ' + emp.last_name,
                'empNumber':emp.emp_number,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimeStamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }
        if project_id == '' and emp_num:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == emp_num,InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        else:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
            


    @classmethod
    def return_all_by_all_ts(cls,emp_num,project_id,date,dept_id,user):
        if not emp_num and not project_id and not date and not dept_id:
            return InternalTimesheetModel.return_all_time_sheets(emp_num)
        if date != '':
            min_date = date[0].split('T')[0]
            max_date = date[1].split('T')[0]
        else:
            min_date = '01-01-1977'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        def to_json(x):
            if user != x.employee_number:
                emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()  
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''
                if x.department_id != '':
                    department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                    department_name = department.department_name if department else ''
                else:
                    department_name = ''

                return {

                    'timesheetId': x.timesheet_id,
                    'employeeName':emp.first_name + ' ' + emp.last_name,
                    'empNumber':emp.emp_number,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimeStamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:
                print('entered else')
                pass
        if dept_id and project_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.department_id == (dept_id if dept_id else InternalTimesheetModel.department_id),InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        elif project_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        elif dept_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.department_id == (dept_id if dept_id else InternalTimesheetModel.department_id),InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        else:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}



    @classmethod
    def return_all_by_manteam_ts(cls,emp_num,project_id,date):
        if date == '':
            min_date = '01-01-1997'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[0] != '':
            min_date = date[0].split('T')[0]
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[1] != '': 
            max_date = date[1].split('T')[0]
            min_date = '01-01-1997'
        else:
            max_date = date[1].split('T')[0]
            min_date = date[0].split('T')[0]

        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''

            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name + ' ' + emp.last_name,
                'empNumber':emp.emp_number,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }
        if project_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        else:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}




class ProjectModel(db.Model):
    __tablename__ = 'project'

    project_id = db.Column(db.String(40), primary_key=True)
    client_id = db.Column(db.String(40), ForeignKey('client_details.client_id'))
    project_type_id = db.Column(db.Integer, ForeignKey('project_category_reference.project_type_id'))
    work_location = db.Column(db.String(60), nullable=True)
    project_name = db.Column(db.String(255), nullable=False)
    project_description = db.Column(db.String(255), nullable=True)
    scheduled_start_date = db.Column(db.Date, nullable=True)
    scheduled_end_date = db.Column(db.Date, nullable=True)
    actual_start_date = db.Column(db.Date, nullable=True)
    actual_end_date = db.Column(db.Date, nullable=True)
    revenue_amount = db.Column(db.Numeric, nullable=True)
    project_manager_id = db.Column(db.Integer, ForeignKey('employee.employee_id'), nullable=False)
    proj_manager_phone_number= db.Column(db.String(20),nullable=True)
    project_sponsor_name = db.Column(db.String(150), nullable=True)
    project_status = db.Column(db.String(1), nullable=True)
    daily_work_hours = db.Column(db.Float,nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    project_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    work_location_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    project_employee_model_ref = relationship("ProjectEmployeeModel", uselist=False, backref="project")
    internal_timesheet_id = relationship("InternalTimesheetModel", uselist=False, backref = "project")
   
    

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_active_projects(cls):
        def to_json(x):
            return {
                'project_id': x.project_id,
                'project_name': x.project_name,
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter_by(project_status='A').all()))}

    @classmethod
    def return_all(cls):
        def to_json(x):
            w = Employee1Model.query.filter_by(employee_id=x.project_manager_id).first()
            if x.actual_start_date == None:
                actual_start_date = ''
            else:
                actual_start_date = str(x.actual_start_date)
            return {
                'project_id': x.project_id,  
                'project_name': x.project_name,
                'client_name':ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'actual_start_date': actual_start_date,
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_status':x.project_status
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}

    @classmethod
    def return_all_projects(cls):
        def to_json(x):
            return {
                'project_name': x.project_name,
                        }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}

    @classmethod
    def return_all_client_projects(cls, b):
        def to_json(x):
            return {
                'project_id': x.project_id,
                'project_type': ProjectTypeModel.find_by_project_type_id(x.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(x.project_type_id) else None,
                'project_name': x.project_name,
                'scheduled_start_date': str(x.scheduled_start_date),
                'scheduled_end_date': str(x.scheduled_end_date),
                'project_manager_name': Employee1Model.query.filter_by(
                    employee_id=x.project_manager_id).first().first_name,
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter_by(client_id=b).all()))}

    @classmethod
    def find_by_project_type_id(cls, proid):
        return cls.query.filter_by(project_id=proid).first()

    @classmethod
    def find_by_pro_list(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x).first()
            return {
            "project_name": a.project_name,
            "project_id": a.project_id

            }

        return {'Projects': list(map(lambda x: to_json(x), pro_list))}

    @classmethod
    def find_by_pro_list1(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x.project_id).first()
            return {
            "project_name": a.project_name,
            "project_id": a.project_id

            }

        return {'Projects': list(map(lambda x: to_json(x), pro_list))}

    @classmethod
    def for_projects_list(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x).first()
            w = Employee1Model.query.filter_by(employee_id=a.project_manager_id).first()
            creator = Employee1Model.query.filter_by(employee_id=a.created_by).first()
            z = ClientDetailsModel.query.filter_by(client_id=a.client_id).first().organization_name

            return {
                'project_id': a.project_id,
                'client_name':z,
                'project_type': ProjectTypeModel.find_by_project_type_id(a.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(a.project_type_id) else None,
                'project_name': a.project_name,
                'project_description': a.project_description,
                'scheduled_start_date': str(a.scheduled_start_date),
                'scheduled_end_date': str(a.scheduled_end_date),
                'actual_start_date': str(a.actual_start_date if a.actual_start_date else ''),
                'actual_end_date': str(a.actual_end_date if a.actual_end_date else ''),
                'revenue_amount': str(a.revenue_amount if a.revenue_amount else ''),
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_contact': w.primary_phone_number,
                'project_sponsor_name': a.project_sponsor_name,
                'modified_time':str(a.modified_time),
                'created_by':creator.first_name+' '+creator.last_name,
                'project_status':  'Active' if a.project_status=='A' else 'Inactive' if a.project_status=='I' else 'Terminated' if a.project_status=='T' else '',  
            }

        return list(map(lambda x: to_json(x), pro_list))

    @classmethod
    def manager_projects(cls, z,a):
        def to_json(x):
            w = Employee1Model.query.filter_by(employee_id=x.project_manager_id).first()
            creator = Employee1Model.query.filter_by(employee_id=x.created_by).first()
            z = ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name
            if x.actual_start_date == None:
                actual_start_date = ''
            else:
                actual_start_date = x.actual_start_date
            if x.actual_end_date == None:
                actual_end_date = ''
            else:
                actual_end_date = x.actual_end_date
            if x.revenue_amount== None:
                revenue_amount = ''
            else:
                revenue_amount = x.revenue_amount
            return {
                'project_id': x.project_id,
                'client_name':z,
                'project_type': ProjectTypeModel.find_by_project_type_id(x.project_type_id).project_type_name if ProjectTypeModel.find_by_project_type_id(x.project_type_id) else None,
                'project_name': x.project_name,
                'project_description': x.project_description,
                'scheduled_start_date': str(x.scheduled_start_date),
                'scheduled_end_date': str(x.scheduled_end_date),
                'actual_start_date': str(actual_start_date),
                'actual_end_date': str(actual_end_date),
                'revenue_amount': str(revenue_amount),
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_contact': w.primary_phone_number,
                'project_sponsor_name': x.project_sponsor_name,
                'modified_time':str(x.modified_time),
                'created_by':creator.first_name+' '+creator.last_name,
                'project_status':  'Active' if x.project_status=='A' else 'Inactive' if x.project_status=='I' else 'Terminated' if x.project_status=='T' else '',  
            }
        if a == 0:
            pro_list = list(map(lambda x: to_json(x), cls.query.filter_by(project_manager_id=z).order_by(ProjectModel.project_status.asc(),ProjectModel.modified_time.desc()).all()))
            total_projects = len(pro_list)
            count = 0
            for i in pro_list:
                if i['project_status'] == 'Active':
                    count += 1
            return {'projects': pro_list,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}
        elif a == 1:
            b = list(map(lambda x: to_json(x), cls.query.filter_by(project_manager_id=z).all())) + list(map(lambda x: to_json(x), cls.query.filter_by(created_by=z).all()))
            b = list({v['project_id']:v for v in b}.values())
            b.sort(key=lambda x:x.get('modified_time'),reverse=True)
            b.sort(key=lambda x:x.get('project_status'))
            total_projects = len(b)
            count = 0
            for i in b:
                if i['project_status'] == 'Active':
                    count += 1
            
            return {'projects': b,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}



class Address1Model(db.Model):
    __tablename__ = 'address'

    address_id = db.Column(db.Integer, primary_key=True)
    address_line1 = db.Column(db.String(255), nullable=False)
    address_line2 = db.Column(db.String(255), nullable=True)
    city = db.Column(db.String(70), nullable=False)
    state_name = db.Column(db.String(60), nullable=False)
    zip_code = db.Column(db.String(15), nullable=False)
    country = db.Column(db.String(60), nullable=False)
    clientcontactaddress = relationship("ClientContactModel", uselist=False, backref="address")
    supplier_address_rel = relationship("SupplierAddressModel", uselist=False, backref="address")
    project_address_rel = relationship("ProjectModel", uselist=False, backref="address")

    
    def save_to_db(self):   
        db.session.add(self)
        db.session.commit()


    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


class EmployeeSignupSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Employee1Model
        unknown = EXCLUDE

class EmpInactivationReasonDataModel(db.Model):
    __tablename__ = 'emp_inactive_reason'

    emp_inactive_reason_id = db.Column(db.Integer, primary_key=True)
    emp_inactive_reason = db.Column(db.String(30), nullable=True)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'inactive_reason_id': x.emp_inactive_reason_id,
                'inactive_reason': x.emp_inactive_reason,
                }
        return {'inactive_reasons': list(map(lambda x: to_json(x), cls.query.all()))}



class TerminationReasonDataModel(db.Model):  
    __tablename__ = 'termination_reason'

    termination_reason_id = db.Column(db.Integer, primary_key=True)
    termination_intent = db.Column(db.String(30), nullable=True)
    termination_reason = db.Column(db.String(50), nullable=False)
    termination_ref = relationship("Employee1Model", uselist=False,backref="termination_reason")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    
    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'termination_reason_id': x.termination_reason_id,
                'termination_intent': x.termination_intent,
                'termination_reason': x.termination_reason,
            }

        return {'termination_reasons': list(map(lambda x: to_json(x), cls.query.all()))}

class VacationWeeks(db.Model):  
    __tablename__ = 'vacation_weeks_reference'

    vacation_weeks_id = db.Column(db.Integer, primary_key=True)
    vacation_weeks = db.Column(db.String(30), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

class SickHours(db.Model):  
    __tablename__ = 'sick_hours_reference'

    sick_hours_id = db.Column(db.Integer, primary_key=True)
    sick_hours = db.Column(db.String(30), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

          
class ClientDetailsModel(db.Model):
    __tablename__ = 'client_details'

    client_id = db.Column(db.String(40), primary_key=True)
    client_status = db.Column(db.String(1), nullable=False)
    organization_name = db.Column(db.String(255), nullable=False)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    client_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    client_project_ref = relationship("ProjectModel", uselist=False, backref="client_details")
    client_contact = relationship("ClientContactModel", uselist=False, backref="client_details")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_id(cls, clientid):
        return cls.query.filter_by(client_id=clientid).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'client_id': x.client_id,
                'client_status': 'Active' if x.client_status=='A' else 'Inactive' if x.client_status=='I' else 'Terminated' if x.client_status=='T' else 'Prospect' if x.client_status=='P' else '',
                'organization_name': x.organization_name,
                "client_created_date":str(datetime.datetime.strptime(str(x.created_time),"%Y-%m-%d %H:%M:%S.%f").date())
            }
        return {'client_details': list(map(lambda x: to_json(x), ClientDetailsModel.query.order_by(ClientDetailsModel.client_status.asc(),ClientDetailsModel.modified_time.desc()).all()))}

 

    @classmethod
    def return_all_clientids(cls):
        def to_json(x):

            return {
                'client_id': x.client_id,
                'client_status': 'Active' if x.client_status=='A' else 'Inactive' if x.client_status=='I' else 'Terminated' if x.client_status=='T' else 'Prospect' if x.client_status=='P' else '',
                'organization_name': x.organization_name,   
            }

        return {'client_details': list(map(lambda x: to_json(x), ClientDetailsModel.query.filter_by(client_status='A').all()))}




class BussinessSectorModel(db.Model):
    __tablename__ = 'business_sector_reference'

    bussiness_sector_id = db.Column(db.Integer, primary_key=True)
    bussiness_sector_type = db.Column(db.String(30), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_bussiness_sector_type(cls, bustype):
        return cls.query.filter_by(bussiness_sector_type=bustype).first().bussiness_sector_id

    @classmethod
    def find_by_bussiness_sector_type_id(cls, bustypeid):
        return cls.query.filter_by(bussiness_sector_id=bustypeid).first().bussiness_sector_type

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'bussiness_sector_id': x.bussiness_sector_id,
                'bussiness_sector_type': x.bussiness_sector_type,
            }

        return {'bussiness_sector_types': list(map(lambda x: to_json(x), BussinessSectorModel.query.all()))}

class ProjectTypeModel(db.Model):
    __tablename__ = 'project_category_reference'

    project_type_id = db.Column(db.Integer, primary_key=True)
    project_type_name = db.Column(db.String(30), nullable=False)
    project_type_ref = relationship("ProjectModel", uselist=False, backref="project_category_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_project_type_id(cls, protypeid):
        return cls.query.filter_by(project_type_id=protypeid).first()

    @classmethod
    def find_by_project_type_name(cls, protypename):
        return cls.query.filter_by(project_type_name=protypename).first().project_type_id

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'project_type_id': x.project_type_id,
                'project_type_name': x.project_type_name,
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}


class UserRolesModel(db.Model):
    __tablename__ = 'user_roles'

    user_role_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey('users.userid'))
    roles = db.Column(ARRAY(db.Text), default=[4])

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls, uid):
        def to_json(x):
            return {
                'user_id': x.user_id,
                'role_name': RoleModel.find_by_role_id(x.role_id)
            }

        return {'role_privileges': list(map(lambda x: to_json(x), cls.query.filter_by(user_id=uid).all()))}

    @classmethod
    def find_by_user_id(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().role_id

    @classmethod
    def find_by_role_name(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().role_id

    @classmethod
    def find_by_user_id2(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().roles

    @classmethod
    def find_to_get_user_id(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().user_id

    @classmethod
    def find_by_user_id_to_all(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first()

    @classmethod
    def find_for_role_assign(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).all()

class UserRolesSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserRolesModel


class UserModel(db.Model):
    __tablename__ = 'users'

    userid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    reset_password = db.Column(db.Boolean,nullable=True)
    user_role_ref = relationship("UserRolesModel", uselist=False, backref="users")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



    def get_token(self, expiration=1800):
        s = Serializer(app.config['SECRET_KEY'], expiration)
        return s.dumps({'user': self.userid}).decode('utf-8')

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username
            }

        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


    @staticmethod
    def verify_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        id = data.get('user')
        if id:
            return UserModel.query.get(id).userid
        return None

class UsersSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserModel
        unknown = EXCLUDE


class DesignationModel(db.Model):
    __tablename__ = 'designation_reference'

    designation_id = db.Column(db.Integer, primary_key=True)
    designation_name = db.Column(db.String(100), nullable=False)
    emp_education = relationship("Employee1Model", uselist=False, backref="designation_reference")
    invitation_mail = relationship("EmployeeInviteStatus", uselist=False, backref="designation_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_designation_id(cls, desg_id):
        return cls.query.filter_by(designation_id=desg_id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'designation_id': x.designation_id,
                'designation_name': x.designation_name,
            }

        return {'designation_types': list(map(lambda x: to_json(x), DesignationModel.query.all()))}


class EmploymentTypeModel(db.Model):
    __tablename__ = 'employment_type_reference'

    emp_type_id = db.Column(db.Integer, primary_key=True)
    emp_type = db.Column(db.String(100), nullable=False)
    emp_education = relationship("Employee1Model", uselist=False, backref="employment_type_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_emp_type_id(cls, type_id):
        return cls.query.filter_by(emp_type_id=type_id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'emp_type_id': x.emp_type_id,
                'emp_type': x.emp_type,
            }

        return {'employment_types': list(map(lambda x: to_json(x), EmploymentTypeModel.query.all()))}


class DepartmentModel(db.Model):
    __tablename__ = 'department_reference'

    department_id = db.Column(db.Integer, primary_key=True)
    department_name = db.Column(db.String(100), nullable=False)
    emp_education = relationship("Employee1Model", uselist=False, backref="department_reference")
    client_contact = relationship("ClientContactModel", uselist=False, backref="department_reference")
    internal_ts = relationship("InternalTimesheetModel", uselist=False, backref="department_reference")
    invitation_mail = relationship("EmployeeInviteStatus", uselist=False, backref="department_reference")



    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_department_id(cls, dept_id):
        return cls.query.filter_by(department_id=dept_id).first()

    @classmethod
    def find_by_department_name(cls, dept_name):
        return cls.query.filter_by(department_name=dept_name).first().department_id

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'department_id': x.department_id,
                'department_name': x.department_name,
            }

        return {'departments': list(map(lambda x: to_json(x), DepartmentModel.query.all()))}




class ConsultantSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ThirdPartyConsultantModel
        unknown = EXCLUDE


class ConsultantContactSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ConsultantContactModel
        unknown = EXCLUDE


class StaffingSupplierSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = StaffingSupplierModel
        unknown = EXCLUDE


class AddressSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Address1Model
        unknown = EXCLUDE


class ProjectSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ProjectModel
        unknown = EXCLUDE

class ClientDetailsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ClientDetailsModel
        unknown = EXCLUDE


class ClientContactSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ClientContactModel
        unknown = EXCLUDE

class EmployeePrimaryProjectModel(db.Model):
    __tablename__ = 'employee_primary_project'

    employee_primary_project_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    project_id = db.Column(db.String(40), ForeignKey('project.project_id'))
    created_by = db.Column(db.Integer, nullable=True)
    modified_by = db.Column(db.Integer, nullable=True)
    created_time = db.Column(db.DateTime, nullable=True)
    modiified_time = db.Column(db.DateTime, nullable=True)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class DashboardStats(db.Model):
    __tablename__ = 'dashboard_stats'

    stat_id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    date = db.Column(db.Date,nullable=True)
    billable = db.Column(db.Integer,nullable=True)
    on_billing =db.Column(db.Integer,nullable=True)
    on_bench = db.Column(db.Integer,nullable=True)
    non_billing = db.Column(db.Integer,nullable=True)
    total_employees = db.Column(db.Integer,nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'month': str(x.date.strftime('%b')) + '-'+str(x.date.strftime('%Y')),
                'billable':x.billable,
                'non_billable':x.non_billing,
                'total':x.total_employees,
                'on_billing':x.on_billing,
                'on_bench':x.on_bench,
            }

        return list(map(lambda x: to_json(x), cls.query.all()))






