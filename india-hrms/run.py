from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_migrate import Migrate
from flask_mail import Mail, Message
from flask_sqlalchemy import SignallingSession
from history_meta import versioned_session   
from flask_marshmallow import Marshmallow
import logging
versioned_session(SignallingSession)


app = Flask(__name__)


CORS(app, support_credentials=True, origins='*')
api = Api(app)   
mail= Mail(app)
cors = CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres#321@localhost/hrappstestingdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'some-secret-string'
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

app.config['CORS_HEADERS'] = 'Content-Type'

app.config['MAIL_SERVER']='west.EXCH081.serverdata.net'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'demo@prutech.com'
app.config['MAIL_PASSWORD'] = 'INDoffice#2019'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)

#db = SQLAlchemy(app,engine_options={ 'connect_args': { 'connect_timeout': 5 }})
db = SQLAlchemy(app)
ma = Marshmallow(app)
jwt = JWTManager(app)
migrate = Migrate(app, db)


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)

#logging.basicConfig(filename='server.log',level=logging.INFO,format='%(asctime)s-%(name)s-%(message)s')
#logging.basicConfig(level=logging.DEBUG,format='%(asctime)s-%(name)s-%(message)s')

def before_first_request():
    try:
        logging.info('Entered into {0}'.format(before_first_request.__name__))
        import models  
        
        global inactive_reason_map,supplier_contact_type_map,privileges_map,business_sector_map,vendorship_type_map,project_type_map,termination_reason_map,diversity_status_map,designation_map,edu_branch_map,coverage_type_map,benefit_type_map,department_map,employment_type_map,countries_map,emergency_relation_map,ts_types_map

            
        countries_map = models.CountryModel.return_all()

        if len(countries_map) == 0: 
            logging.debug() 
            raise ValueError('Error, Could Not Load Data Of Countries List')
            
            
        emergency_relation_map = list(map(lambda x:x.emergency_relation_type,models.EmergencyRelationModel.query.all()))
        if len(emergency_relation_map) == 0: 
            logging.debug() 
            raise ValueError('Error, Could Not Load Data For Dependent Relation Dropdown')


        ts_types_map = list(map(lambda x:x.timesheet_type,models.InternalTimesheetTypesModel.query.all()))
        if len(ts_types_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Timesheet Types Dropdown')
            
        employment_type_map = [i.emp_type for i in models.EmploymentTypeModel.query.all()]
        if len(employment_type_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Employment Types Dropdown')

        department_map = models.DepartmentModel.return_all()
        if len(department_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Department Dropdown')

            
        termination_reason_map = models.TerminationReasonDataModel.return_all()
        if len(termination_reason_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For termination reasons Dropdown')
            
        designation_map = [i.designation_name for i in models.DesignationModel.query.all()]
        if len(designation_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Designation Dropdown')
            
            
        project_type_map = models.ProjectTypeModel.return_all()
        if len(project_type_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Project Type Dropdown')
            
            
        business_sector_map = models.BussinessSectorModel.return_all()
        if len(business_sector_map) == 0:
            logging.debug()  
            raise ValueError('Error, Could Not Load Data For Business Sector Type Dropdown')

        privileges_map = {}
        for i in models.PrivilegeModel.query.distinct(models.PrivilegeModel.privilege_category).all():
            privileges_map[i.privilege_category] = models.PrivilegeModel.ordered(i.privilege_category)
        if len(privileges_map) == 0:  
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Privileges Dropdown')

        supplier_contact_type_map = [i.supplier_contact_type for i in models.SupplierContactTypeModel.query.all()]
        if len(supplier_contact_type_map) == 0:
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Supplier Contact Type Dropdown')

        inactive_reason_map =  models.EmpInactivationReasonDataModel.return_all()
        if len(inactive_reason_map ) == 0:
            logging.debug()
            raise ValueError('Error, Could Not Load Data For Employee Inactive Reason Dropdown')


        return globals()
    except Exception as e:
        db.session.rollback()
        logging.debug(str(e))
        return {"errorMessage":str(e)},500
    finally:
        db.session.close()

  
import views, models, resources

# Static EndPoints 18 ca
api.add_resource(resources.GetStateByName,'/statebynamedd')
api.add_resource(resources.GetCitiesByName,'/citybynamedd')
api.add_resource(resources.GetCountries, '/getcountry')
api.add_resource(resources.EmergencyRelationType, '/emergencyreldd')
api.add_resource(resources.InternalTimesheetTypeDropDown, '/itstypeldd')
api.add_resource(resources.EmploymentType,'/employmenttypedd')
api.add_resource(resources.DepartmentList, '/departmentdd')
api.add_resource(resources.DesignationType,'/designationdd')
api.add_resource(resources.Terminationdd,'/terminationdd')
api.add_resource(resources.ProjectTypeList, '/projecttypelist')
api.add_resource(resources.SupplierConType,'/supcontactdd')
api.add_resource(resources.InactiveReasonDD, '/inactypedd')
api.add_resource(resources.VacationWeeksDropDowns, '/vacationweekdd')
api.add_resource(resources.SickHoursDropDowns, '/sickhourdd')


#validations 2 ca
api.add_resource(resources.InvitationValidation, '/validateinvitation')
api.add_resource(resources.PasswordMailValidation, '/passmailvalidation') 

#Endpoint for login 3 ca
api.add_resource(resources.AllEmployees, '/login')
api.add_resource(resources.UserLogoutRefresh, '/logout/refresh')
api.add_resource(resources.TokenRefresh, '/token/refresh')


#Endpoint for password change 1 ca
api.add_resource(resources.ChangePassword, '/changepass')

# user management 13 ca 
api.add_resource(resources.GetRp, '/getrp')
api.add_resource(resources.GetRole, '/getrole')
api.add_resource(resources.AddPrivilege, '/addprivilege')
api.add_resource(resources.RoleEdit, '/editrole')
api.add_resource(resources.RolesList, '/roleslist')
api.add_resource(resources.PrivilegesList, '/privilegeslist')
api.add_resource(resources.GetOwnRole, '/getownrole')
api.add_resource(resources.GetRolePrivileges, '/getroleprivileges')
api.add_resource(resources.AddPrivilegeToRole, '/assignprivileges')
api.add_resource(resources.RoleAssignment, '/assignrole')
api.add_resource(resources.ValidateRole, '/validaterole')
api.add_resource(resources.AddRoleWithPrivileges, '/addrolewithprivileges')
api.add_resource(resources.GetUserPrivileges,'/getuserprivileges')   

# Endpoints for client_timesheets 2 ca
api.add_resource(resources.ManagerEmp,'/manageremp')


#new ts endpoints 5  ca
api.add_resource(resources.MyTimeSheetProjectDropDown,'/mytsprodd')
api.add_resource(resources.MyTimeSheets,'/viewmyits')  # to view employee time sheets
api.add_resource(resources.MyTimesheetsFilter,'/myitsfilter')  # to filter my time sheets 
api.add_resource(resources.AllTimeSheetProjectDropDown,'/alltsprodd')  # to get all active projects 
api.add_resource(resources.AllTimesheet,'/getallits')   # to get all timesheets except theirs

# Internal Time sheets 23 ca
api.add_resource(resources.HrItsProjectFilter,'/allitsprofilter')  # to get all projects 
api.add_resource(resources.HrItsEmployeeFilter,'/allitsempfilter') # to get all employees
api.add_resource(resources.HrItsCombinedFilter,'/allitsemprofilter') # to get all employees based on project
api.add_resource(resources.HrItsDeptEmpFilter,'/allitsemdepfilter') # to get all employees based on department
api.add_resource(resources.AllInternalTimeSheetFilter,'/allitsfilter') # all timesheets filter
api.add_resource(resources.RepManOrNot,'/managerits') # to view project manager timesheets
api.add_resource(resources.TeamTsDropDown,'/teamtsdd')
api.add_resource(resources.EmployeeList, '/emplist')  
api.add_resource(resources.NewClientList, '/newclientlist')
api.add_resource(resources.ProjectsList, '/projectslist2')
api.add_resource(resources.AllEmpList,'/allemplist')
api.add_resource(resources.BillableActiveEmpList,'/allbillablemep')  
api.add_resource(resources.ManItsProFilter,'/manitsprofilter') 
api.add_resource(resources.ManItsEmpFilter,'/manitsempfilter')
api.add_resource(resources.EmpListPro,'/manemp')
api.add_resource(resources.ClientProjects, '/clientprojects')
api.add_resource(resources.InternalProjectTimeSheetFilter,'/itsfilter')   # to filter project and all timesheets
api.add_resource(resources.InternalTimeSheetApproval,'/itsapproval')  # to approve/reject timesheets
api.add_resource(resources.SaveInternalTImesheet,'/saveits')   # to save timesheets
api.add_resource(resources.ViewInternalTimesheet,'/viewits')   # to view timesheets in express entry
api.add_resource(resources.GetAllTimesheet,'/allits')    # to view all timesheets
api.add_resource(resources.EmployeeProjects,'/itsempprojects') # for drop down
api.add_resource(resources.DeleteTimesheets,'/deletets')

# All endpoints related to employee registration 2 ca
api.add_resource(resources.UserInvite, '/invite')
api.add_resource(resources.EmployeeSignUp, '/signup')


# consultant 11 ca
api.add_resource(resources.AddThirdPartyConsultant,'/addthirdpartycons')
api.add_resource(resources.ConsultantEdit, '/consultantdetailsedit')
api.add_resource(resources.ConsultantEmerEdit, '/consultantemerdetailsedit')
api.add_resource(resources.ConsultantList, '/consultantlist')
api.add_resource(resources.RecruiterList, '/recruiterlist')
api.add_resource(resources.ConsultantView, '/consultantview')
api.add_resource(resources.TotalConsultants, '/consultantcount')
api.add_resource(resources.ConsultantInactive,'/consinactive')
api.add_resource(resources.ValidateConsultantNumber, '/validateconsnumber')
api.add_resource(resources.ActivateConsultant, '/activatecon')


#employee 16
api.add_resource(resources.EmployeeBasicDetailsEdit, '/employeebasicdetailsedit')
api.add_resource(resources.EmploymentDetailsEdit,'/empdeteditoradd')
#api.add_resource(resources.Address1Edit, '/editadd1')
api.add_resource(resources.EmployeeListFilter, '/emplistfilter')
api.add_resource(resources.EmployeeInactive,'/empinactive')
api.add_resource(resources.UserLogoutAccess, '/logout')
api.add_resource(resources.ActivateEmployee, '/activateemp')
api.add_resource(resources.SendMailToken, '/resetlinkemail')
api.add_resource(resources.ResetPassword, '/resetpassword')
api.add_resource(resources.AllUsers, '/users')
api.add_resource(resources.EmployeeDetails, '/empdetails')   
api.add_resource(resources.EmployeeDetailsById, '/empdetailsbyid')
api.add_resource(resources.TotalEmployees, '/totalemp')
api.add_resource(resources.UserLogin, '/dashboard')
api.add_resource(resources.UpdatePaidTimeOff, '/updatepaidtimeoff')

#client 14 
api.add_resource(resources.AddClient, '/addclient')
api.add_resource(resources.EditClientContact, '/editclientcontact')
api.add_resource(resources.AddClientContact, '/addclientcontact')
api.add_resource(resources.DeleteClient, '/deleteclient')
api.add_resource(resources.ClientView, '/viewclient')
api.add_resource(resources.ClientResource, '/clientresource')
api.add_resource(resources.ActivateClient, '/activateclient')
api.add_resource(resources.TotalClients, '/totalclients')
api.add_resource(resources.ValidateClientName, '/validateclientname')
api.add_resource(resources.ClientContactInactivation, '/inactiveclientcontact')
#api.add_resource(resources.ClinetContractInactivation, '/inactiveclientcontract')


#project 9 c
api.add_resource(resources.AddProject, '/addproject')
api.add_resource(resources.ProjectEdit, '/editproject')
api.add_resource(resources.DeleteProject, '/deleteproject')
api.add_resource(resources.ResourceAssignment, '/assignresource')
api.add_resource(resources.ProjectView, '/viewproject')
api.add_resource(resources.ActivateProject, '/activatepro')
api.add_resource(resources.ProjectsList2, '/projectslist')              # to get project list based on login
api.add_resource(resources.ModifyPrimaryProject,'/modifyprimepro')
api.add_resource(resources.ValidateProjectName, '/validateprojectname')

#supplier  18 c
api.add_resource(resources.AddSupplier,'/addsupplier')
api.add_resource(resources.AddSupplierContact,'/addsuppliercontact')
api.add_resource(resources.SupplierDetailsEdit, '/supplierdetailsedit')
api.add_resource(resources.SupplierAddressEdit, '/supplieraddressedit')
api.add_resource(resources.SupplierContactEdit, '/suppliercontactedit') 
api.add_resource(resources.AddSupplierContact1,'/addsuppliercontact1')
api.add_resource(resources.GetAllSuppliers, '/getallsuppliers')
api.add_resource(resources.GetAllSuppliersForCons, '/getallsuppliersforcons')
api.add_resource(resources.SupplierView, '/supplierview')
api.add_resource(resources.ActivateSupplier, '/activatesupplier')
api.add_resource(resources.TotalSuppliers, '/suppliercount')
api.add_resource(resources.DeleteSupplier, '/inactivatesupplier')
api.add_resource(resources.ValidateSupplierName, '/validatesuppliername')
api.add_resource(resources.SupplierContactInactive, '/inactivatesuppliercontact')
api.add_resource(resources.SuppAddInactivation, '/inactivesuppadd')


# dashboard 19 c
api.add_resource(resources.HRDashboardBNBR, '/resourcescount')
api.add_resource(resources.HRDashboardNJ, '/newjoineescount')
api.add_resource(resources.HRDashboardFT, '/futureterminationscount')

api.add_resource(resources.ManagersList, '/managerslist')
api.add_resource(resources.ClientListForProject, '/clientsforproject')
api.add_resource(resources.ContractList, '/contractlist')
api.add_resource(resources.EmpProjects,'/empproject')
api.add_resource(resources.ProjectListEmp,'/empprojects')  
api.add_resource(resources.ProjectManagerDD,'/promanagerdd')
api.add_resource(resources.OwnClientsList,'/ownclientlist')
api.add_resource(resources.DesignationValidation, '/validatedesg')
api.add_resource(resources.LoginInvitation, '/logininvite') 
api.add_resource(resources.LoginEmail, '/loginemail')

api.add_resource(resources.TerminatedEmpList, '/terminatedemplist')
api.add_resource(resources.InviteAll, '/inviteall')  
api.add_resource(resources.Hemanth, '/u')  


if __name__ == "__main__":
    before_first_request()
    app.run(host='10.0.0.115', port=4000, debug=True,use_reloader=False)