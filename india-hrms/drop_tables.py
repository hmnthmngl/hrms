from run import db

from models import EmployeeInviteStatus,DashboardStats,PasswordMailStatus,RevokedTokenModel,SupplierAddressModel,SupplierContactModel,ConsultantIdModel,ThirdPartyConsultantModel,ConsultantContactModel,UserRolesModel,UserModel,EmployeePrimaryProjectModel,ClientContactModel,ProjectEmployeeModel,StaffingSupplierModel,Employee1Model,InternalTimesheetModel,ProjectModel,Address1Model,ClientDetailsModel


EmployeeInviteStatus.__table__.drop(db.engine)
PasswordMailStatus.__table__.drop(db.engine)
RevokedTokenModel.__table__.drop(db.engine)
ClientContactModel.__table__.drop(db.engine)
ProjectEmployeeModel.__table__.drop(db.engine)
ConsultantIdModel.__table__.drop(db.engine)
ConsultantContactModel.__table__.drop(db.engine)
ThirdPartyConsultantModel.__table__.drop(db.engine)
SupplierAddressModel.__table__.drop(db.engine)
SupplierContactModel.__table__.drop(db.engine)
StaffingSupplierModel.__table__.drop(db.engine)
EmployeePrimaryProjectModel.__table__.drop(db.engine)
InternalTimesheetModel.__table__.drop(db.engine)
ProjectModel.__table__.drop(db.engine)
DashboardStats.__table__.drop(db.engine)
ClientDetailsModel.__table__.drop(db.engine)
Employee1Model.__table__.drop(db.engine)
Address1Model.__table__.drop(db.engine)
UserRolesModel.__table__.drop(db.engine)
UserModel.__table__.drop(db.engine)

#db.drop_all()
