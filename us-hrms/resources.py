from flask_restful import Resource
from models import UserModel, RevokedTokenModel, VisaTypeModel, ProjectTypeModel,VacationWeeks,SickHours, \
    ClientvendorshipTypeModel, Employee1Model, ProjectEmployeeModel, RoleModel, PrivilegeModel, RolePrivilegeModel, \
    CertificationsModel, EducationModel, DepartmentModel, DesignationModel, WorkAuthorizationModel, EmploymentTypeModel, \
    GreenCardModel, EmployeeContactModel, CobraModel, BenefitsModel, \
    EmployeeVisaDetailsModel, DependentVisaModel, EadModel, SalaryModel, SalaryHistoryModel, ClientDetailsModel, \
    BussinessSectorModel, ClientVendorshipModel, ClientLocationModel, ClientContactModel, ProjectModel, \
    StaffingSupplierModel, SupplierContactModel, \
    ThirdPartyConsultantModel, UserRolesModel, TimeSheetModel, PayTypeListModel, PayrollFrequencyListModel, \
    EmergencyRelationModel, DependentRelationModel,EmpInactivationReasonDataModel, \
    CoverageTypeListModel, BenefitTypeListModel, EadTypeListModel, \
     WorkAuthorizationListModel, \
     LcaWageListModel, Address1Model, DependentContactModel, \
    EmployeeContactModel, UsCitizenPassportDetailsModel, \
    CountryModel, StateModel, CitiesModel, DegreeTypeListModel, HighestDegreeListModel,AttorneyModel,  \
    DiversityStatusListModel, HistoryLogModel,DashboardStats, \
     StaffingSupplierModel, SupplierAddressModel, SupplierContactModel,TerminationReasonDataModel,EmployeePrimaryProjectModel,SupplierContractModel, \
    ThirdPartyConsultantModel, ConsultantContactModel, ConsultantIdModel, \
    SupplierContactTypeModel, EmployeeBillRateModel,InternalTimesheetModel,InternalTimesheetTypesModel, EmployeeInviteStatus,PasswordMailStatus,CertificationSchema
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,get_jwt_identity, get_raw_jwt)
from flask import request, jsonify  
import uuid
import run
from run import mail,db
from flask_mail import Message
from passlib.hash import pbkdf2_sha256 as sha256
import base64
import itertools
import logging
import os, csv, pandas as pd, json
from sqlalchemy.sql import func
from flask_expects_json import expects_json
from datetime import datetime, timedelta ,date
from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
import marshmallow.validate
import re
from marshmallow import Schema, fields, post_load, ValidationError, validates, validate, EXCLUDE,INCLUDE
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import calendar  
import time
from sqlalchemy.sql import operators, extract
from sqlalchemy import and_, or_
from sqlalchemy import exc
from functools import wraps

#logging.basicConfig(filename='server.log',level=logging.INFO,format='%(asctime)s-%(name)s-%(message)s')
#logging.basicConfig(level=logging.INFO,format='%(asctime)s-%(name)s-%(message)s')
static_data_instance = run.before_first_request()

def prefix_decorator(class_name):
    def decorated_function(original_function):
        @wraps(original_function)
        def wrapper_function(*arsg,**kwargs):
            try:
                logging.info('Zebra {0}'.format(class_name))
                result = original_function(*arsg,**kwargs)
                return result
            except KeyError as e:
                logging.error("Error, Please Provide {0}".format(str(e).replace("'",'')))
                return {"errorMessage": "Error, Please Provide {0}".format(str(e).replace("'",''))},500
            except exc.ProgrammingError as e:
                logging.error("Invalid {0}".format(re.sub(r"[\d_]"," ",list(e.params.keys())[0]).strip()))
                db.session.rollback()
                return {"errorMessage": "Invalid {0}".format(re.sub(r"[\d_]"," ",list(e.params.keys())[0]).strip())},500
            except exc.IntegrityError as e:
                logging.error("Error, {0} Already Exists".format(e.params[list(e.params.keys())[0]]))
                db.session.rollback()
                return {"errorMessage": "Error, {0} Already Exists".format(e.params[list(e.params.keys())[0]])},500 
            except Exception as e:
                logging.exception('Exception Occured , {0}'.format(str(e)))
                db.session.rollback()
                return {"errorMessage":str(e)},500
            finally:
                db.session.close()
        return wrapper_function
    return decorated_function


class VacationWeeksDropDowns(Resource):
    def get(self):
        return {"vacation_weeks":[i.vacation_weeks for i in VacationWeeks.query.all()]}

class SickHoursDropDowns(Resource):
    def get(self):
        return {"sick_hours":[i.sick_hours for i in SickHours.query.all()]}


class HighestDegreeDropDowns(Resource):
    def get(self):
        return {"highest_types": static_data_instance['highest_degree_map']}

class InactiveReasonDD(Resource):
    def get(self):
        return static_data_instance['inactive_reason_map']

class DependentRelationType(Resource):
    def get(self):
        return {"dependent_relation_type": static_data_instance['dependent_relation_map']}
        
class DegreeTypeDropDowns(Resource):
    def get(self):
        return {"degree_types": static_data_instance['degree_type_map']}
        
class GetCountries(Resource):
    def get(self):
        return static_data_instance['countries_map']
        
class VisaType(Resource):
    def get(self):
        return {"visa_type": static_data_instance['visa_types_map']}
        
class EmergencyRelationType(Resource):
    def get(self):
        return {"emergency_relation_type": static_data_instance['emergency_relation_map']}
        
class InternalTimesheetTypeDropDown(Resource):
    def get(self):
        return {"its_types": static_data_instance['ts_types_map']}
        
class EdubranchDropDowns(Resource):
    def get(self):
        return static_data_instance['edu_branch_map']
        
class EmploymentType(Resource):
    def get(self):
        return {"employment_types": static_data_instance['employment_type_map']}
        
class DepartmentList(Resource):
    def get(self):
        return static_data_instance['department_map']
        
class LcaWageDropDowns(Resource):
    def get(self):
        return {"wage_type": static_data_instance['lca_wagelist_map']}
        
class PayTypesDropDowns(Resource):
    def get(self):
        return {"pay_type": static_data_instance['pay_types_map']}
        
class Terminationdd(Resource):
    def get(self):
        return static_data_instance['termination_reason_map']
        
class AttorneyDropDown(Resource):
    def get(self):
        return {"attorney": static_data_instance['attorney_map']}
        
        
class WorkAuthType(Resource):
    def get(self):
        return {"auth_type": static_data_instance['work_auth_type_map']}


class DesignationType(Resource):
    def get(self):
        return {"designation_type": static_data_instance['designation_map']}


class DiversityStatusType(Resource):
    def get(self):
        return {"diversity_status_types": static_data_instance['diversity_status_map']}
        
class PayrollDropDowns(Resource):
    def get(self):
        return {"pay_roll_frequency": static_data_instance['payroll_frequency_map']}


class CoverageTypesDropDowns(Resource):
    def get(self):
        return {"coverage_types": static_data_instance['coverage_type_map'] }


class BenefitsType(Resource):
    def get(self):
        return {"benefit_types": static_data_instance['benefit_type_map']}

class EadType(Resource):
    def get(self):
        return {"ead_type": static_data_instance['ead_type_map']}
        
        
class ProjectTypeList(Resource):
    def get(self):
        return static_data_instance['project_type_map']
        
class VendorsTypeList(Resource):
    def get(self):
        return static_data_instance['vendorship_type_map']


class BussinessSectorTypeList(Resource):
    def get(self):
        return static_data_instance['business_sector_map']

class PrivilegesList(Resource):
    def get(self):	 
        return static_data_instance['privileges_map']

class SupplierConType(Resource):
    def get(self):	
            return {"contact_type": static_data_instance['supplier_contact_type_map']}



# Static Resources

class GetAllCertifications(Resource):
    def get(self): 
        certifications = CertificationsModel.query.all()
        certification_schema = CertificationSchema(many=True)
        output = certification_schema.dump(certifications)
        return jsonify({'certifications' : output})
 
class BenefitsCount(Resource):
    @jwt_required
    @prefix_decorator('BenefitsCount')
    def get(self): 
        # for benefits in array
        # health = len(set(list(itertools.chain.from_iterable([BenefitsModel.query.filter(BenefitsModel.benefit_types.contains(['1']),BenefitsModel.coverage_type_id!=5).all(),BenefitsModel.query.filter(BenefitsModel.benefit_types.contains(['2']),BenefitsModel.coverage_type_id!=5).all(),BenefitsModel.query.filter(BenefitsModel.benefit_types.contains(['3']),BenefitsModel.coverage_type_id!=5).all()]))))
        # medical = BenefitsModel.query.filter(BenefitsModel.benefit_types.contains(['1']),BenefitsModel.coverage_type_id!=5).count()
        # dental = BenefitsModel.query.filter(BenefitsModel.benefit_types.contains(['2']),BenefitsModel.coverage_type_id!=5).count()
        # vision = BenefitsModel.query.filter(BenefitsModel.benefit_types.contains(['3']),BenefitsModel.coverage_type_id!=5).count()
        # for individual benefits 
        health = BenefitsModel.query.filter(BenefitsModel.benefit_type_id.in_([x.benefit_type_id for x in BenefitTypeListModel.query.filter(BenefitTypeListModel.benefit_type.in_(['Medical','Dental','Vision']),BenefitsModel.coverage_type_id!=5)]),BenefitsModel.employee_id.in_([y.employee_id for y in Employee1Model.query.filter_by(employee_status='A')])).distinct(BenefitsModel.employee_id).count()
        medical = BenefitsModel.query.filter(BenefitsModel.benefit_type_id == 1,BenefitsModel.coverage_type_id!=5,BenefitsModel.employee_id.in_([y.employee_id for y in Employee1Model.query.filter_by(employee_status='A')])).count()
        dental = BenefitsModel.query.filter(BenefitsModel.benefit_type_id == 2,BenefitsModel.coverage_type_id!=5,BenefitsModel.employee_id.in_([y.employee_id for y in Employee1Model.query.filter_by(employee_status='A')])).count()
        vision = BenefitsModel.query.filter(BenefitsModel.benefit_type_id == 3,BenefitsModel.coverage_type_id!=5,BenefitsModel.employee_id.in_([y.employee_id for y in Employee1Model.query.filter_by(employee_status='A')])).count()
        waived = BenefitsModel.query.filter_by(coverage_type_id = (CoverageTypeListModel.query.filter_by(coverage_type='Waived').first().coverage_type_id)).distinct(BenefitsModel.employee_id).count()
        medical_waived = BenefitsModel.query.filter(BenefitsModel.coverage_type_id == (CoverageTypeListModel.query.filter_by(coverage_type='Waived').first().coverage_type_id),BenefitsModel.benefit_type_id == 1).count()
        dental_waived = BenefitsModel.query.filter(BenefitsModel.coverage_type_id == (CoverageTypeListModel.query.filter_by(coverage_type='Waived').first().coverage_type_id),BenefitsModel.benefit_type_id == 2).count()
        vision_waived = BenefitsModel.query.filter(BenefitsModel.coverage_type_id == (CoverageTypeListModel.query.filter_by(coverage_type='Waived').first().coverage_type_id),BenefitsModel.benefit_type_id == 3).count()
        added = BenefitsModel.query.filter(extract('month', BenefitsModel.benefit_start_date)== datetime.today().month,extract('year', BenefitsModel.benefit_start_date) == datetime.today().year).count()
#        termination = Employee1Model.query.filter(extract('day', Employee1Model.termination_date)>= datetime.today().day,extract('month', Employee1Model.termination_date)== datetime.today().month,extract('year', Employee1Model.termination_date) == datetime.today().year).count()
        termination = Employee1Model.query.filter(extract('day', Employee1Model.termination_date)>= datetime.today().day,extract('month', Employee1Model.termination_date)== datetime.today().month,extract('year', Employee1Model.termination_date) == datetime.today().year,Employee1Model.employee_id.in_([x.employee_id for x in BenefitsModel.query.filter(BenefitsModel.benefit_type_id.in_([x.benefit_type_id for x in BenefitTypeListModel.query.filter(BenefitTypeListModel.benefit_type.in_(['Medical','Dental','Vision']),BenefitsModel.coverage_type_id!=5)])).all()])).count()
        on_cobra = CobraModel.query.filter(CobraModel.end_date>=datetime.today()).count()
        logging.info("Success")
        emp = [x.employee_id for x in BenefitsModel.query.filter(BenefitsModel.benefit_type_id.in_([x.benefit_type_id for x in BenefitTypeListModel.query.filter(BenefitTypeListModel.benefit_type.in_(['Medical','Dental','Vision']),BenefitsModel.coverage_type_id!=5)])).all()]
        print({'health_benefits':health, 'medical':medical, 'dental':dental, 'vision':vision, 'medical_waived':medical_waived, 'dental_waived': dental_waived, 'vision_waived':vision_waived, 'benefits_waived':waived,'added':added,'termination':termination,'on_cobra':on_cobra})
        return {'health_benefits':health, 'medical':medical, 'dental':dental, 'vision':vision, 'medical_waived':medical_waived, 'dental_waived': dental_waived, 'vision_waived':vision_waived, 'benefits_waived':waived,'added':added,'termination':termination,'on_cobra':on_cobra}


class ClientContracts(Resource):
    @jwt_required
    @prefix_decorator('ClientContracts')
    def get(self): 
        logging.info("Success")
        return ClientDetailsModel.return_contracts()

class SupplierContracts(Resource):
    @jwt_required
    @prefix_decorator('SupplierContracts')
    def get(self): 
        logging.info("Success")
        return StaffingSupplierModel.supplier_contracts()

class HRDashboardBNBR(Resource):
    @prefix_decorator('HRDashboardBNBR')
    def get(self):
        total_employees = Employee1Model.query.filter_by(employee_status='A').count()
        billable = Employee1Model.query.filter(Employee1Model.billable_resource=='Y',Employee1Model.employee_status=='A').count()
        non_billable = total_employees-billable
        on_billing = list(set(list(itertools.chain.from_iterable([i.employee_id for i in ProjectEmployeeModel.query.all()]))))
        on_billing = len([i for i in on_billing if not 'CON' in str(i)])
        on_bench = billable-on_billing
        todays_date = date.today()
        tomorrow_date = todays_date+timedelta(1)
        all_records = DashboardStats.query.order_by(DashboardStats.date.asc()).all()
        if all_records:
            if [todays_date.month,todays_date.year] == [all_records[-1].date.month,all_records[-1].date.year]:
                record = DashboardStats.query.filter_by(stat_id=all_records[-1].stat_id).first()
                record.date = todays_date
                record.billable = billable
                record.on_billing = on_billing
                record.on_bench = on_bench
                record.total_employees = total_employees
                record.non_billing = non_billable
                record.save_to_db()
                data = DashboardStats.return_all()
                logging.info("Success")
                return {"resources_count": data[::-1][0:13]} 
            else:
                record = DashboardStats(date=todays_date,billable=billable,on_billing=on_billing,on_bench=on_bench,total_employees=total_employees,non_billing=non_billable)
                record.save_to_db()
                data = DashboardStats.return_all()
                logging.info("Success")
                return {"resources_count": data[::-1][0:13]} 
        else:
            record = DashboardStats(date=todays_date,billable=billable,on_billing=on_billing,on_bench=on_bench,total_employees=total_employees,non_billing=non_billable)
            record.save_to_db()
            data = DashboardStats.return_all()
            logging.info("Success")
            return {"resources_count": data[::-1][0:13]} 

class HRDashboardNJ(Resource):
    @prefix_decorator('HRDashboardNJ')
    def get(self): 
        present_date = datetime.now().strftime("%m-%d-%Y")
        date = present_date.split('-')[0] + '-' +'01'+ '-' + present_date.split('-')[-1]
        x = Employee1Model.return_all_for_new_hires(date)
        logging.info("Success")
        return Employee1Model.return_all_for_new_hires(date) 


class VisaCount(Resource):
    @jwt_required
    @prefix_decorator('VisaCount')
    def get(self):
        h1_visa = EmployeeVisaDetailsModel.query.filter_by(visa_type_id=(VisaTypeModel.query.filter_by(visa_type='H1').first().visa_type_id)).count()
        us_citizen = Employee1Model.query.filter_by(us_citizen=True).count()
        green_card = GreenCardModel.query.count()
        logging.info("Success")
        return {"h1_visa":h1_visa, "us_citizen":us_citizen, "green_card":green_card}

class H1Renewals(Resource):
    @prefix_decorator('H1Renewals')
    def get(self):	
        todays_date = datetime.now()
        start_date = todays_date.strftime("%m-%d-%Y")
        end_date = (todays_date+ relativedelta(months=6)).strftime("%m-%d-%Y")
        logging.info("success")
        return EmployeeVisaDetailsModel.return_h1_renewals(start_date=start_date,end_date=end_date)

class RepManOrNot(Resource):
    @jwt_required
    @prefix_decorator('RepManOrNot')
    def get(self):	 
        username = get_jwt_identity() 
        emp_id  = Employee1Model.query.filter_by(email_id=username).first()  
        if not emp_id:
            logging.debug("Employee does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id ).first()
        repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).first()
        print(pro_manager,repo_manager)
        if pro_manager and repo_manager:
            projects = [i.project_id for i in ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id ).all()]
            a = []
            for i in projects:
                b = ProjectEmployeeModel.query.filter_by(project_id=i).first()
                if b:
                    a.append(b.employee_id)
            project_employees = list(set([int(j) for i in a for j in i]))
            reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all()]))
            z = project_employees + reporting_employees
            return InternalTimesheetModel.return_all_reporting_manager(z,emp_id.employee_id )
        elif pro_manager:
            pid = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id ).all()
            list_ts = []
            b = []
            for i in pid:
                list_ts.append(InternalTimesheetModel.return_all_by_pro(i.project_id,emp_id.employee_id ))
            for j in list_ts:
                b.append(j["timeSheets"])
            return {"timeSheets": list(filter((None).__ne__, list(itertools.chain.from_iterable(b)))) }
        elif repo_manager:
            employees = [i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all()]
            return InternalTimesheetModel.return_all_reporting_manager(employees,emp_id.employee_id )
        else:
            return {"timeSheets": []}

class ManItsProFilter(Resource):
    @jwt_required
    @prefix_decorator('ManItsProFilter')
    def get(self): 
        username = get_jwt_identity() 
        emp_id = Employee1Model.query.filter_by(email_id=username).first()  
        if emp_id: 
            pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id).all()
            repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id).first()
            if (pro_manager and repo_manager) or pro_manager:
                projects = [i.project_id for i in pro_manager]
                return ProjectModel.find_by_pro_list(projects)  
            else:
                return {"Projects":[]}
        else:
            logging.debug("Employee Does Not Exist")
            return {'errorMessage':'Employee Does Not Exists'}

class ProjectManagerDD(Resource):
    @jwt_required
    @prefix_decorator('ProjectManagerDD')
    def get(self):	
        username = get_jwt_identity()
        logging.info("Success")
        return Employee1Model.return_all_active_projectfor()

class ManItsEmpFilter(Resource):
    @jwt_required
    @prefix_decorator('ManItsEmpFilter')
    def get(self):
        print('hemanth')
        username = get_jwt_identity() 
        emp_id  = Employee1Model.query.filter_by(email_id=username).first()
        if not emp_id:
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"} 
        pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id.employee_id).all()
        repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).first()
        print(pro_manager,repo_manager,'---------------------------------------')
        projects = [i.project_id for i in pro_manager]
        if (pro_manager and repo_manager):
            print('entered')
            a = []
            for i in projects:
                b = ProjectEmployeeModel.query.filter_by(project_id=i).first()
                if b:
                    a.append(b.employee_id)
            project_employees = list(set([int(j) for i in a for j in i]))
            reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all()]))
            z = list(set(project_employees + reporting_employees))
            z = [str(i) for i in z if i != emp_id.employee_id]
            return Employee1Model.return_all_emps(list(set(z)))
        elif pro_manager:
            print('entered pro_manager')
            a = []
            for i in projects:
                employees = ProjectEmployeeModel.query.filter_by(project_id=i).first()
                if employees:
                    a.append(employees.employee_id)
            print(a)
            b = list(set([j for i in a for j in i if j != str(emp_id.employee_id)]))
            print(b,emp_id.employee_id,'*****************')
            return Employee1Model.return_all_emps(b)
        elif repo_manager:
            employees = [str(i.employee_id) for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id.employee_id ).all() if i.employee_id != emp_id.employee_id]
            return Employee1Model.return_all_emps(list(set(employees)))
        else:
            return {"Employees":[]}

class HrItsEmployeeFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsEmployeeFilter')
    def get(self):
        username = get_jwt_identity() 
        return Employee1Model.return_all_active_projectfor_et1(username)

#to get all time sheets
class GetAllTimesheet(Resource):
    @prefix_decorator('GetAllTimesheet')
    def get(self):
        return InternalTimesheetModel.return_all()

# to get project timesheets
class InternalTeamTimeSheet(Resource):
    @jwt_required
    @prefix_decorator('InternalTeamTimeSheet')
    def get(self):	
        username = get_jwt_identity()
        user = UserModel.query.filter_by(username=username).first().userid
        pid = ProjectModel.query.filter_by(project_manager_id=user).all()
        list_ts = []
        b = []
        for i in pid:
            list_ts.append(InternalTimesheetModel.return_all_by_pro(i.project_id,user))
        for j in list_ts:
            b.append(j["timeSheets"])
        return {"timeSheets": list(filter((None).__ne__, list(itertools.chain.from_iterable(b)))) }

class ProjectListEmp(Resource):
    @jwt_required
    @prefix_decorator('ProjectListEmp')
    def get(self):	 
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employeeid = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        managerprojects = ProjectModel.query.filter_by(project_manager_id=employeeid).all()
        if managerprojects:
            logging.info("Success")
            return {"projects_list": [i.project_name for i in managerprojects]}

class TotalConsultants(Resource):
    @prefix_decorator('TotalConsultants')
    def get(self):
        total_consultants = ThirdPartyConsultantModel.query.count()
        active_consultants = ThirdPartyConsultantModel.query.filter_by(consultant_status='A').count()
        inactive_consutants = ThirdPartyConsultantModel.query.filter(ThirdPartyConsultantModel.consultant_status.in_(['I','T'])).count()
        logging.info('Success')
        return {"total_consultants": total_consultants, "active_consultants": active_consultants, "inactive_consultants": inactive_consutants}

class TotalSuppliers(Resource):
    @prefix_decorator('TotalSuppliers')
    def get(self): 
        total_suppliers = StaffingSupplierModel.query.count()
        active_suppliers = StaffingSupplierModel.query.filter_by(contract_status='A').count()
        inactive_suppliers = StaffingSupplierModel.query.filter(StaffingSupplierModel.contract_status.in_(['I','T'])).count()
        logging.info("Success")
        return {"total_suppliers": total_suppliers, "active_suppliers": active_suppliers, "inactive_suppliers": inactive_suppliers}

class ConsultantList(Resource):
    @prefix_decorator('ConsultantList')
    def get(self):	
        return ThirdPartyConsultantModel.return_all_consultants()

class RecruiterList(Resource):
    @prefix_decorator('RecruiterList')
    def get(self):	 
        return Employee1Model.return_all_recruiters(Employee1Model.query.filter_by(designation_id=3,employee_status='A').order_by(Employee1Model.first_name.asc()).all())

class GetAllSuppliers(Resource):
    @jwt_required
    @prefix_decorator('GetAllSuppliers')
    def get(self):
        return StaffingSupplierModel.return_all_suppliers()

class GetAllSuppliersForCons(Resource):
    @jwt_required
    @prefix_decorator('GetAllSuppliersForCons')
    def get(self):
        return StaffingSupplierModel.return_all_suppliers_cons()

class AllSuppliers(Resource):
    @jwt_required
    @prefix_decorator('AllSuppliers')
    def get(self): 
        return StaffingSupplierModel.return_all_suppliers_for_consultants()

class TotalEmployees(Resource):
    @prefix_decorator('TotalEmployees')
    def get(self): 
        total_employees = Employee1Model.query.count()
        active_employees = Employee1Model.query.filter_by(employee_status='A').count()   
        inactive_employees = Employee1Model.query.filter(Employee1Model.employee_status.in_(['I','T'])).count()
        logging.info("Success")
        return {"total_employees": total_employees, "active_employees": active_employees, "inactive_employees": inactive_employees}

class ProjectsList2(Resource):
    @jwt_required
    @prefix_decorator('ProjectsList2')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employee_id = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        manager_id = ProjectModel.query.filter_by(project_manager_id=employee_id).all()
        project_creator = ProjectModel.query.filter_by(created_by=employee_id).all()
        x = ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(employee_id)])).all()
        projects = []
        for i in manager_id:
            projects.append(i.project_id)
        for i in project_creator:
            projects.append(i.project_id)
        for i in x:
            projects.append(i.project_id)
        projects = list(set(projects))
        complete_data = ProjectModel.for_projects_list(projects)
        total_projects = len(projects)
        complete_data.sort(key=lambda x:x.get('modified_time'),reverse=True)
        complete_data.sort(key=lambda x:x.get('project_status'))
        count = 0
        for i in complete_data:
            if i['project_status'] == 'Active':
                count += 1
        logging.info("Success")
        return {"projects":complete_data,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}

class OwnClientsList(Resource):
    @jwt_required
    @prefix_decorator('OwnClientsList')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employee_id = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        logging.info("Success")
        return ProjectEmployeeModel.return_all_clients(employee_id)

class ManagersList(Resource):
    @prefix_decorator('ManagersList')
    def get(self):	
        manager_role_id = RoleModel.find_by_role_name('Manager')
        roles = UserRolesModel.query.all()
        managers_id_list = []
        for i in roles:
            if str(manager_role_id) in i.roles:
                managers_id_list.append(i.user_id)
        logging.info("Success")
        return Employee1Model.return_all_managers(managers_id_list)

class AllUsers(Resource):
    @jwt_required
    @prefix_decorator('AllUsers')
    def get(self): 
        username = get_jwt_identity()
        if not username:
            logging.debug('User {} Does Not Exist'.format(username))
            return {'message': 'User {} Does Not Exist'.format(username)}
        return UserModel.return_all()

class RolesList(Resource):
    @jwt_required
    @prefix_decorator('RolesList')
    def get(self): 
        return RoleModel.return_all()

class UserLogin(Resource):
    @jwt_required
    @prefix_decorator('UserLogin')
    def get(self):
        username = get_jwt_identity()
        userid = UserModel.query.filter_by(username=username).first()
        current_emp = Employee1Model.find_by_employee_id(userid.userid)
        present_address1 = Address1Model.query.filter_by(address_id=current_emp.present_address_id).first()
        permanent_address1 = Address1Model.query.filter_by(address_id=current_emp.permanent_address_id).first()
        work_address1 = Address1Model.query.filter_by(address_id=current_emp.work_address_id).first()
        de_ssn = str(base64.b64decode(current_emp.ssn).decode('ascii'))
        dob = str(base64.b64decode(current_emp.date_of_birth).decode('ascii'))
        emp_designation = DesignationModel.find_by_designation_id(current_emp.designation_id)
        emp_department = DepartmentModel.find_by_department_id(current_emp.department_id)
        emp_type = EmploymentTypeModel.find_by_emp_type_id(current_emp.emp_type_id)
        empnumber = current_emp.employee_id
        emp_certification = CertificationsModel.return_all(empnumber)
        emp_education = EducationModel.return_all(current_emp.employee_id)
        emp_work_auth = WorkAuthorizationModel.find_by_employeenumber(empnumber)
        green_card = GreenCardModel.find_by_employeenumber(empnumber)
        emp_cobra = CobraModel.find_by_employeenumber(empnumber)
        emp_benefits = BenefitsModel.find_by_employeenumber(empnumber)
        emp_ead = EadModel.find_by_employeenumber(empnumber)
        emp_visa = EmployeeVisaDetailsModel.find_by_employeenumber(empnumber)
        emp_salary = SalaryModel.find_by_employeenumber(empnumber)
        emergency_contact = EmployeeContactModel.return_all1(empnumber)['users']
        dependent_contact = DependentContactModel.return_all1(empnumber)['users']
        proemp = ProjectEmployeeModel.return_all_empprojects(current_emp.employee_id)
        reporting_manager_name = Employee1Model.query.filter_by(employee_id=current_emp.reporting_manager_id).first()
        recruiter = current_emp.recruiter
        reporting_manager = reporting_manager_name.first_name + ' ' +  reporting_manager_name.last_name if reporting_manager_name else ''
        termination = TerminationReasonDataModel.query.filter_by(termination_reason_id=current_emp.termination_reason_id).first()
        termination_reason = termination.termination_reason if termination else ''
        termination_intent = termination.termination_intent if termination else ''
        termination_id = current_emp.termination_reason_id
        inactive_reason = EmpInactivationReasonDataModel.query.filter_by(emp_inactive_reason_id = current_emp.inactive_reason_id).first()
        benefits = BenefitsModel.return_all(empnumber)

        if current_emp.employment_end_date:
            employment_end_date = str(current_emp.employment_end_date)
        else:
            employment_end_date = ''

        if work_address1:
            work_address_id = work_address1.address_id
            work_address_line1 = work_address1.address_line1
            work_address_line2 = work_address1.address_line2
            work_city = work_address1.city
            work_state_name = work_address1.state_name
            work_country = work_address1.country
            work_zip_code = work_address1.zip_code

        else:
            work_address_id = None
            work_address_line1 = None
            work_address_line2 = None
            work_city = None
            work_state_name = None
            work_country = None
            work_zip_code = None


        if current_emp.us_citizen == True:
            record = UsCitizenPassportDetailsModel.query.filter_by(employee_id=current_emp.employee_id).first()
            if record:
                passport_num = record.passport_number
                pass_start_date = record.start_date
                pass_end_date = record.end_date
            else:
                passport_num = ''
                pass_start_date = ''
                pass_end_date = ''
        else:
            passport_num = ''
            pass_start_date = ''
            pass_end_date = ''


        if emp_designation:
            emp_designation = emp_designation.designation_name
        else:
            emp_designation = None

        if emp_department:
            emp_department = emp_department.department_name
        else:
            emp_department = None

        if emp_type:
            emp_type = emp_type.emp_type
        else:
            emp_type = None
        if emp_work_auth:
            auth_type = emp_work_auth.auth_type
            auth_start_date = str(emp_work_auth.auth_start_date)
            auth_end_date = str(emp_work_auth.auth_end_date)
        else:
            auth_type = None
            auth_start_date = None
            auth_end_date = None

        if emp_salary:
            salary_pay_type = PayTypeListModel.query.filter_by(pay_type_id=emp_salary.pay_type_id).first().pay_type
            salary_start_date = str(emp_salary.salary_start_date)
            if emp_salary.salary_end_date:
                salary_end_date = str(emp_salary.salary_end_date)
            else:
                salary_end_date = ''
            pay_amount = emp_salary.pay_amount
            payroll_frequency = PayrollFrequencyListModel.query.filter_by(pay_freq_id=emp_salary.pay_freq_id).first().pay_frequency
            if emp_salary.last_rise_date:
                last_rise_date = str(emp_salary.last_rise_date)
            else:
                last_rise_date = ''

            salary_last_rise_amount = emp_salary.salary_last_rise_amount
        else:
            salary_pay_type = ''
            salary_start_date = ''
            salary_end_date = ''
            pay_amount = ''
            payroll_frequency = ''
            last_rise_date = ''
            salary_last_rise_amount = ''

        if green_card:
            gc_job_title = green_card.gc_job_title
            gc_salary = green_card.gc_salary
            gc_prevailing_wage = green_card.gc_prevailing_wage
            gc_priority_date = green_card.priority_date
            gc_current_status = green_card.current_status
            gc_approval_status = green_card.approval_status
        else:
            gc_job_title = ''
            gc_salary = ''
            gc_prevailing_wage = ''
            gc_priority_date = ''
            gc_current_status = ''
            gc_approval_status = ''

        if emp_cobra:
            cobra_start_date = str(emp_cobra.start_date)
            cobra_end_date = str(emp_cobra.end_date)
            cobra_provider_name = emp_cobra.provider_name
            cobra_amount_covered = emp_cobra.amount_covered
            cobra_coverage_details = emp_cobra.coverage_details
        else:
            cobra_start_date = ''
            cobra_end_date = ''
            cobra_provider_name = ''
            cobra_amount_covered = ''
            cobra_coverage_details = ''

#        if emp_benefits:
#            emp_coverage_type = CoverageTypeListModel.query.filter_by(coverage_type_id=emp_benefits.coverage_type_id).first().coverage_type if emp_benefits.coverage_type_id else ''
#            emp_benefit_start_date = emp_benefits.benefit_start_date
#            emp_benefit_status = emp_benefits.benefit_status
#            try: 
#                benefit_types = [BenefitTypeListModel.query.filter_by(benefit_type_id=i).first().benefit_type for i in emp_benefits.benefit_types]
#            except Exception as e:
#                logging.exception('Exception Occured , {0}'.format(str(e)))
#                benefit_types = []
#                
#        else:
#            benefit_types = ''
#            emp_coverage_type = ''
#            emp_benefit_start_date = ''
#            emp_benefit_status = ''
        if emp_ead:
            emp_ead_type = EadTypeListModel.query.filter_by(ead_type_id=emp_ead.ead_type_id).first().ead_type
            emp_valid_from = emp_ead.valid_from
            emp_valid_to = emp_ead.valid_to
            emp_sponsor_name = emp_ead.sponsor_name
        else:
            emp_ead_type = ''
            emp_valid_from = ''
            emp_valid_to = ''
            emp_sponsor_name = ''

        if emp_visa:
            visa_type = VisaTypeModel.find_by_employeenumber(emp_visa.visa_type_id)
            lca_add = Address1Model.query.filter_by(address_id=emp_visa.lca_work_address_id).first()
            lca_altadd = Address1Model.query.filter_by(address_id=emp_visa.lca_alternate_address_id).first()
            visa_number = emp_visa.visa_number
            job_title = emp_visa.job_title
            visa_start_date = emp_visa.visa_start_date
            visa_end_date = emp_visa.visa_end_date
            visa_salary = emp_visa.visa_salary
            visa_title = emp_visa.visa_title
            lca_salary = emp_visa.lca_salary
            attorney = emp_visa.attorney if emp_visa.attorney else ''
            lca_wage_level = LcaWageListModel.query.filter_by(lca_wage_level_id=emp_visa.lca_wage_level_id).first().lca_wage_level if emp_visa.lca_wage_level_id else ''
            lca_prevailing_wages = emp_visa.lca_prevailing_wages
            if emp_visa.lca_start_date:
                lca_start_date = str(emp_visa.lca_start_date)
            else:
                lca_start_date = ''
            if emp_visa.lca_end_date:
                lca_end_date = str(emp_visa.lca_end_date)
            else:
                lca_end_date = ''
            current_status = 'Active' if emp_visa.current_status=='A' else 'Inactive' if emp_visa.current_status=='I' else 'Terminated' if emp_visa.current_status=='T' else ''
            if lca_add:
                lca_add_id = lca_add.address_id
                lca_add_line1 = lca_add.address_line1
                lca_add_line2 = lca_add.address_line2
                lca_add_city = lca_add.city
                lca_add_state = lca_add.state_name
                lca_add_zip = lca_add.zip_code
                lca_add_country = lca_add.country
            else:
                lca_add_id = ''
                lca_add_line1 = ''
                lca_add_line2 = ''
                lca_add_city = ''
                lca_add_state = ''
                lca_add_zip = ''
                lca_add_country = ''

            if lca_altadd:
                lca_altadd_id = lca_altadd.address_id
                lca_altadd_line1 = lca_altadd.address_line1
                lca_altadd_line2 = lca_altadd.address_line2
                lca_altadd_city = lca_altadd.city
                lca_altadd_state = lca_altadd.state_name
                lca_altadd_zip = lca_altadd.zip_code
                lca_altadd_country = lca_altadd.country
            else:
                lca_altadd_id = ''
                lca_altadd_line1 = ''
                lca_altadd_line2 = ''
                lca_altadd_city = ''
                lca_altadd_state = ''
                lca_altadd_zip = ''
                lca_altadd_country = ''


        else:
            visa_type = ''
            visa_number = ''
            job_title = ''
            visa_start_date = ''
            visa_end_date = ''
            visa_salary = '' 
            attorney = ''
            visa_title = ''
            lca_work_location = ''
            lca_alternate_location = ''
            lca_salary = ''
            lca_wage_level = ''
            lca_prevailing_wages = ''
            lca_start_date = ''
            lca_end_date = ''
            current_status = ''
            lca_add_id = ''
            lca_add_line1 = ''
            lca_add_line2 = ''
            lca_add_city = ''
            lca_add_state = ''
            lca_add_zip = ''
            lca_add_country = ''
            lca_altadd_id = ''
            lca_altadd_line1 = ''
            lca_altadd_line2 = ''
            lca_altadd_city = ''
            lca_altadd_state = ''
            lca_altadd_zip = ''
            lca_altadd_country = ''

        return { 
            "benefits" :benefits,
            "inactive_reason_id" : inactive_reason.emp_inactive_reason if inactive_reason else '',
            "same_as_ca":current_emp.same_as_ca if current_emp.same_as_ca else '' ,
            "resource_billable":"Yes" if current_emp.billable_resource == 'Y' else "No", 
            "attorney_name":attorney,
            "termination_date":str(current_emp.termination_date),
            "comments" : current_emp.comments,
            "employee_status_last_updated_date":str(current_emp.employee_status_last_updtd_date).split(' ')[0] if current_emp.employee_status_last_updtd_date else '',
            'employee_id': current_emp.employee_id,
            'employee_number': current_emp.emp_number,
            'first_name': current_emp.first_name,
            'middle_name': current_emp.middle_name,
            'last_name': current_emp.last_name,
            'reporting_manager_name': reporting_manager,
            'recruiter_name': recruiter,
            'reporting_manager_id': current_emp.reporting_manager_id,
            'email': current_emp.email_id.lower(),
            'date_of_birth': 'xx-xx-xxxx',
            'ssn': 'xxx-xx-xxxx',   
            'gender': 'Male' if current_emp.gender=='M' else 'Female' if current_emp.gender=='F' else 'Other' if current_emp.gender=='O' else '',
            'project_details': proemp['employee_projects'],
            'primary_phone_number': current_emp.primary_phone_number,
            'alternate_phone_number': current_emp.alternate_phone_number,
            'marital_status': 'Single' if current_emp.marital_status=='S' else 'Married' if current_emp.marital_status=='M' else 'Divorced' if current_emp.marital_status=='D' else 'Widowed' if current_emp.marital_status=='W' else '',
            'present_add_id': present_address1.address_id,
            'present_line1': present_address1.address_line1,
            'present_line2': present_address1.address_line2,
            'present_city': present_address1.city,
            'present_state': present_address1.state_name,
            'present_country': present_address1.country,
            'present_zip': present_address1.zip_code,
            'permanent_add_id': permanent_address1.address_id,
            'permanent_line1': permanent_address1.address_line1,
            'permanent_line2': permanent_address1.address_line2,
            'permanent_city': permanent_address1.city,
            'permanent_state': permanent_address1.state_name,
            'permanent_country': permanent_address1.country,
            'permanent_zip': permanent_address1.zip_code,
            'work_add_id': work_address_id,
            'work_line1': work_address_line1,
            'work_line2': work_address_line2,
            'work_city': work_city,
            'work_state': work_state_name,
            'work_country': work_country,
            'work_zip': work_zip_code,
            'employment_start_date': str(current_emp.employment_start_date),
            'employment_end_date': str(employment_end_date),
            'recruitment method': current_emp.recruitment_method,  
            'sick_days': current_emp.eligible_sick_hours if current_emp.eligible_sick_hours  else '',
            'eligible_vacation_days':  current_emp.eligible_vacation_weeks if current_emp.eligible_vacation_weeks else '',
            'paid_holidays':'Yes' if current_emp.paid_holidays is True else 'No' if  current_emp.paid_holidays is False else '', 
            'employee_status': 'Active' if current_emp.employee_status=='A' else 'Inactive' if current_emp.employee_status=='I' else 'Terminated' if current_emp.employee_status=='T' else '',
            'termination_reason': termination_reason,
            'termination_intent': termination_intent,
            'termination_id': termination_id, 
            'rehire_eligibility': current_emp.rehire_eligibility,
            'cobra_eligibility': current_emp.cobra_eligibility,
            'benefits_eligibility': current_emp.benefits_eligibility,
            'certifications': emp_certification,
            'education_details': emp_education['education_details'],
            'auth_type': auth_type,
            'auth_start_date': str(auth_start_date),
            'auth_end_date': str(auth_end_date),
            'gc_job_title': gc_job_title,
            'gc_salary': str(gc_salary),
            'gc_prevailing_wage': str(gc_prevailing_wage),
            'gc_priority_date': str(gc_priority_date),
            'gc_current_status': gc_current_status,
            'gc_approval_status': gc_approval_status,
            'cobra_start_date': str(cobra_start_date),
            'cobra_end_date': str(cobra_end_date),
            'cobra_provider_name': cobra_provider_name,
            'cobra_amount_covered': str(cobra_amount_covered),
            'cobra_coverage_details': cobra_coverage_details,
            'emp_ead_type': emp_ead_type,
            'emp_valid_from': str(emp_valid_from),
            'emp_valid_to': str(emp_valid_to),
            'emp_sponsor_name': emp_sponsor_name,
            'salary_pay_type': salary_pay_type,
            'salary_start_date': str(salary_start_date),
            'salary_end_date': str(salary_end_date),
            'pay_amount': str(pay_amount),
            'payroll_frequency': payroll_frequency,
            'last_rise_date': str(last_rise_date),
            'salary_last_rise_amount': str(salary_last_rise_amount),
            'employee_designation': emp_designation,
            'employee_department': emp_department,
            'employee_type': emp_type,
            'emergency_contact': emergency_contact,
            'dependent_contact': dependent_contact,
            'visa_type': visa_type,
            'visa_number': visa_number,
            'job_title': job_title,
            'visa_start_date': str(visa_start_date),
            'visa_end_date': str(visa_end_date),
            'visa_salary': str(visa_salary) if visa_salary else '',
            'visa_title': visa_title,
            'lca_salary': str(lca_salary) if lca_salary else '',
            'lca_wage_level': lca_wage_level,
            'lca_prevailing_wages': str(lca_prevailing_wages) if lca_prevailing_wages else '',
            'lca_start_date': str(lca_start_date),
            'lca_end_date': str(lca_end_date),
            'current_status': current_status,
            'lca_add_id': lca_add_id,
            'lca_add_line1': lca_add_line1,
            'lca_add_line2': lca_add_line2,
            'lca_add_city': lca_add_city,
            'lca_add_state': lca_add_state,
            'lca_add_zip': lca_add_zip,
            'lca_add_country': lca_add_country,
            'lca_altadd_id': lca_altadd_id,
            'lca_altadd_line1': lca_altadd_line1,
            'lca_altadd_line2': lca_altadd_line2,
            'lca_altadd_city': lca_altadd_city,
            'lca_altadd_state': lca_altadd_state,
            'lca_altadd_zip': lca_altadd_zip,
            'lca_altadd_country': lca_altadd_country,
            'account_manager':current_emp.account_manager,
            'us_citizenship':current_emp.us_citizen if current_emp.us_citizen else '',
            'passport_number': passport_num,
            'passport_start_date' : str(pass_start_date),
            'passport_expiration_date': str(pass_end_date)
        }

class UserLogoutAccess(Resource):
    @jwt_required
    @prefix_decorator('UserLogoutAccess')
    def get(self):
        logging.info('Entered into {0}'.format(self.__class__.__name__))
        jti = get_raw_jwt()['jti']
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        return {'message': 'Logged Out Successfully'}

class GetOwnRole(Resource):
    @jwt_required
    @prefix_decorator('GetOwnRole')
    def get(self): 
        username = get_jwt_identity()
        requested_emp_id = UserModel.find_by_username(username)
        if requested_emp_id:
            requested_emp_role_id = UserRolesModel.find_to_get_user_id(requested_emp_id.userid)
            if requested_emp_role_id:
                user_role_obj = UserRolesModel.query.filter_by(user_id=requested_emp_role_id).first()
                return {'roles': [RoleModel.find_by_role_id(x) for x in user_role_obj.roles]}
            else:
                return {'roles':[]}
        else:
            return{"errorMessage":"Requested Employee Does Not Exist"}      

class TotalClients(Resource):
    @prefix_decorator('TotalClients')
    def get(self):	
        total_clients = ClientDetailsModel.query.count()
        active_clients = ClientDetailsModel.query.filter_by(client_status='A').count()
        inactive_clients = ClientDetailsModel.query.filter(ClientDetailsModel.client_status.in_(['P','I','T'])).count()
        logging.info('success')
        return {"total_clients": total_clients, "active_clients": active_clients, "inactive_clients": inactive_clients}

class ClientListForProject(Resource):
    @jwt_required
    @prefix_decorator('ClientListForProject')
    def get(self):
        return ClientDetailsModel.return_all_clientids()

class ContractList(Resource):
    @jwt_required
    @prefix_decorator('ContractList')
    def get(self): 
        return ClientVendorshipModel.return_all()

class GetStateByName(Resource):
    @prefix_decorator('GetStateByName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['country_name'] and type(data['country_name']) == str:
            country_obj = CountryModel.query.filter_by(country_name=data['country_name']).first()
            if country_obj:
                logging.info('states returned succesfully')
                return StateModel.return_all(country_obj.country_id)
            else:
                logging.debug('Could Not Find States for {}'.format(data['country_name']))
                return {'errorMessage':'Could Not Find States for {}'.format(data['country_name'])}
        else:
            logging.debug("Invalid Country Name")
            return {'errorMessage': "Invalid Country Name"}       

class GetCitiesByName(Resource):
    @prefix_decorator('GetCitiesByName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['state_name'] and type(data['state_name']) == str:
            state_obj = StateModel.query.filter_by(state_name=data['state_name']).first()
            if state_obj:
                logging.info('cities returned succesfully')
                return CitiesModel.return_all(state_obj.state_id)
            else:
                logging.debug('Could Not Find Cities for {}'.format(data['state_name']))
                return {'errorMessage':'Could Not Find Cities for {}'.format(data['state_name'])}
        else:
            logging.debug("Invalid State Name")
            return {'errorMessage': "Invalid State Name"}

class TeamTsDropDown(Resource):
    @jwt_required
    @prefix_decorator('TeamTsDropDown')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        manager_pro = ProjectModel.query.filter_by(project_manager_id=user_id).all()
        return ProjectModel.find_by_pro_list1(manager_pro)

class EmployeeList(Resource):
    @jwt_required
    @prefix_decorator('EmployeeList')
    def get(self): 
        return Employee1Model.return_all()

class NewClientList(Resource):
    @jwt_required
    @prefix_decorator('NewClientList')
    def get(self): 
        return ClientDetailsModel.return_all()

class ProjectsList(Resource):
    @jwt_required
    @prefix_decorator('ProjectsList')
    def get(self):
        return ProjectModel.return_all()

class AllEmpList(Resource):
    @jwt_required
    @prefix_decorator('AllEmpList')
    def get(self):
        employee_obj = Employee1Model.return_all_active_forproject()
        consultant_obj = ThirdPartyConsultantModel.return_all_active_forproject()
        all_emp = {}
        all_emp['Employees'] = employee_obj['Employees'] + consutant_obj['consultants']  
        return all_emp

class BillableActiveEmpList(Resource):
    @jwt_required
    @prefix_decorator('BillableActiveEmpList')
    def get(self):	 
        employee_obj = Employee1Model.return_all_billable_active_employees()
        consultant_obj = ThirdPartyConsultantModel.return_all_active_forproject()
        all_emp = {}
        all_emp['Employees'] = employee_obj['Employees'] + consultant_obj['consultants']
        return all_emp

class ManagerEmp(Resource):
    @jwt_required
    @prefix_decorator('ManagerEmp')
    def get(self):
        username = get_jwt_identity()
        user_id = UserModel.query.filter_by(username=username).first().userid
        employee_id = Employee1Model.query.filter_by(employee_id=user_id).first().employee_id
        pros = ProjectModel.query.filter_by(project_manager_id=employee_id).all()
        res = []
        ct_res = []
        for i in pros:
            res.append(ProjectEmployeeModel.find_by_project_id(i.project_id))
        for j in res:
            ct_res.append(j['projects_resources'])
        resources = [dict(y) for y in set(tuple(x.items()) for x in list(itertools.chain.from_iterable(ct_res)))]
        logging.info("Success")
        return {"resources": resources}

class MyTimesheet(Resource):
    @jwt_required
    @prefix_decorator('MyTimesheet')
    def get(self): 
        username = get_jwt_identity()
        user = UserModel.query.filter_by(username=username).first()
        if not user:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        return TimeSheetModel.return_for_spec_emp(user.userid)

class TimeSheet(Resource):
    @jwt_required
    @prefix_decorator('TimeSheet')
    def get(self):
        return TimeSheetModel.return_all()

class TeamTimeSheet(Resource):
    @jwt_required
    @prefix_decorator('TeamTimeSheet')
    def get(self):
        username = get_jwt_identity()
        user = UserModel.query.filter_by(username=username).first().userid
        pid = ProjectModel.query.filter_by(project_manager_id=user).all()
        list_ts = [TimeSheetModel.return_all_by_pro(i.project_id) for i in pid]
        return {"Projects": list(itertools.chain.from_iterable([j["Projects"] for j in list_ts]))}


class EmployeeInactive(Resource):
    @jwt_required
    @prefix_decorator('EmployeeInactive')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date = datetime.now()
        record = Employee1Model.query.filter_by(employee_id=data['employeeId']).first()
        if not record:
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(data['employeeId'])])).all():
            logging.debug("Failed,Employee Assigned To Active Project")
            return {"errorMessage":"Failed,Employee Assigned To Active Project"}
        record.employee_status = 'I'
        record.inactive_reason_id = data['inactive_reason_id']
        record.comments = data['comments']
        record.modified_time = date
        record.employee_status_last_updtd_date = date
        record.save_to_db()  
        logging.info("Employee Inactivated successfully")      
        return {"Message": "Employee Inactivated successfully"}

class ConsultantInactive(Resource):
    @jwt_required
    @prefix_decorator('ConsultantInactive')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        record = ThirdPartyConsultantModel.query.filter_by(consultant_id=data['consultantId']).first()
        if record:
            record.consultant_status = 'I'
            record.comments = data['comments']
            record.modified_time = date_obj
            record.consultant_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info("Consultant Inactivated successfully")
            return {"Message": "Consultant Inactivated successfully"}
        else:
            logging.debug('Consultant Does not Exists')
            return {'errorMessage':'Consultant Does not Exists'}   


# Resources related to Employee Registration

class addcertifschema(Schema):
    certification_name = fields.String(validate=validate.Length(max=255),required=False)
    issued_by = fields.String(validate=validate.Length(max=255),required=False)
    issued_on = fields.Date(format="%m-%d-%Y",allow_none=True,required=False)
    certification_url = fields.String(required=False)

    @post_load
    def create_person(self, data, **kwargs):
        return data


class AddCertifications(Resource):
    @jwt_required
    @prefix_decorator('AddCertifications')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = UserModel.query.filter_by(username=username).first()
        if userid:
            a = Employee1Model.find_by_employee_id(userid.userid)
            if CertificationsModel.query.filter_by(employee_id=a.employee_id,certification_name=data['certification_name'],issued_by=data['issued_by'],issued_on=data['issued_on'],certification_url=data['certification_url'],status='A').first():
                logging.debug("Certification Already Exists With Same Data")
                return {"errorMessage":"Certification Already Exists With Same Data"}
            if datetime.strptime(data['issued_on'],'%m-%d-%Y') < datetime.strptime(base64.b64decode(a.date_of_birth).decode('ascii'),'%m-%d-%Y'):
                logging.debug("Certification Issued Date Cannot Be Older Than Employee Date Of Birth")
                return {'errorMessage':"Certification Issued Date Cannot Be Older Than Employee Date Of Birth"}
            try:    
                schema = addcertifschema(unknown=INCLUDE)
                address = schema.load(data)
                result = schema.dump(address)
                new_certification = CertificationsModel(
                        employee_id=a.employee_id,
                        certification_name=result['certification_name'],
                        issued_by=result['issued_by'],
                        issued_on=result['issued_on'],
                        certification_url=result['certification_url'],
                        status='A'
                        )
                new_certification.save_to_db()
                a.modified_time = datetime.now()
                a.save_to_db()
                logging.debug('Cerification Added Successfully')
                return {'message': 'Cerification Added Successfully'}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
        else:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}

class EmployeeSignUp(Resource):
    @prefix_decorator('EmployeeSignUp')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['emailId'].lower()
        e = Employee1Model.query.filter(Employee1Model.employee_status.in_(['A','I'])).all()
        invite_record_obj = EmployeeInviteStatus.query.filter_by(employee_number=data['employeeNumber'], employee_email=email).first()
        if not invite_record_obj:
            logging.info('Data Mismatch, Cannot Add Employee')
            return {"errorMessage":"Data Mismatch, Cannot Add Employee"}
        if e:
            for i in e:
                if data['ssn'] == base64.b64decode(i.ssn).decode('ascii'):
                    logging.debug('SSN Should be Unique')
                    return {'errorMessage':'SSN Should be Unique'}
        if Employee1Model.query.filter_by(email_id=email).first():
            logging.debug("Employee With {0} Already Exists".format(email))
            return {"errorMessage":"Employee With {0} Already Exists".format(email)}
        try:
            role = RoleModel.query.filter_by(role_name=data['role']).first()
            dept = DepartmentModel.query.filter_by(department_name=data['employee_department']).first()
            desg = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
            if dept:
                dept = dept.department_id
            else:
                logging.debug('Department Does Not Exists')
                return {'errorMessage':'Department Does Not Exists'}
            if desg:
                desg = desg.designation_id
            else:
                logging.debug('Designation Does Not Exists')
                return {'errorMessage':'Designation Does Not Exists'}
            if role:
                role_id = role.id
            else:
                logging.debug("Role Does Not Exists")
                return {"errorMessage":"Role Does Not Exists"}
            schema1 = AddressSchema(unknown=EXCLUDE)
            address1 = schema1.load(data['currentAddress'])
            address2 = schema1.load(data['permanentAddress'])
            schema2 = EmployeeSignupSchema(unknown=EXCLUDE)
            empdet = schema2.load(data)
            if not UserModel.query.filter_by(username=email).first():
                user_obj = UserModel(username=email,password=UserModel.generate_hash(data['password']),reset_password=data['reset_password'])
                user_obj.save_to_db()
                user_id = UserModel.query.filter_by(username=email).first()
                if user_id:
                    user_role_obj = UserRolesModel(user_id=user_id.userid, roles=[str(role_id)])
                    user_role_obj.save_to_db()
                else:
                    logging.debug('Unable to save User')
                    return {'errorMessage':'Unable to save User'}
            user_id = UserModel.query.filter_by(username=email).first()

            current_address_obj = Address1Model(
                 address_line1=data['currentAddress']['addressLine1'],
                 address_line2=data['currentAddress']['addressLine2'],
                 city=data['currentAddress']['city'],
                 state_name=data['currentAddress']['state'],
                 zip_code=data['currentAddress']['zip'],
                 country=data['currentAddress']['country']
            )
            current_address_obj.save_to_db()       
            permanent_address_obj = Address1Model(
                 address_line1=data['permanentAddress']['addressLine1'],
                 address_line2=data['permanentAddress']['addressLine2'],
                 city=data['permanentAddress']['city'],
                 state_name=data['permanentAddress']['state'],
                 zip_code=data['permanentAddress']['zip'],
                 country=data['permanentAddress']['country']
            )
            permanent_address_obj.save_to_db()
            date_obj = datetime.now()
            employee_details_obj = Employee1Model(
                 employee_id=user_id.userid,
                 first_name=data['employeeFirstName'],
                 middle_name=data['employeeMiddleName'],
                 last_name=data['employeeLastName'],
                 email_id=email,
                 ssn=base64.b64encode(data['ssn'].encode('ascii')).decode('ascii'),
                 gender='M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O' if data['gender']=='Other' else '',
                 primary_phone_number=data['primaryPhoneNumber'],
                 alternate_phone_number=data['alternatePhoneNumber'],
                 marital_status='S' if data['maritalStatus']=='Single' else 'M' if data['maritalStatus']=='Married' else 'D' if data['maritalStatus']=='Divorced' else 'W' if data['maritalStatus']=='widowed' else '',
                 date_of_birth=base64.b64encode(data['dateOfBirth'].encode('ascii')).decode('ascii'),
                 employment_start_date=data['joiningDate'],
                 billable_resource=data['billableResource'][0],
                 employee_status='A',
                 emp_number=data['employeeNumber'],
                 present_address_id=current_address_obj.address_id,
                 permanent_address_id=permanent_address_obj.address_id,
                 department_id=dept,
                 designation_id=desg,
                 created_time = date_obj,
                 modified_time = date_obj,
                 employee_status_last_updtd_date = date_obj,
                 same_as_ca = data['same_as_ca']
             )
            employee_details_obj.save_to_db()
            invite_record_obj.invitation_status = "Inactive"
            invite_record_obj.save_to_db()
            logging.debug("Basic Details Added Succesfully")
            return {"message": "Basic Details Added Succesfully", "employeeId": user_id.userid}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class AddressSchema(Schema):
    address_line1 = fields.String(validate=validate.Length(max=255),data_key="addressLine1",required=False)
    address_line2 = fields.String(validate=validate.Length(max=255),data_key="addressLine2",required=False)
    city = fields.String(validate=validate.Length(max=100),data_key="city",required=False)
    state_name = fields.String(validate=validate.Length(max=100),data_key="state",required=False)
    zip_code = fields.String(data_key="zip",required=False)
    country = fields.String(validate=validate.Length(max=100),data_key="country",required=False)

    @validates('zip_code')
    def validate_age(self, zip_code):
        if not (len(zip_code) == 5 or len(zip_code) == 6):
            raise ValidationError('The zip code provided is invalid')

    @post_load
    def save_address(self, data, **kwargs):
        return data


class EmployeeSignupSchema(Schema):
    first_name = fields.String(validate=validate.Length(max=120),data_key="employeeFirstName",required=True)
    middle_name = fields.String(validate=validate.Length(max=120),data_key="employeeMiddleName",required=False)
    last_name = fields.String(validate=validate.Length(max=120),data_key="employeeLastName",required=True)
    email_id = fields.String(validate=validate.Length(max=120),data_key="emailId",required=True)
    ssn = fields.String(validate=validate.Length(max=120),data_key="ssn",required=True)
    gender = fields.String(validate=validate.Length(max=120),data_key="gender",required=True)
    primary_phone_number = fields.String(validate=validate.Length(max=120),data_key="primaryPhoneNumber",required=True)
    alternate_phone_number = fields.String(validate=validate.Length(max=120),data_key="alternatePhoneNumber",required=False)
    marital_status = fields.String(validate=validate.Length(max=120),data_key="maritalStatus",required=False)
    date_of_birth = fields.Date(format='%m-%d-%Y',data_key="dateOfBirth",required=True)
    emp_number = fields.String(data_key="employeeNumber",required=True)

    @validates('email_id')
    def validate_age(self, email_id):
        if (email_id == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',email_id)) and len(email_id)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

    @validates('emp_number')
    def validate_age(self, emp_number):
        if len(str(emp_number)) != 6 :
            raise ValidationError('Invalid Employee Number')


    @post_load
    def save_employee(self, data, **kwargs):
        return data


class UsersSchema(Schema):
    username = fields.Email(data_key="emailId",required=True)
    password = fields.String(validate=validate.Length(max=120),data_key="password",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

class DependentSchema(Schema):
    dependent_relation_type = fields.String(validate=validate.Length(max=50),data_key="dependentRelationType",required=True)
    contact_first_name = fields.String(validate=validate.Length(max=30),data_key="dependentFirstName",required=True)
    contact_middle_name = fields.String(validate=validate.Length(max=30),data_key="dependentMiddleName",required=False)
    contact_last_name = fields.String(validate=validate.Length(max=30),data_key="dependentLastName",required=True)
    contact_dob = fields.String(validate=validate.Length(max=20),data_key="dependentDob",required=False)
    contact_gender = fields.String(validate=validate.Length(max=30),data_key="dependentGender",required=False)
    contact_ssn = fields.String(validate=validate.Length(max=120),data_key="dependentSsn",required=False)
    mobile = fields.String(data_key="mobile",required=True)
    alt_mobile = fields.String(data_key="altMobile",required=False)
    contact_email = fields.String(data_key="dependentEmail",required=False)
    
    @validates('contact_email')
    def validate_age(self, contact_email):
        if (contact_email == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',contact_email)) and len(contact_email)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

    @validates('mobile')
    def validate_age(self, mobile):
        if len(mobile) == 0 or len(mobile) == 10:
            pass
        else:
            raise ValidationError('The Mobile Number is invalid')

    @validates('alt_mobile')
    def validate_age(self, alt_mobile):
        if len(alt_mobile) == 0 or len(alt_mobile) == 10:
            pass
        else:
            raise ValidationError('The Mobile Number is invalid')


    @post_load
    def save_employee(self, data, **kwargs):
        return data


class DependentSignUpNew(Resource):
    @prefix_decorator('DependentSignUpNew')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if len([i['dependentFirstName'] for i in data]) != len(set([i['dependentFirstName'] for i in data])):
            logging.debug("Cannot Save Dependents With Same First Name")
            return {"errorMessage":"Cannot Save Dependents With Same First Name"}
        for item in data:
            if DependentContactModel.query.filter_by(contact_first_name=item['dependentFirstName'],employee_id=item['employeeId']).first():
                logging.debug("Dependent With Same First Name Already Exists")
                return {"errorMessage":"Dependent With Same First Name Already Exists"}
            if not Employee1Model.query.filter_by(employee_id=item['employeeId']).first():
                logging.debug("Employee With {0} Does Not Exist".format(item['employeeId']))
                return {"errorMessage":"Employee With {0} Does Not Exist".format(item['employeeId'])}
        for i in data:
            if not DependentRelationModel.query.filter_by(dependent_relation_type=i['dependentRelationType']).first():
                logging.debug("Please Provide Valid Dependent Relation Type")
                return {"errorMessage":"Please Provide Valid Dependent Relation Type"}
        ssn_list = []
        for j in data:
            if j['dependentSsn']:
                ssn_list.append(j['dependentSsn'])
                s = base64.b64encode(j['dependentSsn'].encode('ascii')).decode('ascii')
                if DependentContactModel.query.filter_by(contact_ssn=s,status='A').first() or Employee1Model.query.filter_by(employee_id=j['employeeId'],ssn=s).first():
                    logging.debug('SSN Should Be Unique')
                    return {'errorMessage':'SSN Should Be Unique'}
        if len(ssn_list) != len(set(ssn_list)):
            return {"errorMessage":"SSN Should be Unique"}
        for item in data:
            try:
                schema = AddressSchema(unknown=INCLUDE)
                address = schema.load(item)
                result1 = schema.dump(address)
                schema1 = DependentSchema(unknown=INCLUDE)
                empdep = schema1.load(item)
                result2 = schema1.dump(empdep)
            except ValidationError as err:
                return {"errorMessage":err.messages},500
        for item in data:
            dependent_address_obj = Address1Model(
                address_line1=item['addressLine1'],
                address_line2=item['addressLine2'],
                city=item['city'],
                state_name=item['state'],
                zip_code=item['zip'],
                country=item['country']
            )
            dependent_address_obj.save_to_db()
            address_id = dependent_address_obj.address_id
            date_obj = datetime.now()
            dependent_details_obj = DependentContactModel(
                employee_id=item['employeeId'],
                dependent_address_id=address_id,
                dependent_relation_id=DependentRelationModel.query.filter_by(dependent_relation_type=item['dependentRelationType']).first().dependent_relation_id,
                contact_first_name=item['dependentFirstName'],
                contact_middle_name=item['dependentMiddleName'],
                contact_last_name=item['dependentLastName'],
                contact_dob=base64.b64encode(item['dependentDob'].encode('ascii')).decode('ascii'),
                contact_gender='M' if item['dependentGender']=='Male' else 'F' if item['dependentGender']=='Female' else 'O' if item['dependentGender']=='Other' else '',
                contact_ssn=base64.b64encode(item['dependentSsn'].encode('ascii')).decode('ascii'),
                mobile=item['mobile'],
                alt_mobile=item['altMobile'],
                contact_email=item['dependentEmail'].lower(),
                status = 'A',
                created_by = item['employeeId'],
                created_time =date_obj, 
                modified_time = date_obj,
                same_as_emp_current_add = item['same_as_ca']
            )
            dependent_details_obj.save_to_db()
        logging.debug("Dependent Contact Details Added Succesfully")
        return {"message": "Dependent Contact Details Added Succesfully"}    

class CertificationSchema(Schema):
    employee_certification_id = fields.Integer(data_key="certificationId",required=False)
    employee_id = fields.Integer(data_key="employee_id",required=False)
    certification_name = fields.String(data_key="certificationName",validate=validate.Length(max=255),required=False)
    issued_by = fields.String(data_key="issuedBy",validate=validate.Length(max=255),required=False)
    issued_on = fields.Date(format='%m-%d-%Y',allow_none=True,data_key="issuedOn",required=False)
    certification_url = fields.String(data_key="certificationUrl",required=False)
    @post_load
    def create_person(self, data, **kwargs):
        return data


class CertificationsSignUp(Resource):
    @prefix_decorator('CertificationsSignUp')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        for i in data:
            try:
                employee_dob = Employee1Model.query.filter_by(employee_id=i['employeeId']).first()
                if datetime.strptime(i['issuedOn'],'%m-%d-%Y') < datetime.strptime(base64.b64decode(employee_dob.date_of_birth).decode('ascii'),'%m-%d-%Y'):
                    logging.debug("Certification Issued Date Cannot Be Older Than Employee Date Of Birth")
                    return {'erroMessage':"Certification Issued Date Cannot Be Older Than Employee Date Of Birth"}
            except:
                logging.debug('Employee Details Does not Exists')
                return {"errorMessage":'Employee Details Does not Exists'}
        try:
            for item in data:
                schema = CertificationSchema(unknown=INCLUDE)
                edu = schema.load(item)
                i = schema.dump(edu)
                if CertificationsModel.query.filter_by(employee_id=item['employeeId'],status='A',certification_name=i['certificationName'],issued_by=i['issuedBy'],issued_on=i['issuedOn'],certification_url=i['certificationUrl']).first():
                    logging.debug()
                    {"errorMessage":"Certification Already Exists With Same Data"}
                new_certification = CertificationsModel(
                        employee_id=item['employeeId'],
                        certification_name=i['certificationName'],
                        issued_by=i['issuedBy'],
                        issued_on=i['issuedOn'],
                        certification_url=i['certificationUrl'],
                        status='A'  
                        )
                new_certification.save_to_db()
            if len(data) == 1:
                logging.debug('Cerification Added Successfully')
                return {'message': 'Cerification Added Successfully'}
            elif len(data)>=1:
                logging.debug("Certifications Added Successfully")
                return {"message": "Certifications Added Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500      

class EducationSignUp(Resource):
    @prefix_decorator('EducationSignUp')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        # branch is Major/Concentration
        emp_edu_obj = EducationModel.query.filter_by(employee_id=data['employeeId']).first()
        if emp_edu_obj:
            logging.debug("Education Details Already Saved")
            return {'errorMessage':"Education Details Already Saved"}
        else:
            for i in data['educationArray']:
                try:
                    schema = EducationSchema(unknown=INCLUDE)
                    empedu = schema.load(i)
                    result = schema.dump(empedu)
                    a = HighestDegreeListModel.query.filter_by(highest_degree_type=result['highestDegree']).first()
                    b = DegreeTypeListModel.query.filter_by(degree_duration=result['numberOfYears']).first()
                    c = CountryModel.query.filter_by(country_name=result['country']).first()
                    if not a:
                        logging.debug("Highest Degree Type Does Not Exists")
                        return {"errorMessage":"Highest Degree Type Does Not Exists"}
                    if not b:
                        logging.debug("Invalid data for Number of Years")
                        return {"errorMessage":"Invalid data for Number of Years"}
                    if not c:
                        logging.debug("Country Name Does Not Exists")
                        return {"errorMessage":"Country Name Does Not Exists"}
                except ValidationError as err:
                    logging.exception('Exception Occured, {0}'.format(err.messages))
                    return {"errorMessage":'Exception Occured, {0}'.format(err.messages)}
            for i in data['educationArray']:
                if EducationModel.query.filter_by(employee_id=data['employeeId'],branch=i['branch'],highest_degree_id = HighestDegreeListModel.query.filter_by(highest_degree_type=i['highestDegree']).first().highest_degree_id).first():
                    return{"errorMessage":"Cannot Add Duplicate Education Details"}
                education_obj = EducationModel(
                    employee_id=data['employeeId'],
                    highest_degree_id=HighestDegreeListModel.query.filter_by(highest_degree_type=i['highestDegree'],).first().highest_degree_id,
                    year_awarded=i['yearAwarded'],
                    institution_name=i['institutionName'],
                    degree_id=DegreeTypeListModel.query.filter_by(degree_duration=i['numberOfYears']).first().degree_id,
                    branch=i['branch'],
                    country_id=CountryModel.query.filter_by(country_name=i['country']).first().country_id
                )
                education_obj.save_to_db()
            logging.debug("Education Details Added Successfully")
            return {"message": "Education Details Added Successfully"}          

class UsSignUp(Resource):
    @prefix_decorator('UsSignUp')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        try:
            schema = UsSignUpSchema(unknown=INCLUDE)
            usdet = schema.load(data)
            result = schema.dump(usdet)
            emp_record = Employee1Model.query.filter_by(employee_id=result['employeeId']).first()
            if emp_record:
                emp_record.us_citizen = True
                emp_record.save_to_db()
            else:
                logging.debug("Employee Does Not Exists")
                return {"errorMessage":"Employee Does Not Exists"}
            record2 = UsCitizenPassportDetailsModel(employee_id=result['employeeId'],passport_number=result['passportNumber'],start_date=result['startDate'],end_date=result['endDate'])
            record2.save_to_db()
            logging.debug("Details Added Successfully")
            return {"message": "Details Added Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500     

class UsSignUpSchema(Schema):
    employeeId = fields.Integer(required=False)
    passportNumber = fields.String(validate=validate.Length(max=20),required=False)
    startDate = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    endDate = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)

    @post_load
    def create_person(self, data, **kwargs):
        return data

class VisaSignUp(Resource):
    @prefix_decorator('VisaSignUp')
    def post(self):
        data = request.get_json(force=True) 
        print(data)
        if EmployeeVisaDetailsModel.query.filter_by(employee_id=data['employeeId']).first():
            logging.debug("Visa Details Already Saved")
            return {"errorMessage":"Visa Details Already Saved"}
        if EmployeeVisaDetailsModel.query.filter_by(visa_number=data['visaNumber']).first():
            logging.debug("Invalid Visa Number")
            return {"errorMessage":"Invalid Visa Number"}
        try:
            schema = VisaSignUpSchema(unknown=EXCLUDE)
            visa = schema.load(data)
            result = schema.dump(visa)
            try:
                visa_type = VisaTypeModel.query.filter_by(visa_type=result['visaType']).first()
                emp_record = Employee1Model.query.filter_by(employee_id=result['employeeId']).first()
            except Exception as e:
                logging.exception('Exception Occured , {0}'.format(str(e)))
                return {"erroMessage":str(e)}
            visa_details_obj = EmployeeVisaDetailsModel(
                employee_id=result['employeeId'],
                visa_number=result['visaNumber'],
                visa_type_id=visa_type.visa_type_id,
                visa_start_date=result['visaStartDate'],
                visa_end_date=result['visaEndDate'],
                attorney=result['attorney'],
                visa_salary=result['visaSalary'],
                job_title=result['jobTitle']
            )
            visa_details_obj.save_to_db()
            emp_record.us_citizen = False
            emp_record.save_to_db()
            logging.debug("Visa Details Added Successfully")
            return {"message": "Visa Details Added Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            #raise err
            return {"errorMessage":err.messages},500

class GreenCardSchema(Schema):
    employee_id = fields.Integer(data_key="employeeId",required=False)
    gc_job_title = fields.String(data_key="gcJobTitle",validate=validate.Length(max=150),required=False)
    gc_salary = fields.String(data_key="gcSalary",required=False)
    gc_prevailing_wage = fields.String(data_key="gcPrevailingWage",required=False)
    priority_date = fields.Date(format='%m-%d-%Y',data_key="priorityDate",required=False)
    current_status = fields.String(data_key="currentStatus",validate=validate.Length(max=30),required=False)
    approval_status = fields.String(validate=validate.Length(max=30),data_key="approvalStatus",required=False)
    gc_number = fields.String(validate=validate.Length(max=20),data_key="gc_number",required=False)

    @post_load
    def save_gc(self, data, **kwargs):
        return data


class GreenCardSignUp(Resource):
    @prefix_decorator('GreenCardSignUp')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        try:
            schema = GreenCardSchema(unknown=INCLUDE)
            usdet = schema.load(data)
            result = schema.dump(usdet)
            gc_details_obj = GreenCardModel(
                employee_id=result['employeeId'],
                gc_job_title=result['gcJobTitle'],
                gc_salary=result['gcSalary'],
                gc_prevailing_wage=result['gcPrevailingWage'],
                priority_date=result['priorityDate'],
                current_status=result['currentStatus'],
                approval_status=result['approvalStatus'],
                gc_number = result['gc_number']
            )
            gc_details_obj.save_to_db()
            logging.debug("Green Card Details Added Successfully")
            return {"message": "Green Card Details Added Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class EmergencySchema(Schema):
    employee_id = fields.Integer(data_key="employeeId",required=True)
    emergency_relation_type = fields.String(validate=validate.Length(max=50),data_key="emergencyRelationType",required=True)
    contact_first_name = fields.String(validate=validate.Length(max=30),data_key="emergencyFirstName",required=True)
    contact_middle_name = fields.String(validate=validate.Length(max=30),data_key="emergencyMiddleName",required=False)
    contact_last_name = fields.String(validate=validate.Length(max=30),data_key="emergencyLastName",required=True)
    mobile = fields.String(validate=validate.Length(max=30),data_key="mobile",required=True)
    alt_mobile = fields.String(validate=validate.Length(max=30),data_key="altMobile",required=False)
    contact_email = fields.Email(allow_none=True,data_key="emergencyEmail",required=False)

    @validates('contact_email')
    def validate_age(self, contact_email):
        if (contact_email == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',contact_email)) and len(contact_email)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

    @validates('mobile')
    def validate_age(self, mobile):
        if len(mobile) != 10:
            raise ValidationError('The Mobile Number is invalid')

    @validates('alt_mobile')
    def validate_age(self, alt_mobile):
        if len(alt_mobile) == 0 or len(alt_mobile) == 10:
            pass
        else:
            raise ValidationError('The Alternate Mobile Number is invalid')


    @post_load
    def save_emergency_details(self, data, **kwargs):
        return data


class EmergencySignUpNew(Resource):
    @prefix_decorator('EmergencySignUpNew')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if len(data)>2:
            return {"errorMessage":"Cannot Add More Than Two Emergrncy Contacts"}
        else:
            try:
                for item in data:
                    schema = AddressSchema(unknown=INCLUDE)
                    usdet = schema.load(item)
                    result = schema.dump(usdet)
                    schema1 = EmergencySchema(unknown=INCLUDE)
                    usdet1 = schema1.load(item)
                    result1 = schema1.dump(usdet1)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                #raise err
                return {"errorMessage":err.messages},500
            for item in data:
                emp = Employee1Model.query.filter_by(employee_id=item['employeeId']).first()
                rel = EmergencyRelationModel.query.filter_by(emergency_relation_type=item['emergencyRelationType']).first()
                if not emp:
                    logging.debug('Employee Does Not Exists')
                    return {'errorMessage':'Employee Does Not Exists'}
                if not rel:
                    logging.debug("Emergency Relation Type Not Exists, Please Select From Drop Down")
                    return {"errorMessage":"Emergency Relation Type Not Exists, Please Select From Drop Down"}
            for item in data:
                emp = Employee1Model.query.filter_by(employee_id=item['employeeId']).first()
                if emp:
                    if (item['mobile'] in [emp.primary_phone_number if emp.primary_phone_number else 0,emp.alternate_phone_number if emp.alternate_phone_number else 0]) or (item['altMobile'] in [emp.primary_phone_number if emp.primary_phone_number else 0,emp.alternate_phone_number if emp.alternate_phone_number else 0]):
                        logging.debug("Emergency Phone Number Can Not Be Same as Employee Phone Number")
                        return {"errorMessage":"Emergency Phone Number Can Not Be Same as Employee Phone Number"}
                    elif item['emergencyEmail'].lower()==emp.email_id:
                        logging.debug("Emergency email Id Can Not Be Same as Employee email Id")
                        return {"errorMessage":"Emergency Email Id Can Not Be Same As Employee email Id"}
                    emr_emp = EmployeeContactModel.query.filter_by(employee_id=emp.employee_id,status='A').all()
                    for i in emr_emp:
                        if (item['mobile'] in [i.mobile if i.mobile else 0,i.alt_mobile if i.alt_mobile else 0]) or (item['altMobile'] in [i.mobile if i.mobile else 0,i.alt_mobile if i.alt_mobile else 0]):
                            logging.debug("Emergency Phone Number Already Exists for Other Emergency Contact")
                            return {"errorMessage":"Emergency Phone Number Already Exists for Other Emergency Contact"}
                        elif item['emergencyEmail'].lower()==i.contact_email:
                            logging.debug("Emergency email Id Already Exists for Other Emergency Contact")
                            return {"errorMessage":"Emergency Email Id Already Exists for Other Emergency Contact"}
                else:    
                    logging.debug("Employee Does Not Exists")
                    return{'errorMessage':"Employee Does Not Exists"}
            if len(data) == 2:
                if data[0]['mobile'] == data[1]['mobile']:
                    logging.debug("Both Emergency Contacts Cannot Have Same Mobile Number")
                    return {"errorMessage":"Both Emergency Contacts Cannot Have Same Mobile Number"}
                if (data[0]['altMobile'] == data[1]['altMobile']) and data[0]['altMobile'] and data[1]['altMobile']:
                    logging.debug("Both Emergency Contacts Cannot Have Same Alternate Mobile Number")
                    return {"errorMessage":"Both Emergency Contacts Cannot Have Same Alternate Mobile Number"}
                if data[0]['emergencyEmail'].lower() == data[1]['emergencyEmail'].lower():
                    logging.debug("Both Emergency Contacts Cannot Have Same Email Address")
                    return {"errorMessage":"Both Emergency Contacts Cannot Have Same Email Address"}
            for item in data:
                emergency_address_obj = Address1Model(
                    address_line1=item['addressLine1'],
                    address_line2=item['addressLine2'],
                    city=item['city'],
                    state_name=item['state'],
                    zip_code=item['zip'],
                    country=item['country']
                ) 
                emergency_address_obj.save_to_db()
                address_id = emergency_address_obj.address_id
                date_obj = datetime.now()
                emergency_details_obj = EmployeeContactModel(
                    employee_id=item['employeeId'],
                    emergency_address_id=address_id,
                    emergency_relation_id=EmergencyRelationModel.query.filter_by(emergency_relation_type=item['emergencyRelationType']).first().emergency_relation_id,
                    contact_first_name=item['emergencyFirstName'],
                    contact_middle_name=item['emergencyMiddleName'],
                    contact_last_name=item['emergencyLastName'],
                    mobile=item['mobile'],
                    alt_mobile=item['altMobile'],
                    contact_email=item['emergencyEmail'].lower(),
                    created_by = item['employeeId'],
                    created_time = date_obj,
                    modified_time = date_obj,
                    status = 'A',
                    same_as_emp_current_add = item['same_as_ca']
                )
                emergency_details_obj.save_to_db()
            logging.debug("Emergency Contact Details Added Succesfully")
            return {"message": "Emergency Contact Details Added Succesfully"}  

               
class InvitationValidation(Resource):
    @prefix_decorator('InvitationValidation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        desg = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
        dept = DepartmentModel.query.filter_by(department_name=data['employee_department']).first()
        invitation_obj = EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=data['employeeNumber'],employee_email=data['email'].lower(),employee_role=data['role'],designation_id=desg.designation_id,department_id=dept.department_id,joining_date=data['joining_date'],billable_resource=data['billable_resource'][0]).first()
        if invitation_obj:
            logging.info("Success")
            return {"message":"Success"}
        else:
            logging.debug("Employee Already Registered")
            return {"errorMessage":"Employee Already Registered"}

class AllEmployees(Resource):
    @prefix_decorator('AllEmployees')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        current_user = UserModel.find_by_username(email)
        emp_name = Employee1Model.query.filter_by(email_id=email).first()
        if not current_user:
            logging.debug('User {} Doesn\'t Exist'.format(email))
            return {'message': 'User {} Doesn\'t Exist'.format(email)} 
#        print(Employee1Model.query.filter_by(employee_id=6).first().termination_date) 
        if emp_name:  
            if UserModel.verify_hash(data['password'], current_user.password):
                employee_obj = Employee1Model.query.filter_by(email_id=email).first()
                if employee_obj.employee_status == 'T':
                    if datetime.today() > datetime.strptime(str(employee_obj.termination_date), "%Y-%m-%d"):
                        return {"message":"Login Not Allowed"}
                expires = timedelta(hours=12)
                access_token = create_access_token(identity=email, expires_delta=expires)
                refresh_token = create_refresh_token(identity=email, expires_delta=expires)
                logging.info('Success')
                return {
                    'employee_name': emp_name.first_name+' '+emp_name.last_name,
                    'user_name': data['username'],
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'message':"Logged In Successfully",
                    'reset_password':current_user.reset_password if current_user.reset_password else ''
                }
            else:
                logging.debug("Invalid Email/Password")
                return {"message": "Invalid Email/Password"}
        else:
            logging.info('User {} Doesn\'t Exist'.format(email))
            return {'message': 'User {} Doesn\'t Exist'.format(email)}

# Resources for client_timesheets endpoints

class MyTimesheetSubmit(Resource):
    @jwt_required
    @prefix_decorator('MyTimesheetSubmit')
    def post(self):
        username = get_jwt_identity()
        data = request.get_json(force=True)
        emp_num = UserModel.query.filter_by(username=username).first()
        if not emp_num:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        return TimeSheetModel.return_all_by_man_ts(emp_num.userid, **data)

class EmpListPro(Resource):
    @jwt_required
    @prefix_decorator('EmpListPro')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        employees = ProjectEmployeeModel.query.filter_by(project_id=data["project_id"]).first()
        if not employees:
            logging.debug('Employees Does Not Exist')
            return {"errorMessages":"Employees Does Not Exist"}
        return Employee1Model.return_all_emps(employees.employee_id)

class ClientProjects(Resource):
    @jwt_required
    @prefix_decorator('ClientProjects')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ClientDetailsModel.query.filter_by(client_id=data['client_id']).first():
            logging.debug("Client Does Not Exist")
            return {"errorMessage":"Client Does Not Exist"}
        return ProjectModel.return_all_client_projects(data['client_id'])

class TimeSheetFilter(Resource):
    @jwt_required
    def post(self):
        data = request.get_json(force=True)
        if (data['emp_num'] and data['emp_num'][0] == 'C'):
            return [{
                'time_sheet_id': '',
                'time_sheet_date': '',
                'project_name': '',
                'project_id': '',
                'employee_id': '',
                'client_name': '',
                'regular_hours': '',
                'ot_hours': '',
                'billable_hours': '',
                'sick_hours': '',
                'remarks': '',
                'time_sheet_approval': '',
                'work_description': ''
            }]
        else:
            return TimeSheetModel.return_all_by_man_ts(**data)


class HrTimeSheetSubmit(Resource):
    @jwt_required
    def post(self):
        data = request.get_json(force=True)
        if (data['emp_num'] and data['emp_num'][0] == 'C'):
            return [{
                'time_sheet_id': '',
                'time_sheet_date': '',
                'project_name': '',
                'project_id': '',
                'employee_id': '',
                'client_name': '',
                'regular_hours': '',
                'ot_hours': '',
                'billable_hours': '',
                'sick_hours': '',
                'remarks': '',
                'time_sheet_approval': '',
                'work_description': ''
            }]

        if data:
            project_id = data['project_id']
            client_id = data['client_id']
            return TimeSheetModel.return_all_by_emp_ts(**data)
        else:
            return {'errorMessage':'Please Provide Valid Data'}

class TimeSheetApprove(Resource):
    @jwt_required
    @prefix_decorator('TimeSheetApprove')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        time_id = data['time_sheet_id']
        for ts_id in time_id:
            record = TimeSheetModel.query.filter_by(time_sheet_id=ts_id).first()
            record.time_sheet_approval = data["time_sheet_approval"]
            record.remarks = data["remarks"]
            record.save_to_db()
            emp = Employee1Model.query.filter_by(employee_id=record.employee_id).first()
            emp_email = emp.email_id
            emp_name = emp.first_name + ' ' + emp.last_name
            project = ProjectModel.query.filter_by(project_id=record.project_id).first().project_name
            day = str(record.time_sheet_date)
            rec = ClientDetailsModel.query.filter_by(
                client_id=record.client_id).first().organization_name + ', ' + project + ', ' + day + ', ' + record.work_description

            if data["time_sheet_approval"] != "approved":
                msg = Message('Timesheet Status Notification', sender='support@prutech.com', recipients=[emp_email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Your timesheet for project <b>{1}</b> on <b>{2}</b> is <br> {3} </br>
                <br> has been <b>{4}</b> due to {5} </br>
                <br> <b>Thanks,
                <br> PRUTECH Team.
                </b>""".format(emp_name, project, day, rec, data["time_sheet_approval"], data["remarks"])
                mail.send(msg)
            else:
                msg = Message('Timesheet Status Notification', sender='support@prutech.com', recipients=[emp_email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Your timesheet for project <b>{1}</b> on <b>{2}</b> is <br> {3} </br>
                <br> has been <b>{4}</b> </br>
                <br> <b>Thanks,
                <br> PRUTECH Team.
                </b>""".format(emp_name, project, day, rec, data["time_sheet_approval"])
                mail.send(msg)
        return {"message": data["time_sheet_approval"]}

class ClientResource(Resource):
    @jwt_required
    @prefix_decorator('ClientResource')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ClientDetailsModel.query.filter_by(client_id=data['client_id']).first():
            return {"errorMessage":"Client Does Not Exist"}
        res = [ProjectEmployeeModel.find_by_project_id(i.project_id) for i in ProjectModel.query.filter_by(client_id=data['client_id']).all()]
        return {"resources": [dict(y) for y in set(tuple(x.items()) for x in list(itertools.chain.from_iterable([j['projects_resources'] for j in res])))]}

class GCEditAddSchema(Schema):
    employee_id = fields.Integer(data_key="employeeId",required=False)
    gc_job_title = fields.String(validate=validate.Length(max=150),required=False)
    gc_salary = fields.Float(required=False)
    gc_prewailing_wages = fields.Float(required=False)
    gc_date = fields.Date(format='%m-%d-%Y',required=False)
    current_status = fields.String(validate=validate.Length(max=30),required=False)
    approval_status = fields.String(validate=validate.Length(max=30),required=False)
    gc_number = fields.String(validate=validate.Length(max=20),required=False)

    @post_load
    def save_gc(self, data, **kwargs):
        return data


class GCEditAdd(Resource):
    @jwt_required
    @prefix_decorator('GCEditAdd')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        current_emp = Employee1Model.query.filter_by(emp_number=data['employee_number']).first()
        if not current_emp:
            logging.debug("Employee Does Not Exists")
            return {"errorMessage":"Employee Does Not Exists"}
        record = GreenCardModel.find_by_employeenumber(current_emp.employee_id)
        try:
            schema = GCEditAddSchema(unknown=INCLUDE)
            usdet = schema.load(data)
            result = schema.dump(usdet)
            if record:
                record.gc_prevailing_wage = result['gc_prewailing_wages']
                record.gc_salary = result['gc_salary']
                record.priority_date = result['gc_date']
                record.gc_job_title = result['gc_job_title']
                record.current_status = result['current_status']
                record.approval_status = result['approval_status']
                record.gc_number = result['gc_number']
                record.save_to_db()
                current_emp.modified_time = date_obj
                current_emp.save_to_db()
                logging.debug("Green Card Details updated Successfully")
                return {"message": "Green card details updated successfully"}
            else:
                b = GreenCardModel(
                    employee_id=current_emp.employee_id,
                    gc_prevailing_wage=result['gc_prewailing_wages'],
                    gc_salary=result['gc_salary'],
                    priority_date=result['gc_date'],
                    gc_job_title=result['gc_job_title'],
                    current_status = result['current_status'],
                    approval_status = result['approval_status'],
                    gc_number = result['gc_number']
                )
                b.save_to_db()
                current_emp.modified_time = date_obj
                current_emp.save_to_db()
                logging.debug("Green Card Details updated Successfully")
                return {"message": "Green Card Details updated Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500             

class ChangePassword(Resource):
    @jwt_required
    @prefix_decorator('ChangePassword')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        record = UserModel.query.filter_by(username=username).first()
        if record:
            if UserModel.verify_hash(data['current_password'], record.password):
                print('no')
                if UserModel.verify_hash(data['new_password'], record.password):
                    print('no1')
                    logging.debug("New Password And Current Password Cannot Be Same")
                    return {"errorMessage": "New Password And Current Password Cannot Be Same"}
                record.password = UserModel.generate_hash(data['new_password'])
                record.reset_password = False
                record.save_to_db()
                logging.info("Password Reset is Successful")
                return {"message": "Password Reset is Successful"}
            else:
                logging.debug("Incorrect Current Password")
                return {"errorMessage": "Incorrect Current Password"}
        else:
            print('yes')
            logging.debug("User {} Does Not Exist".format(username))
            return {"errorMessage":"User {} Does Not Exist".format(username)}

class PasswordMailValidation(Resource):
    @prefix_decorator('PasswordMailValidation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if PasswordMailStatus.query.filter_by(token=data['token'],token_status="Active").first():
            logging.info('success')
            return {"message":"success"}
        else:
            login.debug("Your Link Was Invalid")
            return {"errorMessage":"Your Link Was Invalid"}

class SendMailToken(Resource):
    @prefix_decorator('SendMailToken')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        record = UserModel.query.filter_by(username=email).first()
        if record :
            names = email.split('@')[0]
            token = record.get_token()
            reset_url = data['base_url'] + '?token=' + token
            msg = Message('Password reset link', sender='support@prutech.com', recipients=[email])
            msg.html = """<b style = "font-family:calibri,garamond,serif;font-size:16px;"> Hi {0},<br>
            <br> You received Password reset link from PRUTECH to HRMS Portal.
            <br>
            <br> To reset <a href={1}>click here</a></br>
            <br> Thanks,
            <br> PRUTECH Team.
            </b>""".format(names, reset_url)
            mail.send(msg)
            mail_validation_obj = PasswordMailStatus(token=token,token_status="Active")
            mail_validation_obj.save_to_db()
            logging.info("Maill Sent")
            return {"message": "Mail Sent"}
        else:
            logging.debug("{0} Is Not A Valid User".format(email))
            return {"errorMessage":"{0} Is Not A Valid User".format(email)}

class ResetPassword(Resource):
    @prefix_decorator('ResetPassword')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        user_id = UserModel.verify_token(data['token'])
        if user_id:
            record = UserModel.query.filter_by(userid=user_id).first()
            record.password = UserModel.generate_hash(data['new_password'])
            record.save_to_db()
            mail_validation_obj = PasswordMailStatus.query.filter_by(token=data['token']).first()
            mail_validation_obj.token_status= "Inactive"
            mail_validation_obj.save_to_db()
            logging.info("Password Has Been Reset Successfully")
            return {"message": "Password Has Been Reset Successfully"}
        else:
            logging.debug("Token Expired or Invalid")
            return {"message": "Token Expired or Invalid"}

# this is for adding new dependent
class DependentsSignUp1(Resource):
    @jwt_required
    @prefix_decorator('DependentsSignUp1')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        if data['employee_number']:
            userid = Employee1Model.query.filter_by(email_id=username).first()
            employee = Employee1Model.query.filter_by(emp_number=data['employee_number']).first()
            data['dependent_contact_relation'] = DependentRelationModel.query.filter_by(dependent_relation_type=data['dependent_contact_relation']).first()
            if userid and employee:
                if DependentContactModel.query.filter_by(contact_first_name=data['contact_first_name'],employee_id=employee.employee_id,status='A').first():
                    logging.debug("Dependent With Same First Name Already Exists")
                    return {"errorMessage":"Dependent With Same First Name Already Exists"}
                data['contact_gender'] = 'M' if data['contact_gender']=='Male' else 'F' if data['contact_gender']=='Female' else 'O' if data['contact_gender']=='Other' else ''
                if data['dependent_contact_relation']:
                    data['dependent_contact_relation'] = data['dependent_contact_relation'].dependent_relation_id
                else:
                    logging.debug('Dependent Relation Type Does Not Exists')
                    return {'errorMessage':'Dependent Relation Type Does Not Exists'}
                if data['contact_ssn']:
                    s = base64.b64encode(data['contact_ssn'].encode('ascii')).decode('ascii')
                    if DependentContactModel.query.filter_by(contact_ssn=s,status='A').first() or Employee1Model.query.filter_by(employee_id=employee.employee_id,ssn=s).first():
                        logging.debug('SSN Should Be Unique')
                        return {'errorMessage':'SSN Should Be Unique'}
                try:
                    schema = DependentsSignUp1Schema(unknown=INCLUDE)
                    project = schema.load(data)
                    result = schema.dump(project)
                    addressrecord = Address1Model(
                        address_line1=result['address_line1'],
                        address_line2=result['address_line2'],
                        city=result['city'],
                        state_name=result['state_name'],
                        zip_code=result['zip_code'],
                        country=result['country']
                    )
                    addressrecord.save_to_db()
                    date_obj = datetime.now()
                    contactrecord = DependentContactModel(
                        employee_id=employee.employee_id,
                        contact_first_name=result['contact_first_name'],
                        contact_middle_name=result['contact_middle_name'],
                        contact_last_name=result['contact_last_name'],
                        mobile=result['mobile'],
                        alt_mobile=result['alt_mobile'],
                        contact_email=result['contact_email'].lower(),
                        contact_gender=result['contact_gender'],
                        dependent_relation_id=result['dependent_contact_relation'],
                        contact_dob=base64.b64encode(result['contact_dob'].encode('ascii')).decode('ascii'),
                        contact_ssn=base64.b64encode(result['contact_ssn'].encode('ascii')).decode('ascii'),
                        dependent_address_id=addressrecord.address_id,
                        status = 'A',
                        created_by = userid.employee_id,
                        created_time = date_obj,
                        modified_time = date_obj,
                        same_as_emp_current_add = data['same_as_ca']
                    )
                    contactrecord.save_to_db()
                    employee.modified_time = date_obj
                    employee.save_to_db()
                    logging.debug("Added Successfully")
                    return {"message": "Added Successfully"}
                except ValidationError as err:	  
                    logging.exception('Exception Occured, {0}'.format(err.messages))
                    return {'errorMessage':err.messages},500
                    #raise err
            else:
                logging.debug("Employee Does Not Exist")
                return {"errorMessage":"Employee Does Not Exist"}
        else:
            logging.debug("Please Provide Valid Employee Number")
            return {"errorMessage":"Please Provide Valid Employee Number"}

class DependentsSignUp2Schema(Schema):
    address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    city = fields.String(validate=validate.Length(max=70),required=False)
    state_name = fields.String(validate=validate.Length(max=60),required=False)
    zip_code = fields.String(validate=validate.Length(max=15),required=False)
    country = fields.String(validate=validate.Length(max=60),required=False)
    contact_first_name = fields.String(validate=validate.Length(max=75),required=False)
    contact_middle_name = fields.String(validate=validate.Length(max=75),required=False)
    contact_last_name = fields.String(validate=validate.Length(max=75),required=False)
    mobile = fields.String(validate=validate.Length(max=20),required=False)
    alt_mobile = fields.String(validate=validate.Length(max=20),required=False)
    contact_email = fields.String(validate=validate.Length(max=120),required=False)
    emergency_relation_type = fields.Integer(required=False)

    @post_load
    def create_person(self, data, **kwargs):
        return data

    @validates('contact_email')
    def validate_age(self, contact_email):
        if (contact_email == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',contact_email)) and len(contact_email)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

# this is for new emergency add
class DependentsSignUp2(Resource):
    @jwt_required
    @prefix_decorator('DependentsSignUp2')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        current_emp = Employee1Model.query.filter_by(emp_number=data['employee_number']).first()
        if userid and current_emp:
            try:
                if EmployeeContactModel.query.filter_by(employee_id=current_emp.employee_id,status='A').count() >= 2:
                    logging.debug("Cannot Add More Than 2 Emergency Contacts")
                    return {"errorMessage":"Cannot Add More Than 2 Emergency Contacts"}
                data['emergency_relation_type'] = EmergencyRelationModel.query.filter_by(emergency_relation_type=data['emergency_relation_type']).first()
                if data['emergency_relation_type']:
                    data['emergency_relation_type'] = data['emergency_relation_type'].emergency_relation_id
                else:
                    logging.debug("Something Went Wrong")
                    return {"errorMessage":"Something Went Wrong"}
                schema = DependentsSignUp2Schema(unknown=INCLUDE)
                address = schema.load(data)
                result = schema.dump(address)
                if (result['mobile'] in [current_emp.primary_phone_number if current_emp.primary_phone_number else 0,current_emp.alternate_phone_number if current_emp.alternate_phone_number else 0]) or (result['alt_mobile'] in [current_emp.primary_phone_number if current_emp.primary_phone_number else 0,current_emp.alternate_phone_number if current_emp.alternate_phone_number else 0]):
                    logging.debug("Emergency Phone Number Can Not Be Same as Employee Phone Number")
                    return {"errorMessage":"Emergency Phone Number Can Not Be Same as Employee Phone Number"}
                elif current_emp.email_id and result['contact_email'].lower() and result['contact_email'].lower()==current_emp.email_id:
                    logging.debug("Emergency email Id Can Not Be Same as Employee email Id")
                    return {"errorMessage":"Emergency email Id Can Not Be Same as Employee email Id"}
                emr_emp = EmployeeContactModel.query.filter_by(employee_id=current_emp.employee_id,status='A').all()
                for i in emr_emp:
                    if (result['mobile'] in [i.mobile if i.mobile else 0,i.alt_mobile if i.alt_mobile else 0]) or (result['alt_mobile'] in [i.mobile if i.mobile else 0,i.alt_mobile if i.alt_mobile else 0]):
                        logging.debug("Emergency Phone Number Already Exists for Other Emergency Contact")
                        return {"errorMessage":"Emergency Phone Number Already Exists for Other Emergency Contact"}
                    elif i.contact_email and result['contact_email'].lower()==i.contact_email:
                        logging.debug("Emergency email Id Already Exists for Other Emergency Contact")
                        return {"errorMessage":"Emergency email Id Already Exists for Other Emergency Contact"}
                addressrecord = Address1Model(
                    address_line1=result['address_line1'],
                    address_line2=result['address_line2'],
                    city=result['city'],
                    state_name=result['state_name'],
                    zip_code=result['zip_code'],
                    country=result['country']
                )
                addressrecord.save_to_db()
                date_obj = datetime.now()
                emergencyrecord = EmployeeContactModel(
                    employee_id=current_emp.employee_id,
                    emergency_address_id=addressrecord.address_id,
                    contact_first_name=result['contact_first_name'],
                    contact_middle_name=result['contact_middle_name'],
                    emergency_relation_id=result['emergency_relation_type'],
                    contact_last_name=result['contact_last_name'],
                    mobile=result['mobile'],
                    alt_mobile=result['alt_mobile'],
                    contact_email=result['contact_email'].lower(),
                    created_by = userid.employee_id,
                    created_time = date_obj,
                    modified_time = date_obj,
                    status = 'A',
                    same_as_emp_current_add = data['same_as_ca']
                )
                emergencyrecord.save_to_db()
                current_emp.modified_time = date_obj
                current_emp.save_to_db()
                logging.debug("Added Successfully")
                return {"message": "Added Successfully"}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messsages}
        else:
            logging.debug("Employee Does Not Exist")
            return{"erroeMessage":"Employee Does Not Exist"}

class GetRp(Resource):
    @jwt_required
    @prefix_decorator('GetRp')
    def post(self):
        username = get_jwt_identity()
        data = request.get_json(force=True)
        print(data)
        userid = UserModel.query.filter_by(username=data['username'].lower()).first()
        if userid:
            user_role_obj = UserRolesModel.query.filter_by(user_id=userid.userid).first()
            if user_role_obj:
                list_of_role_privileges = []
                for x in user_role_obj.roles:
                    role_privilege_obj = RolePrivilegeModel.query.filter_by(role_id=int(x)).first().role_privileges
                    if role_privilege_obj:
                        for i in role_privilege_obj:
                            list_of_role_privileges.append(PrivilegeModel.query.filter_by(id=int(i)).first().privilege_name)
                    else:
                        logging.debug('Role {0} Does Not Have Privileges'.format(RoleModel.query.filter_by(id=int(i)).first().role_name))
                        return {'errorMessage':'Role {0} Does Not Have Privileges'.format(RoleModel.query.filter_by(id=int(i)).first().role_name)}
                logging.info("Success")
                return {"user_role_privileges": list_of_role_privileges}
            else:
                logging.debug('No Role(s) Assigned To The User')
                return {'errorMessage':'No Role(s) Assigned To The User'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage':'User Does Not Exist'}


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    @prefix_decorator('UserLogoutRefresh')
    def post(self):
        jti = get_raw_jwt()['jti']
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        logging.info('Refresh Token Has Been Revoked')
        return {'message': 'Refresh Token Has Been Revoked'}


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    @prefix_decorator('TokenRefresh')
    def post(self):
        current_user = get_jwt_identity()
        expires = timedelta(hours=12)
        access_token = create_access_token(identity=current_user, expires_delta=expires)
        logging.info('Success')
        return {'access_token': access_token}

# this endpoint we are using for project resource assignment. Used this in timesheet also change this in timesheet as it is dedicated for project module

class EmployeeDetailsById(Resource):
    @jwt_required
    @prefix_decorator('EmployeeDetailsById')
    def post(self):	
        username = get_jwt_identity()
        data = request.get_json(force=True)
        if not Employee1Model.query.filter_by(employee_id=data['employee_id']).first():
            return {"errorMessage":"Employee With {0} Id Does Not Exist".format(data['employee_id'])}
        print(data)
        current_emp = Employee1Model.find_by_employee_id(data['employee_id'])
        present_address1 = Address1Model.query.filter_by(address_id=current_emp.present_address_id).first()
        permanent_address1 = Address1Model.query.filter_by(address_id=current_emp.permanent_address_id).first()
        work_address1 = Address1Model.query.filter_by(address_id=current_emp.work_address_id).first()
        de_ssn = str(base64.b64decode(current_emp.ssn).decode('ascii'))
        ssnmask = 'xxx-xx-' + de_ssn.split('-')[-1]
        emp_designation = DesignationModel.find_by_designation_id(current_emp.designation_id)
        emp_department = DepartmentModel.find_by_department_id(current_emp.department_id)
        emp_type = EmploymentTypeModel.find_by_emp_type_id(current_emp.emp_type_id)
        empnumber = current_emp.employee_id
        emp_certification = CertificationsModel.return_all(empnumber)  
        emp_education = EducationModel.return_all(data['employee_id'])
        dob = str(base64.b64decode(current_emp.date_of_birth).decode('ascii'))
        emp_work_auth = WorkAuthorizationModel.find_by_employeenumber(empnumber)
        green_card = GreenCardModel.find_by_employeenumber(empnumber)
        emp_cobra = CobraModel.find_by_employeenumber(empnumber)
        emp_benefits = BenefitsModel.find_by_employeenumber(empnumber)
        emp_ead = EadModel.find_by_employeenumber(empnumber)
        emp_visa = EmployeeVisaDetailsModel.find_by_employeenumber(empnumber)
        emp_salary = SalaryModel.find_by_employeenumber(empnumber)
        emergency_contact = EmployeeContactModel.return_all1(empnumber)['users']
        proemp = ProjectEmployeeModel.return_all_empprojects(data['employee_id'])
        dependent_contact = DependentContactModel.return_all1(empnumber)['users']
        roles = [RoleModel.query.filter_by(id=x).first().role_name for x in UserRolesModel.find_by_user_id2(empnumber)]
        reporting_manager_name = Employee1Model.query.filter_by(employee_id=current_emp.reporting_manager_id).first()
        recruiter = current_emp.recruiter
        reporting_manager = reporting_manager_name.first_name + ' ' +  reporting_manager_name.last_name if reporting_manager_name else ''
        termination = TerminationReasonDataModel.query.filter_by(termination_reason_id=current_emp.termination_reason_id).first()
        termination_reason = termination.termination_reason if termination else ''
        termination_intent = termination.termination_intent if termination else ''
        termination_id = current_emp.termination_reason_id
        inactive_reason = EmpInactivationReasonDataModel.query.filter_by(emp_inactive_reason_id = current_emp.inactive_reason_id).first()
        benefits = BenefitsModel.return_all(empnumber)

        if current_emp.employment_end_date:
            employment_end_date = str(current_emp.employment_end_date)
        else:
            employment_end_date = ''

        if current_emp.us_citizen == True:
            record = UsCitizenPassportDetailsModel.query.filter_by(employee_id=current_emp.employee_id).first()
            if record:
                passport_num = record.passport_number
                pass_start_date = record.start_date
                pass_end_date = record.end_date
            else:
                passport_num = ''
                pass_start_date = ''
                pass_end_date = ''
        else:
            passport_num = ''
            pass_start_date = ''
            pass_end_date = ''

        if work_address1:
            work_address_id = work_address1.address_id
            work_address_line1 = work_address1.address_line1
            work_address_line2 = work_address1.address_line2
            work_city = work_address1.city
            work_state_name = work_address1.state_name
            work_country = work_address1.country
            work_zip_code = work_address1.zip_code

        else:
            work_address_id = None
            work_address_line1 = None
            work_address_line2 = None
            work_city = None
            work_state_name = None
            work_country = None
            work_zip_code = None

        if emp_designation:
            emp_designation = emp_designation.designation_name
        else:
            emp_designation = None

        if emp_department:
            emp_department = emp_department.department_name
        else:
            emp_department = None

        if emp_type:
            emp_type = emp_type.emp_type
        else:
            emp_type = None
        if emp_work_auth:
            auth_type = emp_work_auth.auth_type
            auth_start_date = str(emp_work_auth.auth_start_date)
            auth_end_date = str(emp_work_auth.auth_end_date)
        else:
            auth_type = None
            auth_start_date = None
            auth_end_date = None

        if emp_salary:
            salary_pay_type = PayTypeListModel.query.filter_by(pay_type_id=emp_salary.pay_type_id).first().pay_type
            salary_start_date = str(emp_salary.salary_start_date)
            if emp_salary.salary_end_date:
                salary_end_date = str(emp_salary.salary_end_date)
            else:
                salary_end_date = ''
            pay_amount = emp_salary.pay_amount
            payroll_frequency = PayrollFrequencyListModel.query.filter_by(pay_freq_id=emp_salary.pay_freq_id).first().pay_frequency
            if emp_salary.last_rise_date:
                last_rise_date = str(emp_salary.last_rise_date)
            else:
                last_rise_date = ''
            salary_last_rise_amount = emp_salary.salary_last_rise_amount

        else:
            salary_pay_type = ''
            salary_start_date = ''
            salary_end_date = ''
            pay_amount = ''
            payroll_frequency = ''
            last_rise_date = ''
            salary_last_rise_amount = ''

        if green_card:
            gc_job_title = green_card.gc_job_title
            gc_salary = green_card.gc_salary
            gc_prevailing_wage = green_card.gc_prevailing_wage
            gc_priority_date = green_card.priority_date
            gc_current_status = green_card.current_status
            gc_approval_status = green_card.approval_status
        else:
            gc_job_title = ''
            gc_salary = ''
            gc_prevailing_wage = ''
            gc_priority_date = ''
            gc_current_status = ''
            gc_approval_status = ''

        if emp_cobra:
            cobra_start_date = str(emp_cobra.start_date)
            cobra_end_date = str(emp_cobra.end_date)
            cobra_provider_name = emp_cobra.provider_name
            cobra_amount_covered = emp_cobra.amount_covered
            cobra_coverage_details = emp_cobra.coverage_details
        else:
            cobra_start_date = ''
            cobra_end_date = ''
            cobra_provider_name = ''
            cobra_amount_covered = ''
            cobra_coverage_details = ''

#        if emp_benefits:
#            emp_coverage_type = CoverageTypeListModel.query.filter_by(coverage_type_id=emp_benefits.coverage_type_id).first().coverage_type if emp_benefits.coverage_type_id else ''
#            emp_benefit_start_date = emp_benefits.benefit_start_date
#            emp_benefit_status = emp_benefits.benefit_status
#            try: 
#                benefit_types = [BenefitTypeListModel.query.filter_by(benefit_type_id=i).first().benefit_type for i in emp_benefits.benefit_types]
#            except Exception as e:
#                logging.exception('Exception Occured , {0}'.format(str(e)))
#                benefit_types = []
#        else:
#            benefit_types = ''
#            emp_coverage_type = ''
#            emp_benefit_start_date = ''
#            emp_benefit_status = ''
        if emp_ead:
            emp_ead_type = EadTypeListModel.query.filter_by(ead_type_id=emp_ead.ead_type_id).first().ead_type
            emp_valid_from = emp_ead.valid_from
            emp_valid_to = emp_ead.valid_to
            emp_sponsor_name = emp_ead.sponsor_name
        else:
            emp_ead_type = ''
            emp_valid_from = ''
            emp_valid_to = ''
            emp_sponsor_name = ''


        if emp_visa:
            visa_type = VisaTypeModel.find_by_employeenumber(emp_visa.visa_type_id)
            lca_add = Address1Model.query.filter_by(address_id=emp_visa.lca_work_address_id).first()
            lca_altadd = Address1Model.query.filter_by(address_id=emp_visa.lca_alternate_address_id).first()
            visa_number = emp_visa.visa_number
            job_title = emp_visa.job_title
            visa_start_date = emp_visa.visa_start_date
            visa_end_date = emp_visa.visa_end_date
            visa_salary = emp_visa.visa_salary
            attorney = emp_visa.attorney if emp_visa.attorney else ''
            visa_title = emp_visa.visa_title
            lca_salary = emp_visa.lca_salary
            lca_wage_level = LcaWageListModel.query.filter_by(lca_wage_level_id=emp_visa.lca_wage_level_id).first().lca_wage_level if emp_visa.lca_wage_level_id else ''
            lca_prevailing_wages = emp_visa.lca_prevailing_wages
            if emp_visa.lca_start_date:
                lca_start_date = str(emp_visa.lca_start_date)
            else:
                lca_start_date = ''
            if emp_visa.lca_end_date:
                lca_end_date = str(emp_visa.lca_end_date)
            else:
                lca_end_date = ''
            current_status = 'Active' if emp_visa.current_status=='A' else 'Inactive' if emp_visa.current_status=='I' else 'Terminated' if emp_visa.current_status=='T' else ''
            if lca_add:
                lca_add_id = lca_add.address_id
                lca_add_line1 = lca_add.address_line1
                lca_add_line2 = lca_add.address_line2
                lca_add_city = lca_add.city
                lca_add_state = lca_add.state_name
                lca_add_zip = lca_add.zip_code
                lca_add_country = lca_add.country
            else:
                lca_add_id = ''
                lca_add_line1 = ''
                lca_add_line2 = ''
                lca_add_city = ''
                lca_add_state = ''
                lca_add_zip = ''
                lca_add_country = ''

            if lca_altadd:
                lca_altadd_id = lca_altadd.address_id
                lca_altadd_line1 = lca_altadd.address_line1
                lca_altadd_line2 = lca_altadd.address_line2
                lca_altadd_city = lca_altadd.city
                lca_altadd_state = lca_altadd.state_name
                lca_altadd_zip = lca_altadd.zip_code
                lca_altadd_country = lca_altadd.country
            else:
                lca_altadd_id = ''
                lca_altadd_line1 = ''
                lca_altadd_line2 = ''
                lca_altadd_city = ''
                lca_altadd_state = ''
                lca_altadd_zip = ''
                lca_altadd_country = ''

        else:
            attorney = ''
            visa_type = ''
            visa_number = ''
            job_title = ''
            visa_start_date = ''
            visa_end_date = ''
            visa_salary = ''
            visa_title = ''
            lca_work_location = ''
            lca_alternate_location = ''
            lca_salary = ''
            lca_wage_level = ''
            lca_prevailing_wages = ''
            lca_start_date = ''
            lca_end_date = ''
            current_status = ''
            lca_add_id = ''
            lca_add_line1 = ''
            lca_add_line2 = ''
            lca_add_city = ''
            lca_add_state = ''
            lca_add_zip = ''
            lca_add_country = ''
            lca_altadd_id = ''
            lca_altadd_line1 = ''
            lca_altadd_line2 = ''
            lca_altadd_city = ''
            lca_altadd_state = ''
            lca_altadd_zip = ''
            lca_altadd_country = ''

        return {
            "benefits" :benefits,
            "inactive_reason_id" : inactive_reason.emp_inactive_reason if inactive_reason else '',
            "same_as_ca":current_emp.same_as_ca if current_emp.same_as_ca else '' ,
            "resource_billable":"Yes" if current_emp.billable_resource == 'Y' else "No", 
            "attroney_name":attorney,
            "termination_date":str(current_emp.termination_date),
            "comments" : current_emp.comments,
            "employee_Status_last_updated_date":str(current_emp.employee_status_last_updtd_date).split(' ')[0] if current_emp.employee_status_last_updtd_date else '',
            'role': roles,
            'employee_id': current_emp.employee_id,
            'project_details': proemp['employee_projects'],
            'employee_number': current_emp.emp_number,
            'first_name': current_emp.first_name,
            'middle_name': current_emp.middle_name,
            'reporting_manager_id': current_emp.reporting_manager_id,
            'last_name': current_emp.last_name,
            'reporting_manager_name': reporting_manager,
            'recruiter_name': recruiter,
            'email': current_emp.email_id.lower(),
            'date_of_birth': 'xx-xx-xxxx',
            'ssn': 'xxx-xx-xxxx',
            'gender': 'Male' if current_emp.gender=='M' else 'Female' if current_emp.gender=='F' else 'Other' if current_emp.gender=='O' else '',
            'primary_phone_number': current_emp.primary_phone_number,
            'alternate_phone_number': current_emp.alternate_phone_number,
            'marital_status': 'Single' if current_emp.marital_status=='S' else 'Married' if current_emp.marital_status=='M' else 'Divorced' if current_emp.marital_status=='D' else 'Widowed' if current_emp.marital_status=='W' else '',
            'present_add_id': present_address1.address_id,
            'present_line1': present_address1.address_line1,
            'present_line2': present_address1.address_line2,
            'present_city': present_address1.city,
            'present_state': present_address1.state_name,
            'present_country': present_address1.country,
            'present_zip': present_address1.zip_code,
            'permanent_add_id': permanent_address1.address_id,
            'permanent_line1': permanent_address1.address_line1,
            'permanent_line2': permanent_address1.address_line2,
            'permanent_city': permanent_address1.city,
            'permanent_state': permanent_address1.state_name,
            'permanent_country': permanent_address1.country,
            'permanent_zip': permanent_address1.zip_code,
            'work_add_id': work_address_id,
            'work_line1': work_address_line1,
            'work_line2': work_address_line2,
            'work_city': work_city,
            'work_state': work_state_name,
            'work_country': work_country,
            'work_zip': work_zip_code,
            'employment_start_date': str(current_emp.employment_start_date),
            'employment_end_date': str(employment_end_date),
            'recruitment_method': current_emp.recruitment_method,
            'sick_days': current_emp.eligible_sick_hours if current_emp.eligible_sick_hours  else '',
            'eligible_vacation_days':  current_emp.eligible_vacation_weeks if current_emp.eligible_vacation_weeks else '',
            'paid_holidays':'Yes' if current_emp.paid_holidays is True else 'No' if  current_emp.paid_holidays is False else '', 
            'employee_status': 'Active' if current_emp.employee_status=='A' else 'Inactive' if current_emp.employee_status=='I' else 'Terminated' if current_emp.employee_status=='T' else '',
            'termination_reason': termination_reason,
            'termination_intent': termination_intent,
            'termination_id': termination_id,
            'rehire_eligibility': current_emp.rehire_eligibility,
            'cobra_eligibility': current_emp.cobra_eligibility,
            'benefits_eligibility': current_emp.benefits_eligibility,
            'certifications': emp_certification,
            'education_details': emp_education['education_details'], 
            'auth_type': auth_type,
            'auth_start_date': str(auth_start_date),
            'auth_end_date': str(auth_end_date),
            'gc_job_title': gc_job_title,
            'gc_salary': str(gc_salary),
            'gc_prevailing_wage': str(gc_prevailing_wage),
            'gc_priority_date': str(gc_priority_date),
            'gc_current_status': gc_current_status,
            'gc_approval_status': gc_approval_status,
            'cobra_start_date': str(cobra_start_date),
            'cobra_end_date': str(cobra_end_date),
            'cobra_provider_name': cobra_provider_name,
            'cobra_amount_covered': str(cobra_amount_covered),
            'cobra_coverage_details': cobra_coverage_details,
            'emp_ead_type': emp_ead_type,
            'emp_valid_from': str(emp_valid_from),
            'emp_valid_to': str(emp_valid_to),
            'emp_sponsor_name': emp_sponsor_name,
            'salary_pay_type': salary_pay_type,
            'salary_start_date': str(salary_start_date),
            'salary_end_date': str(salary_end_date),
            'pay_amount': str(pay_amount),
            'payroll_frequency': payroll_frequency,
            'last_rise_date': str(last_rise_date),
            'salary_last_rise_amount': str(salary_last_rise_amount),
            'employee_designation': emp_designation,
            'employee_department': emp_department,
            'employee_type': emp_type,
            'emergency_contact': emergency_contact,
            'dependent_contact': dependent_contact,
            'visa_type': visa_type,
            'visa_number': visa_number,
            'job_title': job_title,
            'visa_start_date': str(visa_start_date),
            'visa_end_date': str(visa_end_date),
            'visa_salary': str(visa_salary) if visa_salary else '',
            'visa_title': visa_title,
            'lca_salary': str(lca_salary) if lca_salary else '',
            'lca_wage_level': lca_wage_level,
            'lca_prevailing_wages': str(lca_prevailing_wages) if lca_prevailing_wages else '',
            'lca_start_date': str(lca_start_date),
            'lca_end_date': str(lca_end_date),
            'current_status': current_status,
            'lca_add_id': lca_add_id,
            'lca_add_line1': lca_add_line1,
            'lca_add_line2': lca_add_line2,
            'lca_add_city': lca_add_city,
            'lca_add_state': lca_add_state,
            'lca_add_zip': lca_add_zip,
            'lca_add_country': lca_add_country,
            'lca_altadd_id': lca_altadd_id,
            'lca_altadd_line1': lca_altadd_line1,
            'lca_altadd_line2': lca_altadd_line2,
            'lca_altadd_city': lca_altadd_city,
            'lca_altadd_state': lca_altadd_state,
            'lca_altadd_zip': lca_altadd_zip,
            'lca_altadd_country': lca_altadd_country,
            'account_manager':current_emp.account_manager,
            'us_citizenship':current_emp.us_citizen if current_emp.us_citizen else '',
            'passport_number': passport_num,
            'passport_start_date' : str(pass_start_date),
            'passport_expiration_date': str(pass_end_date)
        }

class EmployeeDetails(Resource):
    @jwt_required
    @prefix_decorator('EmployeeDetails')
    def post(self):	
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        if not Employee1Model.query.filter_by(emp_number=data['employee_number']).first():
            return {"errorMessage":"Employee With {0} EMployee Number Does Not Exist".format(data['employee_number'])}

        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        present_address1 = Address1Model.query.filter_by(address_id=current_emp.present_address_id).first()
        permanent_address1 = Address1Model.query.filter_by(address_id=current_emp.permanent_address_id).first()
        work_address1 = Address1Model.query.filter_by(address_id=current_emp.work_address_id).first()
        de_ssn = str(base64.b64decode(current_emp.ssn).decode('ascii'))
        ssnmask = 'xxx-xx-' + de_ssn.split('-')[-1]
        dob = str(base64.b64decode(current_emp.date_of_birth).decode('ascii'))
        emp_designation = DesignationModel.find_by_designation_id(current_emp.designation_id)
        emp_department = DepartmentModel.find_by_department_id(current_emp.department_id)
        emp_type = EmploymentTypeModel.find_by_emp_type_id(current_emp.emp_type_id)
        empnumber = current_emp.employee_id
        emp_certification = CertificationsModel.return_all(empnumber)
        emp_education = EducationModel.return_all(current_emp.employee_id)
        emp_work_auth = WorkAuthorizationModel.find_by_employeenumber(empnumber)
        roles = [RoleModel.query.filter_by(id=x).first().role_name for x in UserRolesModel.find_by_user_id2(empnumber)]
        proemp = ProjectEmployeeModel.return_all_empprojects(empnumber)
        green_card = GreenCardModel.find_by_employeenumber(empnumber)
        emp_cobra = CobraModel.find_by_employeenumber(empnumber)
        emp_benefits = BenefitsModel.find_by_employeenumber(empnumber)
        emp_ead = EadModel.find_by_employeenumber(empnumber)
        emp_visa = EmployeeVisaDetailsModel.find_by_employeenumber(empnumber)
        emp_salary = SalaryModel.find_by_employeenumber(empnumber)
        emergency_contact = EmployeeContactModel.return_all1(empnumber)['users']
        dependent_contact = DependentContactModel.return_all1(empnumber)['users']
        reporting_manager_name = Employee1Model.query.filter_by(employee_id=current_emp.reporting_manager_id).first()
        recruiter = current_emp.recruiter
        reporting_manager = reporting_manager_name.first_name + ' ' +  reporting_manager_name.last_name if reporting_manager_name else ''
        termination = TerminationReasonDataModel.query.filter_by(termination_reason_id=current_emp.termination_reason_id).first()
        termination_reason = termination.termination_reason if termination else ''
        termination_intent = termination.termination_intent if termination else ''
        termination_id = current_emp.termination_reason_id
        inactive_reason = EmpInactivationReasonDataModel.query.filter_by(emp_inactive_reason_id = current_emp.inactive_reason_id).first()
        benefits = BenefitsModel.return_all(empnumber)

        if current_emp.employment_end_date:
            employment_end_date = str(current_emp.employment_end_date)
        else:
            employment_end_date = ''

        if current_emp.us_citizen == True:
            record = UsCitizenPassportDetailsModel.query.filter_by(employee_id=current_emp.employee_id).first()
            if record:
                passport_num = record.passport_number
                pass_start_date = record.start_date
                pass_end_date = record.end_date
            else:
                passport_num = ''
                pass_start_date = ''
                pass_end_date = ''
        else:
            passport_num = ''
            pass_start_date = ''
            pass_end_date = ''

        if work_address1:
            work_address_id = work_address1.address_id
            work_address_line1 = work_address1.address_line1
            work_address_line2 = work_address1.address_line2
            work_city = work_address1.city
            work_state_name = work_address1.state_name
            work_country = work_address1.country
            work_zip_code = work_address1.zip_code
        else:
            work_address_id = None
            work_address_line1 = None
            work_address_line2 = None
            work_city = None
            work_state_name = None
            work_country = None
            work_zip_code = None

        if emp_designation:
            emp_designation = emp_designation.designation_name
        else:
            emp_designation = None

        if emp_department:
            emp_department = emp_department.department_name
        else:
            emp_department = None

        if emp_type:
            emp_type = emp_type.emp_type
        else:
            emp_type = None

        if emp_work_auth:
            auth_type = emp_work_auth.auth_type
            auth_start_date = str(emp_work_auth.auth_start_date)
            auth_end_date = str(emp_work_auth.auth_end_date)
        else:
            auth_type = None
            auth_start_date = None
            auth_end_date = None

        if emp_salary:
            salary_pay_type = PayTypeListModel.query.filter_by(pay_type_id=emp_salary.pay_type_id).first().pay_type
            salary_start_date = str(emp_salary.salary_start_date)
            if emp_salary.salary_end_date:
                salary_end_date = str(emp_salary.salary_end_date)
            else:
                salary_end_date = ''
            pay_amount = emp_salary.pay_amount
            payroll_frequency = PayrollFrequencyListModel.query.filter_by(pay_freq_id=emp_salary.pay_freq_id).first().pay_frequency
            if emp_salary.last_rise_date:
                last_rise_date = str(emp_salary.last_rise_date)
            else:
                last_rise_date = ''
            salary_last_rise_amount = emp_salary.salary_last_rise_amount
        else:
            salary_pay_type = ''
            salary_start_date = ''
            salary_end_date = ''
            pay_amount = ''
            payroll_frequency = ''
            last_rise_date = ''
            salary_last_rise_amount = ''

        if green_card:
            gc_job_title = green_card.gc_job_title
            gc_salary = green_card.gc_salary
            gc_prevailing_wage = green_card.gc_prevailing_wage
            gc_priority_date = green_card.priority_date
            gc_current_status = green_card.current_status
            gc_approval_status = green_card.approval_status
        else:
            gc_job_title = ''
            gc_salary = ''
            gc_prevailing_wage = ''
            gc_priority_date = ''
            gc_current_status = ''
            gc_approval_status = ''

        if emp_cobra:
            cobra_start_date = str(emp_cobra.start_date)
            cobra_end_date = str(emp_cobra.end_date)
            cobra_provider_name = emp_cobra.provider_name
            cobra_amount_covered = emp_cobra.amount_covered
            cobra_coverage_details = emp_cobra.coverage_details
        else:
            cobra_start_date = ''
            cobra_end_date = ''
            cobra_provider_name = ''
            cobra_amount_covered = ''
            cobra_coverage_details = ''

#        if emp_benefits:
#            emp_coverage_type = CoverageTypeListModel.query.filter_by(coverage_type_id=emp_benefits.coverage_type_id).first().coverage_type if emp_benefits.coverage_type_id else ''
 #           emp_benefit_start_date = emp_benefits.benefit_start_date
#            emp_benefit_status = emp_benefits.benefit_status
#            try: 
#                benefit_types = [BenefitTypeListModel.query.filter_by(benefit_type_id=i).first().benefit_type for i in emp_benefits.benefit_types]
#            except Exception as e:
#                logging.exception('Exception Occured , {0}'.format(str(e)))
#                benefit_types = []
#        else:
#            benefit_types = ''
#            emp_coverage_type = ''
#            emp_benefit_start_date = ''
#            emp_benefit_status = ''
        if emp_ead:
            emp_ead_type = EadTypeListModel.query.filter_by(ead_type_id=emp_ead.ead_type_id).first().ead_type
            emp_valid_from = emp_ead.valid_from
            emp_valid_to = emp_ead.valid_to
            emp_sponsor_name = emp_ead.sponsor_name
        else:
            emp_ead_type = ''
            emp_valid_from = ''
            emp_valid_to = ''
            emp_sponsor_name = ''

        if emp_visa:
            visa_type = VisaTypeModel.find_by_employeenumber(emp_visa.visa_type_id)
            lca_add = Address1Model.query.filter_by(address_id=emp_visa.lca_work_address_id).first()
            lca_altadd = Address1Model.query.filter_by(address_id=emp_visa.lca_alternate_address_id).first()
            visa_number = emp_visa.visa_number
            job_title = emp_visa.job_title
            visa_start_date = emp_visa.visa_start_date
            visa_end_date = emp_visa.visa_end_date
            visa_salary = emp_visa.visa_salary
            attorney = emp_visa.attorney if emp_visa.attorney else ''
            visa_title = emp_visa.visa_title
            lca_salary = emp_visa.lca_salary
            lca_wage_level = LcaWageListModel.query.filter_by(lca_wage_level_id=emp_visa.lca_wage_level_id).first().lca_wage_level if emp_visa.lca_wage_level_id else ''
            lca_prevailing_wages = emp_visa.lca_prevailing_wages
            if emp_visa.lca_start_date:
                lca_start_date = str(emp_visa.lca_start_date)
            else:
                lca_start_date = ''
            if emp_visa.lca_end_date:
                lca_end_date = str(emp_visa.lca_end_date)
            else:
                lca_end_date = ''
            current_status = 'Active' if emp_visa.current_status=='A' else 'Inactive' if emp_visa.current_status=='I' else 'Terminated' if emp_visa.current_status=='T' else ''

            if lca_add:
                lca_add_id = lca_add.address_id
                lca_add_line1 = lca_add.address_line1
                lca_add_line2 = lca_add.address_line2
                lca_add_city = lca_add.city
                lca_add_state = lca_add.state_name
                lca_add_zip = lca_add.zip_code
                lca_add_country = lca_add.country
            else:
                lca_add_id = ''
                lca_add_line1 = ''
                lca_add_line2 = ''
                lca_add_city = ''
                lca_add_state = ''
                lca_add_zip = ''
                lca_add_country = ''

            if lca_altadd:
                lca_altadd_id = lca_altadd.address_id
                lca_altadd_line1 = lca_altadd.address_line1
                lca_altadd_line2 = lca_altadd.address_line2
                lca_altadd_city = lca_altadd.city
                lca_altadd_state = lca_altadd.state_name
                lca_altadd_zip = lca_altadd.zip_code
                lca_altadd_country = lca_altadd.country
            else:
                lca_altadd_id = ''
                lca_altadd_line1 = ''
                lca_altadd_line2 = ''
                lca_altadd_city = ''
                lca_altadd_state = ''
                lca_altadd_zip = ''
                lca_altadd_country = ''
        else:

            attorney = ''
            visa_type = ''
            visa_number = ''
            job_title = ''
            visa_start_date = ''
            visa_end_date = ''
            visa_salary = ''
            visa_title = ''
            lca_work_location = ''
            lca_alternate_location = ''
            lca_salary = ''
            lca_wage_level = ''
            lca_prevailing_wages = ''
            lca_start_date = ''
            lca_end_date = ''
            current_status = ''
            lca_add_id = ''
            lca_add_line1 = ''
            lca_add_line2 = ''
            lca_add_city = ''
            lca_add_state = ''
            lca_add_zip = ''
            lca_add_country = ''
            lca_altadd_id = ''
            lca_altadd_line1 = ''
            lca_altadd_line2 = ''
            lca_altadd_city = ''
            lca_altadd_state = ''
            lca_altadd_zip = ''
            lca_altadd_country = ''

        return {
            "benefits" : benefits,
            "inactive_reason_id" : str(inactive_reason.emp_inactive_reason) if inactive_reason else '',
            "same_as_ca": str(current_emp.same_as_ca) if current_emp.same_as_ca else '' ,
            "resource_billable": "Yes" if current_emp.billable_resource == 'Y' else "No", 
            "attorney_name": str(attorney),
            "termination_date":str(current_emp.termination_date),
            "comments" : str(current_emp.comments),
            "employee_Status_last_updated_date": str(current_emp.employee_status_last_updtd_date).split(' ')[0] if current_emp.employee_status_last_updtd_date else '',
            'project_details': proemp['employee_projects'],
            'employee_id': current_emp.employee_id,
            'employee_number': current_emp.emp_number,
            'first_name': current_emp.first_name,
            'middle_name': current_emp.middle_name,
            'last_name': current_emp.last_name,
            'reporting_manager_name': reporting_manager,
            'recruiter_name': recruiter,
            'email': current_emp.email_id.lower() if current_emp.email_id else '',
            'date_of_birth': 'xx-xx-xxxx',
            'reporting_manager_id': current_emp.reporting_manager_id,
            'ssn': 'xxx-xx-xxxx',
            'gender': 'Male' if current_emp.gender=='M' else 'Female' if current_emp.gender=='F' else 'Other' if current_emp.gender=='O' else '',
            'primary_phone_number': current_emp.primary_phone_number,
            'alternate_phone_number': current_emp.alternate_phone_number,
            'marital_status': 'Single' if current_emp.marital_status=='S' else 'Married' if current_emp.marital_status=='M' else 'Divorced' if current_emp.marital_status=='D' else 'Widowed' if current_emp.marital_status=='W' else '',
            'present_add_id': present_address1.address_id,
            'present_line1': present_address1.address_line1,
            'present_line2': present_address1.address_line2,
            'present_city': present_address1.city,
            'present_state': present_address1.state_name,
            'present_country': present_address1.country,
            'present_zip': present_address1.zip_code,
            'permanent_add_id': permanent_address1.address_id,
            'permanent_line1': permanent_address1.address_line1,
            'permanent_line2': permanent_address1.address_line2,
            'permanent_city': permanent_address1.city,
            'permanent_state': permanent_address1.state_name,
            'permanent_country': permanent_address1.country,
            'permanent_zip': permanent_address1.zip_code,
            'work_add_id': work_address_id,
            'work_line1': work_address_line1,
            'work_line2': work_address_line2,
            'work_city': work_city,
            'work_state': work_state_name,
            'work_country': work_country,
            'work_zip': work_zip_code,
            'employment_start_date': str(current_emp.employment_start_date),
            'employment_end_date': str(employment_end_date),
            'recruitment_method': current_emp.recruitment_method,
            'sick_days': current_emp.eligible_sick_hours if current_emp.eligible_sick_hours  else '',
            'eligible_vacation_days':  current_emp.eligible_vacation_weeks if current_emp.eligible_vacation_weeks else '',
            'paid_holidays':'Yes' if current_emp.paid_holidays is True else 'No' if  current_emp.paid_holidays is False else '', 
            'employee_status': 'Active' if current_emp.employee_status=='A' else 'Inactive' if current_emp.employee_status=='I' else 'Terminated' if current_emp.employee_status=='T' else '',
            'termination_reason': termination_reason,
            'termination_intent': termination_intent,
            'termination_id': termination_id,
            'rehire_eligibility': current_emp.rehire_eligibility,
            'cobra_eligibility': current_emp.cobra_eligibility,
            'benefits_eligibility': current_emp.benefits_eligibility,
            'certifications': emp_certification,
            'education_details': emp_education['education_details'],
            'auth_type': auth_type,
            'auth_start_date': str(auth_start_date),
            'auth_end_date': str(auth_end_date),
            'gc_job_title': gc_job_title,
            'gc_salary': str(gc_salary),
            'gc_prevailing_wage': str(gc_prevailing_wage),
            'gc_priority_date': str(gc_priority_date),
            'gc_current_status': gc_current_status,
            'gc_approval_status': gc_approval_status,
            'cobra_start_date': str(cobra_start_date),
            'cobra_end_date': str(cobra_end_date),
            'cobra_provider_name': cobra_provider_name,
            'cobra_amount_covered': str(cobra_amount_covered),
            'cobra_coverage_details': cobra_coverage_details,
            'emp_ead_type': emp_ead_type,
            'emp_valid_from': str(emp_valid_from),
            'emp_valid_to': str(emp_valid_to),
            'emp_sponsor_name': emp_sponsor_name,
            'salary_pay_type': salary_pay_type,
            'salary_start_date': str(salary_start_date),
            'salary_end_date': str(salary_end_date),
            'pay_amount': str(pay_amount),
            'payroll_frequency': payroll_frequency,
            'last_rise_date': str(last_rise_date),
            'salary_last_rise_amount': str(salary_last_rise_amount),
            'employee_designation': emp_designation,
            'employee_department': emp_department,
            'employee_type': emp_type,
            'emergency_contact': emergency_contact,
            'dependent_contact': dependent_contact,
            'visa_type': visa_type,
            'visa_number': visa_number,
            'job_title': job_title,
            'visa_start_date': str(visa_start_date),
            'visa_end_date': str(visa_end_date),
            'visa_salary': str(visa_salary) if visa_salary else '',
            'visa_title': visa_title,
            'lca_salary': str(lca_salary) if lca_salary else '',
            'lca_wage_level': lca_wage_level,
            'lca_prevailing_wages': str(lca_prevailing_wages) if lca_prevailing_wages else '',
            'lca_start_date': str(lca_start_date),
            'lca_end_date': str(lca_end_date),
            'current_status': current_status,
            'lca_add_id': lca_add_id,
            'lca_add_line1': lca_add_line1,
            'lca_add_line2': lca_add_line2,
            'lca_add_city': lca_add_city,
            'lca_add_state': lca_add_state,
            'lca_add_zip': lca_add_zip,
            'lca_add_country': lca_add_country,
            'lca_altadd_id': lca_altadd_id,
            'lca_altadd_line1': lca_altadd_line1,
            'lca_altadd_line2': lca_altadd_line2,
            'lca_altadd_city': lca_altadd_city,
            'lca_altadd_state': lca_altadd_state,
            'lca_altadd_zip': lca_altadd_zip,
            'lca_altadd_country': lca_altadd_country,
            'role': roles,
            'account_manager':current_emp.account_manager,
            'us_citizenship': current_emp.us_citizen if current_emp.us_citizen else '',
            'passport_number': passport_num,
            'passport_start_date' : str(pass_start_date),
            'passport_expiration_date': str(pass_end_date)
        }

class AddressEditSchema(Schema):
    employee_number = fields.Integer(required=False)
    address_field = fields.String(validate=validate.Length(max=20),required=False)
    address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    city = fields.String(validate=validate.Length(max=100),required=False)
    state_name = fields.String(validate=validate.Length(max=100),required=False)
    zip_code = fields.String(required=False)
    country = fields.String(validate=validate.Length(max=100),required=False)

    @validates('zip_code')
    def validate_age(self, zip_code):
        if (len(zip_code)>6 and not zip_code.isdigit() and len(zip_code)<5):
            raise ValidationError('The zip code provided is invalid')

    @validates('address_line2')
    def validate_age(self, address_line2):
        if (address_line2==''):
            pass

    @post_load
    def save_address(self, data, **kwargs):
        return data

class Address1Edit(Resource):
    @jwt_required
    @prefix_decorator('Address1Edit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        schema = AddressEditSchema(unknown=INCLUDE)
        address = schema.load(data)
        result = schema.dump(address)
        date_obj = datetime.now()
        current_emp = Employee1Model.find_by_employeenumber(result['employee_number'])
        if current_emp:
            if data['address_id']:
                if data['address_field'] == 'present_address_id':
                    dep_rec = DependentContactModel.query.filter_by(employee_id=current_emp.employee_id).all()
                    emer_rec = EmployeeContactModel.query.filter_by(employee_id=current_emp.employee_id).all()
                    if current_emp.same_as_ca:
                        padd_rec = Address1Model.query.filter_by(address_id=current_emp.permanent_address_id).first()
                        padd_rec.address_line1 = result['address_line1']
                        padd_rec.address_line2 = result['address_line2']
                        padd_rec.city = result['city']
                        padd_rec.state_name = result['state_name']
                        padd_rec.zip_code = result['zip_code']
                        padd_rec.country = result['country']
                        # commit changes
                        padd_rec.save_to_db()
                    if emer_rec:
                        for i in emer_rec:
                            if i.same_as_emp_current_add:
                                emer_add_rec = Address1Model.query.filter_by(address_id=i.emergency_address_id).first()
                                emer_add_rec.address_line1 = result['address_line1']
                                emer_add_rec.address_line2 = result['address_line2']
                                emer_add_rec.city = result['city']
                                emer_add_rec.state_name = result['state_name']
                                emer_add_rec.zip_code = result['zip_code']
                                emer_add_rec.country = result['country']
                                # commit changes
                                emer_add_rec.save_to_db()
                    if dep_rec:
                        for i in dep_rec:
                            if i.same_as_emp_current_add:
                                dep_add_rec = Address1Model.query.filter_by(address_id=i.dependent_address_id).first()
                                dep_add_rec.address_line1 = result['address_line1']
                                dep_add_rec.address_line2 = result['address_line2']
                                dep_add_rec.city = result['city']
                                dep_add_rec.state_name = result['state_name']
                                dep_add_rec.zip_code = result['zip_code']
                                dep_add_rec.country = result['country']
                                # commit changes
                                dep_add_rec.save_to_db()
                elif result['address_field'] == 'permanent_address_id':
                    current_emp1 = Employee1Model.find_by_employeenumber(result['employee_number'])
                    current_emp1.same_as_ca = data['same_as_ca']
                    current_emp1.save_to_db()
                record = Address1Model.query.filter_by(address_id=data['address_id']).first()
                record.address_line1 = result['address_line1']
                record.address_line2 = result['address_line2']
                record.city = result['city']
                record.state_name = result['state_name']
                record.zip_code = result['zip_code']
                record.country = result['country']
                # commit changes
                record.save_to_db()
                current_emp.modified_time = date_obj
                current_emp.save_to_db()
                return {"message": "Address Updated Sucessfully"}
                
            else:
                a = Address1Model(address_line1=result['address_line1'],
                                  address_line2=result['address_line2'],
                                  city=result['city'],
                                  state_name=result['state_name'],
                                  zip_code=result['zip_code'],
                                  country=result['country'])
                a.save_to_db()
                if result['address_field'] == 'present_address_id':
                    current_emp.present_address_id = a.address_id
                    current_emp.modified_time = date_obj
                elif result['address_field'] == 'permanent_address_id':
                    current_emp.permanent_address_id = a.address_id
                    current_emp.modified_time = date_obj
                elif result['address_field'] == 'work_address_id':
                    current_emp.work_address_id = a.address_id
                    current_emp.modified_time = date_obj
                current_emp.save_to_db()
                return {"message": "Address Updated Sucessfully"}
        else:
            return{"errorMessage":"Employee Does Not Exist"}

class DependentsSignUp2Schema(Schema):
    emergency_address_id = fields.Integer(required=False)
    address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    city = fields.String(validate=validate.Length(max=70),required=False)
    state_name = fields.String(validate=validate.Length(max=60),required=False)
    zip_code = fields.String(validate=validate.Length(max=15),required=False)
    country = fields.String(validate=validate.Length(max=60),required=False)
    contact_first_name = fields.String(validate=validate.Length(max=75),required=False)
    contact_middle_name = fields.String(validate=validate.Length(max=75),required=False)
    contact_last_name = fields.String(validate=validate.Length(max=75),required=False)
    mobile = fields.String(validate=validate.Length(max=20),required=False)
    alt_mobile = fields.String(validate=validate.Length(max=20),required=False)
    contact_email = fields.String(validate=validate.Length(max=120),required=False)
    emergency_relation_type = fields.Integer(required=False)
    contact_id = fields.Integer(required=False)


    @validates('mobile')
    def validate_age(self, mobile):
        if len(mobile) !=10:
            raise ValidationError('The Mobile Number is invalid')

    @validates('alt_mobile')
    def validate_age(self, alt_mobile):
        if len(mobile) == 0 or len(mobile) == 10:
            pass
        else:
            raise ValidationError('The Alternate Mobile Number is invalid')


    @validates('contact_email')
    def validate_age(self, contact_email):
        if (contact_email == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',contact_email)) and len(contact_email)<120:
            pass
        else:
            raise ValidationError('The email is invalid')

    @post_load
    def create_person(self, data, **kwargs):
        return data



class EmployeeEmergencyEdit1(Resource):
    @jwt_required
    @prefix_decorator('EmployeeEmergencyEdit1')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        date_obj = datetime.now()
        if userid and current_emp:
            det = EmergencyRelationModel.query.filter_by(emergency_relation_type=data['emergency_relation_type']).first()
            if det:
                data['emergency_relation_type'] = det.emergency_relation_id
            else:
                logging.debug("Please Choose Valid Emergency Relation Type")
                return {"errorMessage":"Please Choose Valid Emergency Relation Type"}
            try:    
                schema = DependentsSignUp2Schema(unknown=INCLUDE)
                address = schema.load(data)
                result = schema.dump(address)
                if (result['mobile'] in [current_emp.primary_phone_number if current_emp.primary_phone_number else 0,current_emp.alternate_phone_number if current_emp.alternate_phone_number else 0]) or (result['alt_mobile'] in [current_emp.primary_phone_number if current_emp.primary_phone_number else 0,current_emp.alternate_phone_number if current_emp.alternate_phone_number else 0]):
                    logging.debug("Emergency Phone Number Can Not Be Same as Employee Phone Number")
                    return {"errorMessage":"Emergency Phone Number Can Not Be Same as Employee Phone Number"}
                elif result['contact_email'].lower()==current_emp.email_id:
                    logging.debug("Emergency email Id Can Not Be Same as Employee email Id")
                    return {"errorMessage":"Emergency email Id Can Not Be Same as Employee email Id"}
                emr_emp = EmployeeContactModel.query.filter_by(employee_id=current_emp.employee_id,status='A').all()
                for i in emr_emp:
                    if (result['mobile'] in [i.mobile if (i.mobile and i.contact_id!=result['contact_id']) else 0,i.alt_mobile if (i.alt_mobile and i.contact_id!=result['contact_id']) else 0]) or (result['alt_mobile'] in [i.mobile if (i.mobile and i.contact_id!=result['contact_id']) else 0,i.alt_mobile if (i.alt_mobile and i.contact_id!=result['contact_id']) else 0]):
                        logging.debug("Emergency Phone Number Already Exists for Other Emergency Contact")
                        return {"errorMessage":"Emergency Phone Number Already Exists for Other Emergency Contact"}
                    elif i.contact_email and(result['contact_email'].lower()==i.contact_email) and (i.contact_id!=result['contact_id']):
                        logging.debug("Emergency email Id Already Exists for Other Emergency Contact")
                        return {"errorMessage":"Emergency email Id Already Exists for Other Emergency Contact"}
                conrecord = EmployeeContactModel.query.filter_by(employee_id=current_emp.employee_id,contact_id=data['contact_id']).first()
                conaddress = Address1Model.query.filter_by(address_id=result['emergency_address_id']).first()
                if conaddress and conrecord:
                    conrecord.contact_first_name = result['contact_first_name']
                    conrecord.contact_middle_name = result['contact_middle_name']
                    conrecord.contact_last_name = result['contact_last_name']
                    conrecord.mobile = result['mobile']
                    conrecord.alt_mobile = result['alt_mobile']
                    conrecord.contact_email = result['contact_email'].lower()
                    conrecord.same_as_emp_current_add=data['same_as_ca']
                    conrecord.modified_by = userid.employee_id
                    conrecord.modified_time = date_obj
                    conrecord.emergency_relation_id = result['emergency_relation_type']
                    conaddress.address_line1 = result['address_line1']
                    conaddress.address_line2 = result['address_line2']
                    conaddress.city = result['city']
                    conaddress.state_name = result['state_name']
                    conaddress.zip_code = result['zip_code']
                    conaddress.country = result['country']
                    conrecord.save_to_db()
                    conaddress.save_to_db()
                    current_emp.modified_time = date_obj
                    current_emp.save_to_db()
                    logging.debug('Details Updated Successfully')
                    return {'message': 'Details Updated Successfully'}
                else:
                    logging.debug("Emergency Contact Record Not Found")
                    return {"errorMessage":"Emergency Contact Record Not Found"}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
        else:
            logging.debug()
            return {"errorMessage":"User Does Not Exist"} 

class DependentsSignUp1Schema(Schema):
    address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    city = fields.String(validate=validate.Length(max=70),required=False)
    state_name = fields.String(validate=validate.Length(max=60),required=False)
    zip_code = fields.String(validate=validate.Length(max=15),required=False)
    country = fields.String(validate=validate.Length(max=60),required=False)
    contact_first_name = fields.String(validate=validate.Length(max=75),required=False)
    contact_middle_name = fields.String(validate=validate.Length(max=75),required=False)
    contact_last_name = fields.String(validate=validate.Length(max=75),required=False)
    mobile = fields.String(validate=validate.Length(max=20),required=False)
    alt_mobile = fields.String(validate=validate.Length(max=20),required=False)
    contact_email = fields.String(validate=validate.Length(max=120),required=False)
    contact_gender = fields.String(validate=validate.Length(max=1),required=False)
    dependent_contact_relation = fields.Integer(required=False)
    contact_dob = fields.String(validate=validate.Length(max=120),required=False)
    contact_ssn = fields.String(validate=validate.Length(max=20),required=False)


    @validates('contact_email')
    def validate_age(self, contact_email):
        if (contact_email == '' or re.search('^[a-zA-Z0-9\._]+[@]\w+[.]\w{2,3}$',contact_email)) and len(contact_email)<120:
            pass
        else:
            raise ValidationError('The email is invalid')
    @validates('mobile')
    def validate_age(self, mobile):
        if len(mobile) == 0 or len(mobile) == 10:
            pass
        else:
            raise ValidationError('The Mobile Number is invalid')

    @validates('alt_mobile')
    def validate_age(self, alt_mobile):
        if len(alt_mobile) == 0 or len(alt_mobile) == 10:
            pass
        else:
            raise ValidationError('The Mobile Number is invalid')
   
    @post_load
    def create_person(self, data, **kwargs):
        return data



class EmployeeDependentEdit1(Resource):
    @jwt_required
    @prefix_decorator('EmployeeDependentEdit1')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        try:
            userid = Employee1Model.query.filter_by(email_id=username).first()
            det = DependentRelationModel.query.filter_by(dependent_relation_type=data['dependent_contact_relation']).first()
            if det and userid and data['employee_number']:
                data['dependent_contact_relation'] = det.dependent_relation_id
            else:   
                logging.debug("User Does Not Exist")
                return{"errorMessage":"User Does Not Exist"}
            data['contact_gender'] = 'M' if data['contact_gender']=='Male' else 'F' if data['contact_gender']=='Female' else 'O' if data['contact_gender']=='Other' else ''
            schema = DependentsSignUp1Schema(unknown=INCLUDE)
            empdet = schema.load(data)
            result = schema.dump(empdet)
            date_obj = datetime.now()
            if data['employee_number'] and data['contact_first_name'] and data['contact_id']:
                current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
                dep = DependentContactModel.query.filter_by(contact_first_name=result['contact_first_name'],employee_id=current_emp.employee_id).first()
                if dep and (dep.contact_id!=data['contact_id']):
                    logging.debug("Dependent With Same First Name Already Exists")
                    return {"errorMessage":"Dependent With Same First Name Already Exists"}
                conrecord = DependentContactModel.query.filter_by(employee_id=current_emp.employee_id,contact_id=data['contact_id']).first()
                conaddress = Address1Model.query.filter_by(address_id=data['dependent_address_id']).first()
                if conrecord and conaddress:
                    conrecord.dependent_relation_id = result['dependent_contact_relation']
                    conrecord.contact_first_name = result['contact_first_name']  
                    conrecord.contact_middle_name = result['contact_middle_name']
                    conrecord.contact_last_name = result['contact_last_name']
                    conrecord.contact_gender = result['contact_gender']
                    conrecord.mobile = result['mobile']
                    conrecord.alt_mobile = result['alt_mobile']
                    conrecord.contact_email = result['contact_email'].lower()
                    conrecord.same_as_emp_current_add=data['same_as_ca']
                    conrecord.modified_by = userid.employee_id
                    conrecord.modified_time = date_obj 
                    conaddress.address_line1 = result['address_line1']
                    conaddress.address_line2 = result['address_line2']
                    conaddress.city = result['city']
                    conaddress.state_name = result['state_name']
                    conaddress.zip_code = result['zip_code']
                    conaddress.country = result['country']
                    conrecord.save_to_db()
                    conaddress.save_to_db()
                    current_emp.modified_time = date_obj
                    current_emp.save_to_db()
                    logging.debug('Details Updated Successfully')
                    return {'message': 'Details Updated Successfully'}
                else:
                    logging.debug("Dependent Record Does Not Exist")
                    return {"errorMessage":"Dependent Record Does Not Exist"}
            else:
                logging.debug("Please Provide Valid Employee Number")
                return {"errorMessage":"Please Provide Valid Employee Number"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class CertificationsEdit(Resource):
    @jwt_required
    @prefix_decorator('CertificationsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        dob = Employee1Model.query.filter_by(email_id=username).first()
        if not dob:
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}    
        if datetime.strptime(data['issuedOn'],'%m-%d-%Y') < datetime.strptime(base64.b64decode(dob.date_of_birth).decode('ascii'),'%m-%d-%Y'):
            logging.debug("Certification Issued Date Cannot Be Older Than Employee Date Of Birth")
            return {'erroMessage':"Certification Issued Date Cannot Be Older Than Employee Date Of Birth"}
        try:
            schema = CertificationSchema()
            certification = schema.load(data)
            result = schema.dump(certification)
            record = CertificationsModel.query.filter_by(employee_certification_id=result['certificationId']).first()
            # change the values you want to update
            record.certification_name = result['certificationName']
            record.issued_by = result['issuedBy']
            record.issued_on = result['issuedOn']
            record.certification_url = result['certificationUrl']
            # commit changes
            record.save_to_db()
            logging.debug("Certification Details Updated Successfully")
            return {"message":"Certification Details Updated Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class GetRole(Resource):
    @jwt_required
    @prefix_decorator('GetRole')
    def post(self):
        username = get_jwt_identity()
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        userid = UserModel.query.filter_by(username=email).first()
        if userid:            
            user_roles_obj = UserRolesModel.query.filter_by(user_id=userid.userid).first()
            if user_roles_obj:
                logging.info("Roles Returned Succesfully")
                return {'roles': [RoleModel.find_by_role_id(x) for x in user_roles_obj.roles]}
            else:
                logging.debug("Role(s) Not Assigned To The User {}".format(email))
                return {"errorMessage":"Role(s) Not Assigned To The User {}".format(email)}
        else:
            logging.debug("User Does Not Exist")
            return{"errorMessage":"User Does Not Exist"}

class GetUserPrivileges(Resource):
    @jwt_required
    @prefix_decorator('GetUserPrivileges')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        user_id = UserModel.query.filter_by(username=data['emailId'].lower()).first()
        if user_id:
            user_roles = UserRolesModel.query.filter_by(user_id=user_id.userid).first()
            if user_roles:
                privileges_list = []
                for role in user_roles.roles:
                    for privilege_id in RolePrivilegeModel.query.filter_by(role_id=role).first().role_privileges:
                        privileges_list.append(PrivilegeModel.query.filter_by(id=privilege_id).first().privilege_name)
                return {"privileges": privileges_list}
            else:
                return {'errorMessage': "User Doesn't Have Any Roles Assigned"}
        else:
            return {"errorMessage":"User Does Not Exist"}

class ValidateClientName(Resource):
    @jwt_required
    @prefix_decorator('ValidateClientName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['organization_name'].lower().strip() not in [i.organization_name.lower().strip() for i in ClientDetailsModel.query.all()]:
            logging.info('Success')
            return {'message':'Success'}
        else:
            logging.debug('Client With Same Name Already Exists')
            return {'errorMessage':'Client With Same Name Already Exists'}

class ClientDetailsSchema(Schema):
    client_vendorship_type_id = fields.Integer(data_key="client_vendorship_type",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    msa_number = fields.String(validate=validate.Length(max=15),data_key="msa_number",required=True)
    number_of_contracts = fields.Integer(data_key="project_type_name",required=False)
    client_contract_details = fields.String(validate=validate.Length(max=255),data_key="client_contract_details",required=False)
    organization_name = fields.String(validate=validate.Length(max=255),data_key="organization_name",required=True)
    comments = fields.String(validate=validate.Length(max=255),data_key="comments",required=False)  
    vendorship_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="vendorship_start_date",required=True)
    vendorship_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="vendorship_end_date",required=True)
    client_status = fields.String(validate=validate.Length(max=1),data_key="client_status",required=True)

    @post_load
    def save_project(self, data, **kwargs):
        return data

class ClientLocationSchema(Schema):
    location_id = fields.Integer(data_key="project_id",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    location_name = fields.String(validate=validate.Length(max=255),data_key="location_name",required=True)
    business_concentration = fields.String(validate=validate.Length(max=255),data_key="business_concentration",required=True)

    @post_load
    def save_project(self, data, **kwargs):
        return data

class ClientVendorshipSchema(Schema):
    contract_id = fields.String(validate=validate.Length(max=40),data_key="contract_id",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    bussiness_sector_id = fields.Integer(data_key="bussiness_sector_type",required=True)
    contract_name = fields.String(validate=validate.Length(max=255),data_key="contract_name",required=True)
    term_limits = fields.Integer(data_key="term_limits",required=True)
    contract_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="contract_start_date",required=True)
    contract_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="contract_end_date",required=True)
    contract_number = fields.String(validate=validate.Length(max=25),data_key="contract_number",required=True)

    @post_load
    def save_project(self, data, **kwargs):
        return data

class ClientContactSchema(Schema):
    contact_id = fields.String(validate=validate.Length(max=40),data_key="contact_id",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    department_id = fields.Integer(data_key="department_type",required=True)
    contact_address_id = fields.Integer(data_key="contact_address_id",required=False)
    contact_full_name = fields.String(validate=validate.Length(max=150),data_key="contact_full_name",required=True)
    contact_title = fields.String(validate=validate.Length(max=50),data_key="contact_title",required=True)
    contact_work_phone = fields.String(validate=validate.Length(max=20),data_key="contact_work_phone",required=True)
    contact_cell_phone = fields.String(validate=validate.Length(max=20),data_key="contact_cell_phone",required=False)
    contact_email = fields.Email(data_key="contact_email",required=True)
    reporting_manager_name = fields.String(validate=validate.Length(max=150),data_key="reporting_manager_name",required=True)

    @post_load
    def save_project(self, data, **kwargs):
        return data



class AddClient(Resource):
    @jwt_required
    @prefix_decorator('AddClient')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        contract_numbers = [i['contract_number'] for i in data['contracts']]
        contact_emails = []
        contact_phone_numbers = []
        for i in data['contacts']:
            contact_emails.append(i['contact_email'].lower())
            contact_phone_numbers.append(i['contact_work_phone'])
        if ClientDetailsModel.query.filter(func.lower(ClientDetailsModel.organization_name)==data['organization_name'].lower().strip()).first():
            logging.debug("A client already exists with same name")
            return {"errorMessage": "A Client Already Exists With Same Name"} 
        if len(list(set(contract_numbers))) != len(contract_numbers):
            logging.debug("Contract Number Cannot Be Same")
            return {"errorMessage":"Contract Number Cannot Be Same"}
        if len(list(set(contact_emails))) != len(contact_emails):
            logging.debug("Contact Email Cannot Be Same")
            return {"errorMessage":"Contact Email Cannot Be Same"}
        if len(list(set(contact_phone_numbers))) != len(contact_phone_numbers):
            logging.debug("Contact Phone Numbers Cannot Be Same")
            return {"errorMessage":"Contact Phone Numbers Cannot Be Same"}
        for i in contract_numbers:
            if ClientVendorshipModel.query.filter_by(contract_number=i).first():
                logging.debug("A Client With Same Contract Number Already Exists")
                return {"errorMessage":"A Client With Same Contract Number Already Exists"}
        if ClientDetailsModel.query.filter_by(organization_name=data['organization_name']).first():
            logging.debug("A client already exists with same name")
            return {"errorMessage": "A client already exists with same name"} 
        else:
            client_id = str(uuid.uuid4())
            try:
                try:    
                    data['client_vendorship_type'] = ClientvendorshipTypeModel.find_by_client_vendorship_type(data['client_vendorship_type'])
                    data['client_status'] = 'A' if data['client_status']=='Active' else 'I' if data['client_status']=='Inactive' else 'T' if data['client_status']=='Terminated' else ''
                except Exception as e:	
                    logging.exception("Please Provide Valid Client Vendorship Type")
                    return {'errorMessage':"Please Provide Valid Client Vendorship Type"}
                schema = ClientDetailsSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                schema1 = ClientLocationSchema(unknown=EXCLUDE)
                project = schema1.load(data)
                result1 = schema1.dump(project)
                for i in data['contracts']:
                    try:
                        i['bussiness_sector_type'] = BussinessSectorModel.find_by_bussiness_sector_type(i['bussiness_sector_type'])
                        contract_id = str(uuid.uuid4())
                    except Exception as e:	 
                        logging.exception("Please Provide Valid Business Sector Type")
                        return {"errorMessage":"Please Provide Valid Business Sector Type"}
                    schema2 = ClientVendorshipSchema(unknown=EXCLUDE)
                    project = schema2.load(i)
                    result2 = schema2.dump(project)
                for i in data['contacts']:
                    try:
                        i['department_type'] = DepartmentModel.find_by_department_name(i['department_type'])
                        contract_id = str(uuid.uuid4())
                        contact_id = str(uuid.uuid4())
                    except Exception as e:
                        logging.exception("Please Provide Valid Department Type")
                        return {"errorMessage":"Please Provide Valid Department Type"}
                    schema3 = AddressSchema(unknown=EXCLUDE)
                    project = schema3.load(i)
                    result3 = schema3.dump(project)
                    schema4 = ClientContactSchema(unknown=EXCLUDE)
                    project = schema4.load(i)
                    result4 = schema4.dump(project)    
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
                #raise err
            new_client_details = ClientDetailsModel(
                client_id=client_id,
                client_vendorship_type_id=result['client_vendorship_type'],
                client_status=result['client_status'],
                vendorship_start_date=result['vendorship_start_date'],
                vendorship_end_date=result['vendorship_end_date'],
                msa_number=result['msa_number'],
                organization_name=result['organization_name'].strip(),
                client_contract_details=result['client_contract_details'],
                created_time=date_obj,
                modified_time=date_obj,
                client_status_last_updtd_date=date_obj
            )
            new_client_details.save_to_db()
            new_client_location = ClientLocationModel(
                client_id=client_id,
                location_name=result1['location_name'],
                business_concentration=result1['business_concentration']
            )
            new_client_location.save_to_db()
            for i in data['contracts']:
                contract_id = str(uuid.uuid4())
                new_client_vendorship = ClientVendorshipModel(
                    contract_id=contract_id,
                    client_id=client_id,
                    bussiness_sector_id=i['bussiness_sector_type'],
                    contract_name=i['contract_name'],
                    contract_start_date=i['contract_start_date'],
                    contract_end_date=result2['contract_end_date'],
                    term_limits=i['term_limits'],
                    contract_number=i['contract_number'],
                    status = 'A',
                    created_by = userid,
                    created_time = date_obj,
                    modified_time = date_obj
                )
                new_client_vendorship.save_to_db()
            for i in data['contacts']:
                add = Address1Model(
                    address_line1=i['addressLine1'],
                    address_line2=i['addressLine2'],
                    city=i['city'],
                    state_name=i['state'],
                    zip_code=i['zip'],
                    country=i['country']
                )
                add.save_to_db()
                addid = add.address_id
                contact_id = str(uuid.uuid4())
                new_client_contact = ClientContactModel(
                    contact_id=contact_id,
                    client_id=client_id,
                    department_id=i['department_type'],
                    contact_address_id=addid,
                    contact_full_name=i['contact_full_name'],
                    contact_title=i['contact_title'],
                    contact_work_phone=i['contact_work_phone'],
                    contact_email=i['contact_email'].lower(),
                    reporting_manager_name=i['reporting_manager_name'],
                    status = 'A',
                    created_by = userid,
                    created_time = date_obj,
                    modified_time = date_obj
                )
                new_client_contact.save_to_db()
            logging.debug("Client Added Successfully")
            return {"message": "Client Added Successfully"} 

class ClientView(Resource):
    @jwt_required
    @prefix_decorator('ClientView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        client_obj = ClientDetailsModel.query.filter_by(client_id=data['client_id']).first()
        if not client_obj:
            logging.debug("Client Does Not Exist")
            return {"errorMessage":"Client Does Not Exist"}
        client_location_obj = ClientLocationModel.query.filter_by(client_id=data['client_id']).first()
        client_vendorship_obj = ClientVendorshipModel.return_all_by_clientid(data['client_id'])
        clientcondetails = ClientContactModel.return_all_new(data['client_id'])['client_contact_details']
        return {"client_id": client_obj.client_id,
                "client_vendorship_type": ClientvendorshipTypeModel.find_by_client_vendorship_id(client_obj.client_vendorship_type_id),
                "client_status": 'Active' if client_obj.client_status=='A' else 'Inactive' if client_obj.client_status=='I' else 'Terminated' if client_obj.client_status=='T' else 'Prospect'  if client_obj.client_status=='P' else '',
                'vendorship_start_date': str(client_obj.vendorship_start_date),
                'vendorship_end_date': str(client_obj.vendorship_end_date),
                'msa_number': client_obj.msa_number,
                'number_of_contracts': str(client_obj.number_of_contracts),
                'organization_name': client_obj.organization_name,
                'client_contract_details': client_obj.client_contract_details,
                'client_vendor_contract_details': client_vendorship_obj,
                "clientcondetails": clientcondetails,
                "location_name": client_location_obj.location_name if client_location_obj else '',
                "business_concentration": client_location_obj.business_concentration if client_location_obj else '',
                "termination_reason":client_obj.comments,
                "client_created_date":str(datetime.strptime(str(client_obj.created_time),"%Y-%m-%d %H:%M:%S.%f").date()),
                "client_Status_last_updated_date":str(client_obj.client_status_last_updtd_date)
                }

class EditClientContact(Resource):
    @jwt_required
    @prefix_decorator('EditClientContact')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        record3 = ClientContactModel.query.filter_by(contact_id=data['contact_id']).first()
        record5 = Address1Model.query.filter_by(address_id=record3.contact_address_id).first()
        if record3 and record5 and userid:
            all_similar_phone = ClientContactModel.query.filter_by(client_id=record3.client_id,contact_work_phone=data['contact_work_phone']).all() 
            all_similar_email = ClientContactModel.query.filter_by(client_id=record3.client_id,contact_email=data['contact_email'].lower()).all()
            
            if all_similar_phone:
                for i in all_similar_phone:
                    if i.contact_id != data['contact_id']:
                        logging.info("Already A Contact Exists With Same Contact Phone Number")
                        return {"errorMessage":"Already A Contact Exists With Same Contact Phone Number"}
            if all_similar_email:
                for i in all_similar_email:
                    if i.contact_id != data['contact_id']:  
                        logging.info("Already A Contact Exists With Same Email")
                        return {"errorMessage":"Already A Contact Exists With Same Email"}
            try:
                data['department_type'] = DepartmentModel.find_by_department_name(data['department_type'])
            except Exception as e:	 
                logging.exception('Please Provide Valid Department Type')
                return {'errorMessage': 'Please Provide Valid Department Type'}
            try:
                schema = ClientContactSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                schema1 = AddressSchema(unknown=EXCLUDE)
                project = schema1.load(data)
                result1 = schema1.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                #raise err
                return {'errorMessage': err.messages}, 500
            record3.contact_full_name = result['contact_full_name']
            record3.contact_title = result['contact_title']
            record3.contact_work_phone = result['contact_work_phone']
            record3.contact_email = result['contact_email'].lower()
            record3.department_id = result['department_type']
            record3.reporting_manager_name = result['reporting_manager_name']
            record3.modified_by = userid.employee_id
            record3.modified_time = datetime.now()
            record5.address_line1 = result1['addressLine1']
            record5.address_line2 = result1['addressLine2']
            record5.city = result1['city']
            record5.state_name = result1['state']
            record5.zip_code = result1['zip']
            record5.country = result1['country']
            record3.save_to_db()
            record5.save_to_db()
            logging.debug('Client Contact Details Updated Successfully')
            return {'message': 'Client Contact Details Updated Successfully'}
        else: 
            logging.exception('Client Contact Does Not Exist')
            return {'message': 'Client Contact Does Not Exist'}

class AddClientContact(Resource):
    @jwt_required
    @prefix_decorator('AddClientContact')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()   
        date_obj = datetime.now()
        if ClientContactModel.query.filter_by(client_id=data['client_id'],contact_email=data['contact_email'].lower()).first():
            logging.debug("A Contact Already Exists With Same Contact Email")
            return {"errorMessage": "A Contact Already Exists With Same Contact Email"}
        if ClientContactModel.query.filter_by(client_id=data['client_id'],contact_work_phone=data['contact_work_phone']).first():
            logging.debug("A Contact Already Exists With Same Contact Phone Number")
            return {"errorMessage": "A Contact Already Exists With Same Contact Phone Number"}    
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        record1 = ClientDetailsModel.find_by_client_id(data['client_id'])
        if username:
            contact_id = str(uuid.uuid4())
            try:            
                data['department_type'] = DepartmentModel.find_by_department_name(data['department_type'])
            except Exception as e:
                logging.exception('Please Provide Valid Department Type')
                return {'errorMessage': 'Please Provide Valid Department Type'}
            try:
                schema = ClientContactSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                schema1 = AddressSchema(unknown=EXCLUDE)
                project = schema1.load(data)
                result1 = schema1.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
            add = Address1Model(
                address_line1=result1['addressLine1'],
                address_line2=result1['addressLine2'],
                city=result1['city'],
                state_name=result1['state'],
                zip_code=result1['zip'],
                country=result1['country']
            )
            add.save_to_db()
            addid = add.address_id
            new_client_contact = ClientContactModel(
                contact_id=contact_id,
                client_id=result['client_id'],
                department_id=result['department_type'],
                contact_address_id=addid,
                contact_full_name=result['contact_full_name'],
                contact_title=result['contact_title'],
                contact_work_phone=result['contact_work_phone'],
                contact_email=result['contact_email'].lower(),
                reporting_manager_name=result['reporting_manager_name'],
                status = 'A',
                created_by = userid,
                modified_time = date_obj,
                created_time = date_obj
            )
            new_client_contact.save_to_db()
            record1.modified_time = date_obj
            record1.save_to_db()
            logging.debug('Client Contact Added Successfully')
            return {'message': 'Client Contact Added Successfully'}
        else: 
            logging.exception('User Does Not Exist')
            return {'message': 'User Does Not Exist'}

class EditClientContract(Resource):
    @jwt_required
    @prefix_decorator('EditClientContract')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        record1 = ClientDetailsModel.find_by_client_id(data['client_id'])
        record2 = ClientVendorshipModel.query.filter_by(client_id=data['client_id'],contract_id=data['contract_id']).first()
        rec = ClientVendorshipModel.query.filter_by(contract_number=data['contract_number']).first()
        if rec and (rec.contract_id!=data['contract_id']):
            logging.debug('Contract With Same Contract Number Already Exists')
            return {'errorMessage':'Contract With Same Contract Number Already Exists'}
        if record1:
            try:
                try:
                    data['bussiness_sector_type'] = BussinessSectorModel.find_by_bussiness_sector_type(data['bussiness_sector_type'])
                except:
                    logging.exception("Please Select Valid Business Sector Type")
                    return {"errorMessage":"Please Select Valid Business Sector Type"}
                schema = ClientVendorshipSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
            record1.modified_time = date_obj #this field may not be necessary at all
            record2.contract_name = result['contract_name']
            record2.bussiness_sector_id = result['bussiness_sector_type']
            record2.contract_start_date = result['contract_start_date']
            record2.contract_end_date = result['contract_end_date']
            record2.term_limits = result['term_limits']
            record2.contract_number = result['contract_number']
            record2.modified_by = userid
            record2.modified_time = date_obj
            record2.save_to_db()
            record1.save_to_db()
            logging.info('Client Contract details Updated successfully')
            return {'message': 'Client Contract details Updated successfully'}
        else: 
            logging.exception('Client Does Not Exist')
            return {'errorMessage': 'Client Does Not Exist'}

class AddClientContract(Resource):
    @jwt_required
    @prefix_decorator('AddClientContract')
    def post(self):	
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        record1 = ClientDetailsModel.find_by_client_id(data['client_id'])
        if ClientVendorshipModel.query.filter_by(contract_number=data['contract_number']).first():
            logging.debug('Contract With Same Contract Number Already Exists')
            return {'errorMessage':'Contract With Same Contract Number Already Exists'}
        if userid and record1:
            contract_id = str(uuid.uuid4())
            try:  
                data['bussiness_sector_type'] = BussinessSectorModel.find_by_bussiness_sector_type(data['bussiness_sector_type'])
                schema = ClientVendorshipSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                new_client_vendorship = ClientVendorshipModel(
                    contract_id=contract_id,
                    client_id=result['client_id'],
                    bussiness_sector_id=result['bussiness_sector_type'],
                    contract_name=result['contract_name'],
                    contract_start_date=result['contract_start_date'],
                    contract_end_date=result['contract_end_date'],
                    term_limits=result['term_limits'],
                    contract_number=result['contract_number'],
                    status = 'A',
                    created_by = userid.employee_id,
                    created_time = date_obj,
                    modified_time = date_obj
                )
                new_client_vendorship.save_to_db()
                record1.modified_time = date_obj
                record1.save_to_db()
                logging.debug('Client Contract Details Added Successfully')
                return {'message': 'Client Contract Details Added Successfully'}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        else:
            logging.debug('Client Does Not Exist')
            return {'message': 'Client Does Not Exist'}

class ClientVendorshipEdit(Resource):
    @jwt_required
    @prefix_decorator('ClientVendorshipEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        val = ClientDetailsModel.query.filter(func.lower(ClientDetailsModel.organization_name)==data['organization_name'].lower().strip()).first()
        if val and val.client_id != data['client_id']:
            logging.debug("A Client Already Exists With Same Name")
            return {"errorMessage": "A Client Already Exists With Same Name"}
        record9 = ClientDetailsModel.find_by_client_id(data['client_id'])
        if record9:
            if record9.client_status != 'A' if data['client_status']=='Active' else 'I' if data['client_status']=='Inactive' else 'T' if data['client_status']=='Terminated' else 'P' if data['client_status']=='Prospect' else '':
                last_update_date = date_obj
            else:
                last_update_date = record9.client_status_last_updtd_date
        else:
            logging.debug("Please Provide Valid Client Id")
            return {"errorMessage":"Please Provide Valid Client Id"}
        record1 = ClientDetailsModel.find_by_client_id(data['client_id'])
        record4 = ClientLocationModel.find_by_client_id(data['client_id'])
        if data['client_status'] != 'Active':
            cliprorec = ProjectModel.query.filter_by(client_id=data['client_id']).all()
            if cliprorec:
                for i in cliprorec:
                    if i.project_status == 'A':
                        logging.debug("Failed,An Active Project Exists Under This Client")
                        return {"errorMessage":"Failed,An Active Project Exists Under This Client"}
        if record1:
            try:
                data['client_vendorship_type'] = ClientvendorshipTypeModel.find_by_client_vendorship_type(data['client_vendorship_type'])
                data['client_status'] = 'A' if data['client_status']=='Active' else 'I' if data['client_status']=='Inactive' else 'T' if data['client_status']=='Terminated' else 'P' if data['client_status']=='Prospect' else ''
                schema = ClientDetailsSchema(unknown=EXCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
                record1.client_vendorship_type_id =  data['client_vendorship_type']
                record1.vendorship_start_date = data['vendorship_start_date']
                record1.vendorship_end_date = data['vendorship_end_date']
                record1.msa_number = data['msa_number']
                record1.comments = data['termination_reason']
                record1.client_status = data['client_status']
                record1.organization_name = data['organization_name'].strip()
                record1.client_contract_details = data['client_contract_details']
                record1.modified_time = date_obj
                record4.location_name = data['location_name']
                record4.business_concentration = data['business_concentration']
                record1.client_status_last_updtd_date = last_update_date
                record1.save_to_db()
                record4.save_to_db()
                if data['client_status'] == 'A':
                    logging.debug('Client Vendorship Details Updated Successfully')
                    return {'message': 'Client Vendorship Details Updated Successfully'}
                elif data['client_status'] == 'I':
                    logging.debug('Client Inactivated Successfully')
                    return {'message': 'Client Inactivated Successfully'}
                elif data['client_status'] == 'T':
                    logging.debug('Client Terminated Successfully')
                    return {'message': 'Client Terminated Successfully'}
                else:
                    logging.debug('Client Satus Changed to Prospect Successfully')
                    return {'message': 'Client Satus Changed to Prospect Successfully'}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        else: 
            logging.exception('Client Does Not Exist')
            return {'message': 'Client Does Not Exist'}

class ContractListForProject(Resource):
    @jwt_required
    @prefix_decorator('ContractListForProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ClientDetailsModel.query.filter_by(client_id=data['client_id']):
            logging.debug("Client Does Not Exist")
            return {"errorMessage":"Client Does Not Exist"}
        logging.info("Success")
        return ClientVendorshipModel.return_all_for_projects(data['client_id'])


class DeleteClient(Resource):
    @jwt_required
    @prefix_decorator('DeleteClient')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        clientprorecord = ProjectModel.query.filter_by(client_id=data['client_id']).first()
        client_record = ClientDetailsModel.query.filter_by(client_id=data['client_id']).first()
        if not client_record:
                logging.exception('Client Does Not Exist')
                return {"errorMessage":"Client Does Not Exist"}
        if clientprorecord:
            if clientprorecord.project_status == 'A':
                return {"errorMessage":"Failed,There Is An Active Project Under This Client"}
            else:
                if client_record:
                    client_record.client_status = 'I'
                    client_record.client_status_last_updtd_date = date_obj
                    client_record.comments = data['comments']
                    client_record.modified_time = date_obj
                    client_record.save_to_db()
                    return {"Message": "Client Inactivated Successfully"}
        else:
            if client_record:
                client_record.client_status = 'I'
                client_record.client_status_last_updtd_date = date_obj
                client_record.comments = data['comments']
                client_record.modified_time = date_obj
                client_record.save_to_db()
                return {"Message": "Client Inactivated Successfully"}

class DeleteSupplier(Resource):
    @jwt_required
    @prefix_decorator('DeleteSupplier')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        clientprorecord = ThirdPartyConsultantModel.query.filter_by(supplier_id=data['supplier_id']).all()
        if clientprorecord:
            for i in clientprorecord:
                if i.consultant_status == 'A':
                    logging.debug("Failed,There Is An Active Consultant Under This Supplier")
                    return {"errorMessage":"Failed,There Is An Active Consultant Under This Supplier"}
        record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        record.contract_status = 'I'
        record.supplier_status_last_updtd_date = date_obj
        record.comments = data['comments']
        record.modified_time = date_obj
        record.save_to_db()
        logging.info("Supplier Inactivated Successfully")
        return {"Message": "Supplier Inactivated Successfully"}


class AddPrivilege(Resource):
    @jwt_required
    def post(self):
        data = request.get_json(force=True)
        if data['privilege_name']:
            if PrivilegeModel.query.filter_by(privilege_name=data['privilege_name']).first():
                return {'message': 'Privilege {} Already Exists'.format(data['privilege_name'])}
            else:
                new_privilege = PrivilegeModel(
                    privilege_name=data['privilege_name']
                )
                new_privilege.save_to_db()
                return {'message': 'privilege Added Successfully'}
        else:
            return{"errorMessage":"Please Provide Valid Privilege Name"}


class RoleAssignment(Resource):
    @jwt_required
    @prefix_decorator('RoleAssignment')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        requested_emp_id_for = UserModel.find_by_username(data['username'].lower())
        if requested_emp_id_for:
            a = [RoleModel.find_by_role_name(i) for i in data['roles']]                    
            record = UserRolesModel.query.filter_by(user_id=requested_emp_id_for.userid).first()
            if not record:
                new_record = UserRolesModel(
                    user_id=requested_emp_id_for.userid,
                    roles=a
                )
                new_record.save_to_db()
            else:
                record.user_id = requested_emp_id_for.userid,
                record.roles = a
                record.save_to_db()
            logging.info("Added Succefully")
            return {"message": "Added Succefully"}
        else:
            logging.debug("User Does Not Exist")
            return{"errorMessage":"User Does Not Exist"}

class AddPrivilegesToRole(Resource):
    @jwt_required
    @prefix_decorator('AddPrivilegesToRole')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        requested_emp_id = UserModel.find_by_username(username)
        requested_role_id = RoleModel.find_by_role_name(data['role_name'])
        if requested_emp_id:
            role_privileges = RolePrivilegeModel(
                role_id=requested_role_id,
                role_privileges=data['role_privileges'],
            )
            role_privileges.save_to_db()
            return {'message': 'Privileges Added Successfully'}
        else:
            return {'message': 'Please Provide Valid Data'}


class First(Resource):
    @prefix_decorator('RoleEdit')
    def post(self):
        data = request.get_json(force=True)
        role_id = RoleModel.query.filter_by(role_name=data['role_name']).first().id
        return {"errorMessage": "Error"}


      
class RoleEdit(Resource):
    @jwt_required  
    @prefix_decorator('RoleEdit')
    def post(self):
        logging.info('Entered into {0}'.format(self.__class__.__name__))
        data = request.get_json(force=True)
        print(data)
        record = RoleModel.query.filter_by(id=data['role_id']).first()
        record.role_name = data['role_name']
        record.save_to_db()
        logging.info("Role Edited To {}".format(data['role_name']))
        return {"message": "Role Edited To {}".format(data['role_name'])}

class GetRolePrivileges(Resource):
    @jwt_required
    @prefix_decorator('GetRolePrivileges')
    def post(self):
        logging.info('Entered into {0}'.format(self.__class__.__name__))
        data = request.get_json(force=True)
        print(data)
        requested_role_id = RoleModel.find_by_role_name(data['role_name'])
        role_privileges_obj = RolePrivilegeModel.query.filter_by(role_id=requested_role_id).first()
        if role_privileges_obj:
            logging.info("Success")
            return {
                'role_name': data['role_name'],
                'privileges': [PrivilegeModel.query.filter_by(id=x).first().privilege_name for x in role_privileges_obj.role_privileges]
            }
        else:
            logging.debug("Role {} Dosen't Have Privileges".format(data['role_name']))
            return {"errorMessage":"Role {} Dosen't Have Privileges".format(data['role_name'])}

class ProjectSchema(Schema):
    project_id = fields.String(validate=validate.Length(max=40),data_key="project_id",required=False)
    client_id = fields.String(validate=validate.Length(max=40),data_key="client_id",required=False)
    contract_id = fields.String(validate=validate.Length(max=40),data_key="contract_id",required=False)
    project_type_id = fields.Integer(data_key="project_type_name",required=True)
    work_location = fields.String(validate=validate.Length(min=1,max=255),data_key="work_location",required=True)
    project_name = fields.String(validate=validate.Length(min=1,max=40),data_key="project_name",required=True)
    project_description = fields.String(validate=validate.Length(max=255),data_key="project_description",required=True)  
    scheduled_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="scheduled_start_date",required=True)
    scheduled_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="scheduled_end_date",required=False)
    actual_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="actual_start_date",required=False)
    actual_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="actual_end_date",required=False)
    revenue_amount = fields.Float(allow_none=True,data_key="project_revenue",required=False)
    project_manager_id = fields.Integer(data_key="project_manager",required=True)
    proj_manager_phone_number = fields.String(validate=validate.Length(max=20),data_key="project_manager_contact_number",required=True)  
    project_sponsor_name = fields.String(validate=validate.Length(min=1,max=150),data_key="project_sponsor",required=True)
    project_status = fields.String(validate=validate.Length(max=1),data_key="project_status",required=False)
    daily_work_hours = fields.Float(data_key="daily_work_hours",required=False)
    comments = fields.String(validate=validate.Length(max=255),data_key="comments",required=False)

    @post_load
    def save_project(self, data, **kwargs):
        return data

class ValidateProjectName(Resource):
    @jwt_required
    @prefix_decorator('ValidateProjectName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['project_name'].lower().strip() not in [i.project_name.lower().strip() for i in ProjectModel.query.all()]:
            logging.info('Success')
            return {'message':'Success'}
        else:
            logging.debug('Project With Same Name Already Exists')
            return {'errorMessage':'Project With Same Name Already Exists'}

class AddProject(Resource):
    @jwt_required
    @prefix_decorator('AddProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if userid:
            try:
                if ProjectModel.query.filter(func.lower(ProjectModel.project_name)==data['project_name'].lower()).first():
                    logging.debug("A Project Already Exists With Same Name")
                    return {"errorMessage":"A Project Already Exists With Same Name"}
                data['project_type_name'] = ProjectTypeModel.query.filter_by(project_type_name=data['project_type_name']).first()
                data['project_manager'] = Employee1Model.find_by_employeenumber(data['project_manager'])
                if data['project_manager']:
                    data['project_manager'] =data['project_manager'].employee_id 
                else:
                    logging.exception("Project Manager Does Not Exist")
                    return {'errorMessage':"Project Manager Does Not Exist"}
                if data['project_type_name']:
                    data['project_type_name'] =data['project_type_name'].project_type_id 
                else:
                    logging.exception("Project Type Does Not Exist")
                    return {'errorMessage':"Project Type Does Not Exist"}
                data['project_status'] = 'A'
                schema = ProjectSchema()
                project = schema.load(data)
                result = schema.dump(project)
                project_id = str(uuid.uuid4())
                if not ClientDetailsModel.query.filter_by(client_id=result['client_id']).first():
                    return {"errorMessage":"Client Does Not Exist"}
                if not ClientVendorshipModel.query.filter_by(contract_id=result['contract_id']).first():
                    return {"errorMessage":"Client Contract Does Not Exist"}
                new_project = ProjectModel(
                    project_id=project_id,
                    client_id=result['client_id'],
                    contract_id=result['contract_id'],
                    project_type_id=result['project_type_name'],
                    project_name=result['project_name'].strip(),
                    project_description=result['project_description'],
                    revenue_amount=result['project_revenue'],
                    project_sponsor_name=result['project_sponsor'],
                    work_location=result['work_location'],
                    scheduled_start_date=result['scheduled_start_date'],
                    scheduled_end_date=result['scheduled_end_date'],
                    project_manager_id=result['project_manager'],
                    proj_manager_phone_number=result['project_manager_contact_number'],
                    project_status=data['project_status'],
                    created_time=date_obj,
                    modified_time=date_obj,
                    created_by = userid.employee_id,
                    modified_by = userid.employee_id,
                    project_status_last_updtd_date = date_obj
                )
                new_project.save_to_db()
                a = [str(data['project_manager'])]
                new_project_employee = ProjectEmployeeModel(project_id=new_project.project_id, employee_id=list(set(a)))
                new_project_employee.save_to_db()
                logging.debug('Project Added Successfully')
                return {'message': 'Project Added Successfully'}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        else:
            logging.debug("User Does Not Exists")
            return{"errorMessage":"User Does Not Exists"}  

class ProjectEdit(Resource):
    @jwt_required
    @prefix_decorator('ProjectEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        record = ProjectModel.find_by_project_type_id(data['project_id'])
        record1= ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
        record2 = ProjectModel.query.filter(func.lower(ProjectModel.project_name)==data['project_name'].lower()).first()
        print(data['project_id'])
        if record2 and record2.project_id!=data['project_id']:
            print(record2.project_id)
            logging.debug("A Project Already Exists With Same Name")
            return {"errorMessage":"A Project Already Exists With Same Name"}
        if record and record1:
            data['project_type_name'] = ProjectTypeModel.find_by_project_type_name(data['project_type_name'])
            data['project_manager'] = Employee1Model.find_by_employeenumber(data['project_manager'])
            if data['project_manager']:
                data['project_manager'] = data['project_manager'].employee_id
            data['project_status'] = 'A' if data['project_status']=='Active' else 'I' if data['project_status']=='Inactive' else 'T' if data['project_status']=='Terminated' else ''
            try:
                schema = ProjectSchema()
                project = schema.load(data)
                result = schema.dump(project)
                manager  = record.project_manager_id
                # change the values you want to update
                record.project_type_id = result['project_type_name']
                record.work_location = result['work_location']
                record.project_name = result['project_name'].strip()
                record.project_description = result['project_description']
                record.scheduled_start_date = result['scheduled_start_date']
                record.scheduled_end_date = result['scheduled_end_date']
                record.actual_start_date = result['actual_start_date']
                record.actual_end_date = result['actual_end_date']
                record.revenue_amount = result['project_revenue']
                record.project_manager_id =   result['project_manager']
                record.proj_manager_phone_number = result['project_manager_contact_number']
                record.project_sponsor_name = result['project_sponsor']
                record.project_status = result['project_status']
                record.modified_time = datetime.now()
                record.modified_by = userid
                # commit changes
                record.save_to_db()
                project = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
                if project:
                    emp_list = project.employee_id
                    emp_list.append(str(result['project_manager']))
                    project.delete_from_db()            
                    project1 = ProjectEmployeeModel(project_id=data['project_id'],employee_id=list(set(emp_list)))
                    project1.save_to_db()
                else:
                    new_project_employee = ProjectEmployeeModel(project_id=data['project_id'], employee_id=list(set(a)))
                    new_project_employee.save_to_db()
                logging.debug("Project Details Updated Successfully")
                return {"message": "Project Details Updated Successfully"}
            except ValidationError as err:
                logging.exception('Exception Occured in {0}, {1}'.format(self.__class__.__name__,err.messages))
                return {'errorMessage': err.messages}, 500
        else:
            logging.debug('Please Provide Valid Project Id')
            return {"errorMessage":'Please Provide Valid Project Id'}

class ResourceAssignment(Resource):
    @jwt_required
    @prefix_decorator('ResourceAssignment')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        requested_emp_id = Employee1Model.find_by_username(username)
        record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
        if record and requested_emp_id:
            project_manager = record.project_manager_id
            if project_manager not in [i['employee_num'] for i in data['assigned_employees']]:
                logging.info("Cannot Unassign Project Manager, Assign Project To Other Employee And Retry")
                return {"errorMessage":"Cannot Unassign Project Manager, Assign Project To Other Employee And Retry"}
            record1 = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
            list_of_removed_employees = []
            for i in record1.employee_id:
                if i not in [str(j['employee_num']) for j in data['assigned_employees']]:
                     list_of_removed_employees.append(i)
            for j in list_of_removed_employees:
                rec = EmployeePrimaryProjectModel.query.filter_by(employee_id=int(j),project_id=data['project_id']).first()
                if rec:
                    if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(j)])).count()>1:
                        logging.debug("Cannot Unassign Employee As It Is A Primary Project")
                        return {"errorMessage":"Cannot Unassign Employee As It Is A Primary Project"}
            for k in data['assigned_employees']:
                if 'CON' in str(k['employee_num']):
                    if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(k['employee_num'])])).count()>=10:
                        e = ThirdPartyConsultantModel.query.filter_by(consultant_id=k['employee_num']).first()
                        logging.debug('{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name))
                        return {'errorMessage':'{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name)}
                elif not ProjectModel.query.filter_by(project_manager_id=k['employee_num']).first():
                    if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(k['employee_num'])])).count()>=10:
                        e = Employee1Model.query.filter_by(employee_id=k['employee_num']).first()
                        logging.debug('{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name))
                        return {'errorMessage':'{0} is Assigned to 10 Projects'.format(e.first_name+' '+e.last_name)} 
            a = []
            for x in data['assigned_employees']:
                if ('CON' not in str(x['employee_num'])) and not EmployeePrimaryProjectModel.query.filter_by(employee_id=x['employee_num']).first():
                    record = EmployeePrimaryProjectModel(employee_id=x['employee_num'],project_id=data['project_id'],created_by = requested_emp_id.employee_id,created_time = date_obj)
                    record.save_to_db()
                a.append(str(x['employee_num']))
            project = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
            if project:
                project.project_id = data['project_id']
                project.employee_id = a
                project.save_to_db()
                record.modified_time=date_obj
                record.save_to_db()
                logging.info('Resources Updated Successfully')
                return {'message': 'Resources Updated Successfully'}
            else:
                new_project_employee = ProjectEmployeeModel(project_id=data['project_id'], employee_id=list(set(a)))
                new_project_employee.save_to_db()
                record.modified_time=date_obj
                record.save_to_db()
                logging.info('Resources Updated Successfully')
                return {'message': 'Resources Updated Successfully'}
        else: 
            logging.debug('Please Provide Valid Project Id')
            return {'errorMessage': 'Please Provide Valid Project Id'}

class ProjectView(Resource):
    @jwt_required
    @prefix_decorator('ProjectView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not ProjectModel.query.filter_by(project_id=data['project_id']).first():
            logging.debug("Project Does Not Exist")
            return {"errorMessage":"Project Does Not Exist"}
        return ProjectEmployeeModel.find_by_project_id2(data['project_id'])

class EmpProjects(Resource):
    @jwt_required
    @prefix_decorator('EmpProjects')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if not Employee1Model.query.filter_by(employee_id=data['employee_id']).first():
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        logging.info("Success")
        return ProjectEmployeeModel.return_all_empprojects(data['employee_id'])

class AddPrivilegeToRole(Resource):
    @jwt_required
    @prefix_decorator('AddPrivilegeToRole')
    def post(self):
        data = request.get_json(force=True)
        print(data)

        requested_role_id = RoleModel.find_by_role_name(data['role_name'])
        if requested_role_id:
            record = RolePrivilegeModel.query.filter_by(role_id=requested_role_id).first()
            privileges = []
            for i in data['role_privileges']:
                privileges.append(PrivilegeModel.query.filter_by(privilege_name=i).first().id)
            if not record:
                role_privileges = RolePrivilegeModel(
                    role_id=requested_role_id,
                    role_privileges=privileges,
                )
                role_privileges.save_to_db()
                logging.info('Privileges Added Successfully')
                return {'message': 'Privileges Added Successfully'}
            else:
                record.role_id = requested_role_id
                record.role_privileges = privileges
                record.save_to_db()
                logging.info('Privileges Added Successfully')
                return {'message': 'Privileges Added Successfully'}
        else:
            logging.debug("Role Does Not Exist")
            return{"errorMessage":"Role Does Not Exist"}


class EmployeeBasicDetailsEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeBasicDetailsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        if not data['employee_number']:
            logging.debug("Please Provide Valid Employee Number")
            return {'errorMessage':"Please Provide Valid Employee Number"}
        record = Employee1Model.find_by_employeenumber(data['employee_number'])
        data['gender'] = 'M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O' if data['gender']=='Other' else ''
        data['marital_status'] = 'S' if data['marital_status']=='Single' else 'M' if data['marital_status']=='Married' else 'D' if data['marital_status']=='Divorced' else 'W' if data['marital_status']=='Widowed' else ''
        if data['us_citizenship']==True:
            try:
                schema = EmployeeBasicEditSchema2(unknown=INCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        else:
            try:
                schema = EmployeeBasicEditSchema1(unknown=INCLUDE)
                project = schema.load(data)
                result = schema.dump(project)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        if record:
            # change the values you want to update
            record.first_name = result['first_name']
            record.middle_name = result['middle_name']
            record.last_name = result['last_name']
            record.gender =   result['gender']
            record.primary_phone_number = result['primary_phone_number']
            record.alternate_phone_number = result['alternate_phone_number']
            record.marital_status = result['marital_status']
            record.modified_time = date_obj
            # commit changes
            record.us_citizen = result['us_citizenship']
            record.save_to_db()
            if result['us_citizenship']==True and UsCitizenPassportDetailsModel.query.filter_by(employee_id=record.employee_id).first():
                record = UsCitizenPassportDetailsModel.query.filter_by(employee_id=record.employee_id).first()
                record.passport_number = result['passportNumber']
                record.start_date = result['start_date']
                record.end_date = result['end_date']
                record.save_to_db()
            elif result['us_citizenship']==True:
                us_rec = UsCitizenPassportDetailsModel(
                    employee_id = record.employee_id,
                    passport_number = result['passportNumber'],
                    start_date = result['start_date'],
                    end_date = result['end_date']
                    )
                us_rec.save_to_db()
                logging.debug("Employee Basic Details Updated Successfully")
            return {"message": "Employee Basic Details Updated Successfully"}
        else:
            logging.debug("No Such Employee Found")
            return {"message": "No Such Employee Found"}

class EmployeeBasicEditSchema1(Schema):
    employee_number = fields.Integer(required=False)
    gender = fields.String(validate=validate.Length(max=1),required=False)
    marital_status = fields.String(validate=validate.Length(max=1),required=False)
    first_name = fields.String(validate=validate.Length(max=75),required=False)
    middle_name = fields.String(validate=validate.Length(max=75),required=False)
    last_name = fields.String(validate=validate.Length(max=75),required=False)
    gender = fields.String(validate=validate.Length(max=150),required=False)
    primary_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    alternate_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    us_citizenship = fields.Boolean(required=False)    

    @post_load
    def save_address(self, data, **kwargs):
        return data
        
class EmployeeBasicEditSchema2(Schema):
    employee_number = fields.Integer(required=False)
    gender = fields.String(validate=validate.Length(max=1),required=False)
    marital_status = fields.String(validate=validate.Length(max=1),required=False)
    first_name = fields.String(validate=validate.Length(max=75),required=False)
    middle_name = fields.String(validate=validate.Length(max=75),required=False)
    last_name = fields.String(validate=validate.Length(max=75),required=False)
    gender = fields.String(validate=validate.Length(max=150),required=False)
    primary_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    alternate_phone_number = fields.String(validate=validate.Length(max=20),required=False)
    us_citizenship = fields.Boolean(required=False)    
    passportNumber = fields.String(validate=validate.Length(max=20),required=False)
    start_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    end_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)

    @post_load
    def save_address(self, data, **kwargs):
        return data

class EmploymentDetailsEditSchema(Schema):
    employee_number = fields.Integer(required=False)
    employee_status = fields.String(validate=validate.Length(max=1),required=False)
    employee_department = fields.Integer(required=False)
    employee_designation = fields.Integer(required=False)
    employee_type = fields.Integer(required=False)
    termination_reason = fields.Integer(allow_none=True,required=False)
    account_manager = fields.String(validate=validate.Length(max=150),required=False)
    reporting_manager_id = fields.Integer(required=False)
    recruiter = fields.String(validate=validate.Length(max=150),required=False)
    termination_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    rehire_eligibility = fields.Boolean(required=False,allow_none=True)

    @post_load
    def save_address(self, data, **kwargs):
        return data

class EmploymentDetailsEdit(Resource):
    @jwt_required
    @prefix_decorator('EmploymentDetailsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        depid = DepartmentModel.find_by_department_name(data['employee_department'])
        desid = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
        emptypeid = EmploymentTypeModel.query.filter_by(emp_type=data['employee_type']).first()
        data['employee_status'] = 'A' if data['employee_status']=='Active' else 'I' if data['employee_status']=='Inactive' else 'T' if data['employee_status']=='Terminated' else ''
        if depid and desid and emptypeid:
            data['employee_designation'] = desid.designation_id
            data['employee_type'] = emptypeid.emp_type_id
            data['employee_department'] = depid
        else:   
            logging.debug("Please Choose Valid Designation/Department/Employment Type")
            return {"errorMessage":"Please Choose Valid Designation/Department/Employment Type"}
        try:    
            schema = EmploymentDetailsEditSchema(unknown=INCLUDE)
            employmentdet = schema.load(data)
            result = schema.dump(employmentdet) 
            if result['employee_status'] != 'A':
                if ProjectEmployeeModel.query.filter(ProjectEmployeeModel.employee_id.contains([str(current_emp.employee_id)])).all():
                    return {"errorMessage":"Failed,Employee Assigned To Active Project"}
            current_emp1 = Employee1Model.find_by_employeenumber(result['employee_number'])
            if current_emp1.employee_status != data['employee_status']:
                last_update_date = date_obj
            else:
                last_update_date = current_emp1.employee_status_last_updtd_date
            if current_emp:
                if result['employee_status'] == 'T': 
                    current_emp.rehire_eligibility = result['rehire_eligibility']
                if result['employee_status'] == 'I': 
                    current_emp.inactive_reason_id = data['inactive_reason_id']
                if result['employee_status'] == 'A': 
                    current_emp.inactive_reason_id = None
                current_emp.department_id = result['employee_department']
                current_emp.designation_id = result['employee_designation']
                current_emp.emp_type_id = result['employee_type']
                current_emp.employee_status = result['employee_status']
                current_emp.termination_reason_id = result['termination_reason']
                current_emp.account_manager = result['account_manager']
                current_emp.reporting_manager_id= result['reporting_manager_id']
                current_emp.recruiter = result['recruiter']
                current_emp.termination_date = result['termination_date']
                current_emp.modified_time = date_obj
                current_emp.employee_status_last_updtd_date = last_update_date
                current_emp.save_to_db()
                if result['employee_status'] == 'T':
                    logging.debug("Employee Terminated Successfully")
                    return {"message": "Employee Terminated Successfully"}
                elif result['employee_status'] == 'I':
                    logging.debug("Employee Inactivated Successfully")
                    return {"message": "Employee Inactivated Successfully"}
                else:
                    logging.debug("Employee Details Updated Successfully")
                    return {"message": "Employee Details Updated Successfully"}
            else:
                b = Employee1Model(
                    employee_id=current_emp.employee_id,
                    department_id=result['employee_department'],
                    designation_id=result['employee_designation'],
                    emp_type_id=result['employee_type'],
                    employee_status=result['employee_status'],
                    termination_reason_id=result['termination_reason'],
                    account_manager=result['account_manager'],
                    reporting_manager_id = result['reporting_manager_id'],
                    recruiter = result['recruiter'],
                    termination_date = result['termination_date'],
                    modified_time = date_obj,
                    employee_status_last_updtd_date=datetime.now(),
                    rehire_eligibility = result['rehire_eligibility'],
                    inactive_reason_id = data['inactive_reason_id'] if data['inactive_reason_id'] else None,
                )
                b.save_to_db()
                if result['employee_status'] == 'T':
                    logging.debug("Employee Terminated Successfully")
                    return {"message": "Employee Terminated Successfully"}
                elif result['employee_status'] == 'I':
                    logging.debug("Employee Inactivated Successfully")
                    return {"message": "Employee Inactivated Successfully"}
                else:
                    logging.debug("Employee Details Updated Successfully")
                    return {"message": "Employee Details Updated Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class EducationSchema(Schema):
    highest_degree = fields.String(data_key="highestDegree",validate=validate.Length(max=100),required=True)
    branch = fields.String(data_key="branch",validate=validate.Length(max=255),required=True)
    country = fields.String(allow_none=True,validate=validate.Length(max=100),data_key="country",required=True)
    year_awarded = fields.String(data_key="yearAwarded",required=False)
    number_of_years = fields.String(allow_none=True,data_key="numberOfYears",required=True)
    institution = fields.String(allow_none=True,validate=validate.Length(max=255),data_key="institutionName",required=True)

    @validates('year_awarded')
    def validate_age(self, year_awarded):
        if (not year_awarded.isdigit()):
            raise ValidationError('The year awarded is invalid')
        if len(year_awarded)!=4:
            raise ValidationError('The year awarded is invalid')

    @post_load
    def save_education(self, data, **kwargs):
        return data

class EmployeeEducationEditSchema(Schema):
    highest_degree = fields.String(validate=validate.Length(max=100),required=True)
    education_branch = fields.String(validate=validate.Length(max=255),required=True)
    education_country = fields.String(validate=validate.Length(max=100),required=True)
    year_awarded = fields.String(required=False)
    number_of_years = fields.String(required=True)
    institution = fields.String(validate=validate.Length(max=255),required=True)

    @validates('year_awarded')
    def validate_age(self, year_awarded):
        if (not year_awarded.isdigit()):
            raise ValidationError('The year awarded is invalid')
        if len(year_awarded)!=4:
            raise ValidationError('The year awarded is invalid')

    @post_load
    def save_education(self, data, **kwargs):
        return data


class EmployeeEducationEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeEducationEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        record = EducationModel.query.filter_by(employee_education_id=data['employee_education_id']).first()
        if current_emp and record:  
            try:    
                schema = EmployeeEducationEditSchema(unknown=INCLUDE)
                empedu = schema.load(data)
                result = schema.dump(empedu)  
                highest_degree = HighestDegreeListModel.query.filter_by(highest_degree_type=result['highest_degree']).first()
                no_of_years = DegreeTypeListModel.query.filter_by(degree_duration=result['number_of_years']).first()
                country_id = CountryModel.query.filter_by(country_name=result['education_country']).first()
                if highest_degree and no_of_years and country_id:
                    for det in EducationModel.query.filter_by(employee_id=current_emp.employee_id,highest_degree_id=highest_degree.highest_degree_id).all():
                        if det.branch.lower().strip() == result['education_branch'].lower().strip():
                            if det.employee_education_id!=data['employee_education_id']:
                                logging.debug("Cannot Add Duplicate Education Details")
                                return{"errorMessage":"Cannot Add Duplicate Education Details"}
                    if record:
                        # change the values you want to update
                        record.highest_degree_id = highest_degree.highest_degree_id
                        record.year_awarded = result['year_awarded']
                        record.branch = result['education_branch']
                        record.country_id =country_id.country_id
                        record.degree_id = no_of_years.degree_id
                        record.institution_name = result['institution']
                        # commit changes
                        record.save_to_db()
                        current_emp.modified_time = date_obj
                        current_emp.save_to_db()
                        logging.debug("Employee Education Details Updated Successfully")
                        return {"message": "Employee Education Details Updated Successfully"}
                    else:
                        edu_record = EducationModel(
                            employee_id = current_emp.employee_id,
                            highest_degree_id = highest_degree.highest_degree_id,
                            year_awarded = result['year_awarded'],
                            branch =result['education_branch'],
                            country_id =country_id.country_id,
                            degree_id = no_of_years.degree_id,
                            institution_name = result['institution']
                            )
                        edu_record.save_to_db()
                        current_emp.modified_time = date_obj
                        current_emp.save_to_db()
                        logging.debug("Employee Education Details Updated Successfully")
                        return {"message": "Employee Education Details Updated Successfully"}
                else:
                    logging.debug("Please Provide Valid Degree Type")
                    return {"errorMessage":"Please Provide Valid Degree Type"}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
        else:
            return {"errorMessage":"User Does Not Exist"}

class AddEducation(Resource):
    @jwt_required
    @prefix_decorator('AddEducation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        highest_degree = HighestDegreeListModel.query.filter_by(highest_degree_type=data['highestDegree']).first()
        no_of_years = DegreeTypeListModel.query.filter_by(degree_duration=data['numberOfYears']).first()
        country_id = CountryModel.query.filter_by(country_name=data['country']).first() 
        emp_id = Employee1Model.query.filter_by(emp_number=data['employee_number']).first()
        if highest_degree and no_of_years and country_id and emp_id and type(data['institutionName']) == str and type(data['branch']) == str and data['yearAwarded'].isdigit() and len(data['yearAwarded'])==4 :
            #if EducationModel.query.filter_by(employee_id=emp_id.employee_id,branch=data['branch'],highest_degree_id = highest_degree.highest_degree_id).first():
            if data['branch'].lower().strip() in [x.branch.lower().strip() for x in EducationModel.query.filter_by(employee_id=emp_id.employee_id,highest_degree_id = highest_degree.highest_degree_id).all()]:
                logging.debug("Cannot Add Dupplicate Education Details")
                return{"errorMessage":"Cannot Add Dupplicate Education Details"}
            if EducationModel.query.filter_by(employee_id=emp_id.employee_id).count() <2 :     
                edu_record = EducationModel(
                    employee_id = emp_id.employee_id,
                    highest_degree_id = highest_degree.highest_degree_id,
                    year_awarded = data['yearAwarded'],
                    branch =data['branch'],
                    country_id =country_id.country_id,
                    degree_id = no_of_years.degree_id,
                    institution_name = data['institutionName']
                    )
                edu_record.save_to_db()
                logging.debug("Employee Education Details Added Successfully")
                return {"message": "Employee Education Details Added Successfully"}
            else:
                logging.debug("Cannot Add More Than Two Education Details ")
                return{"errorMessage":"Cannot Add More Than Two Education Details "}
        else:
            logging.debug("Please Provide Valid Data")
            return{"errorMessage":"Please Provide Valid Data"}


class VisaSignUpSchema(Schema):
    employee_id = fields.Integer(data_key="employeeId",required=True)
    visa_number = fields.String(data_key="visaNumber",validate=validate.Length(max=30),required=True)
    visa_start_date = fields.Date(format='%m-%d-%Y',data_key="visaStartDate",required=True)
    visa_end_date = fields.Date(format='%m-%d-%Y',data_key="visaEndDate",required=True)
    attorney = fields.String(data_key="attorney",validate=validate.Length(max=150),required=True)
    visa_salary = fields.String(data_key="visaSalary",required=True)
    job_title = fields.String(validate=validate.Length(max=150),data_key="jobTitle",required=True)
    visa_type = fields.String(validate=validate.Length(max=30),data_key="visaType",required=True)
    @post_load
    def save_visa(self, data, **kwargs):
        return data

class EmployeeVisaEditSchema(Schema):
    visa_number = fields.String(validate=validate.Length(max=30),required=True)
    visa_start_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=True)
    visa_end_date = fields.Date(format='%m-%d-%Y',required=True,allow_none=True)
    attorney_name = fields.String(validate=validate.Length(max=150),required=True)
    visa_salary = fields.Float(required=True)
    job_title = fields.String(validate=validate.Length(min=1,max=150),required=True)
    visa_type_id = fields.Integer(required=True)

    @post_load
    def save_visa(self, data, **kwargs):
        return data

class EmployeeVisaEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeVisaEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now() 
        employee_number = Employee1Model.query.filter_by(email_id=username).first().emp_number
        if data['employee_number']:
            current_emp = Employee1Model.query.filter_by(emp_number=data['employee_number']).first()
            if not current_emp:
                logging.debug("Employee Does Not Exist")
                return{"errorMessage":"Employee Does Not Exist"}
            vid = VisaTypeModel.query.filter_by(visa_type = data['visa_type_id']).first()
            if vid:
                data['visa_type_id'] = vid.visa_type_id
            else:
                logging.debug("Please Select Proper Visa Type")
                return {"errorMessage":"Please Select Proper Visa Type"}
            record = EmployeeVisaDetailsModel.find_by_employeenumber(current_emp.employee_id)
            try:
                schema = EmployeeVisaEditSchema(unknown=INCLUDE)
                address = schema.load(data)
                result = schema.dump(address)
                if record:
                    visa_num_record = EmployeeVisaDetailsModel.query.filter_by(visa_number=data['visa_number']).first()
                    if visa_num_record and (visa_num_record.visa_id != record.visa_id):
                        print('yes')
                        logging.debug("Invalid Visa Number")
                        return {"errorMessage":"Invalid Visa Number"}

                    # change the values you want to update
                    record.visa_number = result['visa_number']
                    record.job_title = result['job_title']
                    record.visa_start_date = result['visa_start_date']
                    record.visa_end_date = result['visa_end_date']
                    record.visa_salary = result['visa_salary']
                    record.attorney = result['attorney_name']
                    record.visa_type_id = result['visa_type_id']
                    record.modified_by = employee_number
                    record.modified_time = date_obj
                    # commit changes
                    record.save_to_db()
                    logging.debug("Employee Visa Details Updated Successfully")
                    return {"message": "Employee Visa Details Updated Successfully"}
                else:
                    if EmployeeVisaDetailsModel.query.filter_by(visa_number=data['visa_number']).first():
                        logging.debug("Invalid Visa Number")
                        return {"errorMessage":"Invalid Visa Number"}

                    new_record = EmployeeVisaDetailsModel(
                                 employee_id = current_emp.employee_id,
                                 visa_number=result['visa_number'],
                                 job_title=result['job_title'],
                                 visa_start_date=result['visa_start_date'],
                                 visa_end_date=result['visa_end_date'],
                                 visa_salary=result['visa_salary'],
                                 visa_type_id=result['visa_type_id'],
                                 attorney=result['attorney_name'],
                                 created_by=employee_number,
                                 created_time=date_obj)
                    new_record.save_to_db()
                    current_emp.modified_time = date_obj
                    current_emp.save_to_db()
                    logging.debug("Employee Visa Details Updated Successfully")
                    return {"message": "Employee Visa Details Updated Successfully"}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500

class EadEditschema(Schema):
    employee_number = fields.Integer(required=False)
    ead_type = fields.Integer(required=False)
    valid_from = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    valid_to = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    sponsor_name = fields.String(validate=validate.Length(max=255),required=False)

    @post_load
    def create_person(self, data, **kwargs):
        return data


class EmployeeEadEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeEadEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        ead_type_id = EadTypeListModel.query.filter_by(ead_type=data['ead_type']).first()
        if current_emp and ead_type_id:
            data['ead_type'] = ead_type_id.ead_type_id
            try:
                schema = EadEditschema(unknown=INCLUDE)
                address = schema.load(data)
                result = schema.dump(address)
                record = EadModel.find_by_employeenumber(current_emp.employee_id)
                if record:
                    record.ead_type_id = result['ead_type']
                    record.valid_from = result['valid_from']
                    record.valid_to = result['valid_to']
                    record.sponsor_name = result['sponsor_name']
                    record.save_to_db()
                    logging.debug("Ead Details Updated Successfully")
                    return {"message":"EAD Details Updated Successfully"}
                else:
                    b = EadModel(
                        employee_id = current_emp.employee_id,
                        ead_type_id = result['ead_type'],
                        valid_from = result['valid_from'],
                        valid_to = result['valid_to'],
                        sponsor_name = result['sponsor_name']
                    )
                    b.save_to_db()
                    current_emp.modified_time = datetime.now()
                    current_emp.save_to_db()
                    logging.debug("Ead Details Updated Successfully")
                    return {"message": "Ead Details Updated Successfully"}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
        else:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}

class EmployeeLcaEditSchema1(Schema):
    work_address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    work_address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    work_city = fields.String(validate=validate.Length(max=100),required=False)
    work_state_name = fields.String(validate=validate.Length(max=100),required=False)
    work_zip_code = fields.String(required=False)
    work_country = fields.String(validate=validate.Length(max=100),required=False)
    alt_address_line1 = fields.String(validate=validate.Length(max=255),required=False)
    alt_address_line2 = fields.String(validate=validate.Length(max=255),required=False)
    alt_city = fields.String(validate=validate.Length(max=100),required=False)
    alt_state_name = fields.String(validate=validate.Length(max=100),required=False)
    alt_zip_code = fields.String(required=False)
    alt_country = fields.String(validate=validate.Length(max=100),required=False)
    lca_salary = fields.Float(required=False)
    lca_wage_level = fields.Float(required=False)
    lca_prevailing_wages = fields.Float(required=False)
    lca_start_date = fields.Date(format='%m-%d-%Y',required=True)
    lca_end_date = fields.Date(format='%m-%d-%Y',required=True)
    current_status = fields.String(validate=validate.Length(max=1),required=False)

    @post_load
    def save_address(self, data, **kwargs):
        return data

class EmployeeLcaEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeLcaEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        if not current_emp:
            logging.debug("Employee With {0} Employee Number Does Not Exist".format(data['employee_number']))
            return {"errorMessage":"Employee With {0} Employee Number Does Not Exist".format(data['employee_number'])}
        record = EmployeeVisaDetailsModel.find_by_employeenumber(current_emp.employee_id)
        try:
            data['lca_wage_level'] = LcaWageListModel.query.filter_by(lca_wage_level=data['lca_wage_level']).first()
            if data['lca_wage_level']:
                data['lca_wage_level']= data['lca_wage_level'].lca_wage_level_id
                data['current_status'] = 'A' if data['current_status']=='Active' else 'I' if data['current_status']=='Inactive' else 'T'  if data['current_status']=='Terminated' else ''
            else:
                logging.debug("Please Select Valid Lca Wage Level")
                return{"errorMessage":"Please Select Valid Lca Wage Level"}
            schema = EmployeeLcaEditSchema1(unknown=INCLUDE)
            address = schema.load(data)
            result = schema.dump(address)
            if record:
                addrec = Address1Model.query.filter_by(address_id=record.lca_work_address_id).first()
                altaddrec = Address1Model.query.filter_by(address_id=record.lca_alternate_address_id).first()
                if addrec:
                    # change the values you want to update
                    addrec.address_line1=result['work_address_line1']
                    addrec.address_line2=result['work_address_line2']
                    addrec.city=result['work_city']
                    addrec.state_name=result['work_state_name']
                    addrec.zip_code=result['work_zip_code']
                    addrec.country=result['work_country']
                    addrec.save_to_db()
                else:
                    b = Address1Model(
                    address_line1=result['work_address_line1'],
                    address_line2=result['work_address_line2'],
                    city=result['work_city'],
                    state_name=result['work_state_name'],
                    zip_code=result['work_zip_code'],
                    country=result['work_country']
                    )
                    b.save_to_db()
                    record.lca_work_address_id = b.address_id
                if altaddrec:
                    altaddrec.address_line1=result['alt_address_line1']
                    altaddrec.address_line2=result['alt_address_line2']
                    altaddrec.city=result['alt_city']
                    altaddrec.state_name=result['alt_state_name']
                    altaddrec.zip_code=result['alt_zip_code']
                    altaddrec.country=result['alt_country']
                    altaddrec.save_to_db()
                elif data['alt_address_line1'] != '':
                    a = Address1Model(
                    address_line1=result['alt_address_line1'],
                    address_line2=result['alt_address_line2'],
                    city=result['alt_city'],
                    state_name=result['alt_state_name'],
                    zip_code=result['alt_zip_code'],
                    country=result['alt_country']
                    )
                    a.save_to_db()
                    record.lca_alternate_address_id = a.address_id
                record.lca_salary = result['lca_salary']
                record.lca_wage_level_id = result['lca_wage_level']
                record.lca_prevailing_wages = result['lca_prevailing_wages']
                record.lca_start_date = result['lca_start_date']
                record.lca_end_date = result['lca_end_date']
                record.current_status = result['current_status']
                # commit changes
                record.save_to_db()
                current_emp.modified_time = datetime.now()
                current_emp.save_to_db()
                logging.debug("LCA Details Updated Successfully")
                return {"message": "LCA Details Updated Successfully"}
            else:
                addrec = Address1Model(
                    address_line1=result['work_address_line1'],
                    address_line2=result['work_address_line2'],
                    city=result['work_city'],
                    state_name=result['work_state_name'],
                    zip_code=result['work_zip_code'],
                    country=result['work_country']
                    )
                addrec.save_to_db()
                if result['alt_address_line1'] != '':
                    alt_add = Address1Model(
                        address_line1=result['alt_address_line1'],
                        address_line2=result['alt_address_line2'],
                        city=result['alt_city'],
                        state_name=result['alt_state_name'],
                        zip_code=result['alt_zip_code'],
                        country=result['alt_country']
                        )
                    alt_add.save_to_db()
                    alt_address = alt_add.address_id
                else:
                    alt_address = None
                lca_new = EmployeeVisaDetailsModel(
                    employee_id = current_emp.employee_id,
                    lca_work_address_id = addrec.address_id,
                    lca_alternate_address_id = alt_address,
                    lca_salary = result['lca_salary'],
                    lca_wage_level_id = result['lca_wage_level'],
                    lca_prevailing_wages = result['lca_prevailing_wages'],
                    lca_start_date = result['lca_start_date'],
                    lca_end_date = result['lca_end_date'],
                    current_status =result['current_status'],
                    )
                lca_new.save_to_db()
            logging.debug("LCA Details Added Successfully")
            return {"message": "LCA Details Added Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class EmployeeBenefitsEditSchema(Schema):
    benefits_id = fields.Integer(required=False)
    employee_number = fields.Integer(required=False)
    benefits_type = fields.String(required=False)
    coverage_type = fields.String(required=False)
    benefit_start_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)
    benefit_end_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)

    @post_load
    def save_address(self, data, **kwargs):
        return data

class EmployeeBenefitsEdit(Resource):
    @prefix_decorator('EmployeeBenefitsEdit')
    def post(self):  
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        if data['employee_number']:
            current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
            if current_emp:
                try:
                    schema = EmployeeBenefitsEditSchema(unknown=INCLUDE)
                    empbenefit = schema.load(data)
                    result = schema.dump(empbenefit)
                    record1 = BenefitsModel.query.filter_by(employee_id = current_emp.employee_id,benefit_type_id=BenefitTypeListModel.query.filter(func.lower(BenefitTypeListModel.benefit_type) == result['benefits_type'].lower()).first().benefit_type_id).first()
                    if (record1 and record1.benefits_id != result['benefits_id']) or (record1 and int(result['benefits_id'])== 0):
                        logging.debug("Benefit Already Exists")
                        return {"errorMessage":"Benefit Already Exists"}
                    record = BenefitsModel.query.filter_by(benefits_id = result['benefits_id']).first()
                    if record:
                        print('if record')
                        # change the values you want to update
                        record.benefit_type_id = BenefitTypeListModel.query.filter(func.lower(BenefitTypeListModel.benefit_type) == result['benefits_type'].lower()).first().benefit_type_id
                        record.coverage_type_id = CoverageTypeListModel.query.filter(func.lower(CoverageTypeListModel.coverage_type) == result['coverage_type'].lower()).first().coverage_type_id if result['coverage_type'] else None
                        record.benefit_start_date = data['benefit_start_date']
                        record.benefit_end_date = result['benefit_end_date']
                        # commit changes
                        record.save_to_db()
                        current_emp.modified_time = date_obj
                        current_emp.save_to_db()
                        logging.debug( "Employee Benefits Details Updated Successfully")
                        return {"message": "Employee Benefits Details Updated Successfully"}
                    else:
                        print('else record')
                        new_rec = BenefitsModel(
                        benefit_type_id = BenefitTypeListModel.query.filter(func.lower(BenefitTypeListModel.benefit_type) == result['benefits_type'].lower()).first().benefit_type_id,
                        employee_id = current_emp.employee_id,
                        coverage_type_id = CoverageTypeListModel.query.filter(func.lower(CoverageTypeListModel.coverage_type) == result['coverage_type'].lower()).first().coverage_type_id if result['coverage_type'] else None,   
                        benefit_start_date = result['benefit_start_date'],
                        benefit_end_date = result['benefit_end_date']
                        )
                        # commit changes
                        new_rec.save_to_db()
                        current_emp.modified_time = date_obj
                        current_emp.save_to_db()
                        logging.debug( "Employee Benefits Details Added Successfully")
                        return {"message": "Employee Benefits Details Added Successfully"}
                except ValidationError as err: 
                    logging.exception('Exception Occured, {0}'.format(err.messages))
                    return {"errorMessage":err.messages},500
            else:
                logging.debug("Employee Does Not Exist")
                return {"errorMessage":"Employee Does Not Exist"}

#class EmployeeBenefitsEditSchema(Schema):
#    employee_number = fields.Integer(required=False)
#    benefits_type = fields.List(fields.String(),required=False)
#    coverage_type = fields.String(required=False)
#    benefit_start_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)

#    @post_load
#    def save_address(self, data, **kwargs):
#        return data

#class EmployeeBenefitsEdit(Resource):
#    @jwt_required
#    @prefix_decorator('EmployeeBenefitsEdit')
#    def post(self):  
#        data = request.get_json(force=True)
#        date_obj = datetime.now()
#        print(data)
#        if data['employee_number']:
#            current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
#            if current_emp:
#                try:
#                    schema = EmployeeBenefitsEditSchema(unknown=INCLUDE)
#                    empbenefit = schema.load(data)
#                    result = schema.dump(empbenefit)
#                    benefits = list(set([BenefitTypeListModel.query.filter_by(benefit_type =i).first().benefit_type_id for i in result['benefits_type']]))
#                    record = BenefitsModel.find_by_employeenumber(current_emp.employee_id)
#                    if record:
 #                       # change the values you want to update
#                        record.coverage_type_id = CoverageTypeListModel.query.filter_by(coverage_type=result['coverage_type']).first().coverage_type_id if result['coverage_type'] else None
#                        record.benefit_types = benefits
#                        record.benefit_start_date = result['benefit_start_date']
#                        # commit changes
#                        record.save_to_db()
#                        current_emp.modified_time = date_obj
#                        current_emp.save_to_db()
#                        logging.debug( "Employee Benefits Details Updated Successfully")
#                        return {"message": "Employee Benefits Details Updated Successfully"}
#                    else: 
#                        new_rec = BenefitsModel(
#                        benefit_types = benefits,
#                        employee_id = current_emp.employee_id,
#                        coverage_type_id = CoverageTypeListModel.query.filter_by(coverage_type=result['coverage_type']).first().coverage_type_id if result['coverage_type'] else None,   
#                        benefit_start_date = result['benefit_start_date'],
#                        )
#                        # commit changes
#                        new_rec.save_to_db()
#                        current_emp.modified_time = date_obj
#                        current_emp.save_to_db()
#                        logging.debug( "Employee Benefits Details Updated Successfully")
#                        return {"message": "Employee Benefits Details Updated Successfully"}
#                except ValidationError as err: 
#                    logging.exception('Exception Occured, {0}'.format(err.messages))
#                    return {"errorMessage":err.messages},500
#            else:
#                logging.debug("Employee Does Not Exist")
#                return {"errorMessage":"Employee Does Not Exist"}

class SalarySchema(Schema):
    salary_id = fields.Integer(data_key="salary_id",required=False)
    employee_id = fields.Integer(data_key="employee_id",required=False)
    pay_type_id = fields.Integer(data_key="pay_type",required=False)
    pay_freq_id = fields.Integer(data_key="payroll_frequency",required=False)
    salary_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="salary_start_date",required=False)
    salary_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="salary_end_date",required=False)
    pay_amount = fields.Float(data_key="pay_amount",required=False)  
    last_rise_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="last_rise_date",required=False)
    salary_last_rise_amount = fields.Float(data_key="salary_last_rise_amount",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

    @validates('pay_amount')
    def validate_age(self, pay_amount):
        if (pay_amount<0.00 or pay_amount>999999.99):
            raise ValidationError('The pay_amount provided is out of range')

class EmployeeSalaryEdit(Resource):
    @jwt_required
    @prefix_decorator('EmployeeSalaryEdit')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        data['salary_end_date'] = None if not data['salary_end_date'] else data['salary_end_date']
        data['salary_end_date'] = None if not data['salary_end_date'] else data['salary_end_date']
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        data['pay_type'] = PayTypeListModel.query.filter_by(pay_type=data['pay_type']).first()
        data['payroll_frequency'] = PayrollFrequencyListModel.query.filter_by(pay_frequency=data['payroll_frequency']).first()
        if data['pay_type'] and data['payroll_frequency'] and current_emp:
            record = SalaryModel.find_by_employeenumber(current_emp.employee_id)
            data['pay_type'] = data['pay_type'].pay_type_id
            data['payroll_frequency'] = data['payroll_frequency'].pay_freq_id
        else:
            logging.debug("Employee Does Not Exist")
            return{"errorMessage":"Employee Does Not Exist"}
        try:
            schema = SalarySchema(unknown=EXCLUDE)
            salary = schema.load(data)
            result = schema.dump(salary)
            if record:
                # change the values you want to update
                previous_salary = record.pay_amount if record.salary_last_rise_amount else result['pay_amount']
                previous_date = date_obj if record.pay_amount != result['pay_amount'] else (record.last_rise_date if record.last_rise_date else record.salary_start_date)
                record.pay_type_id = result['pay_type']
                record.salary_start_date = result['salary_start_date']
                record.salary_end_date = result['salary_end_date']
                record.pay_amount = result['pay_amount']
                record.pay_freq_id = result['payroll_frequency']
                record.last_rise_date = previous_date
                record.salary_last_rise_amount = previous_salary
                # commit changes
                record.save_to_db()
                current_emp.modified_time = date_obj
                current_emp.save_to_db()
                logging.debug("Employee Salary Details Updated Successfully")
                return {"message": "Employee Salary Details Updated Successfully"}
            else:
                new_rec = SalaryModel(
                employee_id = current_emp.employee_id,  
                pay_type_id = result['pay_type'],
                salary_start_date = result['salary_start_date'],
                salary_end_date = result['salary_end_date'],
                pay_amount = result['pay_amount'],
                pay_freq_id = result['payroll_frequency'],
                last_rise_date = result['salary_start_date'],
                salary_last_rise_amount = result['pay_amount'] )
                new_rec.save_to_db()
                current_emp.modified_time = date_obj
                current_emp.save_to_db()
                logging.debug("Employee Salary Details Updated Successfully")
                return {"message": "Employee Salary Details Updated Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {"errorMessage":err.messages},500

class DeleteProject(Resource):
    @jwt_required
    @prefix_decorator('DeleteProject')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        proemprecord = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
        if proemprecord:
            if len(proemprecord.employee_id)>1:
                logging.debug("Failed, As Active Employees Still Assigned To This Project")
                return {"errorMessage":"Failed, As Active Employees Still Assigned To This Project"}
            record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
            record.project_status = 'I'
            record.comments = data['comments']
            record.modified_time = date_obj
            record.project_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info( "Project Deactivated Successfully")
            return {"Message": "Project Deactivated Successfully"}
        else:
            record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
            record.project_status = 'I'
            record.comments = data['comments']
            record.modified_time = datetime.now()
            record.project_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info( "Project Deactivated Successfully")
            return {"Message": "Project Deactivated Successfully"}

class UserInviteSchema(Schema):
    username = fields.Email(validate=validate.Length(max=120),required=False)
    base_url = fields.String(required=False)
    role = fields.String(validate=validate.Length(max=255),required=False)
    employee_number = fields.Integer(required=False)
    employee_designation = fields.String(validate=validate.Length(max=100),required=False)
    employee_department = fields.String(validate=validate.Length(max=100),required=False)
    billable_resource = fields.String(validate=validate.Length(max=3),required=False)
    date_of_joining = fields.Date(format='%m-%d-%Y',allow_none=True,required=False)


    @validates('employee_number')
    def validate_age(self, employee_number):
        if len(str(employee_number)) != 6 :
            raise ValidationError('Invalid Employee Number')

    @post_load
    def create_user(self, data, **kwargs):
        return data

class UserInvite(Resource):
    @jwt_required
    @prefix_decorator('UserInvite')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        sent_user = get_jwt_identity()
        try:
            schema = UserInviteSchema(unknown=EXCLUDE)
            userinvite = schema.load(data)
            result = schema.dump(userinvite)
            dept = DepartmentModel.query.filter_by(department_name=result['employee_department']).first()
            desg = DesignationModel.query.filter_by(designation_name=result['employee_designation']).first()
            role = RoleModel.query.filter_by(role_name=result['role']).first()
            email = result['username'].lower()
            if dept:
                dept = dept.department_id
            else:
                logging.debug('Department Does Not Exists')
                return {'errorMessage':'Department Does Not Exists'}
            if desg:
                desg = desg.designation_id
            else:
                logging.debug('Designation Does Not Exists')
                return {'errorMessage':'Designation Does Not Exists'}
            if role:
                role = role.id
            else:
                logging.debug("Role Does Not Exists")
                return {"errorMessage":"Role Does Not Exists"} 
            if Employee1Model.query.filter_by(emp_number=result['employee_number']).first():
                logging.debug("Employee Number Already Exists")
                return {"errorMessage": "Employee Number Already Exists"}
            elif Employee1Model.query.filter_by(employee_status="Active", email_id=email).first():
                logging.debug("An Active User Already Exists With Same Email")
                return {"errorMessage": "An Active User Already Exists With Same Email"}
            elif EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=result['employee_number'],employee_email=email).first():
                logging.debug("An Invitation Mail Has Already Been Sent")
                return {"errorMessage": "An Invitation Mail Has Already Been Sent"}
            elif (EmployeeInviteStatus.query.filter_by(employee_number=result['employee_number']).first()):
                logging.debug("An Invitation Mail With Same Employee Number Already Been Sent")
                return {"errorMessage": "An Invitation Mail With Same Employee Number Already Been Sent"}
            elif (EmployeeInviteStatus.query.filter_by(employee_email=email).first()):
                logging.debug("An Invitation Mail With Same Email Already Been Sent")
                return {"errorMessage": "An Invitation Mail With Same Email Already Been Sent"}
            else:
                samp_username_bytes = email.encode('ascii')
                base64_username = base64.b64encode(samp_username_bytes)  
                username = base64_username.decode("ascii")
                samp_role_bytes = result['role'].encode('ascii')
                base64_role = base64.b64encode(samp_role_bytes)
                role = base64_role.decode("ascii")
                samp_emp_num_bytes = str(result['employee_number']).encode('ascii')
                base64_emp_num = base64.b64encode(samp_emp_num_bytes)
                emp_num = base64_emp_num.decode("ascii")
                samp_emp_dept_bytes = result['employee_department'].encode('ascii')
                base64_emp_dept = base64.b64encode(samp_emp_dept_bytes)
                emp_dept = base64_emp_dept.decode("ascii")
                samp_emp_desg_bytes = result['employee_designation'].encode('ascii')
                base64_emp_desg = base64.b64encode(samp_emp_desg_bytes)
                emp_desg = base64_emp_desg.decode("ascii")
                samp_emp_billabilty_bytes = result['billable_resource'].encode('ascii')
                base64_emp_billability = base64.b64encode(samp_emp_billabilty_bytes)
                emp_br = base64_emp_billability.decode("ascii")
                samp_emp_joining_date_bytes = result['date_of_joining'].encode('ascii')
                base64_emp_joining_date = base64.b64encode(samp_emp_joining_date_bytes)
                emp_jd = base64_emp_joining_date.decode("ascii")
                user = Employee1Model.query.filter_by(email_id=sent_user).first()
                if not user:
                    logging.info("User {} Does Not Exist".format(sent_user))
                    {"errorMessage":"User {} Does Not Exist".format(sent_user)}
                base_url = result['base_url'] + '?email=' + username + '&role_id=' + role + '&emp_num=' + emp_num + '&emp_dept=' + emp_dept + '&emp_desg=' + emp_desg + '&emp_jd=' + emp_jd + '&emp_br='+emp_br
                msg = Message('Prutech invitation link', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Greetings from PRUTECH Team!</br>
                <p style="text-align:center"><b>Invitation</b></p>
                <p style="text-align:center"> {1} invited you to join </p>
                <p style="text-align:center"> PRUTECH HR Portal <a href={2}>Signup Here</a></p>   
                <br> <b>Regards,
                <br> PRUTECH Team.
                </b>""".format((result['username'].split('@')[0]).split('.')[0].title(),user.first_name.title()+" "+user.last_name.title(),base_url)
                mail.send(msg)
                invite_record = EmployeeInviteStatus(employee_number=result['employee_number'],
                                                     employee_email=email,designation_id=desg,department_id=dept ,invitation_status="Active",employee_role=result['role'],
                                                     sent_time=datetime.now().strftime('%m-%d-%Y'),billable_resource=result['billable_resource'][0],joining_date=result['date_of_joining'])
                invite_record.save_to_db()
                logging.debug( "Invitation Sent Successfully")
                return {"message": "Invitation Sent Successfully"}
        except ValidationError as err:
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage': err.messages}, 500

class AddSupplier(Resource):
    @jwt_required
    @prefix_decorator('AddSupplier')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        data['contract_status'] = 'A' if data['contract_status']=='Active' else 'I' if data['contract_status']=='Inactive' else 'T' if data['contract_status']=='Terminated' else ''
        supplier_contracts = [i['contractNumber'] for i in data['supplier_contracts']]
        if StaffingSupplierModel.query.filter_by(supplier_name=data['supplier_name']).first():
            return {"errorMessage":"Supplier Name Already Exists"}
        for i in supplier_contracts:
            if SupplierContractModel.query.filter_by(contarct_number=i).first():
                logging.info("Supplier Already Exists With Same Contract Number")
                return {"errorMessage":"Supplier Already Exists With Same Contract Number"}
        if len(list(set(supplier_contracts))) != len(supplier_contracts):
            logging.module("Cannot Have Same Contract Number")
            return {"errorMessage":"Cannot Have Same Contract Number"}
        else:
            try:
                schema1 = StaffingSupplierSchema(unknown=EXCLUDE)  
                supplier = schema1.load(data)
                result1 = schema1.dump(supplier)
                for data_add in data['address']:
                    schema2 = AddressSchema(unknown=EXCLUDE)
                    address = schema2.load(data_add)
                    result2 = schema2.dump(address)
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
            sup = StaffingSupplierModel(
                supplier_name=result1['supplier_name'],
                state_of_incorporation=result1['state_of_incorporation'],
                tax_id_FEIN=result1['tax_id_FEIN'],
                diversity_status=result1['diversity_status_list'],
                diversity_certification_expiry_date=result1['diversity_certification_expiry_date'],
                contact_number=result1['contact_number'],
                contract_status=result1['contract_status'],
                owner_name=result1['owner_name'],
                company_website_url=result1['company_website_url'],
                created_time=date_obj,
                modified_time=date_obj,
                supplier_status_last_updtd_date = date_obj
            )
            sup.save_to_db()
            for data_add in data['address']:
                b = Address1Model(
                    address_line1=data_add['addressLine1'],
                    address_line2=data_add['addressLine2'],
                    city=data_add['city'],
                    state_name=data_add['state'],
                    zip_code=data_add['zip'],
                    country=data_add['country']
                )
                b.save_to_db()
                addid = b.address_id
                c = SupplierAddressModel(
                    address_id=addid,
                    supplier_id=sup.supplier_id,
                    status = 'A',
                    modified_time = date_obj ,
                    created_time = date_obj,
                    created_by = userid
                )
                c.save_to_db()
            for x in data['supplier_contracts']:
                a = SupplierContractModel(
                    supplier_id=sup.supplier_id,
                    contract_name=x['contractName'],
                    contarct_number=x['contractNumber'],
                    start_date=x['contractEffectiveDate'],
                    end_date=x['contractEndDate'],
                    cretaed_by=userid,
                    created_time=date_obj,
                    modified_time=date_obj,
                    status = 'A'
                )
                a.save_to_db()
            return {
                "message": "Supplier Details Added Successfully",
                "supplier_id": sup.supplier_id
            }

class ValidateSupplierName(Resource):
    @jwt_required
    @prefix_decorator('ValidateSupplierName')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['supplier_name'].lower().strip() not in [i.supplier_name.lower().strip() for i in StaffingSupplierModel.query.all()]:
            logging.info('Success')
            return {'message':'Success'}
        else:
            logging.debug('Supplier With Same Name Already Exists')
            return {'errorMessage':'Supplier With Same Name Already Exists'}

class StaffingSupplierSchema(Schema):
    supplier_name = fields.String(validate=validate.Length(max=150),data_key="supplier_name",required=True)
    supplier_id = fields.Integer(data_key="supplier_id",required=False)
    state_of_incorporation = fields.String(validate=validate.Length(max=60),data_key="state_of_incorporation",required=True)
    tax_id_FEIN = fields.String(validate=validate.Length(max=13),data_key="tax_id_FEIN",required=True)
    contract_start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="contract_start_date",required=False)
    contract_end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="contract_end_date",required=False)
    diversity_status = fields.List(fields.String(),data_key="diversity_status_list",required=False)  
    diversity_certification_expiry_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="diversity_certification_expiry_date",required=False)
    contact_number = fields.String(validate=validate.Length(max=20),data_key="contact_number",required=True)
    termination_reason = fields.String(allow_none=True,validate=validate.Length(max=255),data_key="termination_reason",required=False)
    contract_status = fields.String(validate=validate.Length(max=1),data_key="contract_status",required=True)
    owner_name = fields.String(validate=validate.Length(max=150),data_key="owner_name",required=False)
    company_website_url = fields.String(validate=validate.Length(max=255),data_key="company_website_url",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data
  
class SupplierContactSchema(Schema):
    contact_id = fields.Integer(data_key="consultant_id",required=False)
    supplier_id = fields.Integer(data_key="supplier_id",required=False)
    supplier_contact_type_id = fields.Integer(data_key="contact_type",required=True)
    contact_first_name = fields.String(validate=validate.Length(max=75),data_key="contact_first_name",required=True)
    contact_last_name = fields.String(validate=validate.Length(max=75),data_key="contact_last_name",required=True)
    contact_middle_name = fields.String(validate=validate.Length(max=75),data_key="contact_middle_name",required=False)
    contact_phone = fields.String(validate=validate.Length(max=20),data_key="contact_phone",required=True)
    contact_email = fields.String(validate=validate.Length(max=120),data_key="contact_email",required=True)
    contact_title = fields.String(validate=validate.Length(max=50),data_key="contact_title",required=True)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

class SupplierContractSchema(Schema):
    contract_id = fields.Integer(data_key="contract_id",required=False)
    supplier_id = fields.Integer(data_key="supplier_id",required=False)
    contract_number = fields.String(validate=validate.Length(max=50),data_key="contract_number",required=False)
    contract_name = fields.String(validate=validate.Length(max=255),data_key="contract_name",required=False)
    start_date = fields.Date(allow_none=True,format='%m-%d-%Y',validate=validate.Length(max=75),data_key="start_date",required=False)
    end_date = fields.Date(allow_none=True,format='%m-%d-%Y',validate=validate.Length(max=75),data_key="end_date",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data


class AddSupplierContact(Resource):
    @jwt_required
    @prefix_decorator('AddSupplierContact')
    def post(self):
        i = request.get_json(force=True)
        logging.debug(i)
        for data in i['contact']:
            if SupplierContactModel.query.filter_by(supplier_id=i['supplier_id'],contact_email=data['contact_email'].lower()).first():
                return {"errorMessage":"Already Supplier Contact Exists With Same Email"}
            if SupplierContactModel.query.filter_by(supplier_id=i['supplier_id'],contact_phone=data['contact_phone']).first():        
                return {"errorMessage":"Already Supplier Contact Exists With Same Contact Phone Number"}
            d = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first()
            if not d:
                logging.debug("Invalid Contact Type")
                return {"errorMessage":"Invalid Contact Type"}
        for data in i['contact']:
            data['contact_type'] = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first().supplier_contact_type_id
            try:
                schema = SupplierContactSchema(unknown=INCLUDE)
                supplier = schema.load(data)
                result = schema.dump(supplier)
                a = SupplierContactModel(
                    supplier_id=i['supplier_id'],
                    contact_first_name=data['contact_first_name'],
                    contact_last_name=data['contact_last_name'],
                    contact_middle_name=data['contact_middle_name'],
                    supplier_contact_type_id=data['contact_type'],
                    contact_phone=data['contact_phone'],
                    contact_email=data['contact_email'].lower(),
                    contact_title=data['contact_title'],
                    contact_status='A'
                )
                a.save_to_db()
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {'errorMessage': err.messages}, 500
        logging.debug("Supplier Contact Details Added")
        return {"message": "Supplier Contact Details Added"}

class AddSupplierContract(Resource):
    @jwt_required
    @prefix_decorator('AddSupplierContract')
    def post(self):
        i = request.get_json(force=True)
        logging.debug(i)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first().employee_id
        for data in i['contract']:
            a = SupplierContractModel(
                supplier_id=data['supplier_id'],
                contract_name=data['contract_name'],
                contarct_number=data['contract_number'],
                start_date=data['start_date'],
                end_date=data['end_date'],
                created_by=userid,
                created_time=date_obj,
                modified_time=date_obj
            )
            a.save_to_db()
        return {"message": "Supplier Contract Details Added"}

class AddSupplierContact1(Resource):
    @jwt_required
    @prefix_decorator('AddSupplierContact1')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_email=data['contact_email'].lower()).first():
            logging.debug("Already Supplier Contact Exists With Same Email")
            return {"errorMessage":"Already Supplier Contact Exists With Same Email"}
        if SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_phone=data['contact_phone']).first():
            logging.debug("Already Supplier Contact Exists With Same Contact Phone Number")        
            return {"errorMessage":"Already Supplier Contact Exists With Same Contact Phone Number"}
        data['contact_type'] = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first()
        record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if not record:
            logging.debug('Supplier Does Not Exist')
            return {'errorMessage': 'Supplier Does Not Exist'}
        data['contact_type'] = data['contact_type'].supplier_contact_type_id
        schema = SupplierContactSchema(unknown=INCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        a = SupplierContactModel(
        supplier_id=result['supplier_id'],
        contact_first_name=result['contact_first_name'],
        contact_last_name=result['contact_last_name'],
        contact_middle_name=result['contact_middle_name'],
        supplier_contact_type_id=result['contact_type'],
        contact_phone=result['contact_phone'],
        contact_email=result['contact_email'].lower(),
        contact_title=result['contact_title'],
        contact_status='A'
    )
        a.save_to_db()
        record.modified_time = datetime.now()
        record.save_to_db()
        logging.debug( "Supplier Contact Details Added")
        return {"message": "Supplier Contact Details Added"}

class AddSupplierContract1(Resource):
    @jwt_required
    @prefix_decorator('AddSupplierContract1')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()     
        date_obj = datetime.now() 
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            return {'errorMessage': 'User Does Not Exist'}
        schema = SupplierContractSchema(unknown=INCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        if SupplierContractModel.query.filter(func.lower(SupplierContractModel.contarct_number)==result['contract_number'].lower().strip(),SupplierContractModel.status == 'A').first():
            return {'errorMessage': 'Active Contract With Same Contract Number Already Exists'}
        a = SupplierContractModel(
            supplier_id=result['supplier_id'],
            contract_name=result['contract_name'],
            contarct_number=result['contract_number'].strip(),
            start_date=result['start_date'],
            end_date=result['end_date'],
            cretaed_by = userid.employee_id,
            created_time = date_obj,
            modified_by=userid.employee_id,
            modified_time=date_obj,
            status = 'A'
        )
        a.save_to_db()
        record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
        record.modified_time = date_obj
        record.save_to_db()
        return {"message": "Supplier Contract Details Added"}

class SupplierContractSchema(Schema):
    contract_id = fields.Integer(data_key="contract_id",required=False)
    supplier_id = fields.Integer(data_key="supplier_id",required=False)
    contract_number = fields.String(validate=validate.Length(min=1,max=25),required=False,error="Invalid Contract Number")
    contract_name = fields.String(validate=validate.Length(min=1,max=255),required=True)
    start_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="start_date",required=True)
    end_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="end_date",required=True)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

class SupplierView(Resource):
    @jwt_required
    @prefix_decorator('SupplierView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        supplier_obj = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if not supplier_obj:
            logging.debug("Suppier Does Not Exist")
            return {"errorMessage":"Suppier Does Not Exist"}
        supplier_address_obj = SupplierAddressModel.return_all(data['supplier_id'])
        supplier_contact_obj = SupplierContactModel.return_all(data['supplier_id'])
        supplier_contract_obj = SupplierContractModel.return_all(data['supplier_id'])
        consultants_obj = ThirdPartyConsultantModel.return_all_supplier_consultants(data['supplier_id'])
        logging.info("Success")
        return {"supplier_id": supplier_obj.supplier_id,
                "supplier_name": supplier_obj.supplier_name,
                'state_of_incorporation': supplier_obj.state_of_incorporation,
                'tax_id_FEIN': supplier_obj.tax_id_FEIN,
                'diversity_status': supplier_obj.diversity_status,
                'diversity_certification_expiry_date': str(supplier_obj.diversity_certification_expiry_date),
                "contact_number": supplier_obj.contact_number,
                "termination_reason": supplier_obj.termination_reason,
                "contract_status": 'Active' if supplier_obj.contract_status=='A' else 'Inactive' if supplier_obj.contract_status=='I' else 'Terminated' if supplier_obj.contract_status=='T' else '',
                "owner_name": supplier_obj.owner_name,
                "company_website_url": supplier_obj.company_website_url,
                "comments":supplier_obj.comments,
                'address': supplier_address_obj['supplier_address'],
                "supplier_contacts": supplier_contact_obj['supplier_contacts'],
                "supplier_contracts": supplier_contract_obj,
                "supplier_Status_last_updated_date":str(supplier_obj.supplier_status_last_updtd_date),
                "consultants":consultants_obj['consultants']
                }

class SupplierDetailsEdit(Resource):
    @jwt_required
    @prefix_decorator('SupplierDetailsEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record1 = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        dup_supp_name_rec = StaffingSupplierModel.query.filter_by(supplier_name=data['supplier_name']).all()
        for i in dup_supp_name_rec:
            
            if (str(data['supplier_id']) != str(i.supplier_id)):
                print(i.supplier_id,data['supplier_id'])
                return {"errorMessage":"Supplier Name Already Exists"}
        if not record1:
            return {'errorMessage': 'Supplier Does Not Exist'}
        if data['contract_status'] != 'Active':
            all_consultants = ThirdPartyConsultantModel.query.filter_by(supplier_id=data['supplier_id']).all()
            if all_consultants:
                for i in all_consultants:
                    if i.consultant_status == 'A':
                        return {"errorMessage":"Failed, Active Consultants Exists Under This Supplier"}
        data['contract_status'] = 'A' if data['contract_status']=='Active' else 'I' if data['contract_status']=='Inactive' else 'T' if data['contract_status']=='Terminated' else ''
        schema = StaffingSupplierSchema(unknown=EXCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        if record1.contract_status != result['contract_status']:
            last_update_date = date_obj
        else:
            last_update_date = record1.supplier_status_last_updtd_date
        record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
        # change the values you want to update
        record.supplier_name = result['supplier_name']
        record.state_of_incorporation = result['state_of_incorporation']
        record.tax_id_FEIN = result['tax_id_FEIN']
        record.diversity_status = result['diversity_status_list']
        record.diversity_certification_expiry_date = result['diversity_certification_expiry_date']
        record.contact_number = result['contact_number']
        record.owner_name = result['owner_name']
        record.company_website_url = result['company_website_url']
        record.contract_status = result['contract_status']
        record.modified_time = date_obj
        record.supplier_status_last_updtd_date = last_update_date
        record.termination_reason = result['termination_reason']
        # commit changes
        record.save_to_db()
        if data['contract_status'] == 'A':
            logging.debug("Details Updated Sucessfully")
            return {"message": "Details Updated Sucessfully"}
        elif data['contract_status'] == 'I':
            logging.debug("Supplier Inactivated Sucessfully")
            return {"message":"Supplier Inactivated Sucessfully"}
        else:
            logging.debug("Supplier Terminated Sucessfully")
            return {"message":"Supplier Terminated Sucessfully"}

class SupplierAddressEdit(Resource):
    @jwt_required
    @prefix_decorator('SupplierAddressEdit')
    def post(self):
        data = request.get_json(force=True) 
        print(data)
        username = get_jwt_identity()
        date_obj = datetime.now()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        record4 = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if not record4:
            logging.debug('Supplier Does Not Exist')
            return {'errorMessage': 'Supplier Does Not Exist'}
        schema = AddressSchema(unknown=EXCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        if data['address_id']:
            record = Address1Model.query.filter_by(address_id=data['address_id']).first()
            if record:
                # change the values you want to update
                record.address_line1 = result['addressLine1']
                record.address_line2 = result['addressLine2']
                record.city = result['city']
                record.state_name = result['state']
                record.country = result['country']
                record.zip_code = result['zip']
                # commit changes
                record.save_to_db()
                record1 = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
                record1.modified_time = date_obj
                record1.save_to_db()
                record2 = SupplierAddressModel.query.filter_by(address_id=data['address_id']).first()
                record2.modified_time = date_obj
                record2.modified_by = userid.employee_id
                record2.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
            else:
                a = Address1Model(
                    address_line1=result['addressLine1'],
                    address_line2=result['addressLine2'],
                    city=result['city'],
                    state_name=result['state'],
                    country=result['country'],
                    zip_code=result['zip'])
                a.save_to_db()
                b = SupplierAddressModel(
                    supplier_id=data['supplier_id'],
                    address_id=a.address_id,
                    status='A',
                    created_by=userid.employee_id,
                    created_time=date_obj,
                    modified_time=date_obj)
                b.save_to_db()
                record4.modified_time = date_obj
                record4.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
        else:
            a = Address1Model(
                address_line1=result['addressLine1'],
                address_line2=result['addressLine2'],
                city=result['city'],
                state_name=result['state'],
                country=result['country'],
                zip_code=result['zip'])
            a.save_to_db()
            b = SupplierAddressModel(
                supplier_id=data['supplier_id'],
                address_id=a.address_id,
                status='A',
                created_by=userid.employee_id,
                created_time=date_obj,
                modified_time=date_obj)
            b.save_to_db()
            record4.modified_time = date_obj
            record4.save_to_db()
            logging.debug("Details Updated Sucessfully")
            return {"message": "Details Updated Sucessfully"}

class SupplierContactEdit(Resource):
    @jwt_required
    @prefix_decorator('')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        all_similar_phone = SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_phone=data['contact_phone']).all() 
        all_similar_email = SupplierContactModel.query.filter_by(supplier_id=data['supplier_id'],contact_email=data['contact_email'].lower()).all()
        if all_similar_phone:
            for i in all_similar_phone:
                if i.contact_id != data['contact_id']:
                    logging.debug("Already A Contact Exists With Same Contact Phone Number")
                    return {"erroeMessage":"Already A Contact Exists With Same Contact Phone Number"}
        if all_similar_email:
            for i in all_similar_email:
                if i.contact_id != data['contact_id']:
                    logging.debug("Already A Contact Exists With Same Email")
                    return {"erroeMessage":"Already A Contact Exists With Same Email"}
        data['contact_type'] = SupplierContactTypeModel.query.filter_by(supplier_contact_type=data['contact_type']).first()
        if data['contact_type']:
            data['contact_type'] = data['contact_type'].supplier_contact_type_id
        else:
            logging.debug('Invalid Contact Type')
            return {'errorMessage': 'Invalid Contact Type'}
        schema = SupplierContactSchema(unknown=INCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        record = SupplierContactModel.query.filter_by(contact_id=data['contact_id']).first()
        if record:
            # change the values you want to update
            record.contact_first_name = result['contact_first_name']
            record.contact_last_name = result['contact_last_name']
            record.supplier_contact_type_id = result['contact_type']
            record.contact_phone = result['contact_phone']
            record.contact_email = result['contact_email'].lower()
            record.contact_title = result['contact_title']
            # commit changes
            record.save_to_db()
            record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
            record.modified_time = date_obj
            record.save_to_db()
            logging.debug("Details Updated Successfully")
            return {"message": "Details Updated Sucessfully"}
        else:
            a = SupplierContactModel(
                supplier_id=result['supplier_id'],
                contact_first_name=result['contact_first_name'],
                contact_last_name=result['contact_last_name'],
                supplier_contact_type_id=result['contact_type'],
                contact_phone=result['contact_phone'],
                contact_email=result['contact_email'].lower(),
                contact_title=result['contact_title'])
            a.save_to_db()
            record = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
            record.modified_time = date_obj
            record.save_to_db()
            logging.debug("Details Updated Successfully")
            return {"message": "Details Updated Successfully"}

class SupplierContractEdit(Resource):
    @jwt_required
    @prefix_decorator('SupplierContractEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        schema = SupplierContractSchema(unknown=INCLUDE)
        supplier = schema.load(data)
        result = schema.dump(supplier)
        record = SupplierContractModel.query.filter_by(contract_id=result['contract_id']).first()
        if record:
            # change the values you want to update
            record.contract_name = result['contract_name']
            record.start_date = result['start_date']
            record.end_date = result['end_date']
            record.save_to_db()
            record1 = StaffingSupplierModel.query.filter_by(supplier_id=result['supplier_id']).first()
            record1.modified_time = datetime.now()
            record1.save_to_db()
            logging.debug("Supplier Contract Details Updated Successfully")
            return {"message": "Supplier Contract Details Updated Successfully"}
        else:
            logging.debug('Supplier Contract Does Not Exist')
            return {'errorMessage': 'Supplier Contract Does Not Exist'}

class ValidateConsultantNumber(Resource):
    @jwt_required
    @prefix_decorator('ValidateConsultantNumber')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if str(data['consultant_number']).lower().strip() not in [i.consultant_number.lower().strip() for i in ThirdPartyConsultantModel.query.all()]:
            logging.info("Success")
            return {'message':'Success'}
        else:
            logging.debug('Consultant With Same Number Already Exists')
            return {'errorMessage':'Consultant With Same Number Already Exists'}

class AddThirdPartyConsultant(Resource):
    @jwt_required
    @prefix_decorator('AddThirdPartyConsultant')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        if ThirdPartyConsultantModel.query.filter_by(consultant_number=data['consultant_number']).first():
            logging.debug("Consultant With Same Consultant Number Already Exists")
            return{"errorMessage":"Consultant With Same Consultant Number Already Exists"}
        conobj = ConsultantIdModel.query.filter_by(id=1).first()
        conid = conobj.consultant_id
        conidgen = 'CON' + str(conid)
        conobj.consultant_id = conid + 1
        conobj.save_to_db()
        try:
            try:
                data['work_auth'] = WorkAuthorizationListModel.query.filter_by(work_authorization_type=data['work_auth']).first().work_authorization_type_id  #work_authorization
                data['emergency_relation_id'] = EmergencyRelationModel.query.filter_by(emergency_relation_type=data['emergency_relation_id']).first().emergency_relation_id
                data['gender'] = 'M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O' if data['gender']=='Other' else ''
                data['status'] = 'A' if data['status']=='Active' else 'I' if data['status']=='Inactive' else 'T' if data['status']=='Terminated' else ''
            except Exception as e:
                logging.exception('Exception Occured , {0}'.format(str(e)))
                return {'errorMessage':'Please Provide Valid Data'}
            schema1 = ConsultantSchema(unknown=INCLUDE)
            consultant = schema1.load(data)
            result = schema1.dump(consultant)
            schema2 = ConsultantContactSchema(unknown=EXCLUDE)
            contact = schema2.load(data)
            result1 = schema2.dump(contact)
            tparty = ThirdPartyConsultantModel(
                consultant_id=conidgen,
                supplier_id=result['supplier_name'], #supplier_id
                consultant_number=data['consultant_number'],
                first_name=result['first_name'],
                middle_name=result['middle_name'],  
                last_name=result['last_name'],
                date_of_birth=base64.b64encode(result['date_of_birth'].encode('ascii')).decode('ascii'),
                ssn=base64.b64encode(result['ssn'].encode('ascii')).decode('ascii'),
                personal_cell_phone=result['mobile'],  #personal_cell_phone
                personal_email_id=result['email'].lower(),   #personal_email_id
                gender=data['gender'],
                work_authorization_type_id=data['work_auth'],
                work_authorization_expiry_date=result['work_auth_exp_date'],   #work_authorization_expiry_date
                consultant_status=result['status'],
                job_title=result['job_title'],
                worksite_location=result['worksite_location'],
                rehire_eligibility=result['rehire_eligibility'],
                recruiter_name=result['recruiter_name'],
                account_manager=result['account_manager'],
                created_time=date_obj,
                modified_time=date_obj,
                consultant_status_last_updtd_date=date_obj,
            )
            tparty.save_to_db()
            tpartyid = tparty.consultant_id
            conscon = ConsultantContactModel(
                consultant_id=tpartyid,
                emergency_first_name=result1['emergency_first_name'],
                emergency_middle_name=result1['emergency_middle_name'],
                emergency_last_name=result1['emergency_last_name'],
                emergency_phone_number=result1['emergency_mobile'],
                emergency_alternate_phone_number=result1['emergency_alternate_mobile'], 
                emergency_relation_id=result1['emergency_relation_id'], #emergency_relation_type
                emergency_email=result1['emergency_email'].lower()
            )
            conscon.save_to_db()
            return {
                "message": "Consultant details added successfully",
                "consultant_id": tparty.consultant_id
            }
        except ValidationError as err:	
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500

class ConsultantView(Resource):
    @jwt_required
    @prefix_decorator('ConsultantView')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        cons = ThirdPartyConsultantModel.query.filter_by(consultant_id=data['consultant_id']).first()
        conscon = ConsultantContactModel.query.filter_by(consultant_id=data['consultant_id']).first()
        proemp = ProjectEmployeeModel.return_all_empprojects(data['consultant_id'])
        if not cons:
            logging.debug("Consultant Does Not Exist")
            return {"errorMessage":"Consultant Does Not Exist"}
        if conscon:
            emergency_first_name = conscon.emergency_first_name
            emergency_middle_name = conscon.emergency_middle_name
            emergency_last_name = conscon.emergency_last_name
            emergency_phone_number = conscon.emergency_phone_number
            emergency_alternate_phone_number = conscon.emergency_alternate_phone_number
            emergency_relation_type = EmergencyRelationModel.query.filter_by(emergency_relation_id=conscon.emergency_relation_id).first().emergency_relation_type
            emergency_relation_id = conscon.emergency_relation_id
            emergency_email = conscon.emergency_email.lower()
            emergency_contact_id = conscon.emergency_contact_id
        else:
            emergency_first_name = ''
            emergency_middle_name = ''
            emergency_last_name = ''
            emergency_phone_number = ''
            emergency_alternate_phone_number = ''
            emergency_relation_type = ''
            emergency_relation_id = ''
            emergency_email = ''
            emergency_contact_id = ''
        return {'consultant_id': cons.consultant_id,
                'supplier_id': cons.supplier_id,
                'supplier_name': StaffingSupplierModel.query.filter_by(supplier_id=cons.supplier_id).first().supplier_name,
                'consultant_number': cons.consultant_number,
                'first_name': cons.first_name,
                'middle_name': cons.middle_name,
                'last_name': cons.last_name,
                'date_of_birth': cons.date_of_birth,
                'ssn': cons.ssn,
                'consultant_status_last_updated_date': str(cons.consultant_status_last_updtd_date),
                'personal_cell_phone': cons.personal_cell_phone,
                'personal_email_id': cons.personal_email_id.lower(),
                'gender': 'Male' if cons.gender=='M' else 'Female' if cons.gender=='F' else 'Other' if cons.gender=='O' else '',
                'work_authorization': WorkAuthorizationListModel.query.filter_by(work_authorization_type_id=cons.work_authorization_type_id).first().work_authorization_type,
                'work_authorization_expiry_date': str(cons.work_authorization_expiry_date),
                'consultant_status': 'Active' if cons.consultant_status=='A' else 'Inactive' if cons.consultant_status=='I' else 'Terminated' if cons.consultant_status=='T' else '',
                'job_title': cons.job_title,
                'recruiter_name': cons.recruiter_name,
                'worksite_location': cons.worksite_location,
                'rehire_eligibility': 'Yes' if cons.rehire_eligibility=='Y' else 'No' if cons.rehire_eligibility=='N' else '',
                'termination_reason': cons.termination_reason,
                'project_details': proemp['employee_projects'] if proemp else [],
                'emergency_first_name': emergency_first_name,
                'emergency_middle_name': emergency_middle_name,
                'emergency_last_name': emergency_last_name,
                'emergency_phone_number': emergency_phone_number,
                'emergency_alternate_phone_number': emergency_alternate_phone_number,
                'emergency_relation_type': emergency_relation_type,
                'emergency_relation_id': emergency_relation_id,
                'emergency_email': emergency_email.lower(),
                'account_manager': cons.account_manager,
                'emergency_contact_id' :emergency_contact_id
                }            

class ConsultantEdit(Resource):
    @jwt_required
    @prefix_decorator('ConsultantEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        try:
            data['supplier_name']=StaffingSupplierModel.query.filter_by(supplier_name=data['supplier_name']).first().supplier_id
            data['work_auth']=WorkAuthorizationListModel.query.filter_by(work_authorization_type=data['work_auth']).first().work_authorization_type_id
            data['gender'] = 'M' if data['gender']=='Male' else 'F' if data['gender']=='Female' else 'O'  if data['gender']=='Other' else ''
        except Exception as e:
            logging.exception('Exception Occured , {0}'.format(str(e)))
            return {'errorMessage':str(e)}
        try:
            schema = ConsultantSchema()
            consultant = schema.load(data)
            result = schema.dump(consultant)
            record = ThirdPartyConsultantModel.query.filter_by(consultant_id=result['consultant_id']).first()
            # change the values you want to update
            record.personal_email_id = result['email'].lower()
            record.first_name = result['first_name']
            record.gender = result['gender']
            record.last_name = result['last_name']
            record.middle_name = result['middle_name']
            record.personal_cell_phone = result['mobile']
            record.supplier_id = result['supplier_name']
            record.work_authorization_type_id = result['work_auth']
            record.work_authorization_expiry_date = result['work_auth_exp_date']
            record.modified_time = datetime.now()
            # commit changes
            record.save_to_db()
            logging.debug("Consultant Details Updated Sucessfully")
            return {"message": "Consultant Details Updated Sucessfully"}
        except ValidationError as err: 
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500

class ConsultantSchema(Schema):
    consultant_id = fields.String(validate=validate.Length(max=10),data_key="consultant_id",required=False)
    supplier_id = fields.Integer(data_key="supplier_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    middle_name = fields.String(validate=validate.Length(max=75),data_key="middle_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    last_name = fields.String(validate=validate.Length(max=75),data_key="last_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    first_name = fields.String(validate=validate.Length(max=75),data_key="first_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    personal_email_id = fields.Email(data_key="email",required=False,error_messages={"required": "Please Provide A Valid Email Id."})
    gender = fields.String(validate=validate.Length(max=1),data_key="gender",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    work_authorization_type_id = fields.Integer(data_key="work_auth",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    work_authorization_expiry_date = fields.Date(allow_none=True,format='%m-%d-%Y',data_key="work_auth_exp_date",required=False,error_messages={"required": "Please Provide A Valid Expiry Date."})
    personal_cell_phone = fields.String(validate=validate.Length(max=20),data_key="mobile",required=False,error_messages={"required": "Please Provide A Valid Phone Number."})
    consultant_status = fields.String(validate=validate.Length(max=1),data_key="status",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    job_title = fields.String(validate=validate.Length(max=150),data_key="job_title",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    worksite_location = fields.String(validate=validate.Length(max=60),data_key="worksite_location",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    rehire_eligibility = fields.String(validate=validate.Length(max=1),data_key="rehire_eligibility",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    termination_reason = fields.String(validate=validate.Length(max=255),data_key="termination_reason",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    account_manager = fields.String(validate=validate.Length(max=150),data_key="account_manager",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    ssn = fields.String(validate=validate.Length(max=20),data_key="ssn",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    date_of_birth = fields.String(allow_none=True,validate=validate.Length(max=20),data_key="date_of_birth",required=False,error_messages={"required": "Please Provide A Valid First Name."})
    recruiter_name = fields.String(validate=validate.Length(max=150),data_key="recruiter_name",required=False,error_messages={"required": "Please Provide A Valid First Name."})

    @post_load
    def save_employee(self, data, **kwargs):
        return data


class ConsultantEmploymentEdit(Resource):
    @jwt_required
    @prefix_decorator('ConsultantEmploymentEdit')
    def post(self):
        data = request.get_json(force=True)
        date_obj = datetime.now()
        print(data)
        data['status'] = 'A' if data['status']=='Active' else 'I' if data['status']=='Inactive' else 'T' if data['status']=='Terminated' else ''
        data['rehire_eligibility'] = 'Y' if data['rehire_eligibility']=='Y' else 'N' if data['rehire_eligibility']=='N' else ''
        try:
            schema = ConsultantSchema()
            consultant = schema.load(data)
            result = schema.dump(consultant)
            record1 = ThirdPartyConsultantModel.query.filter_by(consultant_id=result['consultant_id']).first()
            if not record1:
                logging.debug('Please Provide Valid Data')
                return {'errorMessage':'Please Provide Valid Data'}
            if record1.consultant_status != result['status']:
                last_update_date = date_obj
            else:
                last_update_date = record1.consultant_status_last_updtd_date
            if result['status'] == 'A':
                rehire_eligibility = ''
                termination_reason = ''
            else:
                rehire_eligibility = result['rehire_eligibility']
                termination_reason = result['termination_reason']
            record = ThirdPartyConsultantModel.query.filter_by(consultant_id=result['consultant_id']).first()
            # change the values you want to update
            record.consultant_status =  result['status']
            record.job_title = result['job_title']
            record.worksite_location = result['worksite_location']
            record.rehire_eligibility = rehire_eligibility
            record.termination_reason = termination_reason
            record.account_manager = result['account_manager']
            record.modified_time = date_obj
            record.consultant_status_last_updtd_date = last_update_date
            # commit changes
            record.save_to_db()
            logging.debug("Details Updated Sucessfully")
            if result['status'] == 'A':
                return {"message": "Details Updated Sucessfully"}
            elif result['status'] == 'I':
                return {"message": "Consultant Inactivated Sucessfully"}
            elif result['status'] == 'T':
                return {"message": "Consultant Terminated Sucessfully"}
        except ValidationError as err: 
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500

class ConsultantContactSchema(Schema):
    consultant_id = fields.String(validate=validate.Length(max=10),data_key="consultant_id",required=False)
    emergency_contact_id = fields.Integer(data_key="supplier_name",required=False)
    emergency_first_name = fields.String(validate=validate.Length(max=75),data_key="emergency_first_name",required=True)
    emergency_last_name = fields.String(validate=validate.Length(max=75),data_key="emergency_last_name",required=True)
    emergency_middle_name = fields.String(validate=validate.Length(max=75),data_key="emergency_middle_name",required=False)
    emergency_email = fields.Email(data_key="emergency_email",required=True)
    emergency_phone_number = fields.String(validate=validate.Length(max=20),data_key="emergency_mobile",required=True)
    emergency_alternate_phone_number = fields.String(validate=validate.Length(max=20),data_key="emergency_alternate_mobile",required=False)
    emergency_relation_id = fields.Integer(data_key="emergency_relation_id",required=True)

    @post_load
    def save_employee(self, data, **kwargs):
        return data


class ConsultantEmerEdit(Resource):
    @jwt_required
    @prefix_decorator('ConsultantEmerEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        data['emergency_relation_id'] = EmergencyRelationModel.query.filter_by(emergency_relation_type=data['emergency_relation_id']).first().emergency_relation_id
        if not data['emergency_relation_id']:
            logging.debug('Please Provide Valid Emergency Relation Type')
            return {'errorMessage':'Please Provide Valid Emergency Relation Type'}
        try:
            schema = ConsultantContactSchema()
            contact = schema.load(data)
            result = schema.dump(contact)
            record = ConsultantContactModel.query.filter_by(consultant_id=result['consultant_id']).first()
            record1 = ThirdPartyConsultantModel.query.filter_by(consultant_id=result['consultant_id']).first()
            if record:
                # change the values you want to update
                record.emergency_email = result['emergency_email'].lower()
                record.emergency_first_name = result['emergency_first_name']
                record.emergency_last_name = result['emergency_last_name']
                record.emergency_phone_number = result['emergency_mobile']
                record.emergency_relation_id = result['emergency_relation_id']
                record.emergency_alternate_phone_number = result['emergency_alternate_mobile']
                record.save_to_db()
                record1.modified_time = date_obj
                record1.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
            else:
                new_record = ConsultantContactModel(consultant_id=result['consultant_id'],
                                                 emergency_first_name=result['emergency_first_name'],
                                                 emergency_last_name=result['emergency_last_name'],
                                                 emergency_phone_number=result['emergency_mobile'],
                                                 emergency_alternate_phone_number=result['emergency_alternate_mobile'],
                                                 emergency_relation_id=result['emergency_relation'],
                                                 emergency_email=result['emergency_email'].lower())   
                new_record.save_to_db()
                record1.modified_time = date_obj
                record1.save_to_db()
                logging.debug("Details Updated Sucessfully")
                return {"message": "Details Updated Sucessfully"}
        except ValidationError as err: 
            logging.exception('Exception Occured, {0}'.format(err.messages))
            return {'errorMessage':err.messages},500

class BillRateEditSchema1(Schema):
    bill_rate = fields.Float(required=False)
    pay_rate = fields.Float(required=False)
    work_week_hours = fields.Integer(required=False)
    from_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=True)
    to_date = fields.Date(format='%m-%d-%Y',allow_none=True,required=True)
    resource_billable = fields.String(validate=validate.Length(max=1),required=False)

    @validates('work_week_hours')
    def validate_empNumber(self, work_week_hours):
        if work_week_hours>168:
            raise ValidationError('Work Week Hours is invalid')

    @post_load
    def save_address(self, data, **kwargs):
        return data

class BillRateEdit(Resource):
    @jwt_required
    @prefix_decorator('BillRateEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        current_emp = Employee1Model.query.filter_by(employee_id=data['employee_id']).first()
        if not ProjectModel.query.filter_by(project_id=data['project_id']).first():
            logging.debug("Project Does Not Exist")
            return {"errorMessage":"Project Does Not Exist"}
        if not Employee1Model.query.filter_by(employee_id=data['employee_id']).first():
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}
        record = EmployeeBillRateModel.find_by_emp_pro(data['employee_id'], data['project_id'])
        if current_emp:
            data['resource_billable'] = 'Y' if data['resource_billable']=='Yes' else 'N' if data['resource_billable']=='No' else ''
            try:
                schema = BillRateEditSchema1(unknown=INCLUDE)
                address = schema.load(data)
                result = schema.dump(address)
                if record:
                    # change the values you want to update
                    record.bill_rate = result['bill_rate']
                    record.pay_rate = result['pay_rate']
                    record.work_week_hours = result['work_week_hours']
                    record.from_date = result['from_date']
                    record.to_date = result['to_date']
                    record.resource_billable = result['resource_billable']
                    # commit changes
                    record.save_to_db()
                    current_emp.modified_time = date_obj
                    current_emp.save_to_db()
                    logging.debug("Details Updated Sucessfully")
                    return {"message": "Details Updated Sucessfully"}
                else:
                    b = EmployeeBillRateModel(
                        employee_id=data['employee_id'],
                        project_id=data['project_id'],
                        bill_rate=result['bill_rate'],
                        pay_rate=result['pay_rate'],
                        work_week_hours=result['work_week_hours'],
                        from_date=result['from_date'],
                        to_date=result['to_date'],
                        resource_billable=result['resource_billable']
                    )
                    b.save_to_db()
                    current_emp.modified_time = date_obj
                    current_emp.save_to_db()
                    logging.debug("Details Updated Successfully")
                    return {"message": "Details Updated Successfully"}
            except ValidationError as err:
                logging.exception('Exception Occured, {0}'.format(err.messages))
                return {"errorMessage":err.messages},500
        else:
            logging.debug("Employee Does Not Exist")
            return {"errorMessage":"Employee Does Not Exist"}

class CobraEditSchema(Schema):
    start_date = fields.Date(validate=validate.Length(max=255),required=False)
    end_date = fields.Date(validate=validate.Length(max=255),required=False)
    employee_number = fields.String(validate=validate.Length(max=70),required=False)
    provider_name = fields.String(validate=validate.Length(max=60),required=False)
    amount_covered = fields.String(validate=validate.Length(max=15),required=False)
    coverage_details = fields.String(validate=validate.Length(max=60),required=False)

    @post_load
    def create_person(self, data, **kwargs):
        return data


class CobraEdit(Resource):
    @jwt_required
    @prefix_decorator('CobraEdit')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        current_emp = Employee1Model.find_by_employeenumber(data['employee_number'])
        record = CobraModel.find_by_employeenumber(current_emp.employee_id)
        if record:
            record.start_date = data['start_date']
            record.end_date = data['end_date']
            record.provider_name = data['provider_name']
            record.amount_covered = data['amount_covered']
            record.coverage_details = data['coverage_details']
            record.save_to_db()
            current_emp.modified_time = date_obj
            current_emp.save_to_db()
            logging.debug("COBRA Details Updated Successfully")
            return {"message": "COBRA Details Updated Successfully"}
        else:
            b = CobraModel(
                employee_id=current_emp.employee_id,
                start_date=data['start_date'],
                end_date=data['end_date'],
                provider_name=data['provider_name'],
                amount_covered=data['amount_covered'],
                coverage_details=data['coverage_details']
            )
            a.save_to_db()
            current_emp.modified_time = date_obj
            current_emp.save_to_db()
            logging.debug("COBRA Details Updated Successfully")
            return {"message": "COBRA Details Updated Successfully"}

class EmployeeProjects(Resource):
    @jwt_required
    @prefix_decorator('EmployeeProjects')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        emp = Employee1Model.query.filter_by(email_id=data['emailId'].lower()).first()
        if not emp:
            logging.debug("Employee Does Not Exists")
            return {'errorMessage':"Employee Does Not Exists"}
        return ProjectEmployeeModel.return_all_employeeprojects(emp.employee_id)

class ViewInternalTimesheet(Resource):
    @prefix_decorator('ViewInternalTimesheet')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        emp = Employee1Model.query.filter_by(email_id=data['emailId'].lower()).first()
        if not emp:
            logging.debug("Employee Does Not Exists")
            return {'errorMessage':"Employee Does Not Exists"}
        try:
            datetime.strptime(data['startDate'].split('T')[0],"%m-%d-%Y")
            datetime.strptime(data['endDate'].split('T')[0],"%m-%d-%Y")
            return InternalTimesheetModel.return_time_sheets(emp.employee_id,data['startDate'],data['endDate'])
        except:	 
            logging.exception('Exception Occured')
            return {'errorMessage':"Please Provide Valid Start Date/End Date"}

class InternalProjectTimeSheetFilter(Resource):
    @jwt_required
    @prefix_decorator('InternalProjectTimeSheetFilter')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['date'] == '' and (data['project_id'] == '') and (data['emp_num'] ==''):
            username = get_jwt_identity() 
            emp_id = Employee1Model.query.filter_by(email_id=username).first().employee_id   
            pro_manager = ProjectModel.query.filter_by(project_manager_id=emp_id).first()
            repo_manager = Employee1Model.query.filter_by(reporting_manager_id=emp_id).first()
            if pro_manager and repo_manager:
                print('entered1')
                projects = [i.project_id for i in ProjectModel.query.filter_by(project_manager_id=emp_id).all()]
                list_pro_ts = [InternalTimesheetModel.return_all_by_pro(i,emp_id) for i in projects]
                list_pro_ts = list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_pro_ts]))))
                reporting_employees = list(set([i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id).all()]))
                list_rep_ts = InternalTimesheetModel.return_all_reporting_manager(reporting_employees,emp_id) 
                return {'timesheets': list({tuple(d.values()): d for d in list_pro_ts+list_rep_ts['timesheets']}.values())}
            elif pro_manager:
                pid = ProjectModel.query.filter_by(project_manager_id=emp_id).all()
                list_ts = [InternalTimesheetModel.return_all_by_pro(i.project_id,emp_id) for i in pid]
                ts = {"timesheets": list(filter((None).__ne__, list(itertools.chain.from_iterable([j["timeSheets"] for j in list_ts])))) }
                return ts
            elif repo_manager:
                employees = [i.employee_id for i in Employee1Model.query.filter_by(reporting_manager_id=emp_id).all()]
                return InternalTimesheetModel.return_all_reporting_manager(employees,emp_id) 
            else:
                return {"timesheets":[]}
        else:
            if data['date']:  
                try:
                    datetime.strptime(data['date'][0].split('T')[0],"%m-%d-%Y")
                    datetime.strptime(data['date'][1].split('T')[0],"%m-%d-%Y")
                    return InternalTimesheetModel.return_all_by_man_ts(**data)
                except:
                    logging.debug('Please Provide Valid Date')
                    return{'errorMessage':'Please Provide Valid Date'}
            else:
                return InternalTimesheetModel.return_all_by_man_ts(**data)
 

class HrItsDeptEmpFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsDeptEmpFilter')
    def post(self):
        data = request.get_json(force=True)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        print(data)
        employees = Employee1Model.query.filter_by(department_id=data['department_id']).all()
        employee_list = [str(i.employee_id) for i in employees]
        return Employee1Model.return_all_emps(employee_list,userid.employee_id)

class SaveInternalTImesheet(Resource):
    @prefix_decorator('SaveInternalTImesheet')
    def post(self):
        data = request.get_json(force=True) 
        print(data)  
        date_obj = datetime.now()
        for item in data:
            if item['tsStartTime'] == '00:00 am':
                hours = 0.00 
            else:
                hours = round(((datetime.strptime(item['tsEndTime'], '%I:%M %p')-datetime.strptime(item['tsStartTime'], '%I:%M %p')).total_seconds()/60.0)/60.0,2)
            emp_id = Employee1Model.query.filter_by(email_id=item['emailId'].lower()).first()
            if item['timeSheetId'] != 0:
                timesheet_obj = InternalTimesheetModel.query.filter_by(timesheet_id=item['timeSheetId']).first()
                timesheet_obj.timesheet_type_id = InternalTimesheetTypesModel.query.filter_by(timesheet_type=item['tsType']).first().timesheet_type_id
                timesheet_obj.ts_day = datetime.strptime(item['tsDate'], '%m-%d-%Y').strftime('%A')
                timesheet_obj.ts_date = item['tsDate']
                timesheet_obj.ts_start_time = item['tsStartTime']
                timesheet_obj.ts_end_time = item['tsEndTime']
                timesheet_obj.total_hours = hours
                timesheet_obj.project_id = item['projectId']
                timesheet_obj.remarks = item['remarks']
                timesheet_obj.last_updated_time_stamp = date_obj
                timesheet_obj.save_to_db()
            else:
                if item['projectId']:
                    project_id = item['projectId']
                else:
                    project_id = None	
                timesheet_obj = InternalTimesheetModel(
                employee_number = emp_id.employee_id,
                timesheet_type_id = InternalTimesheetTypesModel.query.filter_by(timesheet_type=item['tsType']).first().timesheet_type_id,
                ts_day = datetime.strptime(item['tsDate'], '%m-%d-%Y').strftime('%A'),
                ts_date = item['tsDate'],
                ts_start_time = item['tsStartTime'],
                ts_end_time = item['tsEndTime'],
                total_hours = hours,
                project_id = project_id,
                remarks = item['remarks'],    
                status = 'S',
                created_time_stamp = date_obj,
                department_id=emp_id.department_id
                )
                timesheet_obj.save_to_db()
        logging.info("TimeSheets Saved Successfully")
        return {"message": "TimeSheets Saved Successfully"}

class InternalTimeSheetApproval(Resource):
    @jwt_required
    @prefix_decorator('InternalTimeSheetApproval')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        user_id = Employee1Model.query.filter_by(email_id=username).first().employee_id
        for i in data:
            if not i['timesheetId'] or i['timesheetId'] == 'undefined':
                return {"errorMessage": "Please Provide Valid Timesheet"}
            record = InternalTimesheetModel.query.filter_by(timesheet_id=i['timesheetId']).first()
            if record:
                emp = Employee1Model.query.filter_by(employee_id=record.employee_number).first()
                if not emp:
                    logging.debug('Employee for provided TimeSheet Does Not Exists')
                    return {'errorMessage':'Employee for provided TimeSheet Does Not Exists'}
        for item in data:
            record = InternalTimesheetModel.query.filter_by(timesheet_id=item['timesheetId']).first()
            record.status = 'A' if item["timesheetApprovalStatus"]=='Approved' else 'R' if item["timesheetApprovalStatus"]=='Rejected' else ''
            record.approver_comments = item["comments"]
            record.approver_id = user_id
            record.approved_time_stamp = datetime.now()
            record.save_to_db()
            emp = Employee1Model.query.filter_by(employee_id=record.employee_number).first()
            emp_email = emp.email_id
            emp_name = emp.first_name + ' ' + emp.last_name
            project = ProjectModel.query.filter_by(project_id=record.project_id).first()
            project_name = project.project_name if project else ''
            day = str(record.ts_date)
            if item["timesheetApprovalStatus"] != "Approved":
                msg = Message('Timesheet Status Notification', sender='support@prutech.com', recipients=[emp_email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Your timesheet for project <b>{1}</b> on <b>{2}</b>
                <br> has been <b>{3}</b> due to {4} </br>
                <br> <b>Thanks,
                <br> PRUTECH Team.
                </b>""".format(emp_name, project_name, day, 'Rejected', item["comments"])
                mail.send(msg)
            else:
                msg = Message('Timesheet Status Notification', sender='support@prutech.com', recipients=[emp_email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Your timesheet for project <b>{1}</b> on <b>{2}</b>
                <br> has been <b>{3}</b> </br>
                <br> <b>Thanks,
                <br> PRUTECH Team.
                </b>""".format(emp_name, project_name, day, 'Approved')
                mail.send(msg)
        return {"message": item["timesheetApprovalStatus"]}

class Deletecertifcations(Resource):
    @prefix_decorator('Deletecertifcations')
    def post(self): 
        data = request.get_json(force=True)
        print(data)
        certif = CertificationsModel.query.filter_by(employee_certification_id=data['certification_id']).first()
        if certif:
            certif.status = 'I'
            certif.save_to_db()
            logging.info("Certification Deleted Successfully")
            return {"message":"Certification Deleted Successfully"}
        else:
            logging.debug("Certification Does Not Exist")
            return {"message":"Certification Does Not Exist"}

class DeleteTimesheets(Resource):
    @prefix_decorator('DeleteTimesheets')
    def delete(self): 
        data = request.get_json(force=True)
        print(data)
        time_sheet = InternalTimesheetModel.query.filter_by(timesheet_id=data['timesheetId']).first()
        if time_sheet:
            time_sheet.delete_from_db()
            return {"message":"Timesheet Deleted Successfully"}
        else: 
            logging.exception('Please Provide Valid Timesheet Id')
            return {"errorMessage":"Please Provide Valid Timesheet Id"}

class ActivateEmployee(Resource):
    @jwt_required
    @prefix_decorator('ActivateEmployee')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = Employee1Model.query.filter_by(employee_id=data['employee_id']).first()
        if record:    
            record.employee_status = 'A'
            record.inactive_reason_id = None
            record.employee_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Employee Activated Successfully")
            return {"Message": "Employee Activated Successfully"}
        else:
            logging.debug('Employee Does Not Exist')
            return {'errorMessage': 'Employee Does Not Exist'}

class ActivateClient(Resource):
    @jwt_required
    @prefix_decorator('ActivateClient')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = ClientDetailsModel.query.filter_by(client_id=data['client_id']).first()
        if record:
            record.client_status = 'A'
            record.client_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Client Activated Successfully")
            return {"Message": "Client Activated Successfully"}
        else:
            logging.debug('Client Does Not Exist')
            return {'errorMessage': 'Client Does Not Exist'}

class ActivateProject(Resource):
    @jwt_required
    @prefix_decorator('ActivateProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = ProjectModel.query.filter_by(project_id=data['project_id']).first()
        if record:    
            record.project_status = 'A'
            record.project_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Project Activated Successfully")
            return {"Message": "Project Activated Successfully"}
        else:
            logging.debug('Project Does Not Exist')
            return {'errorMessage': 'Project Does Not Exist'}

class ActivateSupplier(Resource):
    @jwt_required
    @prefix_decorator('ActivateSupplier')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
        if record:    
            record.contract_status = 'A'
            record.modified_time = date_obj
            record.supplier_status_last_updtd_date = date_obj
            record.save_to_db()
            logging.info("Supplier Activated Successfully")
            return {"Message": "Supplier Activated Successfully"}
        else:
            logging.debug('Supplier Does Not Exist')
            return {'errorMessage': 'Supplier Does Not Exist'}

class ActivateConsultant(Resource):
    @jwt_required
    @prefix_decorator('ActivateConsultant')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        record = ThirdPartyConsultantModel.query.filter_by(consultant_id=data['consultant_id']).first()
        if record:
            record.consultant_status = 'A'
            record.consultant_status_last_updtd_date = date_obj
            record.modified_time = date_obj
            record.save_to_db()
            logging.info("Consultant Activated Successfully")
            return {"Message": "Consultant Activated Successfully"}
        else:
            logging.debug("Consultant Does not Exists")
            return {"errorMessage":"Consultant Does not Exists"}     


class DesignationValidation(Resource):
    @prefix_decorator('DesignationValidation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        date_obj = datetime.now()
        dept = DepartmentModel.query.filter_by(department_name=data['employee_department']).first()
        desg = DesignationModel.query.filter_by(designation_name=data['employee_designation']).first()
        email = data['email'].lower()
        if not dept:
            logging.debug('Please Select Valid Department')
            return {'errorMessage': 'Please Select Valid Department'}
        if not desg:
            logging.debug('Please Select Valid Designation')
            return {'errorMessage': 'Please Select Valid Designation'}
        if Employee1Model.query.filter_by(emp_number=data['employeeNumber']).first():
            logging.debug("Employee number already exists")
            return {"message": "Employee number already exists"}
        elif Employee1Model.query.filter_by(employee_status="Active", email_id=email).first():
            logging.debug("An active user already exists with same email")
            return {"message": "An active user already exists with same email"}
        elif EmployeeInviteStatus.query.filter_by(invitation_status="Active", employee_number=data['employeeNumber'],employee_email=email).first():
            logging.debug("Employee with this Employee Number/Email Id already exists")
            return {"message": "Employee with this Employee Number/Email Id already exists"}
        elif (EmployeeInviteStatus.query.filter_by(employee_number=data['employeeNumber']).first()):
            logging.debug("Employee with this Employee Number already exists")
            return {"message": "Employee with this Employee Number already exists"}
        elif (EmployeeInviteStatus.query.filter_by(employee_email=email).first()):
            if Employee1Model.query.filter_by(email_id=email).first():
                logging.debug("Employee with this email Id already exists")
                return {"message": "Employee already exists"}
            else:
                record = EmployeeInviteStatus.query.filter_by(employee_email=email).first()
                record.employee_number=data['employeeNumber']
                record.designation_id=desg.designation_id
                record.department_id=dept.department_id
                record.invitation_status="Active"
                record.employee_role=data['role']
                record.sent_time=date_obj.strftime('%m-%d-%Y')
                record.save_to_db()
                logging.info("Successfully saved")
                return {"message":"Successfully saved"}
        else:
            invite_record = EmployeeInviteStatus(employee_number=data['employeeNumber'],employee_email=email,designation_id=desg.designation_id,department_id=dept.department_id ,invitation_status="Active",employee_role=data['role'],sent_time=date_obj.strftime('%m-%d-%Y'))
            invite_record.save_to_db()
            logging.info("Successfully saved")
            return {"message":"Successfully saved"}

class EmpEmerInactivation(Resource):
    @jwt_required
    @prefix_decorator('EmpEmerInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = EmployeeContactModel.query.filter_by(contact_id=data['contact_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Employee Emergency Contact Deleted Successfully")
            return {"message":"Employee Emergency Contact Deleted Successfully"}
        else:
            logging.debug('Employee Emergency Contact Does Not Exist')
            return {'errorMessage': 'Employee Emergency Contact Does Not Exist'}

class EmpDepInactivation(Resource):
    @jwt_required
    @prefix_decorator('EmpDepInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = DependentContactModel.query.filter_by(contact_id=data['contact_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Employee Dependent Deleted Successfully")
            return {"message":"Employee Dependent Deleted Successfully"}
        else:
            logging.debug('Employee Dependent Contact Does Not Exist')
            return {'errorMessage': 'Employee Dependent Contact Does Not Exist'}

class SuppAddInactivation(Resource):
    @jwt_required
    @prefix_decorator('SuppAddInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = SupplierAddressModel.query.filter_by(address_id=data['supplier_address_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Supplier Address Deleted Successfully")
            return {"message":"Supplier Address Deleted Successfully"}
        else:
            logging.debug('Supplier Address Does Not Exist')
            return {'errorMessage': 'Supplier Address Does Not Exist'}

class ClientContactInactivation(Resource):
    @jwt_required
    @prefix_decorator('ClientContactInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = ClientContactModel.query.filter_by(contact_id=data['contact_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Client Contact Deleted Successfully")
            return {"message":"Client Contact Deleted Successfully"}
        else:
            logging.debug('Client Contact Does Not Exist')
            return {'errorMessage': 'Client Contact Does Not Exist'}

class ClinetContractInactivation(Resource):
    @jwt_required
    @prefix_decorator('ClinetContractInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = ClientVendorshipModel.query.filter_by(contract_id=data['contract_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            return {"message":"Client Contract Deleted Successfully"} 
        else:
            return {'errorMessage': 'Client Contract Does Not Exist'}

class SupplierContractInactivation(Resource):
    @jwt_required
    @prefix_decorator('SupplierContractInactivation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        contact = SupplierContractModel.query.filter_by(contract_id=data['supplier_contract_id']).first()
        if contact:
            contact.status = 'I'
            contact.modified_by = userid.employee_id
            contact.modified_time = datetime.now()
            contact.save_to_db()
            logging.info("Supplier Contract Deleted Successfully")
            return {"message":"Supplier Contract Deleted Successfully"}  
        else:
            logging.debug('Supplier Contract Does Not Exist')
            return {'errorMessage': 'Supplier Contract Does Not Exist'}

class EmployeeListFilter(Resource):
    @jwt_required
    @prefix_decorator('EmployeeListFilter')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        if data['date'] != '':  
            datetime.strptime(data['date'][0],"%m-%d-%Y")
            datetime.strptime(data['date'][1],"%m-%d-%Y")
            min_date = data['date'][0].split('T')[0]
            max_date = data['date'][1].split('T')[0]  
        else:
            min_date = '01-01-1977'
            max_date = datetime.now().strftime('%m-%d-%Y')
        return Employee1Model.return_all_for_date_filter(start_date=min_date ,end_date=max_date)

class ModifyPrimaryProject(Resource):
    @jwt_required
    @prefix_decorator('ModifyPrimaryProject')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity() 
        date_obj = datetime.now()
        user_id = Employee1Model.query.filter_by(email_id=username).first().employee_id
        if user_id:
            record = EmployeePrimaryProjectModel.query.filter_by(employee_id=data['employee_id']).first()
            if record:
                record.project_id = data['project_id']
                record.modified_by = user_id
                record.modiified_time = date_obj
                record.save_to_db()
                logging.info("Primary Project Updated Successfully")
                return {"message":"Primary Project Updated Successfully"}
            else:
                primepro = EmployeePrimaryProjectModel(
                    employee_id = data['employee_id'],
                    project_id = data['project_id'],
                    created_by = user_id,
                    created_time = date_obj
                    )
                primepro.save_to_db()
                logging.info("Primary Project Added Successfully")
                return {"message":"Primary Project Added Successfully"}
        else:
            logging.debug("User Does Not Exist")
            return{"errorMessage":"User Does Not Exist"}

class AttorneySchema(Schema):
    attorney_id = fields.Integer(data_key="attorney_id",required=False)
    attorney_name = fields.String(validate=validate.Length(max=255),data_key="attorney_name",required=False)

    @post_load
    def save_employee(self, data, **kwargs):
        return data

class LoginInvitation(Resource):
    @jwt_required
    @prefix_decorator('LoginInvitation')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        record = UserModel.query.filter_by(username=email).first()    
        if record:
            emp = Employee1Model.query.filter_by(employee_id=record.userid).first()
            if emp:                    
                msg = Message('Prutech Login Credentials', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br>Welcome to PruTech Solutions. Your Login ID & the URL are given below.</br>
                <br>UserId: {1}</br>
                <p>To Login you need a password and a temporary password will be sent to you shortly in a separate email. </p>   
                <br> Thank You,
                <br> Prutech Solutions Inc.
                """.format(emp.first_name+' '+emp.last_name,email)
                mail.send(msg)
                msg = Message('Temporary Password', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br>Please find the below temporary password. Kindly reset your password to access your Prutech Solutions account.</br>
                <br>Temporary Password: {1}
                <p><a href={2}>Click Here</a> to Login </p><br>
                <br>Note: Your account can be accessed only after your password is reset.</br>
                <br> Thank You,
                <br> Prutech Solutions Inc.
                """.format(emp.first_name+' '+emp.last_name,data['password'],data['base_url'])
                time.sleep(10)
                mail.send(msg)
                logging.info( "Login Details Sent Successfully")
                return {"message": "Login Details Sent Successfully"}
            else:
                logging.debug('Employee Does Not Exist')
                return {'errorMessage': 'Employee Does Not Exist'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage': 'User Does Not Exist'}

class AddRoleWithPrivileges(Resource):
    @jwt_required
    @prefix_decorator('AddRoleWithPrivileges')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()
        for i in RoleModel.query.all():
            if i.role_name.lower()==data['role_name'].lower():
                logging.debug('Role {} exists'.format(data['role_name']))
                return {'errorMessage': 'Role {} exists'.format(data['role_name'])}
        new_role = RoleModel(role_name=data['role_name'])
        new_role.save_to_db()
        privileges = [PrivilegeModel.query.filter_by(privilege_name=i).first().id for i in data['role_privileges']]
        role_privileges = RolePrivilegeModel(
            role_id=new_role.id,
            role_privileges=privileges,
        )
        role_privileges.save_to_db()
        logging.info('Role Added Successfully')
        return {'message': 'Role Added Successfully'}

class LoginEmail(Resource):
    @prefix_decorator('LoginEmail')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        email = data['username'].lower()
        record = UserModel.query.filter_by(username=email).first()
        if record:
            emp = Employee1Model.query.filter_by(employee_id=record.userid).first()
            if emp:
                msg = Message('Prutech Login link', sender='support@prutech.com', recipients=[email])
                msg.html = """Hi <b>{0}</b>,<br>
                <br> Greetings from PRUTECH Team!</br>
                <br> Your Registration is successful</br>
                <p> You can login to PRUTECH HR Portal with following link</p>
                <p>  <a href={1}>Login Here</a></p>   
                <br> Thank You,
                <br> Prutech Solutions Inc.
                """.format(emp.first_name.title()+" "+emp.last_name.title(),data['base_url'])
                mail.send(msg)
                logging.info("Mail Sent Successfully")
                return {"message": "Mail Sent Successfully"}
            else:
                logging.debug('Employee Does Not Exist')
                return {'errorMessage': 'Employee Does Not Exist'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage': 'User Does Not Exist'}
		
class ValidateRole(Resource):
    @jwt_required
    @prefix_decorator('ValidateRole')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        for i in RoleModel.query.all():
            if i.role_name.lower()==data['role_name'].lower():
                return {'errorMessage': 'Role {} exists'.format(data['role_name'])}
        logging.info("Success")
        return {'message':'Success'}

class SupplierContactInactive(Resource):
    @jwt_required
    @prefix_decorator('SupplierContactInactive')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()       
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if userid:
            sup = SupplierContactModel.query.filter_by(contact_id=data['contact_id']).first()
            record = StaffingSupplierModel.query.filter_by(supplier_id=data['supplier_id']).first()
            if sup and record:
                sup.contact_status = 'I'
                sup.save_to_db()
                record.modified_time = datetime.now()
                record.modified_by = userid.employee_id
                record.save_to_db()
                logging.info("Success")
                return {'message':'Contact Deleted Successfully'}
            else:
                logging.debug('Supplier Contact Does Not Exist')
                return {'errorMessage':'Supplier Contact Does Not Exist'}
        else:
            logging.debug('User Does Not Exist')
            return {'errorMessage':'User Does Not Exist'}

# to view employee timesheets

class HrItsProjectFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsProjectFilter')
    def get(self):
        return ProjectModel.return_all()

class HrItsCombinedFilter(Resource):
    @jwt_required
    @prefix_decorator('HrItsCombinedFilter')
    def post(self):
        data = request.get_json(force=True)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        print(data)
        if not ProjectModel.query.filter_by(project_id=data['project_id']).first():
            logging.debug("Project Does Not Exist")
            return {"errorMessage":"Project Does Not Exist"} 
        employees_list = ProjectEmployeeModel.query.filter_by(project_id=data['project_id']).first()
        if employees_list:
            return Employee1Model.return_all_emps(employees_list.employee_id,userid.employee_id)
        else:
            logging.debug("No Employees Assigned To The Project")
            return {'errorMessage':"No Employees Assigned To The Project"}

class MyTimeSheetProjectDropDown(Resource):
    @jwt_required    
    @prefix_decorator('MyTimeSheetProjectDropDown')
    def get(self):
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("Employee Does not Exist")
            return {"errorMessage":"Employee Does not Exist"}
        return ProjectEmployeeModel.all_employee_projects(userid.employee_id)          

class MyTimeSheets(Resource):
    @jwt_required    
    @prefix_decorator('MyTimeSheets')
    def get(self):
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            logging.debug("Employee Does not Exist")
            return {"errorMessage":"Employee Does not Exist"}
        return InternalTimesheetModel.all_employee_timesheets(userid.employee_id)           

class MyTimesheetsFilter(Resource):
    @jwt_required  
    @prefix_decorator('MyTimesheetsFilter')
    def post(self):
        data = request.get_json(force=True)  
        print(data)
        username = get_jwt_identity()
        userid = Employee1Model.query.filter_by(email_id=username).first()
        if not userid:
            return {"errorMessage":"User Does Not Exist"}
        if not data['date'] and not data['project_id']:
            return InternalTimesheetModel.all_employee_timesheets(userid.employee_id)
        if data['project_id'] and not data['date']:
            return InternalTimesheetModel.filtered_employee_timesheets(userid.employee_id,data['project_id'],data['date'])
        try:
            datetime.strptime(data['date'][0],"%m-%d-%Y")
            datetime.strptime(data['date'][1],"%m-%d-%Y")
        except:	
            logging.exception('Exception Occured')
            return {"errorMessage":"Invalid Date"}   
        return InternalTimesheetModel.filtered_employee_timesheets(userid.employee_id,data['project_id'],data['date'])

class AllTimeSheetProjectDropDown(Resource):
    @jwt_required  
    @prefix_decorator('AllTimeSheetProjectDropDown')  
    def get(self):
        return ProjectModel.return_all_active_projects()     


#to get all timesheets except theirs
class AllTimesheet(Resource):
    @jwt_required
    @prefix_decorator('AllTimesheet')
    def get(self):      
        username = get_jwt_identity()        
        user = UserModel.query.filter_by(username=username).first()
        if not user:
            return {"errorMessage":"User Does Not Exist"}
        return InternalTimesheetModel.return_all_time_sheets(user.userid)

class AllInternalTimeSheetFilter(Resource):
    @jwt_required
    @prefix_decorator('AllInternalTimeSheetFilter')
    def post(self):
        data = request.get_json(force=True)
        print(data)
        username = get_jwt_identity()        
        user = UserModel.query.filter_by(username=username).first()
        if not user:
            logging.debug("User Does Not Exist")
            return {"errorMessage":"User Does Not Exist"}
        if not data['project_id'] and not data['dept_id'] and not data['date'] and not data['emp_num']:
            return InternalTimesheetModel.return_all_time_sheets(user.userid)
        return InternalTimesheetModel.return_all_by_all_ts(data['emp_num'],data['project_id'],data['date'],data['dept_id'],user.userid)

class UpdatePaidTimeOff(Resource):
    @jwt_required  
    @prefix_decorator('UpdatePaidTimeOff')
    def post(self):
        data = request.get_json(force=True)  
        print(data)
        employee_obj = Employee1Model.query.filter_by(emp_number=data['employee_number']).first() 
        if not employee_obj:
            return {"errorMessage":"Employee With {} Employee Number Does Not Exist".format(data['employee_number'])}
        employee_obj.eligible_sick_hours = data['sick']
        employee_obj.eligible_vacation_weeks = data['vacationWeek']
        employee_obj.paid_holidays = True if data['paidHolidays'] == 'Yes' else False
        employee_obj.save_to_db()
        return {"message":"Employee Paid Time Off Deatils Updated Successfully"}   

class HRDashboardFT(Resource):
    @prefix_decorator('HRDashboardFT')
    def get(self): 
        present_date = datetime.now().strftime("%m-%d-%Y")
        date = present_date.split('-')[0] + '-' +'01'+ '-' + present_date.split('-')[-1]
        logging.info("Success")
        return Employee1Model.return_all_for_future_terminations(present_date)



class TerminatedEmpList(Resource):
    @jwt_required  
    @prefix_decorator('TerminationsCount')
    def get(self):  
        todays_date = datetime.now().date()
        year_start_date = '{}-01-01'.format(str(todays_date.year))
        return Employee1Model.return_all_terminated_employees(year_start_date,str(todays_date))
        #terminated_employees = Employee1Model.query.filter(Employee1Model.termination_date>=year_start_date,Employee1Model.termination_date<=str(todays_date)).all()
        #for i in terminated_employees:
            #print(i.first_name)  
        #return {"terminated_employees_count":terminated_employees_count,'year':str(todays_date.year)}

