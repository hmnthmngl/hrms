from run import db
import itertools
import datetime
from models import Employee1Model,ProjectEmployeeModel,DashboardStats

total_employees = Employee1Model.query.filter_by(employee_status='A').count()
billable = Employee1Model.query.filter(Employee1Model.billable_resource=='Y',Employee1Model.employee_status=='A').count()
non_billable = total_employees-billable
on_billing = len(set(list(itertools.chain.from_iterable([i.employee_id for i in ProjectEmployeeModel.query.all()]))))
on_bench = billable-on_billing

record = DashboardStats(date=datetime.date.today(),billable=billable,on_billing=on_billing,on_bench=on_bench,non_billing=non_billable,total_employees=total_employees)
record.save_to_db()


