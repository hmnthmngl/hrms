from run import db
from models import PrivilegeModel
from sqlalchemy import text


db.create_all()

sql1 = text("insert into users (username,password,reset_password) values('naidu@prutech.com','$pbkdf2-sha256$29000$GWNsjTEmRIhRihECYMzZuw$Kw8psJwSat5NQpBroOvAd70lsZEG5s0Q8fRE3dXG1BQ','False');")
sql2 = text("""insert into employee (first_name,middle_name,last_name,email_id,date_of_birth,ssn,gender,primary_phone_number,
marital_status,employment_start_date,employee_status,department_id,designation_id,emp_type_id,
emp_number,present_address_id,permanent_address_id,work_address_id,us_citizen,billable_resource,same_as_ca
) values('Naidu','N','Guttapalle','naidu@prutech.com','MDctMDEtMTk1NA==','MTIzLTEyLTEyMzQ=','M',
'(732)-725-9966','M','2021-04-20','A',2,5,1,
500000,1,2,3,'False','N','True');""")
sql3 = text("""insert into address (address_line1,city,state_name,country,zip_code) values
('38 N Juliet Street','Akutan','Alaska','United States',08830),
('38 N Juliet Street','Iselin','New Jersey','United States',08830),
('555 US Highway 1 South Suite #230','Iselin','New Jersey','United States',08830);""")
sql4 = text("insert into user_roles (user_id,roles) values(1,'{2}');")
sql5 = text("""insert into invite_status (employee_number,employee_email,invitation_status,sent_time,designation_id,department_id,billable_resource,employee_role,joining_date) values
(500000,'naidu@prutech.com','Inactive',CURRENT_DATE,5,2,'N','Executive',CURRENT_DATE);""")
db.engine.execute(sql5)
db.engine.execute(sql1)
db.engine.execute(sql3)
db.engine.execute(sql4)
db.engine.execute(sql2)
sql0 = text("insert into consultant_id_gen (consultant_id) values (1)")
db.engine.execute(sql0)    


