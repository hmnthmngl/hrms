from run import db
from run import ma
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey   
# from sqlalchemy.dialects import postgresql  
from sqlalchemy.dialects.postgresql import ARRAY
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from run import app
import datetime
#from postgresql_audit.flask import versioning_manager
#from sqlalchemy_continuum import make_versioned
from history_meta import Versioned
from sqlalchemy.sql import func
from marshmallow import EXCLUDE
import itertools
from dateutil.relativedelta import relativedelta
from sqlalchemy import or_

#versioning_manager.init(db.Model,actor_cls='UserModel')
#make_versioned(user_cls='UserModel')

class EmployeeInviteStatus(db.Model):  
    __tablename__ = 'invite_status'

    invite_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_number = db.Column(db.Integer, nullable=False)
    employee_email = db.Column(db.String(120), nullable=False)
    employee_role = db.Column(db.String(30), nullable=True)
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'))
    designation_id = db.Column(db.Integer, ForeignKey('designation_reference.designation_id'))
    joining_date = db.Column(db.Date, nullable=True)
    billable_resource = db.Column(db.String(1),nullable=True)
    invitation_status = db.Column(db.String(10), nullable=False)
    sent_time = db.Column(db.Date, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def re(cls):
        def to_json(x):
            return {
                'id': x.invite_id
                }
        return {'id': list(map(lambda x: to_json(x), cls.query.all()))}



class PasswordMailStatus(db.Model):         
    __tablename__ = 'password_mail_status'

    token_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(255), nullable=False)
    token_status = db.Column(db.String(30), nullable=False)    


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class CountryModel(db.Model):
    __tablename__ = 'countries_reference'

    country_id = db.Column(db.Integer, primary_key=True)
    country_name = db.Column(db.String(60), nullable=False)
    phone_code = db.Column(db.String(20), nullable=True)
    currency = db.Column(db.String(20), nullable=True)
    city_model_ref = relationship("CitiesModel", uselist=False, backref="countries_reference")
    state_model_ref = relationship("StateModel", uselist=False, backref="countries_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'country_name': x.country_name,
                'country_id': x.country_id,
                'phone_code': x.phone_code
                }
        return {'countries': list(map(lambda x: to_json(x), cls.query.all()))}

class StateModel(db.Model):
    __tablename__ = 'states_reference'

    state_id = db.Column(db.Integer, primary_key=True)
    state_name = db.Column(db.String(60), nullable=False)
    country_id = db.Column(db.Integer, ForeignKey('countries_reference.country_id'))
    city_model_ref = relationship("CitiesModel", uselist=False, backref="states_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls, countryid):
        def to_json(x):
            return {
                'state_name': x.state_name,
                'state_id': x.state_id,
                }
        return {'states': list(map(lambda x: to_json(x), cls.query.filter_by(country_id=countryid).all()))}

    @classmethod
    def return_all1(cls):
        def to_json(x):
            return {
                'state_name': x.state_name,
                'state_id': x.state_id,
                }
        return {'states': list(map(lambda x: to_json(x), cls.query.all()))}

class CitiesModel(db.Model):
    __tablename__ = 'cities_reference'

    city_id = db.Column(db.Integer, primary_key=True)
    city_name = db.Column(db.String(70), nullable=False)
    state_id = db.Column(db.Integer, ForeignKey('states_reference.state_id'))
    country_id = db.Column(db.Integer, ForeignKey('countries_reference.country_id'))

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls, stateid):
        def to_json(x):
            return {
                'city_name': x.city_name,
                'city_id': x.city_id,
                }
        return {'cities': list(map(lambda x: to_json(x), cls.query.filter_by(state_id=stateid).all()))}

    @classmethod
    def return_all1(cls):
        def to_json(x):
            return {
                'city_name': x.city_name,
                'city_id': x.city_id,
                }
        return {'cities': list(map(lambda x: to_json(x), cls.query.all()))}


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'

    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)


class PrivilegeModel(db.Model):
    __tablename__ = 'privileges'

    id = db.Column(db.Integer, primary_key=True)
    privilege_name = db.Column(db.String(255), nullable=False, unique=True)
    privilege_category = db.Column(db.String(50), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'privilege_id': x.id,
                'privilege_name': x.privilege_name,
                'privlege_category':x.privilege_category
            }

        return {'privileges': list(map(lambda x: to_json(x), PrivilegeModel.query.order_by(cls.privilege_category).all()))}

    @classmethod
    def ordered(cls,key):
        def to_json(x):
            return {
                'privilege_id': x.id,
                'privilege_name': x.privilege_name,
                'privlege_category':x.privilege_category
            }

        return list(map(lambda x: to_json(x), PrivilegeModel.query.filter_by(privilege_category=key).all()))


class CertificationsModel(db.Model):
    __tablename__ = 'employee_certifications'

    employee_certification_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    certification_name = db.Column(db.String(255), nullable=False)
    issued_by = db.Column(db.String(255), nullable=False)
    issued_on = db.Column(db.Date, nullable=False)   
    certification_url = db.Column(db.String(255), nullable=True)
    status = db.Column(db.String(1),nullable=True)

    def save_to_db(self):   
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

    @classmethod
    def return_all(cls, empnumber):
        def to_json(x):
            return {
                'certification_id':x.employee_certification_id,
                'certification_name': x.certification_name,
                'certification_issued_by': x.issued_by,
                'certification_issued_on': str(x.issued_on),
                'certification_url': x.certification_url,
            }

        return {'users': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=empnumber,status='A').all()))}

class CertificationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CertificationsModel
        unknown = EXCLUDE


class EducationModel(db.Model):
    __tablename__ = 'employee_education'

    employee_education_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    branch = db.Column(db.String(255), nullable=True)
    degree_id = db.Column(db.Integer, ForeignKey('degree_duration_reference.degree_id'))
    highest_degree_id = db.Column(db.Integer, ForeignKey('highest_degree_reference.highest_degree_id'))
    country_id = db.Column(db.Integer, ForeignKey('countries_reference.country_id'))
    year_awarded = db.Column(db.String(4), nullable=False)
    institution_name = db.Column(db.String(255), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

    @classmethod
    def return_all(cls, emp_id):
        def to_json(x):
            return {
                'employee_education_id': x.employee_education_id,
                'employee_id': x.employee_id,
                'branch': x.branch,
                'no_of_years': DegreeTypeListModel.query.filter_by(degree_id=x.degree_id).first().degree_duration,
                'highest_degree': HighestDegreeListModel.query.filter_by(highest_degree_id=x.highest_degree_id).first().highest_degree_type,
                'country': CountryModel.query.filter_by(country_id=x.country_id).first().country_name,
                'year_awarded': x.year_awarded,
                'institution_name': x.institution_name,
            }

        return {'education_details': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=emp_id).all()))}


class EducationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = EducationModel
        unknown = EXCLUDE



class WorkAuthorizationModel(db.Model):
    __tablename__ = 'work_authorization'

    auth_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    auth_type = db.Column(db.String(30), nullable=False)
    auth_start_date = db.Column(db.Date, nullable=False)
    auth_end_date = db.Column(db.Date, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()


class GreenCardModel(db.Model):
    __tablename__ = 'green_card'

    gc_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    gc_number = db.Column(db.String(20), nullable=True)
    gc_job_title = db.Column(db.String(150), nullable=False)
    gc_salary = db.Column(db.Numeric, nullable=False)
    gc_prevailing_wage = db.Column(db.Numeric, nullable=False)
    priority_date = db.Column(db.Date, nullable=False)
    current_status = db.Column(db.String(30), nullable=False)
    approval_status = db.Column(db.String(30), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

class GreenCardSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = GreenCardModel
        unknown = EXCLUDE


class DependentContactModel(db.Model):
    __tablename__ = 'employee_dependents'

    contact_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    dependent_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    dependent_relation_id = db.Column(db.Integer, ForeignKey('dependent_relation_reference.dependent_relation_id'))
    contact_first_name = db.Column(db.String(75), nullable=False)
    contact_middle_name = db.Column(db.String(75), nullable=True)
    contact_last_name = db.Column(db.String(75), nullable=False)
    contact_dob = db.Column(db.String(20), nullable=True)
    contact_gender = db.Column(db.String(1), nullable=True)
    contact_ssn = db.Column(db.String(20), nullable=True)
    mobile = db.Column(db.String(20), nullable=False)
    alt_mobile = db.Column(db.String(20), nullable=True)
    contact_email = db.Column(db.String(120), nullable=True)
    status = db.Column(db.String(1),nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    same_as_emp_current_add = db.Column(db.Boolean,nullable=True) 
    #emp_address = relationship("DependentAddressModel", uselist=False, backref="employee_contact")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all1(cls, empnumber):
        def to_json(x):
            dependentadd = Address1Model.query.filter_by(address_id = x.dependent_address_id).first()

            return {
                'contact_id': x.contact_id,
                'dependent_address_id': x.dependent_address_id,
                'contact_first_name': x.contact_first_name,
                'contact_middle_name': x.contact_middle_name,
                'contact_last_name': x.contact_last_name,
                'mobile': x.mobile,
                'alt_mobile': x.alt_mobile,
                'contact_email': x.contact_email,
                'dependent_relation_type': DependentRelationModel.query.filter_by(dependent_relation_id=x.dependent_relation_id).first().dependent_relation_type,
                'contact_dob': "xx-xx-xxxx" if x.contact_dob else '',
                'contact_gender': 'Male' if x.contact_gender=='M' else 'Female' if x.contact_gender=='F' else 'Other' if x.contact_gender=='O' else '',
                'contact_ssn': "xxx-xx-xxxx" if x.contact_ssn else '',
                'address_line1': dependentadd.address_line1,
                'address_line2': dependentadd.address_line2,
                'city': dependentadd.city,
                'state_name': dependentadd.state_name,
                'country': dependentadd.country,
                'zip_code': dependentadd.zip_code,
                'same_as_ca': x.same_as_emp_current_add if x.same_as_emp_current_add else ''

            }

        return {'users': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=empnumber,status='A').all()))}

class DependentSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = DependentContactModel
        unknown = EXCLUDE


# Note: this model is for emergency contact

class EmployeeContactModel(db.Model):
    __tablename__ = 'employee_emergency_contacts'

    contact_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    emergency_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    emergency_relation_id = db.Column(db.Integer, ForeignKey('emergency_relation_reference.emergency_relation_id'))
    contact_first_name = db.Column(db.String(75), nullable=False)
    contact_middle_name = db.Column(db.String(75), nullable=True)
    contact_last_name = db.Column(db.String(75), nullable=False)
    mobile = db.Column(db.String(20), nullable=False)
    alt_mobile = db.Column(db.String(20), nullable=True)
    contact_email = db.Column(db.String(120), nullable=False)
    created_by = db.Column(db.Integer, nullable=True)
    modified_by = db.Column(db.Integer, nullable=True)
    created_time = db.Column(db.DateTime, nullable=True)
    modified_time = db.Column(db.DateTime, nullable=True)
    status = db.Column(db.String(1), nullable=False)
    same_as_emp_current_add = db.Column(db.Boolean,nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

    @classmethod
    def return_all(cls, empnumber):
        def to_json(x):
            return {
                'contact_first_name': x.contact_first_name,
                'contact_middle_name': x.contact_middle_name,
                'contact_last_name': x.contact_last_name,
                'mobile': x.mobile,
                'alt_mobile': x.alt_mobile,
                'contact_email': x.contact_email,


            }

        return {'users': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=empnumber).all()))}



    @classmethod
    def return_all1(cls, empnumber):
        def to_json(x):
            emergencyadd = Address1Model.query.filter_by(address_id = x.emergency_address_id).first()
            emergencyrelid = EmergencyRelationModel.query.filter_by(emergency_relation_id=x.emergency_relation_id).first().emergency_relation_type
            id = emergencyrelid if emergencyrelid else '' 
            return {
                'contact_id': x.contact_id,
                'emergency_address_id': x.emergency_address_id,
                'contact_first_name': x.contact_first_name,
                'contact_middle_name': x.contact_middle_name,
                'contact_last_name': x.contact_last_name,
                'mobile': x.mobile,
                'alt_mobile': x.alt_mobile,
                'emergency_relation_type':emergencyrelid,
                'contact_email': x.contact_email,
                'address_line1': emergencyadd.address_line1,
                'address_line2': emergencyadd.address_line2,
                'city': emergencyadd.city,
                'state_name': emergencyadd.state_name,
                'country': emergencyadd.country,
                'zip_code': emergencyadd.zip_code,
                'same_as_ca': x.same_as_emp_current_add if x.same_as_emp_current_add else ''

            }

        return {'users': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=empnumber,status='A').all()))}


class EmergencySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = EmployeeContactModel
        unknown = EXCLUDE


class CobraModel(db.Model):
    __tablename__ = 'cobra'

    cobra_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=True)
    provider_name = db.Column(db.String(150), nullable=False)
    amount_covered = db.Column(db.Numeric, nullable=False)
    coverage_details = db.Column(db.String(255), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()


class BenefitsModel(db.Model):
    __tablename__ = 'employee_benefits'

    benefits_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    coverage_type_id = db.Column(db.Integer, ForeignKey('coverage_types_reference.coverage_type_id'))
    benefit_type_id = db.Column(db.Integer, ForeignKey('benefit_types_reference.benefit_type_id'))
    benefit_start_date = db.Column(db.Date, nullable=True)
    benefit_end_date = db.Column(db.Date, nullable=True)
    benefit_status = db.Column(db.String(30), nullable=True)
    benefit_types = db.Column(ARRAY(db.Text), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

    @classmethod
    def return_all(cls, empnumber):
        def to_json(x):
            if BenefitTypeListModel.query.filter_by(benefit_type_id = x.benefit_type_id).first().benefit_type.lower() != "none":
                return {
                    'benefits_id':x.benefits_id,
                    'coverage_type_id': CoverageTypeListModel.query.filter_by(coverage_type_id = x.coverage_type_id).first().coverage_type,
                    'benefit_type_id': BenefitTypeListModel.query.filter_by(benefit_type_id = x.benefit_type_id).first().benefit_type,
                    'benefit_start_date': str(x.benefit_start_date),
                    'benefit_status' : x.benefit_status,
                    'benefit_end_date' : str(x.benefit_end_date) if x.benefit_end_date else ''
                    }
            else:
                return {
                    'benefits_id':x.benefits_id,
                    'coverage_type_id': None,
                    'benefit_type_id': BenefitTypeListModel.query.filter_by(benefit_type_id = x.benefit_type_id).first().benefit_type,
                    'benefit_start_date': str(x.benefit_start_date),
                    'benefit_status' : x.benefit_status,
                    'benefit_end_date' : str(x.benefit_end_date)
                }

        return {'benefits': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=empnumber).all()))}


class EmergencyRelationModel(db.Model):
    __tablename__ = 'emergency_relation_reference'

    emergency_relation_id = db.Column(db.Integer, primary_key=True)
    emergency_relation_type = db.Column(db.String(20), nullable=False)
    employee_relation_ref = relationship("EmployeeContactModel", uselist=False, backref="emergency_relation_reference")
    consultant_relation_ref = relationship("ConsultantContactModel", uselist=False, backref="emergency_relation_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'emergencyRelationId': x.emergency_relation_id,
                'emergencyRelationType': x.emergency_relation_type
            }

        return {'emergencyRelation': list(map(lambda x: to_json(x), EmergencyRelationModel.query.all()))}

class DependentRelationModel(db.Model):
    __tablename__ = 'dependent_relation_reference'

    dependent_relation_id = db.Column(db.Integer, primary_key=True)
    dependent_relation_type = db.Column(db.String(20), nullable=False) 
    employee_relation_ref = relationship("DependentContactModel", uselist=False,backref="dependent_relation_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'dependentRelationId': x.dependent_relation_id,
                'dependentRelationType': x.dependent_relation_type
            }

        return {'dependentRelation': list(map(lambda x: to_json(x), DependentRelationModel.query.all()))}




# this is for number of years
class DegreeTypeListModel(db.Model):
    __tablename__ = 'degree_duration_reference'

    degree_id = db.Column(db.Integer, primary_key=True)
    degree_duration = db.Column(db.String(20), nullable=False)
    employee_relation_ref = relationship("EducationModel", uselist=False,backref="degree_duration_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class HighestDegreeListModel(db.Model):
    __tablename__ = 'highest_degree_reference'

    highest_degree_id = db.Column(db.Integer, primary_key=True)
    highest_degree_type = db.Column(db.String(20), nullable=False)
    employee_relation_ref = relationship("EducationModel", uselist=False,backref="highest_degree_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class PayTypeListModel(db.Model):
    __tablename__ = 'pay_types_reference'

    pay_type_id = db.Column(db.Integer, primary_key=True)
    pay_type = db.Column(db.String(20), nullable=False)
    employee_relation_ref = relationship("SalaryModel", uselist=False,backref="pay_types_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()





class CoverageTypeListModel(db.Model):
    __tablename__ = 'coverage_types_reference'

    coverage_type_id  = db.Column(db.Integer, primary_key=True)
    coverage_type = db.Column(db.String(30), nullable=False)
    employee_relation_ref = relationship("BenefitsModel", uselist=False,backref="coverage_types_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class BenefitTypeListModel(db.Model):
    __tablename__ = 'benefit_types_reference'

    benefit_type_id  = db.Column(db.Integer, primary_key=True)
    benefit_type = db.Column(db.String(30), nullable=False)
    employee_relation_ref = relationship("BenefitsModel", uselist=False,backref="benefit_types_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class EadTypeListModel(db.Model):
    __tablename__ = 'ead_types_reference'

    ead_type_id = db.Column(db.Integer, primary_key=True)
    ead_type = db.Column(db.String(20), nullable=False)
    employee_relation_ref = relationship("EadModel", uselist=False,backref="ead_types_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()




class DiversityStatusListModel(db.Model):
    __tablename__ = 'supplier_diversity_status_reference'

    diversity_status_id = db.Column(db.Integer, primary_key=True)
    diversity_status_type = db.Column(db.String(50), nullable=False)
    


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class WorkAuthorizationListModel(db.Model):
    __tablename__ = 'work_authorization_type_reference'

    work_authorization_type_id = db.Column(db.Integer, primary_key=True)
    work_authorization_type = db.Column(db.String(120), nullable=False)
    employee_relation_ref = relationship("ThirdPartyConsultantModel", uselist=False,backref="work_authorization_type_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class DependentVisaModel(db.Model):
    __tablename__ = 'dependent_h4visa'

    dependent_visa_id = db.Column(db.Integer, primary_key=True)
    visa_type_id = db.Column(db.Integer, ForeignKey('visa_type_reference.visa_type_id'))
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    visa_number = db.Column(db.String(255), nullable=True)
    h4_start_date = db.Column(db.Date, nullable=False)
    h4_end_date = db.Column(db.Date, nullable=False)
    current_status = db.Column(db.String(30), nullable=False)  

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()


class EadModel(db.Model):
    __tablename__ = 'ead'

    ead_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    ead_type_id = db.Column(db.Integer, ForeignKey('ead_types_reference.ead_type_id'))
    valid_from = db.Column(db.Date, nullable=False)
    valid_to = db.Column(db.Date, nullable=False)
    sponsor_name = db.Column(db.String(150), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

  
    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

class PayrollFrequencyListModel(db.Model):
    __tablename__ = 'payroll_frequency_reference'

    pay_freq_id = db.Column(db.Integer, primary_key=True)
    pay_frequency = db.Column(db.String(20), nullable=False)
    employee_relation_ref = relationship("SalaryModel", uselist=False,backref="pay_roll_frequency_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class SalaryModel(db.Model):
    __tablename__ = 'employee_salary'

    salary_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    pay_type_id = db.Column(db.Integer, ForeignKey('pay_types_reference.pay_type_id'))
    pay_freq_id = db.Column(db.Integer, ForeignKey('payroll_frequency_reference.pay_freq_id'))
    salary_start_date = db.Column(db.Date, nullable=False)
    salary_end_date = db.Column(db.Date, nullable=True)
    pay_amount = db.Column(db.Numeric, nullable=False)
    last_rise_date = db.Column(db.Date, nullable=True)
    salary_last_rise_amount = db.Column(db.Numeric, nullable=True)
    emp_salary_det = relationship("SalaryHistoryModel", uselist=False, backref="employee_salary")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()




class SalaryHistoryModel(db.Model):
    __tablename__ = 'salary_history'

    salary_history_id = db.Column(db.Integer, primary_key=True)
    salary_id = db.Column(db.Integer, ForeignKey('employee_salary.salary_id'))
    current_amount = db.Column(db.Numeric, nullable=False)
    revised_date = db.Column(db.Date, nullable=False)
    revised_amount = db.Column(db.Numeric, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



class ClientContactModel(db.Model):
    __tablename__ = 'client_contact'

    contact_id = db.Column(db.String(40), primary_key=True)
    client_id = db.Column(db.String(40), ForeignKey('client_details.client_id'))
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'))
    contact_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    contact_full_name = db.Column(db.String(150), nullable=False)
    contact_title = db.Column(db.String(50), nullable=False)
    contact_work_phone = db.Column(db.String(20), nullable=False)
    contact_cell_phone = db.Column(db.String(20), nullable=True)
    contact_email = db.Column(db.String(120), nullable=False)
    reporting_manager_name = db.Column(db.String(150), nullable=False)
    status = db.Column(db.String(1),nullable=True)
    created_by = db.Column(db.Integer, nullable=True)
    modified_by = db.Column(db.Integer, nullable=True)
    created_time = db.Column(db.DateTime, nullable=True)
    modified_time = db.Column(db.DateTime, nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_id(cls, client_id):
        return cls.query.filter_by(client_id=client_id,status='A').first()


    @classmethod
    def return_all_new(cls,clientid):
        def to_json(x):
            clientconadd = Address1Model.query.filter_by(address_id = x.contact_address_id).first()
            return {
                'contact_id': x.contact_id,
                'client_id': x.client_id,
                'department_id': x.department_id,
                'department_name': DepartmentModel.find_by_department_id(x.department_id).department_name,
                'contact_address_id': x.contact_address_id,
                'contact_full_name':x.contact_full_name,
                'contact_title': x.contact_title,
                'contact_work_phone': x.contact_work_phone,
                'contact_email': x.contact_email,
                'reporting_manager_name': x.reporting_manager_name,
                'address_line1': clientconadd.address_line1,
                'address_line2': clientconadd.address_line2,
                'city': clientconadd.city,
                'state_name': clientconadd.state_name,
                'country': clientconadd.country,
                'zip_code': clientconadd.zip_code,
            }

        return {'client_contact_details': list(map(lambda x: to_json(x), cls.query.filter_by(client_id=clientid,status='A').all()))}


class ClientLocationModel(db.Model):
    __tablename__ = 'client_location'

    location_id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.String(40), ForeignKey('client_details.client_id'))
    location_name = db.Column(db.String(255), nullable=False)
    business_concentration = db.Column(db.String(255), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_id(cls, client_id):
        return cls.query.filter_by(client_id=client_id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'client_location_id': x.location_id,
                'client_id': x.client_id,
                'contact_id': x.contact_id,
                'location_name': x.location_name,
                'business_concentration': x.business_concentration,
            }

        return {'client_contact_details': list(map(lambda x: to_json(x), ClientLocationModel.query.all()))}



class ProjectEmployeeModel(db.Model):
    __tablename__ = 'project_employee'

    project_employee_id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.String(40), ForeignKey('project.project_id'))
    employee_id = db.Column(ARRAY(db.Text))

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def all_employee_projects(cls, employee_id):

        def to_json(x):

            return {
            "project_name": ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
            "project_id": x.project_id
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter(cls.employee_id.contains([str(employee_id)])).all()))}

    @classmethod
    def find_by_pro_list(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x).first()
            return {
            "project_name": a.project_name,
            "project_id": a.project_id

            }

        return {'Projects': list(map(lambda x: to_json(x), pro_list))}


    @classmethod
    def return_all_employeeprojects(cls,empid): 
        def to_json(x):
            pro =  ProjectModel.query.filter_by(project_id = x.project_id).first()
            client = ClientDetailsModel.query.filter_by(client_id=pro.client_id).first()
            return {
            'project_id': x.project_id,
            'project_name': pro.project_name,
            'client_id': client.client_id,
            'client_name': client.organization_name
            }

        return {'employee_projects': list(map(lambda x: to_json(x), cls.query.filter(ProjectEmployeeModel.employee_id.contains([str(empid)])).all()))}


    @classmethod
    def find_by_project_id(cls, proid):
        b = ProjectModel.query.filter_by(project_id=proid).first()
        w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
        emp = ProjectEmployeeModel.query.filter_by(project_id=proid).first()

        def to_json(x):
            if 'CON' in str(x):
                a = ThirdPartyConsultantModel.query.filter_by(consultant_id=x).first()
                return {
                    'employee_id':a.consultant_id,
                    'emp_number': a.consultant_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.personal_email_id,
                    'project': b.project_name
                    }
            else:
                a = Employee1Model.find_by_employee_id(int(x))
                return {
                    'employee_id':a.employee_id,
                    'emp_number': a.emp_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.email_id,
                    'project': b.project_name
                    }
        if emp:
            return {'projects_resources': list(map(lambda x: to_json(x), emp.employee_id))}
        else:
            return {'projects_resources': []}

    @classmethod
    def find_by_project_id2(cls, proid):
        b = ProjectModel.query.filter_by(project_id=proid).first()
        w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
        created_by = Employee1Model.query.filter_by(employee_id=b.created_by).first()
        modified_by = Employee1Model.query.filter_by(employee_id=b.modified_by).first()
        if modified_by:
            modifier = modified_by.first_name+' '+modified_by.last_name
        else:
            modifier = ''
        emp = cls.query.filter_by(project_id=proid).first()

        def to_json(x):
            if 'CON' in str(x):
                a = ThirdPartyConsultantModel.query.filter_by(consultant_id=x).first()
                return {
                    'employee_id':a.consultant_id,
                    'emp_number': a.consultant_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.personal_email_id
                    }
            else:
                a = Employee1Model.find_by_employee_id(int(x))
                return {
                    'employee_id':a.employee_id,
                    'emp_number': a.emp_number,
                    'first_name': a.first_name,
                    'middle_name': a.middle_name,
                    'last_name': a.last_name,
                    'email': a.email_id,
                    'primary_project': True if EmployeePrimaryProjectModel.query.filter(EmployeePrimaryProjectModel.project_id==b.project_id,EmployeePrimaryProjectModel.employee_id==int(x)).first() else False
                    }

        if emp:
            return {'project_id': b.project_id,   
                'client_id': b.client_id,
                'client_name':ClientDetailsModel.query.filter_by(client_id=b.client_id).first().organization_name,
                'contract_id': b.contract_id,
                'contract_name':ClientVendorshipModel.query.filter_by(contract_id=b.contract_id).first().contract_name,
                'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id),
                'project_name': b.project_name,
                'project_description': b.project_description,
                'scheduled_start_date': str(b.scheduled_start_date),
                'scheduled_end_date': str(b.scheduled_end_date),
                'actual_start_date': str(b.actual_start_date),
                'actual_end_date': str(b.actual_end_date),
                'project_revenue': str(b.revenue_amount) if b.revenue_amount else '',
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_emp_number':w.emp_number,
                'project_manager_contact': b.proj_manager_phone_number,
                'project_sponsor': b.project_sponsor_name,
                'work_location':b.work_location,
                'project_status': 'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                'termination_reason':b.comments,
                'created_by':created_by.first_name + ' ' + created_by.last_name,
                'modified_by':modifier,
                "project_Status_last_updated_date":str(b.project_status_last_updtd_date),
                'projects_resources': list(map(lambda x: to_json(x), emp.employee_id))}
                 
                

        else:
            return {'project_id': b.project_id,
                'client_id': b.client_id,
                'client_name':ClientDetailsModel.query.filter_by(client_id=b.client_id).first().organization_name,
                'contract_id': b.contract_id,
                'contract_name':ClientVendorshipModel.query.filter_by(contract_id=b.contract_id).first().contract_name,
                'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id),
                'project_name': b.project_name,
                'project_description': b.project_description,
                'scheduled_start_date': str(b.scheduled_start_date),
                'scheduled_end_date': str(b.scheduled_end_date),
                'actual_start_date': str(b.actual_start_date),
                'actual_end_date': str(b.actual_end_date),
                'project_revenue': str(b.revenue_amount) if b.revenue_amount else '',
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_emp_number':w.emp_number,
                'project_manager_contact': b.proj_manager_phone_number,
                'project_sponsor': b.project_sponsor_name,
                'work_location':b.work_location,
                'termination_reason':b.comments,
                'created_by':created_by.first_name + ' ' + created_by.last_name,
                'modified_by':modifier,
                'project_status':'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                "project_Status_last_updated_date":str(b.project_status_last_updtd_date),
                'projects_resources': []}

    @classmethod
    def return_all_new(cls, z,a):
        total_records = ProjectEmployeeModel.query.all()
        list_of_projects = []
        all_projects = []
        for i in total_records:
            if str(z) in i.employee_id:
                list_of_projects.append(i.project_id)
        c = []
        d = []
        for i in list_of_projects:
            b = ProjectModel.query.filter_by(project_id=i).first().modified_time
            c.append(b)
            d.append(b)
        d.sort(reverse=True)
        for i in range(len(d)):
            all_projects.append(list_of_projects[c.index(d[i])])
        if a == 1:
            b = ProjectModel.query.filter_by(created_by=z).all()
            for i in b:
                all_projects.append(i.project_id)

        def to_json(x):
            b = ProjectModel.query.filter_by(project_id=x).first()
            w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
            z = ClientDetailsModel.query.filter_by(client_id=b.client_id).first().organization_name
            creator = Employee1Model.query.filter_by(employee_id=b.created_by).first()

            return {   
                'project_id': b.project_id, 'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id),
                'project_name': b.project_name, 'project_description': b.project_description,
                'scheduled_start_date': str(b.scheduled_start_date), 'scheduled_end_date': str(b.scheduled_end_date),
                'actual_start_date': str(b.actual_start_date), 'actual_end_date': str(b.actual_end_date),
                'revenue_amount': str(b.revenue_amount),
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_contact': w.primary_phone_number, 'project_sponsor_name': b.project_sponsor_name,
                'project_status': 'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                'client_name':z,
                'created_by': creator.first_name+' '+creator.last_name
            }
        projects = list(map(lambda x: to_json(x), all_projects))
        projects = list({v['project_id']:v for v in projects}.values())
        total_projects = len(projects)
        count = 0
        for i in projects:
            if i['project_status'] == 'Active':
                count += 1


        return {'projects': projects,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}


    @classmethod
    def return_all_clients(cls, z):
        total_records = ProjectEmployeeModel.query.all()
        list_of_projects = []
        for i in total_records:
            if str(z) in i.employee_id:
                list_of_projects.append(i.project_id)
        def to_json(x):
            b = ProjectModel.query.filter_by(project_id=x).first()
            w = Employee1Model.query.filter_by(employee_id=b.project_manager_id).first()
            z = ClientDetailsModel.query.filter_by(client_id=b.client_id).first()

            return {
                'project_id': b.project_id, 'project_type': ProjectTypeModel.find_by_project_type_id(b.project_type_id),
                'project_name': b.project_name, 'project_description': b.project_description,
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_status':'Active' if b.project_status=='A' else 'Inactive' if b.project_status=='I' else 'Terminated' if b.project_status=='T' else '',  
                'client_name':z.organization_name,
                'client_id':z.client_id,
                'client_status':z.client_status
            }

        return {'clients': list(map(lambda x: to_json(x), list_of_projects))}



    @classmethod
    def return_all_empprojects(cls,empid):
        def to_json(x):
            pro =  ProjectModel.query.filter_by(project_id = x.project_id).first()
            bill = EmployeeBillRateModel.find_by_emp_pro(empid,x.project_id)
            if bill:
                from_date = '' if bill.from_date == None else bill.from_date
                to_date = '' if bill.to_date == None else bill.to_date
                return {
                'employee_id':empid,
                'project_id': x.project_id,
                'project_name': pro.project_name,
                'client_name': ClientDetailsModel.query.filter_by(client_id = pro.client_id).first().organization_name,
                'work_location': pro.work_location,
                'project_manager_name': Employee1Model.query.filter_by(employee_id=pro.project_manager_id).first().first_name,
                'bill_id': bill.bill_id,
                'bill_rate': str(bill.bill_rate),
                'pay_rate': str(bill.pay_rate),
                'work_week_hours': str(bill.work_week_hours),
                'from_date':str(from_date),
                'to_date':str(to_date),
                'resource_billable':'Yes' if bill.resource_billable=='Y' else 'No' if bill.resource_billable=='N' else '',
                'primary_project': True if EmployeePrimaryProjectModel.query.filter(EmployeePrimaryProjectModel.project_id==x.project_id,EmployeePrimaryProjectModel.employee_id==empid).first() else False

                }
            else:
                return {
                'employee_id':empid,
                'project_id': x.project_id,
                'project_name': pro.project_name,
                'client_name': ClientDetailsModel.query.filter_by(client_id = pro.client_id).first().organization_name,
                'work_location':pro.work_location,
                'project_manager_name': Employee1Model.query.filter_by(employee_id=pro.project_manager_id).first().first_name,
                'bill_id': '',
                'bill_rate':'',
                'pay_rate':'',
                'work_week_hours':'',
                'from_date':'',
                'to_date':'',
                'resource_billable':'',
                'primary_project': True if EmployeePrimaryProjectModel.query.filter(EmployeePrimaryProjectModel.project_id==x.project_id,EmployeePrimaryProjectModel.employee_id==empid).first() else False

                }

        return {'employee_projects': list(map(lambda x: to_json(x), cls.query.filter(ProjectEmployeeModel.employee_id.contains([str(empid)])).all()))}



class StaffingSupplierModel(db.Model):
    __tablename__ = 'staffing_supplier'

    supplier_id = db.Column(db.Integer, primary_key=True)
    supplier_name = db.Column(db.String(150), nullable=False)
    state_of_incorporation = db.Column(db.String(60), nullable=False)
    tax_id_FEIN = db.Column(db.String(13), nullable=False)
    diversity_status = db.Column(ARRAY(db.String(255)), nullable=True)
    diversity_certification_expiry_date = db.Column(db.Date, nullable=True)
    contact_number = db.Column(db.String(20), nullable=True)
    termination_reason = db.Column(db.String(255), nullable=True)
    contract_status = db.Column(db.String(1), nullable=False)
    owner_name = db.Column(db.String(150), nullable=True)
    company_website_url = db.Column(db.String(255), nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    supplier_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    supplier_address_rel = relationship("SupplierAddressModel", uselist=False, backref="staffing_supplier")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_suppliers(cls):
        def to_json(x):
            return {
                'supplier_id': x.supplier_id,  
                'supplier_name': x.supplier_name,  
                'contract_status': 'Active' if x.contract_status=='A' else 'Inactive' if x.contract_status=='I' else 'Terminated' if x.contract_status=='T' else '',
            }

        return {'suppliers': list(map(lambda x: to_json(x), cls.query.order_by(StaffingSupplierModel.contract_status.asc(),StaffingSupplierModel.modified_time.desc()).all()))}
    
    @classmethod
    def return_all_suppliers_cons(cls):
        def to_json(x):
            return {
                'supplier_id': x.supplier_id,  
                'supplier_name': x.supplier_name,  
                'contract_status': 'Active' if x.contract_status=='A' else 'Inactive' if x.contract_status=='I' else 'Terminated' if x.contract_status=='T' else '',
            }

        return {'suppliers': list(map(lambda x: to_json(x), cls.query.filter_by(contract_status='A').order_by(StaffingSupplierModel.supplier_name.asc()).all()))}

    @classmethod
    def return_all_suppliers_for_consultants(cls):
        def to_json(x):
            return {
                'supplier_id': x.supplier_id,  
                'supplier_name': x.supplier_name,
                'contract_status': 'Active' if x.contract_status=='A' else 'Inactive' if x.contract_status=='I' else 'Terminated' if x.contract_status=='T' else '',
            }

        return {'suppliers': list(map(lambda x: to_json(x), cls.query.filter_by(contract_status='A').all()))}

    @classmethod
    def supplier_contracts(cls):
        def to_json(x):
            return SupplierContractModel.return_all(supid=x.supplier_id)
        li = list(map(lambda x: to_json(x), cls.query.filter_by(contract_status='A').all()))
        return {'suppliers': list(itertools.chain.from_iterable(li))}

class SupplierAddressModel(db.Model):
    __tablename__='supplier_address'

    supplier_address_id=db.Column(db.Integer, primary_key=True)
    supplier_id= db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    address_id= db.Column(db.Integer, ForeignKey('address.address_id'))
    status = db.Column(db.String(1),nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True) 
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls,supid):
        def to_json(x):
            y = Address1Model.query.filter_by(address_id=x.address_id).first()
            return {
                'supplier_address_id':x.supplier_address_id,
                'supplier_id': x.supplier_id,
                'address_id': x.address_id,
                'address_line1': y.address_line1,
                'address_line2': y.address_line2,
                'city':y.city,
                'state_name':y.state_name,
                'zip_code':y.zip_code,
                'country':y.country
            }

        return {'supplier_address': list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supid,status='A').all()))}

class SupplierContractModel(db.Model):
    __tablename__ = 'supplier_contract'

    contract_id = db.Column(db.Integer, primary_key=True)
    supplier_id = db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    contarct_number = db.Column(db.String(50),unique=True, nullable=True)
    contract_name = db.Column(db.String(255), nullable=True)
    start_date = db.Column(db.Date, nullable=True)
    end_date = db.Column(db.Date, nullable=True)
    status = db.Column(db.String(1), nullable=True)
    cretaed_by = db.Column(db.Integer, nullable=True)
    modified_by = db.Column(db.Integer, nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()
   
    @classmethod
    def return_all(cls,supid):
        def to_json(x):
            supplier = StaffingSupplierModel.query.filter_by(supplier_id=supid).first()
            return {
                'supplier_id': supplier.supplier_id,  
                'supplier_name': supplier.supplier_name,
                'resources': ThirdPartyConsultantModel.query.filter_by(supplier_id=supplier.supplier_id).count(),
                'contract_id' : x.contract_id,
                'supplier_id': x.supplier_id,
                'contract_number': x.contarct_number,
                'contract_name': x.contract_name,
                'start_date': str(x.start_date),
                'end_date':str(x.end_date),
            }

        return list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supid,status='A').all()))



class SupplierContactModel(db.Model):
    __tablename__ = 'supplier_contact'

    contact_id = db.Column(db.Integer, primary_key=True)
    supplier_id = db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    supplier_contact_type_id = db.Column(db.Integer, ForeignKey('supplier_contact_type_reference.supplier_contact_type_id'))
    contact_first_name = db.Column(db.String(75), nullable=True)
    contact_last_name = db.Column(db.String(75), nullable=True)
    contact_middle_name = db.Column(db.String(75), nullable=True)
    contact_phone = db.Column(db.String(50), nullable=True)
    contact_email = db.Column(db.String(50), nullable=True)
    contact_title = db.Column(db.String(30), nullable=True)
    contact_status = db.Column(db.String(1), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls,supid):
        def to_json(x):
            return {
                'contact_id' : x.contact_id,
                'supplier_id': x.supplier_id,
                'contact_first_name': x.contact_first_name,
                'contact_last_name': x.contact_last_name,
                'contact_middle_name': x.contact_middle_name,
                'contact_type':SupplierContactTypeModel.query.filter_by(supplier_contact_type_id=x.supplier_contact_type_id).first().supplier_contact_type,
                'contact_phone':x.contact_phone,
                'contact_email':x.contact_email,   
                'contact_title':x.contact_title
            }

        return {'supplier_contacts': list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supid,contact_status='A').all()))}

class SupplierContactTypeModel(db.Model):
    __tablename__ = 'supplier_contact_type_reference'

    supplier_contact_type_id = db.Column(db.Integer, primary_key=True)
    supplier_contact_type = db.Column(db.String(50), nullable=False)
    consultant_contact_rel = relationship("SupplierContactModel", uselist=False, backref="supplier_contact_type")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class ConsultantIdModel(db.Model):
    __tablename__ = 'consultant_id_gen'

    id=db.Column(db.Integer, primary_key=True)
    consultant_id = db.Column(db.Integer, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class ThirdPartyConsultantModel(db.Model):
    __tablename__ = 'third_party_consultant'

    consultant_id = db.Column(db.String(10), primary_key=True)
    supplier_id = db.Column(db.Integer, ForeignKey('staffing_supplier.supplier_id'))
    work_authorization_type_id = db.Column(db.Integer, ForeignKey('work_authorization_type_reference.work_authorization_type_id'))
    consultant_number = db.Column(db.String(10), nullable=True)
    first_name = db.Column(db.String(75), nullable=False)
    middle_name = db.Column(db.String(75), nullable=True)
    last_name = db.Column(db.String(75), nullable=False)
    date_of_birth = db.Column(db.String(20), nullable=True)
    ssn = db.Column(db.String(20), nullable=True)
    personal_cell_phone = db.Column(db.String(20), nullable=True)
    personal_email_id = db.Column(db.String(120), nullable=True)
    gender = db.Column(db.String(1), nullable=False)
    work_authorization_expiry_date = db.Column(db.Date, nullable=True)
    consultant_status = db.Column(db.String(1), nullable=True)
    job_title=db.Column(db.String(150), nullable=True)
    worksite_location=db.Column(db.String(60), nullable=True)
    rehire_eligibility=db.Column(db.String(1), nullable=True)
    termination_reason=db.Column(db.String(255), nullable=True)
    account_manager=db.Column(db.String(150), nullable=True)
    recruiter_name=db.Column(db.String(150), nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    consultant_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    consultant_contact_rel = relationship("ConsultantContactModel", uselist=False, backref="third_party_consultant")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_consultants(cls):
        def to_json(x):
            return {
                'consultant_id': x.consultant_id,
                'supplier_name': StaffingSupplierModel.query.filter_by(supplier_id=x.supplier_id).first().supplier_name,
                'consultant_first_name': x.first_name,
                'consultant_last_name': x.last_name,
                'work_authorization': WorkAuthorizationListModel.query.filter_by(work_authorization_type_id=x.work_authorization_type_id).first().work_authorization_type,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other' if x.gender=='O' else '',
                'consultant_status': 'Active' if x.consultant_status=='A' else 'Inactive' if x.consultant_status=='I' else 'Terminated' if x.consultant_status=='T' else ''
            }

        return {'consultants': list(map(lambda x: to_json(x), cls.query.order_by(ThirdPartyConsultantModel.consultant_status.asc(),ThirdPartyConsultantModel.modified_time.desc()).all()))}

    @classmethod
    def return_all_active_forproject(cls):
        def to_json(x):
            return {
                'employee_id':x.consultant_id,
                'employee_number':x.consultant_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email':x.personal_email_id,
                'gender':x.gender,
                'employee_status':'Active' if x.consultant_status=='A' else 'Inactive' if x.consultant_status=='I' else 'Terminated' if x.consultant_status=='T' else ''

            }

        return {'consultants': list(map(lambda x: to_json(x), cls.query.filter_by(consultant_status='A').all()))}

    @classmethod
    def return_all_supplier_consultants(cls,supplier_id):
        def to_json(x):
            return {
                'employee_id':x.consultant_id,
                'employee_number':x.consultant_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email':x.personal_email_id,
                'gender':'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other' if x.gender=='O' else '',
                'employee_status':'Active' if x.consultant_status=='A' else 'Inactive' if x.consultant_status=='I' else 'Terminated' if x.consultant_status=='T' else ''

            }

        return {'consultants': list(map(lambda x: to_json(x), cls.query.filter_by(supplier_id=supplier_id).all()))}





class ConsultantContactModel(db.Model):
    __tablename__ = 'consultant_emergency_contact'

    emergency_contact_id=db.Column(db.Integer, primary_key=True)
    consultant_id = db.Column(db.String(10), ForeignKey('third_party_consultant.consultant_id'))
    emergency_first_name = db.Column(db.String(75), nullable=True)
    emergency_middle_name = db.Column(db.String(75), nullable=True)
    emergency_last_name = db.Column(db.String(75), nullable=True)
    emergency_phone_number = db.Column(db.String(20), nullable=True)
    emergency_alternate_phone_number = db.Column(db.String(20), nullable=True)
    emergency_relation_id = db.Column(db.Integer, ForeignKey('emergency_relation_reference.emergency_relation_id'))
    emergency_email = db.Column(db.String(120), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class EmployeeBillRateModel(db.Model):

    __tablename__ = 'bill_rate'

    bill_id=db.Column(db.Integer, primary_key=True)
    project_id=db.Column(db.String(40), ForeignKey('project.project_id'))
    employee_id = db.Column(db.String(10), nullable=False)
    bill_rate=db.Column(db.Numeric, nullable=True)
    pay_rate=db.Column(db.Numeric, nullable=True)
    work_week_hours=db.Column(db.Integer, nullable=True)
    from_date=db.Column(db.Date, nullable=True)
    to_date=db.Column(db.Date, nullable=True)
    resource_billable=db.Column(db.String(1), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_emp_pro(cls, employee_id, project_id):
        return cls.query.filter_by(employee_id=str(employee_id), project_id=project_id).first()

class RolePrivilegeModel(db.Model):
    __tablename__ = 'roleprivileges'

    role_privileges_id = db.Column(db.Integer, primary_key=True)
    role_id = db.Column(db.Integer, ForeignKey('roles.id'))
    role_privileges = db.Column(ARRAY(db.Text), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'role_id': x.id,
                'role_name': x.role_name
            }

        return {'role_privileges': list(map(lambda x: to_json(x), RolePrivilegeModel.query.all()))}

    @classmethod
    def find_by_role_name(cls, role_name):
        return cls.query.filter_by(role_name=role_name).first()

    @classmethod
    def find_by_role_id(cls, role_id):
        return cls.query.filter_by(role_id=role_id).first()



class TimeSheetModel(db.Model):
    __tablename__='time_sheets'

    time_sheet_id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.String(255), ForeignKey('project.project_id'))
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    client_id = db.Column(db.String(255), ForeignKey('client_details.client_id'))
    time_sheet_date = db.Column(db.Date, nullable=True)
    regular_hours = db.Column(db.String(20), nullable=True)
    ot_hours = db.Column(db.String(20), nullable=True)
    billable_hours = db.Column(db.String(20), nullable=True)
    sick_hours = db.Column(db.String(20), nullable=True)
    remarks = db.Column(db.Text, nullable=True)
    time_sheet_approval = db.Column(db.String(30), nullable=True, default='Approval Pending')
    work_description = db.Column(db.Text, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_by_my_ts(cls,project_id,date):
        if date != '':
            min_date = date[0].split('T')[0]
            max_date = date[1].split('T')[0]
        else:
            min_date = '01-01-1977'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        employeeid = Employee1Model.query.filter_by(emp_number=data['emp_num']).first().employee_id
        def to_json(x):
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'project_id': x.project_id,
                'employee_id': x.employee_id,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter(TimeSheetModel.project_id == (project_id if project_id else TimeSheetModel.project_id),TimeSheetModel.time_sheet_date >= min_date,TimeSheetModel.time_sheet_date <= max_date)
.all()))}

    @classmethod
    def return_all_by_man_ts(cls,emp_num,project_id,date):
        if date != '':
            min_date = date[0].split('T')[0]
            max_date = date[1].split('T')[0]
        else:
            min_date = '01-01-1977'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        def to_json(x):
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'project_id': x.project_id,
                'employee_id': x.employee_id,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter(TimeSheetModel.employee_id == (emp_num if emp_num else TimeSheetModel.employee_id),TimeSheetModel.project_id == (project_id if project_id else TimeSheetModel.project_id),TimeSheetModel.time_sheet_date >= min_date,TimeSheetModel.time_sheet_date <= max_date)
.all()))}


    @classmethod
    def return_all_by_emp_ts(cls,emp_num,date,project_id,client_id):
        if date != '':
            min_date = date[0].split('T')[0]
            max_date = date[1].split('T')[0]    
        else:
            min_date = '01-01-1977'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')

        def to_json(x):
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'employee_id': x.employee_id,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter(TimeSheetModel.employee_id == (emp_num if emp_num else TimeSheetModel.employee_id),TimeSheetModel.project_id == (project_id if project_id else TimeSheetModel.project_id),TimeSheetModel.client_id == (client_id if client_id else TimeSheetModel.client_id),TimeSheetModel.time_sheet_date >= min_date,TimeSheetModel.time_sheet_date <= max_date)
.all()))}


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_id': x.project_id,
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'employee_id': x.employee_id,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}

    @classmethod
    def return_for_spec_emp(cls,empid):
        def to_json(x):
            z = Employee1Model.query.filter_by(employee_id=x.employee_id).first()
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'project_id': x.project_id,
                'employee_name': z.first_name + ' ' + z.middle_name+ ' ' + z.last_name,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter_by(employee_id=empid).all()))}



    @classmethod
    def return_all_by_pro(cls, proid):
        def to_json(x):
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'project_id': x.project_id,
                'employee_id': x.employee_id,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter_by(project_id=proid).all()))}

    @classmethod
    def return_all_by_prolist(cls, prolist):

        def to_json(y):
            x = cls.query.filter_by(project_id=y).first()
            return {
                'time_sheet_id': x.time_sheet_id,
                'time_sheet_date': str(x.time_sheet_date),
                'project_name': ProjectModel.query.filter_by(project_id=x.project_id).first().project_name,
                'project_id': x.project_id,
                'employee_id': x.employee_id,
                'client_name': ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'regular_hours': x.regular_hours,
                'ot_hours': x.ot_hours,
                'billable_hours': x.billable_hours,
                'sick_hours': x.sick_hours,
                'remarks': x.remarks,
                'time_sheet_approval': x.time_sheet_approval,
                'work_description': x.work_description
            }

        return {'Projects': list(map(lambda y: to_json(y), prolist))}



class HistoryLogModel(db.Model):
    __tablename__ = 'history_log'

    history_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=True)
    entity_name = db.Column(db.String(100), nullable=True)
    user_name = db.Column(db.String(100), nullable=True)
    table_name = db.Column(db.String(100), nullable=True)
    time_changed = db.Column(db.DateTime(timezone=True), default=datetime.datetime.utcnow)
    history_field = db.Column(db.Text, nullable=True)
    history_record = db.Column(db.Text, nullable=True)
    new_record = db.Column(db.Text, nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

class RoleModel(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    role_name = db.Column(db.String(255), nullable=False, unique=True)
    role_privilege_ref = relationship("RolePrivilegeModel", uselist=False, backref="roles")

    def save_to_db(self):
        db.session.add(self)  
        db.session.commit()  

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'role_id': x.id,
                'role_name': (x.role_name),
            }

        return {'Roles': list(map(lambda x: to_json(x), RoleModel.query.all()))}

    @classmethod
    def find_by_role_id(cls, role_id):
        return cls.query.filter_by(id=role_id).first().role_name

    @classmethod
    def find_by_role_id_to(cls, role_id):
        return cls.query.filter_by(id=role_id).first()

    @classmethod  
    def find_by_role_name(cls, role_name):
        return cls.query.filter_by(role_name=role_name).first().id


class SupplierContactSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = SupplierContactModel
        unknown = EXCLUDE

class UsCitizenPassportDetailsModel(db.Model):         
    __tablename__ = 'us_citizen_passport_details'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    passport_number = db.Column(db.String(20), nullable=False)    
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=False) 


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class Employee1Model(db.Model):
    __tablename__ = 'employee'

    employee_id = db.Column(db.Integer, primary_key=True)
    emp_number = db.Column(db.Integer, nullable=True,unique=True)
    first_name = db.Column(db.String(75), nullable=False)
    middle_name = db.Column(db.String(75), nullable=True)
    last_name = db.Column(db.String(75), nullable=False)
    email_id = db.Column(db.String(120), nullable=False)
    date_of_birth = db.Column(db.String(20), nullable=False)
    ssn = db.Column(db.String(20), nullable=False)
    gender = db.Column(db.String(1), nullable=False)
    primary_phone_number = db.Column(db.String(20), nullable=False)
    alternate_phone_number = db.Column(db.String(20), nullable=True)
    marital_status = db.Column(db.String(1), nullable=True)
    employment_start_date = db.Column(db.Date, nullable=False)
    employment_end_date = db.Column(db.Date, nullable=True)
    reporting_manager_name = db.Column(db.String(150), nullable=True)
    rep_manager_phone_number = db.Column(db.String(20), nullable=True)
    recruitment_method = db.Column(db.String(30), nullable=True)
    eligible_sick_hours = db.Column(db.String(30), nullable=True)
    eligible_vacation_weeks = db.Column(db.String(30), nullable=True)
    paid_holidays = db.Column(db.Boolean, nullable=True)
    employee_status = db.Column(db.String(1), nullable=True)
    termination_reason_id = db.Column(db.Integer, ForeignKey('termination_reason.termination_reason_id'))  
    inactive_reason_id = db.Column(db.Integer, ForeignKey('emp_inactive_reason.emp_inactive_reason_id'))
    rehire_eligibility = db.Column(db.Boolean, nullable=True)  
    cobra_eligibility = db.Column(db.Boolean, nullable=True)
    benefits_eligibility = db.Column(db.Boolean, nullable=True)
    billable_resource = db.Column(db.String(1),nullable=True)
    account_manager = db.Column(db.String(150), nullable=True)
    recruiter = db.Column(db.String(150), nullable=True)
    us_citizen = db.Column(db.Boolean, nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    employee_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    termination_date = db.Column(db.Date,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    same_as_ca = db.Column(db.Boolean,nullable=True)
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'))
    designation_id = db.Column(db.Integer, ForeignKey('designation_reference.designation_id'))
    emp_type_id = db.Column(db.Integer, ForeignKey('employment_type_reference.emp_type_id'))
    present_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    permanent_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    work_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    recruiter_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    reporting_manager_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    present_add = relationship('Address1Model', foreign_keys='Employee1Model.present_address_id')
    permanent_add = relationship('Address1Model', foreign_keys='Employee1Model.permanent_address_id')
    work_add = relationship('Address1Model', foreign_keys='Employee1Model.work_address_id')
    certifications = relationship("CertificationsModel", uselist=False, backref="employee")
    emp_education = relationship("EducationModel", uselist=False, backref="employee")
    emp_work_auth = relationship("WorkAuthorizationModel", uselist=False, backref="employee")
    green_card = relationship("GreenCardModel", uselist=False, backref="employee")
    emp_contact = relationship("EmployeeContactModel", uselist=False, backref="employee")
    emp_dep_contact = relationship("DependentContactModel", uselist=False, backref="employee")
    emp_cobra = relationship("CobraModel", uselist=False, backref="employee")
    emp_visa_details = relationship("EmployeeVisaDetailsModel", uselist=False, backref="employee")
    emp_dep_visa = relationship("DependentVisaModel", uselist=False, backref="employee")
    emp_ead = relationship("EadModel", uselist=False, backref="employee")
    emp_salary = relationship("SalaryModel", uselist=False, backref="employee")
    pro_emp = relationship("ProjectModel", uselist=False, backref="employee")
    emp_time_sheet = relationship("TimeSheetModel", uselist=False, backref = "employee")
    us_citizenship = relationship("UsCitizenPassportDetailsModel", uselist=False, backref = "employee")
   

    def save_to_db(self):

        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_new(cls):
        a = cls.query(db.func.max(cls.emp_number).label("total_amount")).filter(cls.emp_number>= emp_number).all()
        return a

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(email_id=username).first()

    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(emp_number=empnumber).first()

    @classmethod
    def find_by_employee_id(cls, empid):
        return cls.query.filter_by(employee_id=empid).first()




    @classmethod
    def return_all_for_date_filter(cls,start_date,end_date):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'last_name': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'date_of_birth': x.date_of_birth,
                'employment_start_date': str(x.employment_start_date),
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated', 
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.employment_start_date>=start_date,cls.employment_start_date<=end_date).all()))}

    @classmethod
    def return_all_for_new_hires(cls,date):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'middle_name':x.middle_name,
                'last_name': x.last_name,
                'employment_start_date': str(x.employment_start_date),
                'account_manager': x.account_manager if x.account_manager else '',
                'work_location_city': Address1Model.query.filter_by(address_id=x.work_address_id).first().city if x.work_address_id else '',
                'created_date': str(x.created_time.date())
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.employment_start_date>=date).all()))}


    @classmethod
    def return_all_for_future_terminations(cls,date):
        def to_json(x):
            termination_obj = TerminationReasonDataModel.query.filter_by(termination_reason_id=x.termination_reason_id).first()
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'middle_name':x.middle_name,
                'last_name': x.last_name,
                'termination_date': str(x.termination_date),
                'termination_intent':termination_obj.termination_intent,
                'termination_reason': termination_obj.termination_reason,
                'account_manager': x.account_manager if x.account_manager else '',
                'work_location_city': Address1Model.query.filter_by(address_id=x.work_address_id).first().city if x.work_address_id else ''
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.termination_date>=date).all()))}

    @classmethod
    def return_all_terminated_employees(cls,start_date,end_date):
        def to_json(x):
            termination_obj = TerminationReasonDataModel.query.filter_by(termination_reason_id=x.termination_reason_id).first()
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'middle_name':x.middle_name,
                'last_name': x.last_name,
                'termination_date': str(x.termination_date),
                'termination_intent':termination_obj.termination_intent,
                'termination_reason': termination_obj.termination_reason,
                'account_manager': x.account_manager if x.account_manager else '',
                'work_location_city': Address1Model.query.filter_by(address_id=x.work_address_id).first().city if x.work_address_id else ''
            }

        return {'Employees': list(map(lambda x: to_json(x), cls.query.filter(cls.termination_date>=start_date,cls.termination_date<end_date).all()))}




    @classmethod
    def return_all_active_forproject(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }


        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))}

    @classmethod
    def return_all_billable_active_employees(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'firstname': x.first_name,
                'lastname': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }

   
        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A',billable_resource='Y').all()))}


    @classmethod
    def return_all_active_projectfor(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'employee_name': x.first_name + ' ' + x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }


        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))}

    @classmethod
    def return_all_active_projectfor_et(cls,username):
        def to_json(x):
            if x.email_id != username:
                return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'employee_name': x.first_name + ' ' + x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
                }


        return {'Employees': list(filter((None).__ne__,list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))))}

    @classmethod
    def return_all_active_projectfor_et1(cls,username):
        def to_json(x):
            if x.email_id != username:
                return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'last_name': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'date_of_birth': x.date_of_birth,
                'employment_start_date': str(x.employment_start_date),
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
                }

        return {'Employees': list(filter((None).__ne__,list(map(lambda x: to_json(x), Employee1Model.query.filter_by(employee_status='A').all()))))}


    @classmethod
    def return_all_emps(cls, emplist,empid):
        print('entered return al emps')
        def to_json(x):
            if x[0]=='C':
                y = ThirdPartyConsultantModel.query.filter_by(consultant_id=x).first()
                emp_num = y.consultant_id
                return {
                    'employee_id': emp_num,
                    'first_name': y.first_name,
                    'last_name': y.last_name
                }

            else:
                if x != str(empid):
                    y = cls.find_by_employee_id(x)
                    emp_num = y.employee_id
            
                    return {
                    'employee_id': emp_num,
                     'first_name': y.first_name,
                     'last_name': y.last_name

                    }

        return {'Employees': list(filter((None).__ne__,list(map(lambda x: to_json(x), emplist)))) }


    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'employee_id':x.employee_id,
                'employee_number': x.emp_number,
                'first_name': x.first_name,
                'last_name': x.last_name,
                'email': x.email_id,
                'gender': 'Male' if x.gender=='M' else 'Female' if x.gender=='F' else 'Other',
                'date_of_birth': x.date_of_birth,
                'employment_start_date': str(x.employment_start_date),
                'employee_status':'Active' if x.employee_status=='A' else 'Inactive' if x.employee_status=='I' else 'Terminated',
            }
        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.order_by(Employee1Model.employee_status.asc(),Employee1Model.modified_time.desc()).all()))}


    @classmethod
    def return_all_empnumber(cls):
        def to_json(x):
            return {
                'employee_id': x.employee_id,
                'employee_number': x.emp_number,
                'name': x.first_name+" "+x.last_name
            }

        return {'Employees': list(map(lambda x: to_json(x), Employee1Model.query.all()))}


    @classmethod
    def return_all_managers(cls, managerslist):
        def to_json(x):
            y = cls.find_by_employee_id(x)
            return {
                'employee_number': y.emp_number,
                'name': y.first_name+' '+y.last_name,
                'email': y.email_id,
            }

        return {'Employees': list(map(lambda x: to_json(x), managerslist))}


    @classmethod
    def return_all_recruiters(cls, des):

        def to_json(x):
            a = cls.query.filter_by(designation_id=x.employee_id).first()
            return {
            'employee_id': x.employee_id,
            'employee_number': x.emp_number,
            'employee_name': x.first_name+' '+x.last_name

            }

        return {'recruiters': list(map(lambda x: to_json(x), des))}


class InternalTimesheetTypesModel(db.Model):
    __tablename__ = 'internal_timesheet_type_reference'

    timesheet_type_id = db.Column(db.Integer, primary_key=True)
    timesheet_type = db.Column(db.String(120), nullable=False) 

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    
class InternalTimesheetModel(db.Model):
    __tablename__ = 'internal_timesheet'

    timesheet_id = db.Column(db.Integer, primary_key=True)
    employee_number = db.Column(db.Integer,nullable=True) 
    timesheet_type_id = db.Column(db.Integer,nullable=True)
    ts_day = db.Column(db.String(10), nullable=True)
    ts_date = db.Column(db.Date, nullable=False)
    ts_start_time = db.Column(db.Time, nullable=False)
    ts_end_time = db.Column(db.Time, nullable=False)
    project_id = db.Column(db.String(40), ForeignKey('project.project_id'), nullable=True) #foreign key
    department_id = db.Column(db.Integer, ForeignKey('department_reference.department_id'), nullable=True)
    total_hours = db.Column(db.Float, nullable=False)
    status = db.Column(db.String(1), nullable=True)
    remarks = db.Column(db.String(255), nullable=True)
    created_time_stamp = db.Column(db.DateTime, nullable=True)
    last_updated_time_stamp = db.Column(db.DateTime, nullable=True) 
    approver_id = db.Column(db.Integer,ForeignKey('employee.employee_id'), nullable=True)
    approver = relationship('Employee1Model', foreign_keys='InternalTimesheetModel.approver_id')
    approver_comments = db.Column(db.String(255), nullable=True)
    approved_time_stamp = db.Column(db.DateTime, nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



    @classmethod
    def all_employee_timesheets(cls,empid):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=empid).first()
            approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name if x.approver_id else 'N/A'
            project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name if ProjectModel.query.filter_by(project_id=x.project_id).first() else 'N/A'
            department_name = DepartmentModel.query.filter_by(department_id=x.department_id).first().department_name if DepartmentModel.query.filter_by(department_id=x.department_id).first() else ''
            approver_comments = x.approver_comments if x.approver_comments else ''
            work_hours = round(x.total_hours,2) if x.timesheet_type_id in [1,2] else 0.00
            non_work = 0.00 if x.timesheet_type_id in [1,2] else round(x.total_hours,2)
            
            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name+' '+emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }

        return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter_by(employee_number = empid).order_by(cls.ts_date.desc()).all()))}

    @classmethod
    def filtered_employee_timesheets(cls,userid,project_id,date):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name if x.approver_id else 'N/A' 
            project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name if ProjectModel.query.filter_by(project_id=x.project_id).first() else 'N/A'
            work_hours = round(x.total_hours,2) if x.timesheet_type_id in [1,2] else 0.00
            non_work = 0.00 if x.timesheet_type_id in [1,2] else round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''
            department_name = DepartmentModel.query.filter_by(department_id=x.department_id).first().department_name if DepartmentModel.query.filter_by(department_id=x.department_id).first() else ''
            
            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name + ' ' + emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type if InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first() else '',   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }
        if project_id and not date:
            print('yes')
            return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == userid,InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id)).all()))}
        elif project_id:
            return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == userid,InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= date[0],InternalTimesheetModel.ts_date <= date[1]).all()))}
        else:
            return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == userid,InternalTimesheetModel.ts_date >= date[0],InternalTimesheetModel.ts_date <= date[1]).all()))}




    @classmethod   # to get all timesheets
    def return_all(cls):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'

            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''


            return {
                'timesheetId': x.timesheet_id,
                'employeeNumber':x.employee_number ,
                'employeeName':emp.first_name+' '+emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                'tsDay': x.ts_day,
                'tsDate': str(x.ts_date),
                'tsStartTime': str(x.ts_start_time),
                'tsEndTime': str(x.ts_end_time),
                'projectId': x.project_id,
                'projectName':project,
                'totalHours': x.total_hours,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'createdTimeStamp': str(x.created_time_stamp),
                'approverName': approver,
                'approverComents':approver_comments,
                'departmentName': department_name

            }
        return {'timesheets': list(map(lambda x: to_json(x), cls.query.order_by(cls.ts_date.desc()).all()))}

    @classmethod   # to get all timesheets
    def return_all_time_sheets(cls,user):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first() 
            if emp.employee_id != user:

                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'

                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''



                return {
                    'timesheetId': x.timesheet_id,
                    'employeeNumber':x.employee_number ,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDay': x.ts_day,
                    'tsDate': str(x.ts_date),
                   'tsStartTime': str(x.ts_start_time),
                   'tsEndTime': str(x.ts_end_time),
                   'projectId': x.project_id,
                   'projectName':project,
                   'totalHours': x.total_hours,
                   'workHours': work_hours,
                   'nonWorkHours': non_work,
                   'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                   'remarks': x.remarks,
                   'createdTimeStamp': str(x.created_time_stamp),
                   'approverName': approver,
                   'approverComents':approver_comments,
                   'departmentName': department_name

                }
        return {'timesheets': list(filter((None).__ne__,list(map(lambda x: to_json(x,user), cls.query.order_by(cls.ts_date.desc()).all()))))}



    @classmethod
    def return_all_reporting_manager(cls,emp_list,emp_id):
        def inner_func(x,emp_id):
            def to_json(y,emp_id):
                emp = Employee1Model.query.filter_by(employee_id=y.employee_number).first()

                if emp_id != emp.employee_id:
                    if y.approver_id:
                        approver = Employee1Model.query.filter_by(employee_id=y.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=y.approver_id).first().last_name
                    else:
                        approver = 'N/A'
                    if y.project_id:
                        project = ProjectModel.query.filter_by(project_id=y.project_id).first().project_name
                        project_id = y.project_id
                    else:
                        project = 'N/A'
                        project_id = ''
                    if y.timesheet_type_id in [1,2]:
                        work_hours = round(y.total_hours,2)
                        non_work = 0.00
                    else:
                        work_hours = 0.00
                        non_work = round(y.total_hours,2)
                    approver_comments = y.approver_comments if y.approver_comments else ''

                    department = DepartmentModel.query.filter_by(department_id=y.department_id).first()
                    department_name = department.department_name if department else ''

                   
                    return {
                        'timesheetId': y.timesheet_id,
                        'employeeNumber':y.employee_number ,
                        'employeeName':emp.first_name+' '+emp.last_name,
                        'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=y.timesheet_type_id).first().timesheet_type,
                        'tsDay': y.ts_day,
                        'tsDate': str(y.ts_date),
                        'tsStartTime': str(y.ts_start_time),
                        'tsEndTime': str(y.ts_end_time),  
                        'projectId': project_id,
                        'projectName':project,
                        'totalHours': y.total_hours,
                        'workHours': work_hours,
                        'nonWorkHours': non_work,
                        'status': 'Approved' if y.status=='A' else 'Rejected' if y.status=='R' else 'Submitted' if y.status=='S' else '',
                        'remarks': y.remarks,
                        'createdTimeStamp': str(y.created_time_stamp),
                        'approverName': approver,
                        'approverComents':approver_comments,
                        'departmentName': department_name
                    }

            return list(map(lambda y: to_json(y,emp_id), cls.query.filter_by(employee_number=x).all()))
        return {'timesheets': list(filter((None).__ne__,list(itertools.chain.from_iterable(list(map(lambda x: inner_func(x,emp_id), emp_list))))))}



    @classmethod
    def return_time_sheets(cls,emp,startDate,endDate):
        def to_json(x):
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'

            if x.project_id:
                project = x.project_id
            else:
                project = ''
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''

            return {
                'timeSheetId': x.timesheet_id,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverComents':approver_comments,
                'approverName':approver,
                'departmentName': department_name
            }

        return {'time_sheet': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == emp,InternalTimesheetModel.ts_date >= startDate,InternalTimesheetModel.ts_date <= endDate)
.all()))}


    @classmethod
    def return_all_timesheets(cls,empid):
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=empid).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''


            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name+' '+emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }


        return {'timeSheet': list(map(lambda x: to_json(x), cls.query.filter_by(employee_number = empid).order_by(cls.ts_date.desc()).all()))}

    @classmethod
    def return_all_by_pro(cls, proid,user):
        def to_json(x,user):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if emp.employee_id != user:
            
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+ Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''

                department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                department_name = department.department_name if department else ''


                return {
                    'timeSheetId': x.timesheet_id,
                    'employeeName':emp.first_name+' '+emp.last_name,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimestamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
            else:
                pass
        return {'timeSheets': list(map(lambda x: to_json(x,user), cls.query.filter_by(project_id=proid).order_by(InternalTimesheetModel.ts_date.desc(),InternalTimesheetModel.project_id).all()))}

    @classmethod
    def return_all_by_man_ts(cls,emp_num,project_id,date):
        if date == '':
            min_date = '01-01-1997'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[0] != '':
            min_date = date[0].split('T')[0]
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[1] != '': 
            max_date = date[1].split('T')[0]
            min_date = '01-01-1997'
        else:
            max_date = date[1].split('T')[0]
            min_date = date[0].split('T')[0]
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''


            return {
                'timesheetId': x.timesheet_id,
                'employeeName':emp.first_name + ' ' + emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimeStamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }
        if project_id == '' and emp_num:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == emp_num,InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        else:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
            


    @classmethod
    def return_all_by_all_ts(cls,emp_num,project_id,date,dept_id,user):
        if not emp_num and not project_id and not date and not dept_id:
            return InternalTimesheetModel.return_all_time_sheets(emp_num)
        if date != '':
            min_date = date[0].split('T')[0]
            max_date = date[1].split('T')[0]
        else:
            min_date = '01-01-1977'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        def to_json(x):
            if user != x.employee_number:
                emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()  
                if x.approver_id:
                    approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
                else:
                    approver = 'N/A'
                if x.project_id:
                    project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
                else:
                    project = 'N/A'
                if x.timesheet_type_id in [1,2]:
                    work_hours = round(x.total_hours,2)
                    non_work = 0.00
                else:
                    work_hours = 0.00
                    non_work = round(x.total_hours,2)
                approver_comments = x.approver_comments if x.approver_comments else ''
                if x.department_id != '':
                    department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
                    department_name = department.department_name if department else ''
                else:
                    department_name = ''


                return {

                    'timesheetId': x.timesheet_id,
                    'employeeName':emp.first_name + ' ' + emp.last_name,
                    'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                    'tsDate':str(x.ts_date),
                    'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                    'projectName': project,
                    'workHours': work_hours,
                    'nonWorkHours': non_work,
                    'day':x.ts_day,
                    'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                    'remarks': x.remarks,
                    'approverName':approver,
                    'createdTimeStamp':str(x.created_time_stamp),
                    'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                    'approverComents':approver_comments,
                    'departmentName': department_name
                }
        if dept_id and project_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.department_id == (dept_id if dept_id else InternalTimesheetModel.department_id),InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        elif project_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        elif dept_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.department_id == (dept_id if dept_id else InternalTimesheetModel.department_id),InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        else:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}



    @classmethod
    def return_all_by_manteam_ts(cls,emp_num,project_id,date):
        if date == '':
            min_date = '01-01-1997'
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[0] != '':
            min_date = date[0].split('T')[0]
            max_date = datetime.datetime.now().strftime('%m-%d-%Y')
        elif date[1] != '': 
            max_date = date[1].split('T')[0]
            min_date = '01-01-1997'
        else:
            max_date = date[1].split('T')[0]
            min_date = date[0].split('T')[0]

        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_number).first()
            if x.approver_id:
                approver = Employee1Model.query.filter_by(employee_id=x.approver_id).first().first_name+' '+Employee1Model.query.filter_by(employee_id=x.approver_id).first().last_name
            else:
                approver = 'N/A'
            if x.project_id:
                project = ProjectModel.query.filter_by(project_id=x.project_id).first().project_name
            else:
                project = 'N/A'
            if x.timesheet_type_id in [1,2]:
                work_hours = round(x.total_hours,2)
                non_work = 0.00
            else:
                work_hours = 0.00
                non_work = round(x.total_hours,2)
            approver_comments = x.approver_comments if x.approver_comments else ''

            department = DepartmentModel.query.filter_by(department_id=x.department_id).first()
            department_name = department.department_name if department else ''

            return {
                'timeSheetId': x.timesheet_id,
                'employeeName':emp.first_name + ' ' + emp.last_name,
                'tsType': InternalTimesheetTypesModel.query.filter_by(timesheet_type_id=x.timesheet_type_id).first().timesheet_type,   
                'tsDate':str(x.ts_date),
                'tsStartTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_start_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'tsEndTime':datetime.datetime.strftime(datetime.datetime.strptime(str(x.ts_end_time), '%H:%M:%S'),"%I:%M %p").lower(),
                'projectName': project,
                'workHours': work_hours,
                'nonWorkHours': non_work,
                'day':x.ts_day,
                'status': 'Approved' if x.status=='A' else 'Rejected' if x.status=='R' else 'Submitted' if x.status=='S' else '',
                'remarks': x.remarks,
                'approverName':approver,
                'createdTimestamp':str(x.created_time_stamp),
                'lastUpdatedTimestamp':str(x.last_updated_time_stamp),
                'approverComents':approver_comments,
                'departmentName': department_name
            }
        if project_id:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.project_id == (project_id if project_id else InternalTimesheetModel.project_id),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}
        else:
            return {'timesheets': list(map(lambda x: to_json(x), cls.query.filter(InternalTimesheetModel.employee_number == (emp_num if emp_num else InternalTimesheetModel.employee_number),InternalTimesheetModel.ts_date >= min_date,InternalTimesheetModel.ts_date <= max_date).all()))}




class ProjectModel(db.Model):
    __tablename__ = 'project'

    project_id = db.Column(db.String(40), primary_key=True)
    client_id = db.Column(db.String(40), ForeignKey('client_details.client_id'))
    contract_id = db.Column(db.String(40), ForeignKey('client_vendorship.contract_id'))
    project_type_id = db.Column(db.Integer, ForeignKey('project_category_reference.project_type_id'))
    work_location = db.Column(db.String(60), nullable=True)
    project_name = db.Column(db.String(255), nullable=False)
    project_description = db.Column(db.String(255), nullable=True)
    scheduled_start_date = db.Column(db.Date, nullable=True)
    scheduled_end_date = db.Column(db.Date, nullable=True)
    actual_start_date = db.Column(db.Date, nullable=True)
    actual_end_date = db.Column(db.Date, nullable=True)
    revenue_amount = db.Column(db.Numeric, nullable=True)
    project_manager_id = db.Column(db.Integer, ForeignKey('employee.employee_id'), nullable=False)
    proj_manager_phone_number= db.Column(db.String(20),nullable=True)
    project_sponsor_name = db.Column(db.String(150), nullable=True)
    project_status = db.Column(db.String(1), nullable=True)
    daily_work_hours = db.Column(db.Float,nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    project_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    work_location_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    project_employee_model_ref = relationship("ProjectEmployeeModel", uselist=False, backref="project")
    project_time_sheet = relationship("TimeSheetModel", uselist=False, backref = "project")
    emp_bill_rate_rel = relationship("EmployeeBillRateModel", uselist=False, backref = "project")
    internal_timesheet_id = relationship("InternalTimesheetModel", uselist=False, backref = "project")
   
    

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def return_all_active_projects(cls):
        def to_json(x):
            return {
                'project_id': x.project_id,
                'project_name': x.project_name,
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter_by(project_status='A').all()))}

    @classmethod
    def return_all(cls):
        def to_json(x):
            w = Employee1Model.query.filter_by(employee_id=x.project_manager_id).first()
            if x.actual_start_date == None:
                actual_start_date = ''
            else:
                actual_start_date = str(x.actual_start_date)
            return {
                'project_id': x.project_id,  
                'project_name': x.project_name,
                'client_name':ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name,
                'actual_start_date': actual_start_date,
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_status':x.project_status
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}

    @classmethod
    def return_all_projects(cls):
        def to_json(x):
            return {
                'project_name': x.project_name,
                        }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}

    @classmethod
    def return_all_client_projects(cls, b):
        def to_json(x):
            return {
                'project_id': x.project_id,
                'project_type': ProjectTypeModel.find_by_project_type_id(x.project_type_id),
                'project_name': x.project_name,
                'scheduled_start_date': str(x.scheduled_start_date),
                'scheduled_end_date': str(x.scheduled_end_date),
                'project_manager_name': Employee1Model.query.filter_by(
                    employee_id=x.project_manager_id).first().first_name,
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.filter_by(client_id=b).all()))}

    @classmethod
    def find_by_project_type_id(cls, proid):
        return cls.query.filter_by(project_id=proid).first()

    @classmethod
    def find_by_pro_list(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x).first()
            return {
            "project_name": a.project_name,
            "project_id": a.project_id

            }

        return {'Projects': list(map(lambda x: to_json(x), pro_list))}

    @classmethod
    def find_by_pro_list1(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x.project_id).first()
            return {
            "project_name": a.project_name,
            "project_id": a.project_id

            }

        return {'Projects': list(map(lambda x: to_json(x), pro_list))}

    @classmethod
    def for_projects_list(cls, pro_list):

        def to_json(x):
            a = cls.query.filter_by(project_id=x).first()
            w = Employee1Model.query.filter_by(employee_id=a.project_manager_id).first()
            creator = Employee1Model.query.filter_by(employee_id=a.created_by).first()
            z = ClientDetailsModel.query.filter_by(client_id=a.client_id).first().organization_name

            return {
                'project_id': a.project_id,
                'client_name':z,
                'project_type': ProjectTypeModel.find_by_project_type_id(a.project_type_id),
                'project_name': a.project_name,
                'project_description': a.project_description,
                'scheduled_start_date': str(a.scheduled_start_date),
                'scheduled_end_date': str(a.scheduled_end_date),
                'actual_start_date': str(a.actual_start_date if a.actual_start_date else ''),
                'actual_end_date': str(a.actual_end_date if a.actual_end_date else ''),
                'revenue_amount': str(a.revenue_amount if a.revenue_amount else ''),
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_contact': w.primary_phone_number,
                'project_sponsor_name': a.project_sponsor_name,
                'modified_time':str(a.modified_time),
                'created_by':creator.first_name+' '+creator.last_name,
                'project_status':  'Active' if a.project_status=='A' else 'Inactive' if a.project_status=='I' else 'Terminated' if a.project_status=='T' else '',  
            }

        return list(map(lambda x: to_json(x), pro_list))

    @classmethod
    def manager_projects(cls, z,a):
        def to_json(x):
            w = Employee1Model.query.filter_by(employee_id=x.project_manager_id).first()
            creator = Employee1Model.query.filter_by(employee_id=x.created_by).first()
            z = ClientDetailsModel.query.filter_by(client_id=x.client_id).first().organization_name
            if x.actual_start_date == None:
                actual_start_date = ''
            else:
                actual_start_date = x.actual_start_date
            if x.actual_end_date == None:
                actual_end_date = ''
            else:
                actual_end_date = x.actual_end_date
            if x.revenue_amount== None:
                revenue_amount = ''
            else:
                revenue_amount = x.revenue_amount
            return {
                'project_id': x.project_id,
                'client_name':z,
                'project_type': ProjectTypeModel.find_by_project_type_id(x.project_type_id),
                'project_name': x.project_name,
                'project_description': x.project_description,
                'scheduled_start_date': str(x.scheduled_start_date),
                'scheduled_end_date': str(x.scheduled_end_date),
                'actual_start_date': str(actual_start_date),
                'actual_end_date': str(actual_end_date),
                'revenue_amount': str(revenue_amount),
                'project_manager_name': w.first_name+' '+w.last_name,
                'project_manager_contact': w.primary_phone_number,
                'project_sponsor_name': x.project_sponsor_name,
                'modified_time':str(x.modified_time),
                'created_by':creator.first_name+' '+creator.last_name,
                'project_status':  'Active' if x.project_status=='A' else 'Inactive' if x.project_status=='I' else 'Terminated' if x.project_status=='T' else '',  
            }
        if a == 0:
            pro_list = list(map(lambda x: to_json(x), cls.query.filter_by(project_manager_id=z).order_by(ProjectModel.project_status.asc(),ProjectModel.modified_time.desc()).all()))
            total_projects = len(pro_list)
            count = 0
            for i in pro_list:
                if i['project_status'] == 'Active':
                    count += 1
            return {'projects': pro_list,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}
        elif a == 1:
            b = list(map(lambda x: to_json(x), cls.query.filter_by(project_manager_id=z).all())) + list(map(lambda x: to_json(x), cls.query.filter_by(created_by=z).all()))
            b = list({v['project_id']:v for v in b}.values())
            b.sort(key=lambda x:x.get('modified_time'),reverse=True)
            b.sort(key=lambda x:x.get('project_status'))
            total_projects = len(b)
            count = 0
            for i in b:
                if i['project_status'] == 'Active':
                    count += 1
            
            return {'projects': b,'project_stats':{"total_projects":total_projects,"active":count,"inactive":total_projects-count}}



class Address1Model(db.Model):
    __tablename__ = 'address'

    address_id = db.Column(db.Integer, primary_key=True)
    address_line1 = db.Column(db.String(255), nullable=False)
    address_line2 = db.Column(db.String(255), nullable=True)
    city = db.Column(db.String(70), nullable=False)
    state_name = db.Column(db.String(60), nullable=False)
    zip_code = db.Column(db.String(15), nullable=False)
    country = db.Column(db.String(60), nullable=False)
    emergencyaddress = relationship("EmployeeContactModel", uselist=False, backref="address")
    dependentaddress = relationship("DependentContactModel", uselist=False, backref="address")
    clientcontactaddress = relationship("ClientContactModel", uselist=False, backref="address")
    supplier_address_rel = relationship("SupplierAddressModel", uselist=False, backref="address")
    project_address_rel = relationship("ProjectModel", uselist=False, backref="address")

    
    def save_to_db(self):   
        db.session.add(self)
        db.session.commit()


    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

class EmployeeVisaDetailsModel(db.Model):
    __tablename__ = 'employee_visa_details'

    visa_id = db.Column(db.Integer, primary_key=True)
    visa_type_id = db.Column(db.Integer, ForeignKey('visa_type_reference.visa_type_id'))
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    visa_number = db.Column(db.String(30), nullable=True,unique=True)
    job_title = db.Column(db.String(150), nullable=True)
    visa_start_date = db.Column(db.Date, nullable=True)
    visa_end_date = db.Column(db.Date, nullable=True)
    visa_salary = db.Column(db.Numeric, nullable=True)
    visa_title = db.Column(db.String(50), nullable=True)
    lca_work_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    lca_alternate_address_id = db.Column(db.Integer, ForeignKey('address.address_id'))
    lca_salary = db.Column(db.Numeric, nullable=True)
    lca_wage_level_id = db.Column(db.Integer, ForeignKey('lca_wage_level_reference.lca_wage_level_id'))
    lca_prevailing_wages = db.Column(db.Numeric, nullable=True)
    lca_start_date = db.Column(db.Date, nullable=True)
    lca_end_date = db.Column(db.Date, nullable=True)
    current_status = db.Column(db.String(1), nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    attorney = db.Column(db.String(150), nullable=True)
    lca_work_add = relationship('Address1Model', foreign_keys='EmployeeVisaDetailsModel.lca_work_address_id')
    lca_alternate_add = relationship('Address1Model', foreign_keys='EmployeeVisaDetailsModel.lca_alternate_address_id')

    def save_to_db(self):
         db.session.add(self)
         db.session.commit()


    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(employee_id=empnumber).first()

    @classmethod
    def return_h1_renewals(cls,start_date,end_date):
        visa_type = VisaTypeModel.query.filter_by(visa_type='H1').first().visa_type_id
        start_date = datetime.datetime.now().strftime("%m-%d-%Y")
        end_date = (datetime.datetime.now()+ relativedelta(months=6)).strftime("%m-%d-%Y")
        def to_json(x):
            emp = Employee1Model.query.filter_by(employee_id=x.employee_id).first()
            pro = EmployeePrimaryProjectModel.query.filter_by(employee_id=x.employee_id).first()
            if pro:
                if pro.project_id:
                    return {
                        'employee_name': emp.first_name+' '+emp.last_name,
                        'expiration_date': str(x.visa_end_date),
                        'current_client': ClientDetailsModel.query.filter_by(client_id=(ProjectModel.query.filter_by(project_id=pro.project_id).first().client_id)).first().organization_name,
                        'work_location': ProjectModel.query.filter_by(project_id=pro.project_id).first().work_location,
                        'previous_attorney': x.attorney,
                    }
                else:
                    return {
                        'employee_name': emp.first_name+' '+emp.last_name,
                        'expiration_date': str(x.visa_end_date),
                        'current_client': 'N/A',
                        'work_location': None,
                        'previous_attorney': x.attorney,
                    }
   
            else:
                return {
                    'employee_name': emp.first_name+' '+emp.last_name,
                    'expiration_date': str(x.visa_end_date),
                    'current_client': 'N/A',
                    'work_location': None,
                    'previous_attorney': x.attorney,
                }
        return {'Renewals': list(map(lambda x: to_json(x), cls.query.filter(cls.visa_type_id==visa_type,cls.visa_end_date>=start_date,cls.visa_end_date<=end_date).order_by(cls.visa_end_date.asc()).all()))}

class VisaTypeModel(db.Model):
    __tablename__ = 'visa_type_reference'

    visa_type_id = db.Column(db.Integer, primary_key=True)
    visa_type = db.Column(db.String(30), nullable=False)
    visa_country = db.Column(db.String(30), nullable=False)
    emp_visa_det = relationship("EmployeeVisaDetailsModel", uselist=False, backref="visa_type_reference")
    h4_visa_det = relationship("DependentVisaModel", uselist=False, backref="visa_type_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)  
        db.session.commit()


    @classmethod
    def find_by_employeenumber(cls, empnumber):
        return cls.query.filter_by(visa_type_id=empnumber).first().visa_type if cls.query.filter_by(visa_type_id=empnumber).first() else ''


class LcaWageListModel(db.Model):
    __tablename__ = 'lca_wage_level_reference'

    lca_wage_level_id = db.Column(db.Integer, primary_key=True)
    lca_wage_level = db.Column(db.String(1), nullable=False)
    employee_relation_ref = relationship("EmployeeVisaDetailsModel", uselist=False,backref="lca_wage_level_reference")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


class EmployeeSignupSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Employee1Model
        unknown = EXCLUDE

class EmpInactivationReasonDataModel(db.Model):
    __tablename__ = 'emp_inactive_reason'

    emp_inactive_reason_id = db.Column(db.Integer, primary_key=True)
    emp_inactive_reason = db.Column(db.String(30), nullable=True)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'inactive_reason_id': x.emp_inactive_reason_id,
                'inactive_reason': x.emp_inactive_reason,
                }
        return {'inactive_reasons': list(map(lambda x: to_json(x), cls.query.all()))}



class TerminationReasonDataModel(db.Model):  
    __tablename__ = 'termination_reason'

    termination_reason_id = db.Column(db.Integer, primary_key=True)
    termination_intent = db.Column(db.String(30), nullable=True)
    termination_reason = db.Column(db.String(50), nullable=False)
    termination_ref = relationship("Employee1Model", uselist=False,backref="termination_reason")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    
    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'termination_reason_id': x.termination_reason_id,
                'termination_intent': x.termination_intent,
                'termination_reason': x.termination_reason,
            }

        return {'termination_reasons': list(map(lambda x: to_json(x), cls.query.all()))}

class VacationWeeks(db.Model):  
    __tablename__ = 'vacation_weeks_reference'

    vacation_weeks_id = db.Column(db.Integer, primary_key=True)
    vacation_weeks = db.Column(db.String(30), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

class SickHours(db.Model):  
    __tablename__ = 'sick_hours_reference'

    sick_hours_id = db.Column(db.Integer, primary_key=True)
    sick_hours = db.Column(db.String(30), nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

          
class ClientDetailsModel(db.Model):
    __tablename__ = 'client_details'

    client_id = db.Column(db.String(40), primary_key=True)
    client_vendorship_type_id = db.Column(db.Integer, ForeignKey('client_vendorship_type_reference.client_vendorship_type_id'))
    client_status = db.Column(db.String(1), nullable=False)
    vendorship_start_date = db.Column(db.Date, nullable=False)
    vendorship_end_date = db.Column(db.Date, nullable=False)
    msa_number = db.Column(db.String(15), nullable=False)
    number_of_contracts = db.Column(db.Integer, nullable=True)
    organization_name = db.Column(db.String(255), nullable=False)
    client_contract_details = db.Column(db.String(255), nullable=False)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    comments = db.Column(db.String(255),nullable=True)
    client_status_last_updtd_date = db.Column(db.DateTime,nullable=True)
    client_project_ref = relationship("ProjectModel", uselist=False, backref="client_details")
    client_vendorship = relationship("ClientVendorshipModel", uselist=False, backref="client_details")
    client_location = relationship("ClientLocationModel", uselist=False, backref="client_details")
    client_contact = relationship("ClientContactModel", uselist=False, backref="client_details")
    client_time_sheet = relationship("TimeSheetModel", uselist=False, backref = "client_details")


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_id(cls, clientid):
        return cls.query.filter_by(client_id=clientid).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'client_id': x.client_id,
                'client_status': 'Active' if x.client_status=='A' else 'Inactive' if x.client_status=='I' else 'Terminated' if x.client_status=='T' else 'Prospect' if x.client_status=='P' else '',
                'organization_name': x.organization_name,
                "client_created_date":str(datetime.datetime.strptime(str(x.created_time),"%Y-%m-%d %H:%M:%S.%f").date())
            }
        return {'client_details': list(map(lambda x: to_json(x), ClientDetailsModel.query.order_by(ClientDetailsModel.client_status.asc(),ClientDetailsModel.modified_time.desc()).all()))}

    @classmethod
    def return_contracts(cls):
        def to_json(x):
            return ClientVendorshipModel.return_all_contracts(cid=x.client_id)["client_vendorship"]
        li =  list(map(lambda x: to_json(x), ClientDetailsModel.query.filter_by(client_status='A').all()))

        return {'client_details': list(itertools.chain.from_iterable(li))}
 

    @classmethod
    def return_all_clientids(cls):
        def to_json(x):

            return {
                'client_id': x.client_id,
                'client_status': 'Active' if x.client_status=='A' else 'Inactive' if x.client_status=='I' else 'Terminated' if x.client_status=='T' else 'Prospect' if x.client_status=='P' else '',
                'organization_name': x.organization_name,   
            }

        return {'client_details': list(map(lambda x: to_json(x), ClientDetailsModel.query.filter_by(client_status='A').all()))}


class ClientvendorshipTypeModel(db.Model):
    __tablename__ = 'client_vendorship_type_reference'

    client_vendorship_type_id = db.Column(db.Integer, primary_key=True)
    client_vendorship_type = db.Column(db.String(30), nullable=False)
    client_details = relationship("ClientDetailsModel", uselist=False, backref="client_vendorship_type_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_vendorship_id(cls, clientid):
        return cls.query.filter_by(client_vendorship_type_id=clientid).first().client_vendorship_type

    @classmethod
    def find_by_client_vendorship_type(cls, clienttype):
        return cls.query.filter_by(client_vendorship_type=clienttype).first().client_vendorship_type_id

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'client_vendorship_type_id': x.client_vendorship_type_id,
                'client_vendorship_type': x.client_vendorship_type,
            }

        return {'vendor_types': list(map(lambda x: to_json(x), ClientvendorshipTypeModel.query.all()))}


class ClientVendorshipModel(db.Model):
    __tablename__ = 'client_vendorship'

    contract_id = db.Column(db.String(40), primary_key=True)
    bussiness_sector_id = db.Column(db.Integer, ForeignKey('business_sector_reference.bussiness_sector_id'))
    client_id = db.Column(db.String(40), ForeignKey('client_details.client_id'))
    contract_name = db.Column(db.String(255), nullable=False)
    contract_start_date = db.Column(db.Date, nullable=False)
    contract_end_date = db.Column(db.Date, nullable=False)
    term_limits = db.Column(db.Integer, nullable=True)
    created_by = db.Column(db.Integer,nullable=True)
    modified_by = db.Column(db.Integer,nullable=True)
    created_time = db.Column(db.DateTime,nullable=True)
    modified_time = db.Column(db.DateTime,nullable=True)
    status = db.Column(db.String(1),nullable=True)
    contract_number = db.Column(db.String(50),nullable=True)
    vendor_project_ref = relationship("ProjectModel", uselist=False, backref="client_vendorship")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_client_id(cls, client_id):
        return cls.query.filter_by(client_id=client_id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'contract_id': x.contract_id,
                'bussiness_sector_id': BussinessSectorModel.find_by_bussiness_sector_type_id(x.bussiness_sector_id),
                'client_id': x.client_id,
                'contract_name': x.contract_name,
                'contract_start_date': str(x.contract_start_date),
                'contract_end_date': str(x.contract_end_date),
                'term_limits': x.term_limits,
                'contract_number':x.contract_number if x.contract_number else ''
            }

        return {'client_vendorship': list(map(lambda x: to_json(x), ClientVendorshipModel.query.filter_by(status='A').all()))}

    @classmethod
    def return_all_contracts(cls,cid):
        def to_json(x):
            client = ClientDetailsModel.query.filter_by(client_id=cid).first()
            pro_id = ProjectModel.query.filter_by(contract_id=x.contract_id).all()
            res = []
            ct_res = []
            for i in pro_id:
                res.append(ProjectEmployeeModel.find_by_project_id(i.project_id))
            for j in res:
                ct_res.append(j['projects_resources'])
            resources = [dict(y) for y in set(tuple(x.items()) for x in list(itertools.chain.from_iterable(ct_res)))]

            return {
                'client_id': client.client_id,
                'organization_name': client.organization_name,
                'work_location':ClientLocationModel.query.filter_by(client_id=client.client_id).first().location_name,
                'contract_id': x.contract_number,
                'contract_name': x.contract_name,
                'contract_start_date': str(x.contract_start_date),
                'contract_end_date': str(x.contract_end_date),
                'resources': len({v['employee_id']:v for v in resources}.values()),
            }

        return {'client_vendorship': list(map(lambda x: to_json(x), ClientVendorshipModel.query.filter_by(client_id=cid,status='A').all()))}
    @classmethod
    def return_all_for_projects(cls,clientid):
        def to_json(x):
            return {
                'contract_id': x.contract_id,
                'client_id': x.client_id,
                'contract_name': x.contract_name
                            }

        return {'client_vendorship': list(map(lambda x: to_json(x), ClientVendorshipModel.query.filter_by(client_id=clientid,status='A').all()))}


    @classmethod
    def return_all_by_clientid(cls,clientid):
        def to_json(x):
            return {
                'contract_id': x.contract_id,
                'bussiness_sector_id': x.bussiness_sector_id,
                'client_id': x.client_id,
                'bussiness_sector': BussinessSectorModel.find_by_bussiness_sector_type_id(x.bussiness_sector_id),
                'contract_name': x.contract_name,
                'contract_start_date': str(x.contract_start_date),
                'contract_end_date': str(x.contract_end_date),
                'term_limits': x.term_limits,
                'contract_number':x.contract_number if x.contract_number else ''
            }

        return {'client_vendorship': list(map(lambda x: to_json(x), ClientVendorshipModel.query.filter_by(client_id=clientid,status='A').all()))}


class BussinessSectorModel(db.Model):
    __tablename__ = 'business_sector_reference'

    bussiness_sector_id = db.Column(db.Integer, primary_key=True)
    bussiness_sector_type = db.Column(db.String(30), nullable=False)
    client_vendorship = relationship("ClientVendorshipModel", uselist=False, backref="business_sector_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_bussiness_sector_type(cls, bustype):
        return cls.query.filter_by(bussiness_sector_type=bustype).first().bussiness_sector_id

    @classmethod
    def find_by_bussiness_sector_type_id(cls, bustypeid):
        return cls.query.filter_by(bussiness_sector_id=bustypeid).first().bussiness_sector_type

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'bussiness_sector_id': x.bussiness_sector_id,
                'bussiness_sector_type': x.bussiness_sector_type,
            }

        return {'bussiness_sector_types': list(map(lambda x: to_json(x), BussinessSectorModel.query.all()))}

class ProjectTypeModel(db.Model):
    __tablename__ = 'project_category_reference'

    project_type_id = db.Column(db.Integer, primary_key=True)
    project_type_name = db.Column(db.String(30), nullable=False)
    project_type_ref = relationship("ProjectModel", uselist=False, backref="project_category_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_project_type_id(cls, protypeid):
        return cls.query.filter_by(project_type_id=protypeid).first().project_type_name

    @classmethod
    def find_by_project_type_name(cls, protypename):
        return cls.query.filter_by(project_type_name=protypename).first().project_type_id

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'project_type_id': x.project_type_id,
                'project_type_name': x.project_type_name,
            }

        return {'Projects': list(map(lambda x: to_json(x), cls.query.all()))}


class UserRolesModel(db.Model):
    __tablename__ = 'user_roles'

    user_role_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey('users.userid'))
    roles = db.Column(ARRAY(db.Text), default=[4])

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls, uid):
        def to_json(x):
            return {
                'user_id': x.user_id,
                'role_name': RoleModel.find_by_role_id(x.role_id)
            }

        return {'role_privileges': list(map(lambda x: to_json(x), cls.query.filter_by(user_id=uid).all()))}

    @classmethod
    def find_by_user_id(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().role_id

    @classmethod
    def find_by_role_name(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().role_id

    @classmethod
    def find_by_user_id2(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().roles

    @classmethod
    def find_to_get_user_id(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first().user_id

    @classmethod
    def find_by_user_id_to_all(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).first()

    @classmethod
    def find_for_role_assign(cls, emp_id):
        return cls.query.filter_by(user_id=emp_id).all()

class UserRolesSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserRolesModel


class UserModel(db.Model):
    __tablename__ = 'users'

    userid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    reset_password = db.Column(db.Boolean,nullable=True)
    user_role_ref = relationship("UserRolesModel", uselist=False, backref="users")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()



    def get_token(self, expiration=1800):
        s = Serializer(app.config['SECRET_KEY'], expiration)
        return s.dumps({'user': self.userid}).decode('utf-8')

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username
            }

        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


    @staticmethod
    def verify_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        id = data.get('user')
        if id:
            return UserModel.query.get(id).userid
        return None

class UsersSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserModel
        unknown = EXCLUDE


class DesignationModel(db.Model):
    __tablename__ = 'designation_reference'

    designation_id = db.Column(db.Integer, primary_key=True)
    designation_name = db.Column(db.String(100), nullable=False)
    emp_education = relationship("Employee1Model", uselist=False, backref="designation_reference")
    invitation_mail = relationship("EmployeeInviteStatus", uselist=False, backref="designation_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_designation_id(cls, desg_id):
        return cls.query.filter_by(designation_id=desg_id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'designation_id': x.designation_id,
                'designation_name': x.designation_name,
            }

        return {'designation_types': list(map(lambda x: to_json(x), DesignationModel.query.all()))}


class EmploymentTypeModel(db.Model):
    __tablename__ = 'employment_type_reference'

    emp_type_id = db.Column(db.Integer, primary_key=True)
    emp_type = db.Column(db.String(100), nullable=False)
    emp_education = relationship("Employee1Model", uselist=False, backref="employment_type_reference")

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_emp_type_id(cls, type_id):
        return cls.query.filter_by(emp_type_id=type_id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'emp_type_id': x.emp_type_id,
                'emp_type': x.emp_type,
            }

        return {'employment_types': list(map(lambda x: to_json(x), EmploymentTypeModel.query.all()))}


class DepartmentModel(db.Model):
    __tablename__ = 'department_reference'

    department_id = db.Column(db.Integer, primary_key=True)
    department_name = db.Column(db.String(100), nullable=False)
    emp_education = relationship("Employee1Model", uselist=False, backref="department_reference")
    client_contact = relationship("ClientContactModel", uselist=False, backref="department_reference")
    internal_ts = relationship("InternalTimesheetModel", uselist=False, backref="department_reference")
    invitation_mail = relationship("EmployeeInviteStatus", uselist=False, backref="department_reference")



    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_department_id(cls, dept_id):
        return cls.query.filter_by(department_id=dept_id).first()

    @classmethod
    def find_by_department_name(cls, dept_name):
        return cls.query.filter_by(department_name=dept_name).first().department_id

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'department_id': x.department_id,
                'department_name': x.department_name,
            }

        return {'departments': list(map(lambda x: to_json(x), DepartmentModel.query.all()))}




class ConsultantSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ThirdPartyConsultantModel
        unknown = EXCLUDE


class ConsultantContactSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ConsultantContactModel
        unknown = EXCLUDE


class StaffingSupplierSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = StaffingSupplierModel
        unknown = EXCLUDE


class AddressSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Address1Model
        unknown = EXCLUDE


class ProjectSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ProjectModel
        unknown = EXCLUDE

class ClientDetailsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ClientDetailsModel
        unknown = EXCLUDE

class ClientLocationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ClientLocationModel
        unknown = EXCLUDE

class ClientVendorshipSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ClientVendorshipModel
        unknown = EXCLUDE

class ClientContactSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ClientContactModel
        unknown = EXCLUDE

class EmployeePrimaryProjectModel(db.Model):
    __tablename__ = 'employee_primary_project'

    employee_primary_project_id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, ForeignKey('employee.employee_id'))
    project_id = db.Column(db.String(40), ForeignKey('project.project_id'))
    created_by = db.Column(db.Integer, nullable=True)
    modified_by = db.Column(db.Integer, nullable=True)
    created_time = db.Column(db.DateTime, nullable=True)
    modiified_time = db.Column(db.DateTime, nullable=True)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

class AttorneyModel(db.Model):
    __tablename__ = 'attorney_reference'

    attorney_id = db.Column(db.Integer, primary_key=True)
    attorney_name = db.Column(db.String(255),nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

class AttorneySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = AttorneyModel
        unknown = EXCLUDE



class DashboardStats(db.Model):
    __tablename__ = 'dashboard_stats'

    stat_id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    date = db.Column(db.Date,nullable=True)
    billable = db.Column(db.Integer,nullable=True)
    on_billing =db.Column(db.Integer,nullable=True)
    on_bench = db.Column(db.Integer,nullable=True)
    non_billing = db.Column(db.Integer,nullable=True)
    total_employees = db.Column(db.Integer,nullable=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):   
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'month': str(x.date.strftime('%b')) + '-'+str(x.date.strftime('%Y')),
                'billable':x.billable,
                'non_billable':x.non_billing,
                'total':x.total_employees,
                'on_billing':x.on_billing,
                'on_bench':x.on_bench,
            }

        return list(map(lambda x: to_json(x), cls.query.all()))






